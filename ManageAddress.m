//
//  ManageAddress.m
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ManageAddress.h"

@implementation ManageAddress

@dynamic addressId;
@dynamic addressLine1;
@dynamic flatNumber;
@dynamic tagAddress;
@dynamic latitude;
@dynamic longitude;
@dynamic zipcode;

@end
