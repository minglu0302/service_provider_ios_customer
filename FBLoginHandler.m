
//
//  FBLoginHandler.m
//  FBShareSample
//
//  Created by Surender Rathore on 17/12/13.
//  Copyright (c) 2013 Facebook Inc. All rights reserved.
//

#import "FBLoginHandler.h"
#import <Accounts/Accounts.h>


@interface FBLoginHandler ()

@property (strong, nonatomic) NSArray *readPermission;
@property (strong, nonatomic) NSDictionary *parameters;

@end

@implementation FBLoginHandler

static FBLoginHandler *share;

+ (id)sharedInstance
{
    
    if (!share)
    {
        share  = [[self alloc] init];
    }
    return share;
}
- (instancetype)init
{
    
    if (self = [super init])
    {
        self.readPermission = @[@"public_profile", @"email", @"user_friends"];
        self.parameters = @{@"fields": @"picture, email, name, about, first_name, last_name"};
    }
    return self;
}
/**
 *  Login with facebook
 */
- (void)loginWithFacebook:(UIViewController *)viewController
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login setLoginBehavior:FBSDKLoginBehaviorNative];
    
    
    [login logOut];
    [login logInWithReadPermissions:self.readPermission
     
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                
                                if (error)
                                {
                                    NSLog(@"Login error : %@",[error localizedDescription]);
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(didFailWithError:)])
                                    {
                                        [self.delegate didFailWithError:error];
                                    }
                                }
                                else if (result.isCancelled)
                                {
                                    NSLog(@"Cancelled");
                                    if (self.delegate && [self.delegate respondsToSelector:@selector(didUserCancelLogin)])
                                    {
                                        [self.delegate didUserCancelLogin];
                                    }
                                }
                                else
                                {
                                    NSLog(@"Logged in");
                                    [self getDetailsFromFacebook];
                                }
                                
                            }];
}
/**
 *  Get User details
 */
- (void)getDetailsFromFacebook
{
    
    if ([FBSDKAccessToken currentAccessToken]) 
    {
        [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
        
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                                                       parameters:self.parameters];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error)
        {
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            if (error)
            {
                NSLog(@"Getting details error : %@",[error localizedDescription]);
                if (self.delegate && [self.delegate respondsToSelector:@selector(didFailWithError:)])
                {
                    [self.delegate didFailWithError:error];
                }
            }
            else
            {
                NSLog(@"Fetched user:%@", result);
                
                [self performSelector:@selector(sendBackTheFacebookDetails:)
                           withObject:result
                           afterDelay:0.6];

            }
        }];
    }
}

-(void)sendBackTheFacebookDetails:(NSDictionary *)fbDetails
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFacebookUserLoginWithDetails:)])
    {
        [self.delegate didFacebookUserLoginWithDetails:fbDetails];
    }

}

@end
