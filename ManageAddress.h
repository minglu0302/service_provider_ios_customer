//
//  ManageAddress.h
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ManageAddress : NSManagedObject

@property (nonatomic, retain) NSString * addressId;
@property (nonatomic, retain) NSString * addressLine1;
@property (nonatomic, retain) NSString * flatNumber;
@property (nonatomic, retain) NSString * tagAddress;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;

@end
