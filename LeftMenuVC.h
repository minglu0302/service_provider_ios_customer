//
//  MainVC.h
//  Towtray
//
//  Created by -Tony Lu on 19/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AMSlideMenuMainViewController.h"

@interface LeftMenuVC : AMSlideMenuMainViewController

@property (nonatomic,assign) BOOL isSomethingEnabled;

-(void)setBooleanValue;


@end
