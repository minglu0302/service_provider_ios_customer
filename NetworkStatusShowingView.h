//
//  NetworkStatusShowingView.h
//  UBER
//
//  Created by -Tony Lu on 03/02/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NetworkStatusShowingView : UIView
{
    NSTimer *timer;
}
+ (id)sharedInstance ;
+(void)removeViewShowingNetworkStatus;

@end
