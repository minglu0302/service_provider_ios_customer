//
//  MODAGenericUtilityClass.h
//  MODA
//
//  Created by -Tony Lu on 2/20/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

#define flStrForInt(x)      [MODAGenericUtility nonNullStringForInteger:x]
#define flStrForBool(x)     [MODAGenericUtility nonNullStringForBool:x]
#define flStrForDate(x)     [MODAGenericUtility nonNullStringForDate:x]
#define flStrForStr(x)      [MODAGenericUtility nonNullStringForString:x]
#define flStrForLong(x)     [MODAGenericUtility nonNullStringForLong:x]
#define flStrForDouble(x)   [MODAGenericUtility nonNullStringForDouble:x]
#define flIntForStr(x)      [MODAGenericUtility intFromString:x]
#define flBoolForStr(x)     [MODAGenericUtility boolFromString:x]
#define flDateForStr(x)     [MODAGenericUtility dateFromString:x]
#define flLongForStr(x)     [MODAGenericUtility longFromString:x]
#define flDoubleForStr(x)   [MODAGenericUtility doubleFromString:x]
#define flIntForObj(x)      [MODAGenericUtility intFromObject:x]
#define flBoolForObj(x)     [MODAGenericUtility boolFromObject:x]
#define flDateForObj(x)     [MODAGenericUtility dateFromObject:x]
#define flLongForObj(x)     [MODAGenericUtility longFromObject:x]
#define flDoubleForObj(x)   [MODAGenericUtility doubleFromObject:x]
#define flStrForObj(x)      [MODAGenericUtility stringFromObject:x]

#define flstrForX()         [MODAGenericUtility nonNullStringForString]:x

#define flStrEqualsIgnoreCase(x,y) [MODAGenericUtility isString1:x equalsIgnoreCaseToString2:y]
#define flHTMLEscapeStr(x) [MODAGenericUtility stringByHTMLEscaping:x]

#define flNonEmptyString(str,default) str&&str.length>0?str:default

//Singleton to  provide the OS version check.
#define IS_OS_MAJOR_VERSION_LESS_THAN(x) ([MODAGenericUtility DeviceSystemMajorVersion] < x)

#define startTiming(x) double x = [[NSDate date] timeIntervalSince1970];
#define endTiming(x) double x = [[NSDate date] timeIntervalSince1970];


@interface MODAGenericUtility : NSObject

+ (NSUInteger)DeviceSystemMajorVersion;

//Conversion functions
+ (NSString *) nonNullStringForInteger:(NSInteger) value;
+ (NSString *) nonNullStringForBool:(NSInteger) value;
+ (NSString *) nonNullStringForDate:(NSDate *) date;
+ (NSString *) nonNullStringForString:(NSString *) string;
+ (NSString *) nonNullStringForLong:(long) string;
+ (NSString *) nonNullStringForDouble:(double) string;

+ (NSInteger) intFromString:(NSString *) string;
+ (NSDate *) dateFromString:(NSString *) string;
+ (BOOL) boolFromString:(NSString *) string;
+ (long) longFromString:(NSString *) string;
+ (double) doubleFromString:(NSString *) string;

+ (NSInteger) intFromObject:(id) obj;
+ (NSDate *) dateFromObject:(id) obj;
+ (BOOL) boolFromObject:(id) obj;
+ (long) longFromObject:(id) obj;
+ (double) doubleFromObject:(id) obj;
+ (NSString *) stringFromObject:(id) obj;

@end
