//
//  UploadImagesToAmazonServer.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "UploadImagesToAmazonServer.h"
#import <AWSS3/AWSS3.h>

static UploadImagesToAmazonServer *uploadImageToServer = nil;

@implementation UploadImagesToAmazonServer

+(instancetype)sharedInstance
{
    if (!uploadImageToServer) {
        
        uploadImageToServer = [[self alloc] init];
    }
    return uploadImageToServer;
}

-(void)uploadProfileImageToServer:(UIImage *)profileImage
                         andEmail:(NSString *)userEmail
                          andType:(NSInteger)type
                   withCompletion:(void (^)(NSString *))complete

{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMddhhmmssa"];
    
    NSString *name = [NSString stringWithFormat:@"%@.png",[formatter stringFromDate:[NSDate date]]];
    
    NSString *fullImageName = [NSString stringWithFormat:@"ProfileImages/%@",name];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[formatter stringFromDate:[NSDate date]]]];
    
    NSData *data = UIImageJPEGRepresentation(profileImage,0.8);
    [data writeToFile:getImagePath atomically:YES];
    
    
    [AmazonTransfer upload:getImagePath
                   fileKey:fullImageName
                  toBucket:Bucket
                  mimeType:@"image/png"
           completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error){
               
               if (!error) {
                   
                   [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName] forKey:iServeUserProfilepic];
                   
                   NSLog(@"Uploaded Profile Image:%@",[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                   [[NSNotificationCenter defaultCenter] postNotificationName:iServeProfileImageChanged object:nil userInfo:nil];
                   
                   if(type == 1)
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           [[ProgressIndicator sharedInstance]hideProgressIndicator];
                       });
                   
                   complete([NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                   
               }
               else
               {
                   if(type == 1)
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           [[ProgressIndicator sharedInstance]hideProgressIndicator];
                       });
                   
                   complete([NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
               }
               
           }];
    
}


-(void)uploadJobImagesToServer:(NSArray *)jobImagesarray andBid:(NSString *)bid{
    
    for(int i=0; i<jobImagesarray.count;i++)
    {
        NSString *name = [NSString stringWithFormat:@"%@_%i.png",bid,i];
        
        NSString *fullImageName = [NSString stringWithFormat:@"JobImages/%@",name];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMddhhmmssa"];
        
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[formatter stringFromDate:[NSDate date]]]];
        
        UIImage *jobimage = jobImagesarray[i];
        
        jobimage = [self imageWithImage:jobimage scaledToSize:CGSizeMake(250,250)];

        
        NSData *data = UIImageJPEGRepresentation(jobimage,0.8);
        [data writeToFile:getImagePath atomically:YES];
        
       
        [AmazonTransfer upload:getImagePath
                       fileKey:fullImageName
                      toBucket:Bucket
                      mimeType:@"image/png"
               completionBlock:^(AWSS3TransferUtilityUploadTask *task, NSError *error) {
                   
                   if (!error) {
                       
                       NSLog(@"Uploaded Job Image:%@",[NSString stringWithFormat:@"https://%@.s3.amazonaws.com/%@",Bucket,fullImageName]);
                       
                   }
                   else{
                       
                   }
                   
               }];
        
    }

}

-(void)downloadImageFromAmazon
{
    [AmazonTransfer download:@"ProfileImages/c1@gmail.com.jpg"
                  fromBucket:Bucket
             completionBlock:^(AWSS3TransferUtilityDownloadTask *task,
                              NSURL * __nullable location,
                              NSData * __nullable data,
                              NSError * __nullable error)
     {
         if (!error)
         {
             
                         
         }
         else{
             
         }

         
     }];
}

-(void)deleteImageFromAmazon:(NSString *)imageURL
{
    NSArray *arr = [imageURL componentsSeparatedByString:@"/"];
    [AmazonTransfer deleteObjectFromAWS:[NSString stringWithFormat:@"ProfileImages/%@",arr[arr.count-1]]];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
