//
//  TWTHomeVC.h
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.


#import <UIKit/UIKit.h>
#import "AMSlideMenuLeftTableViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "WildcardGestureRecognizer.h"
#import "LeftMenuVC.h"
#import "MyAppTimerClass.h"

@interface HomeViewController : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate>

//Navigation
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) IBOutlet UIView *navigationTitleview;
@property (weak, nonatomic) IBOutlet UIButton *showmapViewButton;
@property (weak, nonatomic) IBOutlet UIButton *showListViewButton;


//Map
@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UIButton *lockOrUnlockButton;


//Top Address
@property (weak, nonatomic) IBOutlet UIView *topAddressView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchAddressButton;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topAdddressViewTopConstraint;


//ScrollView
@property (weak, nonatomic) IBOutlet UIView *scrollerBackgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollerContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewEqualWidthConstraint;


//Bottom View
@property (weak, nonatomic) IBOutlet UIView *bookNowOrLaterBackGroundView;
@property (weak, nonatomic) IBOutlet UIButton *bookNowButton;
@property (weak, nonatomic) IBOutlet UIButton *bookLaterButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bookNowOrLaterBackgroundViewBottomConstraint;


//Minute View
@property (weak, nonatomic) IBOutlet UILabel *minuteMessageLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *minutesBackgroundView;



//Category Descriprion Views
@property (weak, nonatomic) IBOutlet UIView *bottomViewBackGroundView;

@property (weak, nonatomic) IBOutlet UIView *categoryDescriptionBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *categoryDescriptionView;
@property (weak, nonatomic) IBOutlet UILabel *etaLabel;
@property (weak, nonatomic) IBOutlet UILabel *etaValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *visitFeeLabel;
@property (weak, nonatomic) IBOutlet UILabel *visitFeeValuseLabel;
@property (weak, nonatomic) IBOutlet UILabel *perHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *perHourValueLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomContraint;
@property (weak, nonatomic) IBOutlet UIButton *leftArrowButton;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowButton;


//Date Picker
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerViewBottomConstraints;


//RatingPopUp
@property NSInteger reviewCount;
@property (strong, nonatomic) NSMutableArray *arrayOfReviewNotDoneBookings;


+ (instancetype)getSharedInstance;
- (void)publishToSocket;
- (void)showInvoiceRatingPopUP;

- (IBAction)lockOrUnlockButtonAction:(id)sender;
- (IBAction)searchAddressButtonAction:(id)sender;
- (IBAction)currentLocationButtonAction:(id)sender;
- (IBAction)bookNowButtonAction:(id)sender;
- (IBAction)bookLaterButtonAction:(id)sender;
- (IBAction)showMapViewButtonAction:(id)sender;
- (IBAction)showListViewButtonAction:(id)sender;
- (IBAction)swipeGestureAction:(id)sender;
- (IBAction)leftArrowButtonAction:(id)sender;
- (IBAction)rightArrowButtonAction:(id)sender;
- (IBAction)datePickerDoneButtonAction:(id)sender;
- (IBAction)datePickerCancelButtonAction:(id)sender;


@end
