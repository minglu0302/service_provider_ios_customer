//
//  MainVC.m
//  Towtray
//
//  Created by -Tony Lu on 19/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "LeftMenuVC.h"

@interface LeftMenuVC ()

@end

@implementation LeftMenuVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(NSString *)segueIdentifierForIndexPathInLeftMenu:(NSIndexPath *)indexPath
{
    NSString *identifier=@"";
    switch (indexPath.row)
    {
        case 0:
            identifier = @"Home";
            break;
        case 1:
            identifier = @"History";
            break;
        case 2:
            identifier = @"Payments";
            break;
        case 3:
            identifier = @"myAddress";
            break;
//        case 4:
//            identifier = @"SUPPORT";
//            break;
//        case 5:
//            identifier = @"Invite";
//            break;
        case 4:
            identifier = @"About";
            break;
        case 5:
            identifier = @"LiveChat";
            break;

        default: break;
    }
    return identifier;
}

- (CGFloat)leftMenuWidth
{
    return 221;//self.view.bounds.size.width * 0.75;
}

- (CGFloat)rightMenuWidth
{
    return 100;
}

-(void)setBooleanValue
{
    self.isSomethingEnabled = YES;
}

- (void)configureLeftMenuButton:(UIButton *)button
{
    if(self.isSomethingEnabled)
    {
        CGRect frame = button.frame;
        frame = CGRectMake(0, 0, 30, 30);
        button.frame = frame;
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:LS(@"BACK") forState:UIControlStateNormal];
        
    }
    else
    {
        CGRect frame = button.frame;
        frame = CGRectMake(0, 0, 30, 30);
        button.frame = frame;
        button.backgroundColor = [UIColor clearColor];
        [button setBackgroundImage:[UIImage imageNamed:@"menu_btn_off"] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@"menu_btn_on"] forState:UIControlStateHighlighted];
    }
}
- (void) configureSlideLayer:(CALayer *)layer
{
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 0.3;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 2;
    layer.masksToBounds = YES;
    layer.shadowPath =[UIBezierPath bezierPathWithRect:self.view.layer.bounds].CGPath;
}
- (UIViewAnimationOptions) openAnimationCurve
{
    return UIViewAnimationOptionCurveEaseInOut;
}
- (UIViewAnimationOptions) closeAnimationCurve
{
    return UIViewAnimationOptionCurveEaseInOut;
}

- (AMPrimaryMenu)primaryMenu
{
    return AMPrimaryMenuLeft;
}
- (BOOL)deepnessForLeftMenu
{
    return YES;
}
- (BOOL)deepnessForRightMenu
{
    return YES;
}
- (CGFloat)maxDarknessWhileLeftMenu
{
    return 0.5;
}


@end
