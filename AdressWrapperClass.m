//
//  AdressWrapperClass.m
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AdressWrapperClass.h"
#import "ManageAddress.h"

static AdressWrapperClass *addressWraper = nil;

@implementation AdressWrapperClass

+(instancetype) sharedInstance
{
    if (!addressWraper) {
        
        addressWraper = [[self alloc] init];
    }
    return addressWraper;
    
}

-(void)updateAddressInDatabase:(NSMutableArray *)addressInfo
{
    self.arrayOfAddresses = addressInfo;
    Database *db = [Database sharedInstance];
    
    for (int i =0; i<self.arrayOfAddresses.count; i++)
    {
        NSString *str = self.arrayOfAddresses[i][@"aid"];
        
        if(![self checkAddressIdAddedOrNot:str And:i])
        {
            [db makeDataBaseEntryForManageAddress:self.arrayOfAddresses[i]];
        }
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (BOOL)checkAddressIdAddedOrNot:(NSString *)cardId And:(int)arrIndex
{
    NSArray *array = [Database getManageAddressDetails];
    if ([array count]== 0)
    {
        return 0;
    }
    
    for(int i=0 ; i<[array count];i++)
    {
        ManageAddress *manageAddress = [array objectAtIndex:i];
        
        if ([manageAddress.addressId isEqualToString:self.arrayOfAddresses[arrIndex][@"aid"]])
        {
            return 1;
        }
    }
    return 0;
}

#pragma mark - Web Service Call -

-(void)sendRequestToGetAddress
{
    if([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_cust_id":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID]
                                 };
        
        [[WebServiceHandler sharedInstance]sendRequestToGetAddress:params andDelegate:self];
        
    }
}

-(void)sendEventIdToServer:(NSString *)bid andEventID:(NSString *)eventId
{
    if([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":bid,
                                 @"ent_rem_id":eventId,
                                 @"ent_user_type":@"2"
                                };
        
        [[WebServiceHandler sharedInstance]sendEventToServer:params andDelegate:self];
        
    }
}


#pragma mark - Web service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            
        }
            break;
            
        case 0:
        {
            if(requestType == RequestTypeGetAddress)
            {
                [self updateAddressInDatabase:[[NSMutableArray alloc]initWithArray:response[@"addlist"]]];
            }
            else if (requestType == RequestTypeSendEventId)
            {
                
            }
           
        }
            break;
            
        default:
            break;
    }
    
}

-(void)addReminder:(NSDate *)startDate andEndDate:(NSDate *)endDate Title:(NSString *)title Message:(NSString *)message BookingId:(NSString *)bid
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [EKEvent eventWithEventStore:store];
         //        event.timeZone =  [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.title = title;
         event.notes = message;
         
         
         NSArray *alarms = @[[EKAlarm alarmWithRelativeOffset: - 60.0f *60* 1.0f]];
         event.alarms = alarms;
         
         event.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.startDate = startDate;
         
         event.endDate = endDate;  //set 1 hour meeting
         event.calendar = [store defaultCalendarForNewEvents];
         NSError *err = nil;
         [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
         NSString *eventID = [NSString stringWithFormat:@"%@", event.eventIdentifier];
         NSLog(@"Event Id:%@",eventID);
         [self sendEventIdToServer:bid andEventID:eventID];
        
     }];
    
}

-(void)removeReminder:(NSString *)eventId
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [store eventWithIdentifier:eventId];
         
         if(event)
         {
             NSError *err = nil;
             [store removeEvent:event span:EKSpanThisEvent commit:YES error:&err];
         }
     }];

}


//         //Converting GMt to Current Locale
//         NSArray * arr = [self.selectedDate componentsSeparatedByString:@" "];
//         NSLog(@"Array values are : %@",arr);
//
//
//         NSString *tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"from"]];
//
//
//
//         NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
//
//         NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
//
//
//         [dateFormat setLocale:locale];
//
//         //        [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//
//         [dateFormat setDateFormat:@"YYYY-MM-dd hh:mm a"];
//         NSDate *startDate = [dateFormat dateFromString:tempStringDate];
//
//
//         tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"to"]];
//
//         NSDate *endDate = [dateFormat dateFromString:tempStringDate];
//
//         //        NSTimeInterval end = [self.selectedSlotDictionary[@"etime"] doubleValue];
//         //        NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];

@end
