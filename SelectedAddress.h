//
//  SelectedAddress.h
//  OnTheWay_Customer
//
//  Created by Apple on 03/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface SelectedAddress : NSManagedObject

@property (nonatomic ,retain) id previouslySelectedAddress;

@end
