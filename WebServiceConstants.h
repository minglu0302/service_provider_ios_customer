//
//  WebServiceConstants.h
//  MODA
//
//  Created by -Tony Lu on 2/29/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#ifndef WebServiceConstants_h
#define WebServiceConstants_h


#define BASE_IP @"http://www.goclean-service.com/"
#define BASE_URL BASE_IP @"services.php/"
#define BASE_URL_SUPPORT BASE_IP
#define baseUrlForOriginalImage BASE_IP @"pics/"

typedef enum : NSUInteger
{
    RequestTypeUpdateAppVersion,
    RequestTypeVerifyMobileNumber,
    RequestTypeVerifyReferralCode,
    RequestTypeVerifyEmail,
    RequestTypeGetOTP,
    RequestTypeVerifyOTP,
    RequestTypeSignUp,
    RequestTypeSignIn,
    RequestTypeGetAllApptDetails,
    RequestTypeGetAllOngoingApptDetails,
    RequestTypeGetParticularApptDetails,
    RequestTypeGetChatMessages,
    RequestTypeGetProviderDetails,
    RequestTypeUpdateReview,
    RequestTypeUpdateDispute,
    RequestTypeGetProfileData,
    RequestTypeUpdateProfileData,
    RequestTypeForgetPassword,
    RequestTypeForgotPasswordVerifyPhoneNumber,
    RequestTypeChangePassword,
    RequestTypeSignOut,
    RequestTypeLiveBooking,
    RequestTypeCancelBooking,
    RequestTypeGetServiceGroupDetails,
    RequestTypeGetCancelReasons,
    RequestTypeAddCard,
    RequestTypeGetCardDetails,
    RequestTypeDeleteCard,
    RequestTypeCheckCoupon,
    RequestTypeAddAddress,
    RequestTypeDeleteAddress,
    RequestTypeGetAddress,
    RequestTypeGetSupportDetails,
    RequestTypeSendEventId,

} RequestType;

#define kRequestTypeUpdateAppVersion            @"UpdateAppVersion"

#define kRequestTypeSignUp                      @"slaveSignup"
#define kRequestTypeSignIn                      @"slaveLogin"
#define kRequestTypeLiveBooking                 @"liveBooking"
#define kRequestTypeCheckMobile                 @"checkMobile"
#define kRequestTypeVerifyReferralCode          @"verifyCode"
#define kRequestTypeVerifyEmail                 @"validateEmailZip"
#define kRequestTypeGetOTP                      @"getVerificationCode"
#define kRequestTypeVerifyOTP                   @"verifyPhone"
#define kRequestTypeForgetPassword              @"forgotPassword"
#define kRequestTypeForgotPasswordVerifyPhoneNumber @"ForgotPasswordWithOtp"
#define kRequestTypeChangePassword              @"updatePasswordForUser"

#define kRequestTypeGetProfileData              @"getProfile"
#define kRequestTypeUpdateProfileData           @"updateProfile"
#define kRequestTypeSignOut                     @"logout"

#define kRequestTypeCancelBooking               @"cancelAppointment"
#define kRequestTypeGetServiceGroupDetails      @"getAllServices"
#define kRequestTypeGetCancelReasons            @"getCancellationReson"

#define kRequestTypeGetAllOngoingApptStatus     @"getAllOngoingBookings"
#define kRequestTypeGetAllApptDetails           @"getSlaveAppts"
#define kRequestTypeGetParticularApptDetails    @"getAppointmentDetails"
#define kRequestTypeUpdateReview                @"updateSlaveReview"
#define kRequestTypeUpdateDispute               @"reportDispute"
#define kRequestTypeGetProviderDetails          @"getMasterDetails"
#define kRequestTypeGetChatMessages             @"getallMessages"

#define kRequestTypeAddCard                     @"addCard"
#define kRequestTypeGetCardDetails              @"getCards"
#define kRequestTypeDeleteCard                  @"removeCard"

#define kRequestTypeAddAddress                  @"AddCustomerAddress"
#define kRequestTypeDeleteAddress               @"DeleteCustomerAddress"
#define kRequestTypeGetAddress                  @"GetCustomerAddress"

#define kRequestTypeGetSupportDetails           @"support"

#define kRequestTypeCheckCoupon                 @"checkCoupon"

#define kRequestTypeSendEventId                 @"UpdateBookingReminder"


static NSString *const kTWTFirstName           = @"ent_first_name";
static NSString *const kTWTLastName            = @"ent_last_name";
static NSString *const kTWTEmail               = @"ent_email";
static NSString *const kTWTMobile              = @"ent_phone";
static NSString *const kTWTPhone               = @"ent_mobile";
static NSString *const kTWTPassword            = @"ent_password";
static NSString *const kTWTDeviceType          = @"ent_device_type";
static NSString *const kTWTDeviceId            = @"ent_dev_id";
static NSString *const kTWTPushToken           = @"ent_push_token";
static NSString *const kTWTDateTime            = @"ent_date_time";
static NSString *const kTWTLatitude            = @"ent_latitude";
static NSString *const kTWTLongitude           = @"ent_longitude";
static NSString *const kTWTCity                = @"ent_city";
static NSString *const kTWTEnterUserTypekey    = @"ent_user_type";
static NSString *const kTWTZipCode             = @"zip_code";
static NSString *const kTWTEnterPhone          = @"ent_phone";
static NSString *const kTWTEnterCode           = @"ent_code";
static NSString *const kTWTSessionTokenkey     = @"ent_sess_token";
static NSString *const kTWTEAppointmentDatekey = @"ent_appnt_dt";


// Sign up
static NSString *const kTWTSignupAccessToken   = @"ent_token";
static NSString *const kTWTSignupTermscond     = @"ent_terms_cond";
static NSString *const kTWTSignupPricing       = @"ent_pricing_cond";

#endif /* WebServiceConstants_h */
