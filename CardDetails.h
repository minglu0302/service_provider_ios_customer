//
//  Entity.h
//  privMD
//
//  Created by -Tony Lu on 20/03/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CardDetails : NSManagedObject

@property (nonatomic, retain) NSString * expMonth;
@property (nonatomic, retain) NSString * expYear;
@property (nonatomic, retain) NSString * idCard;
@property (nonatomic, retain) NSString * last4;
@property (nonatomic, retain) NSString * cardtype;

@end
