//
//  Entity.m
//  privMD
//
//  Created by -Tony Lu on 20/03/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import "CardDetails.h"


@implementation CardDetails

@dynamic expMonth;
@dynamic expYear;
@dynamic idCard;
@dynamic last4;
@dynamic cardtype;

@end
