//
//  TWTAmSlideTableViewCell.m
//  Towtray
//
//  Created by -Tony Lu on 19/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AmSlideTableViewCell.h"

@implementation AmSlideTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
 
    if (highlighted)
    {
        self.contentView.backgroundColor=[UIColor colorWithRed:240.0 green:240.0 blue:240.0 alpha:1];
    }
    else
    {
        self.contentView.backgroundColor=[UIColor clearColor];
    }    
}



@end
