//
//  TWTShareVC.m
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ShareViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <FBSDKShareKit/FBSDKShareDialog.h>
#import <FBSDKShareKit/FBSDKShareLinkContent.h>


@interface ShareViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIAlertViewDelegate>
{
    NSString *shareText;
}

@end

@implementation ShareViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.couponCodeLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCouponkey];
    shareText = [NSString stringWithFormat:@"Download Goclean Service using my referral code '%@' and earn discounts on booking essential services on the mobile app",self.couponCodeLabel.text];
    
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}


#pragma mark - UIButton Actions -

- (IBAction)facebookButtonAction:(id)sender {
    
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//        
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        
//        [controller setInitialText:shareText];
//        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
//             [self showPostedSuccessMessage:result];
//        }];
//        
//        [controller addImage:[UIImage imageNamed:@"Icon-Small-50"]];
//        [controller addURL:[NSURL URLWithString:@"http://www.goclean-service.com"]];
//        [self presentViewController:controller animated:YES completion:Nil];
//        
//    }
//    else{
//        
//        UIAlertView *alertView = [[UIAlertView alloc ]initWithTitle:@"No Facebook Account!" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
//        [alertView show];
//        
//    }
    
    
    
    // Put together the dialog parameters
    FBSDKShareLinkContent  *content = [[FBSDKShareLinkContent  alloc]init];
    
    [content setContentTitle: NSLocalizedString(@"Goclean Service", @"Goclean Service")];
    [content setContentURL:[NSURL URLWithString:webSiteURL]];
 
    [content setContentDescription:shareText];
    
    [content setImageURL:[NSURL URLWithString:imgLinkForSharing]];
    
    FBSDKShareDialog* dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeFeedWeb;
    dialog.fromViewController = self;
    [dialog setShareContent:content];
    [dialog show];

}

- (IBAction)twitterButtoAction:(id)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            [self showPostedSuccessMessage:result];
        }];
        
        [controller setInitialText:shareText];
        
        [controller addImage:[UIImage imageNamed:@"Icon-Small-50"]];
        [controller addURL:[NSURL URLWithString:webSiteURL]];
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        
        UIAlertView *alertView = [[UIAlertView alloc ]initWithTitle:LS(@"No Twitter Account!") message:LS(@"There is no Twitter account configured. You may add or create a Twitter account.") delegate:self cancelButtonTitle:LS(@"Cancel") otherButtonTitles:LS(@"Settings"), nil];
        [alertView show];
        
    }

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=Settings"]];
    }
    else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)showPostedSuccessMessage:(NSInteger )result
{
    switch (result) {
        case SLComposeViewControllerResultCancelled:
            
            break;
        case SLComposeViewControllerResultDone:
            [UIHelper showMessage:LS(@"Success!") withTitle:LS(@"Posted successfully") delegate:self];
            break;
            
        default:
            break;
    }

}

- (IBAction)messageButtonAction:(id)sender {
    
    if([MFMessageComposeViewController canSendText]){
        
        MFMessageComposeViewController *viewController = [[MFMessageComposeViewController alloc] init];
        viewController.messageComposeDelegate = self;
       
        viewController.body = shareText;
        [self presentViewController:viewController animated:YES completion:NULL];
        
    }
    else{
        
//        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [warningAlert show];
        [UIHelper showMessage:LS(@"Your device doesn't support SMS!") withTitle:LS(@"Alert!") delegate:self];

    }

}

- (IBAction)emailButtonAction:(id)sender {
    
    if([MFMailComposeViewController canSendMail]){
     
        MFMailComposeViewController *viewController = [[MFMailComposeViewController alloc] init];
        viewController.mailComposeDelegate = self;
        [viewController setSubject:LS(@"Download Goclean Service app")];
        [viewController setMessageBody:shareText isHTML:NO];
        NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"Icon-Small-50"], 1.0);
        [viewController addAttachmentData:data mimeType:@"png" fileName:@"Icon-Small-50.png"];
        [self presentViewController:viewController animated:YES completion:NULL];

    }
    else{
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
//                                                        message:@"Your device doesn't support the composer sheet"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];

        [UIHelper showMessage:LS(@"Your device doesn't support Email") withTitle:LS(@"Alert!") delegate:self];
    }
    
}


#pragma mark - Message Share Delegate -

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
//            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [warningAlert show];
            
            [UIHelper showMessage:LS(@"Failed to send!") withTitle:LS(@"Error") delegate:self];
            
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - Mail Share Delegate -

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}


@end
