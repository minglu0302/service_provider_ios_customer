//
//  CardAddOrDeleteClass.m
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CardAddOrDeleteClass.h"

static CardAddOrDeleteClass *cardAddOrDelete = nil;

@implementation CardAddOrDeleteClass

+(instancetype) sharedInstance
{
    if (!cardAddOrDelete) {
        
        cardAddOrDelete = [[self alloc] init];
    }
    return cardAddOrDelete;

}

-(void)updateCardsInDatabase:(NSMutableArray *)cardsInfo
{
    self.arrayOfCards = cardsInfo;
    Database *db = [Database sharedInstance];
    
    for (int i =0; i<self.arrayOfCards.count; i++)
    {
        NSString *str = self.arrayOfCards[i][@"id"];
        
        if(![self checkCardIdAddedOrNot:str And:i])
        {
            [db makeDataBaseEntry:self.arrayOfCards[i]];
        }
    }
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
}

- (BOOL)checkCardIdAddedOrNot:(NSString *)cardId And:(int)arrIndex
{
    NSArray *array = [Database getCardDetails];
    if ([array count]== 0)
    {
        return 0;
    }
    
    for(int i=0 ; i<[array count];i++)
    {
        CardDetails *fav = [array objectAtIndex:i];
        
        if ([fav.idCard isEqualToString:self.arrayOfCards[arrIndex][@"id"]])
        {
            return 1;
            
        }
    }
    return 0;
    
}

@end
