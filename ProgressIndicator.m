//
//  ProgressIndicator.m
//  HiCab_Passenger
//
//  Created by 3Embed on 14/12/12.
//  Copyright (c) 2012 Mayank. All rights reserved.
//

#import "ProgressIndicator.h"

@implementation ProgressIndicator
@synthesize displayMessage;

static ProgressIndicator *pi = nil;

#define DEAFULT_MESSAGE LS(@"Loading...")

+ (id)sharedInstance {
    
	if (!pi) {
		pi  = [[self alloc] init];
	}
	
	return pi;
}

-(MBProgressHUD*)getSharedInstace
{
    if (!HUD)
    {
        HUD = [[MBProgressHUD alloc] init];
        return HUD;
    }
    return HUD;
}

-(void)hideProgressIndicator
{
    MBProgressHUD *hud = [self getSharedInstace];
    [hud hideAnimated:YES];
    
}


#pragma mark - Show Message On Window -

-(void)showPIOnWindow:(UIWindow*)window withMessage:(NSString*)message
{
    MBProgressHUD *hud = [self getSharedInstace];
   
    hud.label.textColor = [UIColor whiteColor];
    hud.frame = window.frame;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.bezelView.alpha = 1;
    
	[window addSubview:hud];
	
	hud.delegate = self;
    if (message == nil) {
        hud.label.text = DEAFULT_MESSAGE;
    }
	hud.label.text = message;
	
	[hud showAnimated:YES];
}

#pragma mark - Show Message On View -

-(void)showPIOnView:(UIView*)view withMessage:(NSString*)message
{
    MBProgressHUD *hud = [self getSharedInstace];
    hud.label.textColor = [UIColor whiteColor];
    hud.frame = view.frame;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.bezelView.alpha = 1;
    
    [view addSubview:hud];
    
    hud.delegate = self;
    if (message == nil) {
        hud.label.text = DEAFULT_MESSAGE;
    }
    hud.label.text = message;
    
    [hud showAnimated:YES];
    
}

#pragma mark - Change Message -

-(void)changePIMessage:(NSString*)_newMessage
{
    MBProgressHUD *hud = [self getSharedInstace];
    if (_newMessage != nil)
        hud.label.text = _newMessage;
    else
        hud.label.text = DEAFULT_MESSAGE;
}

#pragma mark - Show Message Like Alert -

-(void)showMessage:(NSString*)message On:(UIView*)view
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
	
//    [UIView animateWithDuration:0.5f animations:^{
//    
//        [view layoutIfNeeded];
//    }];

	// Configure for text only and offset down
	hud.mode = MBProgressHUDModeText;
    hud.label.font = [UIFont fontWithName:OpenSans_SemiBold size:10];
    hud.label.numberOfLines = 0;
	hud.label.text = message;
	hud.margin = 10.f;
    hud.contentColor = [UIColor whiteColor];
	hud.removeFromSuperViewOnHide = YES;
    hud.bezelView.backgroundColor = UIColorFromRGB(0x333333);
    hud.frame = CGRectMake(0, view.frame.size.height-200, view.frame.size.width, 150);
	[hud hideAnimated:YES afterDelay:3];
//    [UIView animateWithDuration:0.5f animations:^{
//        
//
//        [view layoutIfNeeded];
//    }];

}

-(void)showRoundProgressBarOnView:(UIView*)view
{
    MBProgressHUD *hud = [self getSharedInstace];
    hud.label.textColor = [UIColor whiteColor];
    hud.frame = view.frame;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.bezelView.alpha = 1;
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    
    
    [view addSubview:hud];
    
    hud.delegate = self;

    [hud showAnimated:YES];
    
}


#pragma mark - MBProgressHUDDelegate methods

- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	
	HUD = nil;
}
@end
