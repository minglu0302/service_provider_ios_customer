//
//  TWTSupportViewController.h
//  Towtray
//
//  Created by -Tony Lu on 24/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,WebServiceHandlerDelegate>

/**
 *    Details List Array for storing the server data
 */
@property (nonatomic,strong) NSMutableArray *detailsOfListArray;
/**
 *    weblink url string for storing the weblink
 */
@property (nonatomic,strong) NSString *webUrlLink;
/**
 *    navtitle string for storing the navtitle
 */
@property (nonatomic,strong) NSString *navTitle;
/**
 *    title string for stroing the title
 */
@property (nonatomic,strong) NSString *titleString;

/**
 *  It store all the object that sending from server side
 */
@property (nonatomic,strong) NSMutableArray  *listOfItemsArray;
/**
 *  Supprot table view outlet
 */
@property (weak, nonatomic) IBOutlet UITableView *supportTableView;

@end

