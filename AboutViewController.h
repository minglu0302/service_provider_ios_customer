//
//  TWTAboutVC.h
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController<UIWebViewDelegate>
/**
 *    Legal Button Outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *legalButton;
/**
 *   Web Button Outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *webButton;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

/**
 *  Array Variable
 */
@property (nonatomic,strong) NSMutableArray *aboutTitleArray;
/**
 *   Web Button Action
 *
 *  @param sender
 */
- (IBAction)webButton:(id)sender;
/**
 *   Legal Button Action
 *
 *  @param sender
 */
- (IBAction)legalButton:(id)sender;
/**
 *  Rate Button Action
 *
 *  @param sender
 */
- (IBAction)rateButton:(id)sender;
/**
 *  Like Button Action
 *
 *  @param sender
 */
- (IBAction)likeButton:(id)sender;

@end
