//
//  TWTHomeVC.m
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.


#import "HomeViewController.h"
#import "PickUpViewController.h"
#import "MyAppTimerClass.h"
#import "ProviderBookingViewController.h"
#import "ProvidersListViewController.h"
#import "ProviderDetailsViewController.h"
#import "LocationServiceViewController.h"
#import "InvoicePopUpView.h"
#import "ServiceGroupViewController.h"
#import "GetCurrentLocation.h"
#import "PickAddressFromMapViewController.h"

//Define Constants tag for my view

#define mapZoomLevel 16

typedef enum
{
   isChanging = 0,
   isFixed = 1
   
}locationchange;

typedef enum {
   
   pickupVC = 0,
   providerList,
   providerDetail,
   providerSummary,
   
}segue;



@interface HomeViewController ()<AMSlideMenuDelegate,SocketWrapperDelegate,GetCurrentLocationDelegate,UIScrollViewDelegate,PickUpAddressDelegate>
{
   GMSGeocoder *geocoder_;
   
   //Path Plotting Variables
   NSMutableArray *waypoints_;
   NSMutableArray *waypointStrings_;
   
   //Initial Lat and Long
   double distanceOfClosetCar;
   
   NSMutableArray *arrayOfProviderTypeButtons;
   NSMutableArray *arrayOfSelectedImages;
   NSMutableArray *arrayOfMinutes;
   
   
   //Selected ProviderType Button Tag
   NSInteger selectedTypeButtonTag;
   NSInteger previouslySelectedTypeButtonTag;
   
   SocketIOWrapper *socketWrapper;
   CGRect screenSize;
   NSUserDefaults *ud;
   
   BOOL isLaterSelected;
   BOOL isNowSelected;
   BOOL isProviderTypeChanged;
   BOOL isComingTocheckProviderTypes;
   
   UIDatePicker *datepicker;
   NSArray *segueArray;
   
   GetCurrentLocation *getCurrentLocation;
   
   AppointmentLocation *apLocaion;
   MyAppTimerClass *myAppTimerClassObj;
   
   UILabel *minuteLabel;
   UIActivityIndicatorView *indicator;
   
   
   InvoicePopUpView *invoicePopUp;
   
   BOOL isCategoryDescriptionViewShowing;
   
   BOOL isLocationVCisShowing;
   LocationServiceViewController *locationVC;
   UIAlertController *alertController;
   
   NSArray *feeTypesArray;
   
   NSDate *scheduleDate;
   
   CGFloat providerImageHeight;

}

@property(nonatomic,strong) NSString *customerEmail;

@property(nonatomic,strong) NSMutableDictionary *allProvidersMarkers;
@property(nonatomic,strong) WildcardGestureRecognizer * tapInterceptor;

@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;

//CLLocation Address

@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;

//Progress Indicator
@property(nonatomic,strong) NSString *subscribedChannel;

//Path Plotting Variables
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)GMSMarker *destinationMarker;

//Providers Details
@property(nonatomic,strong) NSMutableArray *arrayOfProviderEmail;
@property(nonatomic,strong) NSMutableArray *arrayOfProviderTypes;
@property(nonatomic,strong) NSMutableArray *arrayOfProvidersAround;
@property (strong, nonatomic) NSMutableArray *providers;


@property(nonatomic,strong) NSDictionary *addressDetails;
@property(nonatomic,assign) BOOL isAddressManuallyPicked;

@end

static HomeViewController *sharedInstance = nil;

@implementation HomeViewController

@synthesize providers,mapView_;
@synthesize PikUpViewForConfirmScreen;
@synthesize subscribedChannel;


#pragma  mark - Initial Methods -


+ (instancetype)getSharedInstance
{
   return sharedInstance;
}

- (void)viewDidLoad
{
   [super viewDidLoad];
   
   providerImageHeight = 46;

   
   feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];

   getCurrentLocation = [GetCurrentLocation sharedInstance];
   getCurrentLocation.delegate = self;
   [getCurrentLocation getLocation];
   
   ud = [NSUserDefaults standardUserDefaults];
   
   apLocaion = [AppointmentLocation sharedInstance];
   
   self.navigationController.navigationBarHidden = NO;
   locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
   
   screenSize = [[UIScreen mainScreen] bounds];
   sharedInstance = self;
   
   
   isNowSelected = YES;
   isLaterSelected = NO;
   
   self.arrayOfProvidersAround = [[NSMutableArray alloc]init];
   
   segueArray = @[@"toPickupAddressVC",@"toProvidersListVC",@"proListViewController",@"toProviderBookingVC"];
   
   self.addressLabel.text = [ud objectForKey:iServeUserCurrentAddress];
   
   //Initializing MapView
   [self loadingOfMapView];
   
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendHeartBeatToSocket) name:iServeSocketDisconnectedAndConnected object:nil];
   
   
   //Create and Showing of Minute Lable
   //minuteLabel = [[UILabel alloc]initWithFrame:CGRectMake(4, 4, 27, 25)];
   //minuteLabel.font = [UIFont fontWithName:OpenSans_Regular size:9];
   //minuteLabel.textColor = [UIColor whiteColor];
   //minuteLabel.numberOfLines = 0;
   //minuteLabel.adjustsFontSizeToFitWidth = YES;
   //minuteLabel.textAlignment = NSTextAlignmentCenter;
   
   //[self.minutesBackgroundView addSubview:minuteLabel];
   
   //minuteLabel.text = [NSString stringWithFormat:LS(@"No\nProvider")];
   
   
   //Create and Showing of Activity Indicator in Minute View
   indicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(10, 10, 15, 15)];
   
   indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
   
   [self.minutesBackgroundView addSubview:indicator];
   
   self.categoryDescriptionView.layer.borderColor = UIColorFromRGB(0xebebeb).CGColor;
   
   [self loadViewForHomeController];
   
   NSArray *languages = [ud objectForKey:iServeUserLanguages];
   NSLog(@"Languages %@",languages);
   NSString *modelName = [UIHelper getModelName];
   NSLog(@"Model Name:%@",modelName);
   
   if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
      // Remove navigation bar bottom shadow line in iOS 11
      //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
      [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
      self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
   }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
   CGSize imageSize = CGSizeMake(1.0f, 1.0f);
   UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
   
   CGContextRef theContext = UIGraphicsGetCurrentContext();
   CGContextSetFillColorWithColor(theContext, color.CGColor);
   CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
   
   CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
   UIImage *theImage;
   if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
      theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
   } else {
      theImage = [UIImage imageWithCGImage:theCGImage];
   }
   CGImageRelease(theCGImage);
   
   return theImage;
}

- (void)viewWillAppear:(BOOL)animated
{
   [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
   [self changeButtonState:self.showmapViewButton];
   [self performSelector:@selector(sendHeartBeatToSocket)
              withObject:nil
              afterDelay:2];
   
   socketWrapper = [SocketIOWrapper sharedInstance];
   socketWrapper.socketDelegate = self;
  
   
   
   [self performSelector:@selector(hideLoadingProgressBar) withObject:nil afterDelay:3];
   
   //   [self publishToSocket];
   
   myAppTimerClassObj = [MyAppTimerClass sharedInstance];
   [myAppTimerClassObj stopPublishTimer];
   [myAppTimerClassObj startPublishTimer];
   
   [self enableAllButtonInteraction];
  
   
   if(isCategoryDescriptionViewShowing)
   {
       [self hideCategoryDescriptionView];
   }
   isComingTocheckProviderTypes = NO;
   
   
}

-(void)viewDidAppear:(BOOL)animated
{
   [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

- (void)viewWillDisappear:(BOOL)animated
{
   //Stoping Socket Publishing Timer
   [myAppTimerClassObj stopPublishTimer];
   
   /**
    *  Making Appdelegate to get Socket Response
    */
   [[AMSlideMenuMainViewController getInstanceForVC:self]  disableSlidePanGestureForLeftMenu];
   AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   [appD setSocketDelegateToAppDelegate];
   
}

/**
 *  Loading Map View Properties
 */

-(void)loadingOfMapView
{
   dispatch_async(dispatch_get_main_queue(),^{

   _currentLatitude = [[[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLat]floatValue];
   _currentLongitude = [[[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLong]floatValue];
   
   GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude longitude:_currentLongitude zoom:mapZoomLevel];
   
   mapView_.camera = camera;
   mapView_.settings.compassButton = YES;
   mapView_.delegate = self;
   mapView_.myLocationEnabled = YES;
   
   geocoder_ = [[GMSGeocoder alloc] init];
   });
   
}

/**
 *  Loading Of Initial Views
 */
-(void)loadViewForHomeController
{
   self.topAddressView.hidden = NO;
   
   self.arrayOfProviderTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:iServeProvidersTypesArrayKey]];
   
   self.bottomViewBackGroundView.hidden = YES;
   self.bookNowOrLaterBackgroundViewBottomConstraint.constant = 0;
   
   if (self.arrayOfProviderTypes.count > 0)
   {
      [self showProviderTypesScroller];
      [self.scrollView setContentOffset:CGPointMake(0, 0)];
   }
   else{
      
      self.arrayOfProviderTypes = nil;
            [[ProgressIndicator sharedInstance]showMessage:LS(@"Sorry! we are not operational in your region at the moment, please write to info@goclean-service.com") On:self.view];
      
   }
   
   self.lockOrUnlockButton.hidden = YES;
   self.bookNowOrLaterBackGroundView.hidden = NO;
   
}

- (void)didReceiveMemoryWarning
{
   [super didReceiveMemoryWarning];
   // Dispose of any resources that can be recreated.
}

-(void)hideLoadingProgressBar
{
   dispatch_async(dispatch_get_main_queue(),^{
      [[ProgressIndicator sharedInstance]hideProgressIndicator];
   });
}



#pragma mark - SIOSocket Methods -

/**
 *  publishSocketStream to receiving providers Type and Details near around customer current location
 */
-(void)publishToSocket {
   
   if(_currentLatitude != 0)
   {
      NSDictionary *message = @{
                                @"btype":[NSNumber numberWithInt:3],
                                @"email":[ud objectForKey:iServeUserEmail],
                                @"lat": [NSNumber numberWithDouble:_currentLatitude],
                                @"long": [NSNumber numberWithDouble:_currentLongitude],
                              };
   
      NSLog(@"Published Message:%@",message);
      [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:message];
   }
}


/**
 *  Socket Response with Provider Details
 *
 *  @param channelName channel Identification Name
 *  @param message     socket Response Dictionary
 */
-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message{
   
   NSLog(@"HomeVC message: %@",message);
   
   if([channelName isEqualToString:@"UpdateCustomer"])
   {
      if ([[message objectForKey:@"flag"] intValue] == 0) //will get Provider Types From server
      {
         //Update Providers Type Details
         [self socketResponseWithProviderTypes:message];
         
         //Update Providers Email Details
//         [self socketResponseWithProvidersEmails:message];
         
         //Update Providers Arround Details
         [self socketResponseWithProvidersArround:message];
         
      }
      else{
         //Maintain If No Providers Type And No providers
         //Update Providers Type Details
         [self socketResponseWithProviderTypes:message];
      }
      
   }
   else if([channelName isEqualToString:@"CustomerStatus"])
   {//Booking Status Response
      
      NSInteger bstatus = [message[@"st"]integerValue];
      
      if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10){//Booking Response
         
         [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
      }
      
   }
   else if([channelName isEqualToString:@"Message"])//Chat Message Response
   {
      if(message[@"payload"])
      {
         [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
      }
   }

   
   
}


/**
 *  Parsing Of Providers Arround Details Response getting From Socket and Show providers On Map
 *
 *  @param messageDict ProvidersArround details Dictionary
 */
-(void)socketResponseWithProvidersArround:(NSDictionary *)messageDict
{
   if (!self.arrayOfProviderEmail)
   {
      //For the first time when the array is not initialised copy all the data to the array
      
      self.arrayOfProviderEmail = [[NSMutableArray alloc] init];
      self.arrayOfProvidersAround = messageDict[@"msg"][@"masArr"];
      
      
      for (int masArrIndex = 0;masArrIndex < self.arrayOfProvidersAround.count; masArrIndex++)
      {
         
         //Storing Each and Every Providers Email
         NSArray *providerData = self.arrayOfProvidersAround[masArrIndex][@"mas"];
         
         for(NSDictionary *dict in providerData)
         {
            
            [self.arrayOfProviderEmail addObject:dict[@"e"]];
            
         }
      }
      
      [self showDistanceInBottomScroller];
      
      if (self.arrayOfProvidersAround.count > 0){//Checking Any Providers Available or not
         
         providers = [[NSMutableArray alloc] init];
         [providers addObjectsFromArray:self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"]];
      }
      
      if (providers.count > 0)
      {
         //Showing a Selected Type of Providers On MapView and there Distance also
         [self changeMapMarker:selectedTypeButtonTag];
         
      }
      
      
   }
   else {
      
      //When arrayOfProvidersArround Array Have Some Data
      
      NSArray *arrayOfNewProvidersAround = messageDict[@"msg"][@"masArr"];
      NSMutableArray *arrayNewEmails = [[NSMutableArray alloc] init];
      
      //Storing Each and Every Providers Channel now email will change to channel
      for (int masArrIndex = 0;masArrIndex < arrayOfNewProvidersAround.count; masArrIndex++) {
         
         NSArray *providerData = arrayOfNewProvidersAround[masArrIndex][@"mas"];
         
         for(NSDictionary *dict in providerData){
            
            [arrayNewEmails addObject:dict[@"e"]];//change to Channels
            
         }
      }
      
      
      NSArray *previousProvidersDetails = self.arrayOfProvidersAround;
      
      //Copy the updated providers Arround Array to arrayOfProvidersArround Array
      self.arrayOfProvidersAround = [arrayOfNewProvidersAround mutableCopy];
      
      if (self.arrayOfProviderEmail.count > arrayNewEmails.count) {
         //when Providers Goes To Offline
         
         NSMutableArray *emailToRemove = [[NSMutableArray alloc] init];
         
         //need to remove channels From New Providers Channel
         for(NSString *email in self.arrayOfProviderEmail){
            
            if (![arrayNewEmails containsObject:email]) {
               
               //remove Provider
               [emailToRemove addObject:email];
               
               //Removing Provider marker From Map
               GMSMarker *marker =  self.allProvidersMarkers[email];
               
               if(marker){
                  [self removeMarker:marker];
                  [self.allProvidersMarkers removeObjectForKey:email];
               }
            }
         }
         //remove emails
         [self.arrayOfProviderEmail removeObjectsInArray:emailToRemove];
         [self showDistanceInBottomScroller];
      }
      else if (self.arrayOfProviderEmail.count < arrayNewEmails.count) {
         //when Provider Come To Online
         
         //need to add new channels to Providers channel Array
         NSMutableArray *emailsToAdd = [[NSMutableArray alloc] init];
         
         for(NSString *email in arrayNewEmails){
            
            if (![self.arrayOfProviderEmail containsObject:email]) {
               
               //add email
               [emailsToAdd addObject:email];
            }
         }
         if (!providers) {
            providers = [[NSMutableArray alloc]init];
         }
         
         
         //Showing a newly Added Providers On MapView and there Distance also
         if (self.arrayOfProvidersAround.count > 0)
         {
            [providers addObjectsFromArray:self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"]];
            [self changeMapMarker:selectedTypeButtonTag];
            [self.arrayOfProviderEmail addObjectsFromArray:emailsToAdd];
         }
         [self showDistanceInBottomScroller];
         
      }
      else if (![self.arrayOfProvidersAround isEqualToArray:previousProvidersDetails]) {
         
         //If Providers Arround Are Same count But Data will Different
         
         if (self.arrayOfProvidersAround.count > 0)
         {
            [providers addObjectsFromArray:self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"]];
            
            [self addCustomMarkerFor];
            
         }
         
         [self showDistanceInBottomScroller];
      }
      
   }
}


//-(void)socketResponseWithProvidersEmails:(NSDictionary *)messageDict {
//
//    //This will come inside only when the requesting is not happening when the user will request this will stop coming inside so that the array of driver email list will be static
//
//    NSArray *emailData = messageDict[@"es"][selectedTypeButtonTag][@"em"];
//
//
//    if(!self.arrayOfProviderEmail) { //For the first time when the array is not initialised copy all the data to the array
//        self.arrayOfProviderEmail = [emailData mutableCopy];
//    }
//    if (emailData.count != self.arrayOfProviderEmail.count) { //when array count from server changes
//
//        [self clearAndUpdateProviderEmails:emailData];
//
//    }
//    else if (emailData.count == self.arrayOfProviderEmail.count){ //when array count from server is same
//
//        if (![emailData isEqualToArray:self.arrayOFProviderChannels]) {//if the cotent is not same just copy the new array from the server
//
//            [self clearAndUpdateProviderEmails:emailData];
//        }
//
//    }
//}
//
///**
// *  Clears All ProvidersArround Email Details
// *
// *  @param emailData array Of Provider Emails
// */
//-(void)clearAndUpdateProviderEmails:(NSArray *)emailData{
//
//    [self.arrayOfProviderEmail removeAllObjects];
//    self.arrayOfProviderEmail = nil;
//    self.arrayOfProviderEmail = [emailData mutableCopy];
//
//}


/**
 *  Parsing Of Providers Types Response getting From Socket and show Provider Type in ScrollView
 *
 *  @param messageDict contains providerTypes Details
 */
-(void)socketResponseWithProviderTypes:(NSDictionary *)messageDict {
   
   NSArray *arrayOffNewProviderTypes = messageDict[@"msg"][@"types"];
   
   if (arrayOffNewProviderTypes.count > 0) {
      //If any Provfider Types available
      
      if(!self.arrayOfProviderTypes) {
         
         //For the first time when the array is not initialised copy all the data to the array
         self.arrayOfProviderTypes = [arrayOffNewProviderTypes mutableCopy];
         
         
         [self showProviderTypesScroller];
         
         
      }
      else{
         if (arrayOffNewProviderTypes.count != self.arrayOfProviderTypes.count) {
            
            //when array count from server changes
            [self clearAndUpdateProviderTypesDetails:arrayOffNewProviderTypes];
            
         }
         else if (arrayOffNewProviderTypes.count == self.arrayOfProviderTypes.count){ //when array count from server is same
            
            if ([arrayOffNewProviderTypes isEqualToArray:self.arrayOfProviderTypes]) {
               
               //Providers Types Remains Same then dont do anything
               [ud setObject:self.arrayOfProviderTypes forKey:iServeProvidersTypesArrayKey];
               [ud synchronize];
               
            }
            else
            { //if the cotent is not same just copy the new array from the server and update scroller
               
               [self clearAndUpdateProviderTypesDetails:arrayOffNewProviderTypes];
            }
         }
      }
      
      isComingTocheckProviderTypes = NO;
      
   }
   else
   {
      //If there is No Providers Type Available
      
      //removing ScrollView from the View
      if (!isComingTocheckProviderTypes) {
         
         isComingTocheckProviderTypes = YES;
         
         [self.arrayOfProviderTypes removeAllObjects];
         self.arrayOfProviderTypes = nil;
         
         [self.arrayOfProvidersAround removeAllObjects];
         self.arrayOfProvidersAround = nil;
         
         [self.arrayOfProviderEmail removeAllObjects];
         self.arrayOfProviderEmail = nil;
         dispatch_async(dispatch_get_main_queue(),^{
            
//            [[ProgressIndicator sharedInstance]showMessage:@ On:self.view];
            [UIHelper showMessage:LS(@"Sorry! we are not operational in your region at the moment, please write to info@goclean-service.com") withTitle:LS(@"Message")delegate:self];
            self.bookNowOrLaterBackgroundViewBottomConstraint.constant = -50;
            [mapView_ clear];
         });
         [self.allProvidersMarkers removeAllObjects];
         
         
         [ud setObject:[NSArray array] forKey:iServeProvidersTypesArrayKey];
         [ud synchronize];
         
         self.bottomViewBackGroundView.hidden = YES;
         
         [self setMinutesLableTitle:[NSString stringWithFormat:LS(@"No\nProvider")]];
        
         
      }
   }
   
}

/**
 *  Clear All Provider Types Details And Hide Scroll View
 *
 *  @param arrayOffNewProviderTypes updated ArrayOfProviderTypes
 */
-(void)clearAndUpdateProviderTypesDetails:(NSArray *)arrayOffNewProviderTypes{
   
   [self.arrayOfProviderTypes removeAllObjects];
   self.arrayOfProviderTypes = nil;
   
   [self.arrayOfProvidersAround removeAllObjects];
   self.arrayOfProvidersAround = nil;
   
   [self.arrayOfProviderEmail removeAllObjects];
   self.arrayOfProviderEmail = nil;
   
   dispatch_async(dispatch_get_main_queue(),^{
      [mapView_ clear];
   });
   
   [self.allProvidersMarkers removeAllObjects];
   
   self.arrayOfProviderTypes = [arrayOffNewProviderTypes mutableCopy];
   
   self.bottomViewBackGroundView.hidden = YES;
   
   [self showProviderTypesScroller];
   
   [ud setObject:self.arrayOfProviderTypes forKey:iServeProvidersTypesArrayKey];
   [ud synchronize];
   
}



#pragma mark - Provider Types Scroll View Methods -

/**
 *  Show Provider Types ScrollView On Bottom Of Screen
 */
-(void)showProviderTypesScroller
{
   if (self.arrayOfProviderTypes.count == 0)
   {
      //If There is No Provider Types
      [UIHelper showMessage:LS(@"Currently provider types are unavailable in your area.") withTitle:LS(@"Message")delegate:self];
   }
   else
   {
      //If Provider Types Available
      if (self.bottomViewBackGroundView.hidden == YES)
      {
         self.bottomViewBackGroundView.hidden = NO;
        
         
         if(arrayOfProviderTypeButtons.count > 0){
            
            [self removeAllProviderTypesInScrollView];
         }
         
         
         arrayOfProviderTypeButtons = [[NSMutableArray alloc]init];
         arrayOfSelectedImages = [[NSMutableArray alloc]init];
         arrayOfMinutes = [[NSMutableArray alloc]init];
         
         for (int i=0; i < self.arrayOfProviderTypes.count; i++)
         {
            //Creating Of Provider Type Button
            UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
            buttonCategory.frame = CGRectMake(screenSize.size.width/3*i,0,screenSize.size.width/3,100);
            
            buttonCategory.tag = i;
            [arrayOfProviderTypeButtons addObject:buttonCategory];
            
            [buttonCategory addTarget:self action:@selector(providerTypebuttonaction:) forControlEvents:UIControlEventTouchUpInside];
            
            [arrayOfMinutes addObject:[NSNumber numberWithInteger:0]];
            
            if (i == 0)
            {
               selectedTypeButtonTag = -1;
               [self providerTypebuttonaction:buttonCategory];
            }
            
            buttonCategory.titleLabel.font = [UIFont fontWithName:OpenSans_SemiBold size:10];
            [buttonCategory setTitleColor:APP_COLOR forState:UIControlStateSelected];
            [buttonCategory setTitleColor:UIColorFromRGB(0x121212) forState:UIControlStateNormal];
            
            buttonCategory.titleLabel.numberOfLines = 7;
            buttonCategory.titleLabel.minimumScaleFactor = 0.5;

            
            //            buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(0,-buttonCategory.frame.size.width/2, 0, 0);
            
            buttonCategory.titleLabel.textAlignment = NSTextAlignmentCenter;
            buttonCategory.imageView.layer.cornerRadius = providerImageHeight/2;
            //            [buttonCategory setContentMode:UIViewContentModeCenter];
            
            
            dispatch_async(dispatch_get_main_queue(),^{
               
               [self.scrollerContentView addSubview:buttonCategory];
               self.bookNowOrLaterBackgroundViewBottomConstraint.constant = 0;
               
            });
            
            UIImageView *image = [UIImageView new];
            [arrayOfSelectedImages addObject:image];
            
            
         }
         
         
         [self downloadUnSelectedProviderTypeImages];
         [self downloadSelectedProviderTypeImages];
         [self showDistanceInBottomScroller];
         [self setscrollContentViewFrame];
         
      }
      
   }
   
}


/**
 *  Each Provider Type ButtonAction
 *
 *  @param sender Button Properties
 */
-(void)providerTypebuttonaction:(UIButton *)sender {
   
   //Selected Provider Type Button
   UIButton *srcBtn = (UIButton *) sender;
   
   if(srcBtn.selected == YES)
   {
      //      ServiceGroupViewController *serviceGroupVC = (ServiceGroupViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"serviceGroupVC"];
      //      [self.navigationController pushViewController:serviceGroupVC animated:YES];
      
      if(isCategoryDescriptionViewShowing == NO)
      {
         [self showCategoryDescriptionView];
      }
      else
      {
         [self hideCategoryDescriptionView];
      }
      
   }
   else
   {
      //unselecting all Provider types
      for (UIView *button in srcBtn.superview.subviews) {
         if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
         }
      }
      
      //selecting Particular Selected Type Button
      srcBtn.selected = YES;
      
      
      if(selectedTypeButtonTag != srcBtn.tag)
      {//If Selected Other Provider Type Button
         isProviderTypeChanged = YES;
         
         
         previouslySelectedTypeButtonTag = selectedTypeButtonTag;
         if(previouslySelectedTypeButtonTag <0)
            previouslySelectedTypeButtonTag = 0;
         
         selectedTypeButtonTag = srcBtn.tag;
         [self setCategoryDescriptionDetails:self.arrayOfProviderTypes[selectedTypeButtonTag]];
         [self changeMapMarker:selectedTypeButtonTag];
         
      }
      
   }
   
}


/**
 *  Downloading Of Unselected Provider Type Images
 */
-(void)downloadUnSelectedProviderTypeImages
{
   for (int i=0; i < self.arrayOfProviderTypes.count; i++)
   {
      UIButton *buttonCategory = arrayOfProviderTypeButtons[i];
      
      NSString *unSelectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,self.arrayOfProviderTypes[i][@"unSelImg"]];
      
      [buttonCategory.imageView sd_setImageWithURL:[NSURL URLWithString:unSelectedVehicleImageURL]
                                  placeholderImage:nil
                                         completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                            
                                            if(image)
                                            {
                                               image = [self imageWithImage:image scaledToSize:CGSizeMake(providerImageHeight, providerImageHeight)];
                                               
                                               [buttonCategory setImage:image forState:UIControlStateNormal];
                                               
                                            }
                                         }];
      
   }
   
}

/**
 *  Downloading Of Selected Provider Type Images
 */
-(void)downloadSelectedProviderTypeImages{
   
   for (int i=0; i < self.arrayOfProviderTypes.count; i++)
   {
      UIButton *buttonCategory = arrayOfProviderTypeButtons[i];
      NSString *selectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,self.arrayOfProviderTypes[i][@"selImg"]];
      [arrayOfSelectedImages[i] sd_setImageWithURL:[NSURL URLWithString:selectedVehicleImageURL]
                                         completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                            
                                            if(image)
                                            {
                                               image = [self imageWithImage:image scaledToSize:CGSizeMake(providerImageHeight, providerImageHeight)];
                                               
                                               NSLog(@"%@",NSStringFromCGSize(image.size));
                                               [buttonCategory setImage:image forState:UIControlStateSelected];
                                               
                                            }
                                         }];
      
   }
}

/**
 *  Change the Image Size
 */
- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
   float widthScale = 0;
   float heightScale = 0;
   
   widthScale = boxSize.width/originalSize.width;
   heightScale = boxSize.height/originalSize.height;
   
   float scale = MIN(widthScale, heightScale);
   
   CGSize newSize = CGSizeMake(originalSize.width * scale , originalSize.height * scale);
   
   return newSize;
}


/**
 *  Fit The Image To required Size
 */
-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
   
   UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.5);
   [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
   UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
   return newImage;
}

/**
 *  Set The Scroll ContentView Width Constraint To Make Scropllable
 */
-(void)setscrollContentViewFrame
{
   dispatch_async(dispatch_get_main_queue(),^{
      
      if(self.arrayOfProviderTypes.count > 3){
         
         self.scrollContentViewEqualWidthConstraint.constant = (screenSize.size.width/2)*self.arrayOfProviderTypes.count  - screenSize.size.width;
         
         //Set Left And Right Arrow Property
         self.leftArrowButton.hidden = YES;
         self.rightArrowButton.hidden = NO;
      }
      else
      {
         self.scrollContentViewEqualWidthConstraint.constant = 0;
         
         //Set Left And Right Arrow Property
         self.leftArrowButton.hidden = YES;
         self.rightArrowButton.hidden = YES;
      }
      
   });
   
}


/**
 *  Remove All Provider Types From ScrollContentView
 */
-(void)removeAllProviderTypesInScrollView{
   
   dispatch_async(dispatch_get_main_queue(),^{
      
      for(UIButton *typeButton in self.scrollerContentView.subviews)
      {
         [typeButton removeFromSuperview];
      }
      
   });
   
}


#pragma mark - Get Time Between Two Location Methods -

-(void)setMinutesLableTitle:(NSString *)title
{
   minuteLabel.text = title;
}

/**
 *  Show Distance And Provider Type Name in ScrollView
 */
- (void)showDistanceInBottomScroller
{
   
   for(int i=0; i<self.arrayOfProviderTypes.count;i++)
   {
      UIButton *buttonCategory = arrayOfProviderTypeButtons[i];
      
      if(self.arrayOfProvidersAround.count > 0)
      {
         //If any Providers Are Online
         
         if([self.arrayOfProvidersAround[i][@"mas"] count] > 0)
         {
            //If any Providers Are Online
            float distance = [self.arrayOfProvidersAround[i][@"mas"][0][@"d"] floatValue]/iServeDistanceMetric;
            [buttonCategory setTitle:[NSString stringWithFormat:@"%.2f Kms\n\n\n\n\n%@",distance,[self.arrayOfProviderTypes objectAtIndex:i][@"cat_name"]] forState:UIControlStateNormal];
            buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0,(buttonCategory.frame.size.width/2), 0,-(buttonCategory.frame.size.width/2));
            
            [self setProviderTypeButtonContentInsets:buttonCategory];
            
            
            if(selectedTypeButtonTag == buttonCategory.tag)
            {
               [indicator startAnimating];
               indicator.hidden = NO;
            }
            
            NSDictionary *dict = @{
                                   @"providerLat":self.arrayOfProvidersAround[i][@"mas"][0][@"lt"],
                                   @"providerLong":self.arrayOfProvidersAround[i][@"mas"][0][@"lg"],
                                   @"button":buttonCategory,
                                   };
            
            [self performSelector:@selector(showTimeBetweenTwoLocations:)
                       withObject:dict
                       afterDelay:1];
            
            
         }
         else{
            
            __weak typeof(self) weak_self = self;
            //If No Providers Are Online
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               // do something time consuming here, like network request or the like
               // when done, update the UI on the main queue:
               dispatch_async(dispatch_get_main_queue(), ^{
                  // update the UI here
                  [buttonCategory setTitle:[NSString stringWithFormat:LS(@"No Provider\n\n\n\n\n%@"),[weak_self.arrayOfProviderTypes objectAtIndex:i][@"cat_name"]] forState:UIControlStateNormal];
               
            
                  [self setProviderTypeButtonContentInsets:buttonCategory];
                  
                  
                  //Update Minutes For Selected provider Type
                  
                  if(selectedTypeButtonTag == buttonCategory.tag)
                  {
                     [weak_self setMinutesLableTitle:[NSString stringWithFormat:LS(@"No\nProvider")]];
                     [arrayOfMinutes removeObjectAtIndex:selectedTypeButtonTag];
                     [arrayOfMinutes insertObject:[NSNumber numberWithInteger:0] atIndex:selectedTypeButtonTag];
                  }
                  else
                  {
                     [arrayOfMinutes removeObjectAtIndex:buttonCategory.tag];
                     [arrayOfMinutes insertObject:[NSNumber numberWithInteger:0] atIndex:buttonCategory.tag];
                  }
                  
               });
            });
            
         }
         
      }
      else{
         
         //If No Providers Are Online
         
         [buttonCategory setTitle:[NSString stringWithFormat:LS(@"No Provider\n\n\n\n\n%@"),[self.arrayOfProviderTypes objectAtIndex:i][@"cat_name"]] forState:UIControlStateNormal];
         buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0,(buttonCategory.frame.size.width/2), 0,-(buttonCategory.frame.size.width/2));
         [self setProviderTypeButtonContentInsets:buttonCategory];
         
         [arrayOfMinutes removeObjectAtIndex:buttonCategory.tag];
         [arrayOfMinutes insertObject:[NSNumber numberWithInteger:0] atIndex:buttonCategory.tag];
         [self setMinutesLableTitle:[NSString stringWithFormat:LS(@"No\nProvider")]];
         
         
      }
   }
   
}
-(void)setProviderTypeButtonContentInsets:(UIButton *)buttonCategory
{
   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      // do something time consuming here, like network request or the like
      // when done, update the UI on the main queue:
      dispatch_async(dispatch_get_main_queue(), ^{
         // the space between the image and text
         //   CGFloat spacing = 6.0;
         // lower the text and push it left so it appears centered
         //  below the image
         CGSize imageSize = CGSizeMake(providerImageHeight, providerImageHeight);//buttonCategory.imageView.image.size;

         // update the UI here
         buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, 0.0, 0.0); //Bottom: -(imageSize.height + spacing)
         // raise the image and push it right so it appears centered
         //  above the text
         CGSize titleSize = [buttonCategory.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: buttonCategory.titleLabel.font}];
         
         buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, - titleSize.width);// Top: (titleSize.height + spacing)
         
         // increase the content height to avoid clipping
         CGFloat edgeOffset = fabs(titleSize.height - imageSize.height) / 2.0;
         buttonCategory.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
      });
   });
}


-(void)showTimeBetweenCurrentLocationToProviderLocation:(double)providerLat and:(double)providerLong
                                           onCompletion:(void (^)(NSArray *))complete
{
   NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@",_currentLatitude,_currentLongitude,providerLat,providerLong,iServeGoogleServerMapsAPIKey];
   
   NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   
   NSData *jsonData = [NSData dataWithContentsOfURL:url];
   
   if(jsonData != nil)
   {
      NSError *error = nil;
      
      id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
      NSMutableArray *arrDistance=[result objectForKey:@"routes"];
      
      complete(arrDistance);
      
   }
   else
   {
      NSLog(@"N.A.");
   }
}

-(void)showTimeBetweenTwoLocations:(NSDictionary *)dict
{
   [self showTimeBetweenCurrentLocationToProviderLocation:[dict[@"providerLat"] doubleValue]
                                                      and:[dict[@"providerLong"] doubleValue]
                                             onCompletion:^(NSArray * results)
    {
       UIButton *buttonCategory = dict[@"button"];
       
       if(results.count>0)
       {
          NSMutableArray *arrLeg=[[results objectAtIndex:0]objectForKey:@"legs"];
          NSMutableDictionary *dictleg=[arrLeg objectAtIndex:0];
          NSInteger timeInSec =  [[[dictleg   objectForKey:@"duration"] objectForKey:@"value"] integerValue];
          
          NSInteger timeInMin = [self convertTimeInMin:timeInSec];
          NSLog(@"Time:- %td",timeInMin);
          
          
          [arrayOfMinutes removeObjectAtIndex:buttonCategory.tag];
          [arrayOfMinutes insertObject:[NSNumber numberWithInteger:timeInMin] atIndex:buttonCategory.tag];
          
          if(selectedTypeButtonTag == buttonCategory.tag)
          {
             [self setMinutesLableTitle:[NSString stringWithFormat:@"%td\nMIN",[arrayOfMinutes[buttonCategory.tag]integerValue]]];
             
             dispatch_async(dispatch_get_main_queue(),^{
                
                self.etaValueLabel.text = [NSString stringWithFormat:@"%td MIN",[arrayOfMinutes[buttonCategory.tag]integerValue]];
             });
             [indicator stopAnimating];
             indicator.hidden = YES;
          }
       }
       else
       {
          if(selectedTypeButtonTag == buttonCategory.tag)
          {
             [indicator stopAnimating];
             indicator.hidden = YES;
          }
          
       }
       
    }];
   
}


-(NSInteger)convertTimeInMin:(NSInteger)timeFromServer
{
   NSInteger min;
   if (timeFromServer < 60 && timeFromServer <= 0)
   {
      min = 1;
   }
   else if (timeFromServer > 60)
   {
      min = timeFromServer/60;
      
      NSInteger remainingTime = timeFromServer%60;
      
      if (remainingTime < 60 && remainingTime >= 30)
      {
         min++;
      }
   }
   else
   {
      min = 1;
   }
   return min;
}


#pragma mark - Custom Methods -

/**
 *  Adding Gesture to show Map animation
 */
-(void)addgestureToMap
{
   _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
   __weak typeof(self) weakSelf = self;
   _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
   {
      if (touchtype == 1)
      {
         [weakSelf startAnimation];
         
      }
      else {
         
         [weakSelf endAnimation];
      }
   };
   
   [mapView_  addGestureRecognizer:_tapInterceptor];
   
}





/**
 *  Current Location Changed called when address picked manually
 *
 *  @param dictAddress <#dictAddress description#>
 */
- (void) changeCurrentLocation:(NSDictionary *)dictAddress
{
   
   //    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
   
   
   
   GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
   [mapView_ animateWithCameraUpdate:zoomCamera];
   
}

/**
 *  Adding Current Address and Location Details In appointmentLocation Class
 */
-(void)addAllDetailsToAppointmentClass
{
   
   apLocaion.pickupLatitude = [NSNumber numberWithFloat:_currentLatitude] ;
   apLocaion.pickupLongitude =  [NSNumber numberWithFloat:_currentLongitude];
   apLocaion.pickUpAddress =  self.addressLabel.text;
}

/**
 *  Sending HeartBeat To Socket
 */
-(void)sendHeartBeatToSocket
{
   [[CheckAppVersion sharedInstance] sendHeartBeatToSocket];
//   [socketWrapper sendHeartBeatForUser:[ud objectForKey:iServeCustomerID] withStatus:@"1"];
}

-(void)showInvoiceRatingPopUP
{
   if(self.arrayOfReviewNotDoneBookings.count > 0 && self.reviewCount < self.arrayOfReviewNotDoneBookings.count)
   {
      UIWindow *window = [[[UIApplication sharedApplication]delegate]window];
      
      invoicePopUp = [InvoicePopUpView sharedInstance];
      invoicePopUp.bookingStatus = 7;//completed Booking
      
      [invoicePopUp setTopViewFrame];

      [window addSubview:invoicePopUp];
      
      invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
      [UIView animateWithDuration:0.5
                            delay:0.2
                          options: UIViewAnimationOptionBeginFromCurrentState
                       animations:^{
                          
                          invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                          
                       }
                       completion:^(BOOL finished){
                          invoicePopUp.bookingId = [self.arrayOfReviewNotDoneBookings[self.reviewCount] integerValue];
                          [invoicePopUp sendRequestTogetBookingDetails];
                          self.reviewCount = self.reviewCount + 1;
                       }];
      
      
   }
}

-(void)disableAllButtonInteraction
{
   self.showmapViewButton.userInteractionEnabled = NO;
   self.showListViewButton.userInteractionEnabled = NO;
   
   self.searchButton.userInteractionEnabled = NO;
   self.searchAddressButton.userInteractionEnabled = NO;
   self.currentLocationButton.userInteractionEnabled = NO;
   
   self.bookNowButton.userInteractionEnabled = NO;
   self.bookLaterButton.userInteractionEnabled = NO;
   
}

-(void)enableAllButtonInteraction
{
   
   self.showmapViewButton.userInteractionEnabled = YES;
   self.showListViewButton.userInteractionEnabled = YES;
   
   self.searchButton.userInteractionEnabled = YES;
   self.searchAddressButton.userInteractionEnabled = YES;
   self.currentLocationButton.userInteractionEnabled = YES;
   
   self.bookNowButton.userInteractionEnabled = YES;
   self.bookLaterButton.userInteractionEnabled = YES;
   
}

#pragma  mark - Each Provider Type Description Methods -

-(void)setCategoryDescriptionDetails:(NSDictionary *)providerTypeDetails
{
   dispatch_async(dispatch_get_main_queue(),^{
      
      if(self.arrayOfProvidersAround.count > 0)
      {
         if([self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"] count] > 0)
         {
            self.etaValueLabel.text = [NSString stringWithFormat:LS(@"%@ MIN"),arrayOfMinutes[selectedTypeButtonTag]];
         }
         else
         {
            self.etaValueLabel.text = @"--";
         }
      }
      else
      {
         self.etaValueLabel.text = @"--";
      }
      
      
//      NSArray *feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
      NSString *currncySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];
      
      NSInteger amountValue;
      
      switch ([feeTypesArray indexOfObject:providerTypeDetails[@"ftype"]])
      {
         //Feixed Based
         case feeTypeFixed:
         {
            
            self.visitFeeLabel.text = LS(@"Fixed Fee");
            amountValue = [providerTypeDetails[@"fixed_price"]floatValue];
            self.visitFeeValuseLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
            
            self.perHourLabel.text = LS(@"Per Hour");
            self.perHourValueLabel.text = @"--";
            
            
         }
            break;
            
            //Hourly Based And Milleage Based
         case feeTypeHourly:
         {
            self.visitFeeLabel.text = LS(@"Visit Fee");
            amountValue = [providerTypeDetails[@"visit_fees"]floatValue];
            self.visitFeeValuseLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
            
            self.perHourLabel.text = LS(@"Per Hour");
            amountValue = [providerTypeDetails[@"price_min"]floatValue];
            self.perHourValueLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
         }
            
            break;
            
         default:
         {
            self.visitFeeLabel.text = LS(@"Minimum Fee");
            amountValue = [providerTypeDetails[@"min_fees"]floatValue];
            self.visitFeeValuseLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
            
            self.perHourLabel.text = LS(@"KMPH");
            amountValue = [UIHelper convertMilesToKilometer:[providerTypeDetails[@"price_mile"]floatValue]];
            self.perHourValueLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
           
         }
            break;
            
      }
   });

}

-(void)hideCategoryDescriptionView
{
   isCategoryDescriptionViewShowing = NO;
   
//   self.topAdddressViewTopConstraint.constant = 15;
   self.bottomViewBottomContraint.constant = -86;//height of scroll Background View
   
   [UIView animateWithDuration:0.5f animations:^{
      
      [self.view layoutIfNeeded];
   }];

}

-(void)showCategoryDescriptionView
{
   isCategoryDescriptionViewShowing = YES;
   
//   self.topAdddressViewTopConstraint.constant = -100;
   self.bottomViewBottomContraint.constant = 66;//height of Book Now Or Later Background View
   
   [UIView animateWithDuration:0.5f animations:^{
      
      [self.view layoutIfNeeded];
   }];
   
}

#pragma mark - Add Markers on Map View -

/**
 *  Show Provider marker on Map view
 *
 *  @param type ProviderType Number
 */
-(void)changeMapMarker:(NSInteger)type {
   
   providers = [[NSMutableArray alloc] init];
   
   if(self.arrayOfProvidersAround.count > 0)
   {
      
      if([self.arrayOfProvidersAround[type][@"mas"] count] > 0)
      {
         
         [providers addObjectsFromArray:self.arrayOfProvidersAround[type][@"mas"]];
         
         if (providers.count > 0)
         {
            [self addCustomMarkerFor];
            [self setMinutesLableTitle:[NSString stringWithFormat:@"%td\nMIN",[[arrayOfMinutes objectAtIndex:type]integerValue]]];
            
         }
      }
      else
      {
         
         [self setMinutesLableTitle:[NSString stringWithFormat:LS(@"No\nProvider")]];
         [indicator stopAnimating];
         indicator.hidden = YES;
         
         if (isProviderTypeChanged)
         {
            //When Provider Type Changed and providers arround available
            isProviderTypeChanged = NO;
            
            if(_allProvidersMarkers.count > 0){
               //Remove All Markers
//               for(int i=0; i< _allProvidersMarkers.count;i++)
//               {
//                  [self removeMarker:_allProvidersMarkers[self.arrayOfProvidersAround[previouslySelectedTypeButtonTag][@"mas"][i][@"e"]]];
//                  
//               }
               [self.mapView_ clear];
               _allProvidersMarkers = [[NSMutableDictionary alloc] init];
            }
            else{
               
               _allProvidersMarkers = [[NSMutableDictionary alloc] init];
            }
            
         }
         
      }
      
   }
   else
   {
      [self setMinutesLableTitle:[NSString stringWithFormat:LS(@"No\nProvider")]];
      [indicator stopAnimating];
      indicator.hidden = YES;
   }
   
}


/**
 *  Plots Marker on Map
 *
 *  @param medicalSpecialist doctor or nurse
 */
- (void)addCustomMarkerFor
{
   //When Provider Type Changed and providers arround available
   if (isProviderTypeChanged) {
      
      //When Provider Type Changed and providers arround available
      isProviderTypeChanged = NO;
      
      if(_allProvidersMarkers.count > 0){
         //Remove All Markers
         
//         for(int i=0; i< _allProvidersMarkers.count;i++)
//         {
//            [self removeMarker:_allProvidersMarkers[self.arrayOfProvidersAround[previouslySelectedTypeButtonTag][@"mas"][i][@"e"]]];
            [self.mapView_ clear];
            
//         }
         _allProvidersMarkers = [[NSMutableDictionary alloc] init];
      }
      else{
         
         _allProvidersMarkers = [[NSMutableDictionary alloc] init];
      }
      
   }
   
   
   
   for (int i = 0; i < providers.count; i++)
   {
      //If any Provider Are available Of particular Type
      NSDictionary *dict = providers[i];
      
      double latitude = [dict[@"lt"] doubleValue];
      double longitude = [dict[@"lg"] doubleValue];
      
      if (!_allProvidersMarkers[dict[@"e"]]) {
         
         //Adding markers
         __block GMSMarker *providerMarker;
         UIView *view = [self customMarkerForAddingImages:dict[@"i"]];
         
         dispatch_async(dispatch_get_main_queue(),^{
            
            providerMarker = [[GMSMarker alloc]init];
            providerMarker.userData = dict;
            providerMarker.position = CLLocationCoordinate2DMake(latitude, longitude);
            providerMarker.tappable = YES;
            providerMarker.flat = YES;
            
            providerMarker.appearAnimation = kGMSMarkerAnimationPop;
            providerMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
            providerMarker.iconView = view;
            providerMarker.map = mapView_;
            [_allProvidersMarkers setObject:providerMarker forKey:dict[@"e"]];
         });
         
      }
      else {
         
         //Just Updating marker Lat and Long
         GMSMarker *marker = _allProvidersMarkers[dict[@"e"]];
         marker.userData = dict;
         [CATransaction begin];
         [CATransaction setAnimationDuration:2.0];
         marker.position = CLLocationCoordinate2DMake(latitude, longitude);
         [CATransaction commit];
         
      }
      
   }
   
   [providers removeAllObjects];
   
}

/**
 *  Download Provider marker image
 *
 */
-(UIView *)customMarkerForAddingImages:(NSString *)providerImageUrl
{
   if([providerImageUrl isEqual:[NSNull null]])
   {
      providerImageUrl = @"";
   }
   
   UIView *markerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 60)];
   markerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_map_pin_profile_icon"]];
   
   UIImageView *markerImageViewInner = [[UIImageView alloc] initWithFrame:CGRectMake(4.2,4,36,36)];
   markerImageViewInner.layer.cornerRadius = markerImageViewInner.frame.size.height/2;
   markerImageViewInner.clipsToBounds = YES;
   
   
   [markerView addSubview:markerImageViewInner];
   
   //    if (!markerViewDict) {
   //        markerViewDict = [[NSMutableDictionary alloc]init];
   //    }
   //    [markerViewDict setObject:markerView forKey:providerImageUrl];
   
   UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(markerView.frame.size.width/2-10, markerView.frame.size.height/2-15, 20, 20)];
   activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
   
   [markerView addSubview:activityIndicator];
   
   [activityIndicator startAnimating];
   
   [markerImageViewInner sd_setImageWithURL:[NSURL URLWithString:[UIHelper removeWhiteSpaceFromURL:providerImageUrl]]
                           placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                     
                                     [activityIndicator stopAnimating];
                                     
                                     
                                  }];
   
   
   
   return markerView;
   
}

//-(void)removeAllmarkersFromMapView
//{
//   for(int i=0; i< _allProvidersMarkers.count;i++)
//   {
//      [self removeMarker:_allProvidersMarkers[self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"][i][@"e"]]];
//   }
//   
//   [_allProvidersMarkers removeAllObjects];
//   _allProvidersMarkers = [[NSMutableDictionary alloc]init];
//   [mapView_ clear];
//}

/**
 *  Remove Provider Marker From Map View
 *
 *  @param marker marker Properties
 */
-(void)removeMarker:(GMSMarker*)marker{
   
   dispatch_async(dispatch_get_main_queue(),^{
      
      marker.map = mapView_;
      marker.map = nil;
      
   });
}


#pragma mark - Animations -

- (void)startAnimation
{
   [self.view endEditing:YES];
   [UIView beginAnimations:nil context:NULL];
   [UIView setAnimationDuration:0.5];
   
   self.topAdddressViewTopConstraint.constant = -20;
   self.bookNowOrLaterBackgroundViewBottomConstraint.constant = -35;
   
   [UIView commitAnimations];
   
}

- (void)endAnimation{
   
   [UIView beginAnimations:nil context:NULL];
   [UIView setAnimationDuration:0.5];
   
   self.topAdddressViewTopConstraint.constant = 15;
   self.bookNowOrLaterBackgroundViewBottomConstraint.constant = 0;
   
   [UIView commitAnimations];
   
}

#pragma mark - Get Current Location Methods -

- (void)getAddress:(CLLocation *)coordinate and:(BOOL)isCurrentLocation
{
   CLGeocoder *geocoder = [[CLGeocoder alloc] init];
   
   [geocoder reverseGeocodeLocation:coordinate
                  completionHandler:^(NSArray *placemarks, NSError *error)
    {
       if (!(error))
       {
          CLPlacemark *placemark = [placemarks objectAtIndex:0];
          
          self.addressLabel.text = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
          NSLog(@"Selected Address In Home VC:%@",self.addressLabel.text);
          
          if(isCurrentLocation)
          {
             [ud setObject:self.addressLabel.text forKey:iServeUserCurrentAddress];
             
          }
          
       }
       else
       {
          NSLog(@"Failed to update location : %@",error);
       }
    }];
   
}

#pragma mark - GMSMapview Delegate -

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
   
   ProviderDetailsViewController *proDetailsVC = (ProviderDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"providerDetailsVC"];
   
   proDetailsVC.providerId = [marker.userData[@"pid"]integerValue];
   
   [self addAllDetailsToAppointmentClass];
   UIImageView *providerImageView;
   for(UIImageView *subview in marker.iconView.subviews)
   {
      if([subview isKindOfClass:[UIImageView class]])
      {
         providerImageView = subview;
         break;
      }
   }
   
   NSDictionary *proDetails = @{
                                @"miles":[NSString stringWithFormat:@"%.2f Kms away",[marker.userData[@"d"] floatValue]/iServeDistanceMetric],
                                @"rating":marker.userData[@"rat"],
                                @"image":providerImageView.image,
                                @"email":marker.userData[@"e"]
                                };
   
   proDetailsVC.providerDetailsFromPreviousController = proDetails;
   
   NSMutableDictionary *temp = [[NSMutableDictionary alloc]initWithDictionary: self.arrayOfProviderTypes[selectedTypeButtonTag]];
   
   if(marker.userData[@"amt"])
   {
      
      switch ([feeTypesArray indexOfObject:temp[@"ftype"]])
      {
            //Feixed Based
         case feeTypeFixed:
         {
            
            [temp setObject:[NSNumber numberWithDouble:[marker.userData[@"amt"]doubleValue]] forKey:@"fixed_price"];
            
         }
            break;
            
            //Hourly Based And Milleage Based
         case feeTypeHourly:
         {
            [temp setObject:[NSNumber numberWithDouble:[marker.userData[@"amt"]doubleValue]] forKey:@"price_min"];
         }
            
            break;
            
         default:
         {
            [temp setObject:[NSNumber numberWithDouble:[marker.userData[@"amt"]doubleValue]] forKey:@"price_mile"];
         }
            break;
            
      }
      
   }
   
   
   
   proDetailsVC.providerTypeDetails = temp.mutableCopy;
   
   [ud setValue:[NSString stringWithFormat:@"%ld",(long)selectedTypeButtonTag] forKey:@"selectedProviderTypeTag"];
   [ud synchronize];
   
   [self.navigationController pushViewController:proDetailsVC animated:YES];
   return YES;
   
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
   
   if (_isAddressManuallyPicked) {
      //Checking Address Picked Manually (Get Address From Search Address Controller)
      
      _isAddressManuallyPicked = NO;
      return;
   }
   else {
      
      CGPoint point1 = mapView_.center;
      CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
      _currentLatitude = coor.latitude;
      _currentLongitude = coor.longitude;

      CLLocation *location = [[CLLocation alloc]initWithLatitude:_currentLatitude longitude:_currentLongitude];
      [self publishToSocket];
      [self getAddress:location and:NO];
      
   }
   
}


#pragma mark - Get Current Location Delegates -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
   //change map camera postion to current location
   GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                           longitude:longitude
                                                                zoom:mapZoomLevel];
   [mapView_ setCamera:camera];
   
   //save current location to plot direciton on map
   _currentLatitude = latitude;
   _currentLongitude =  longitude;

}

-(void)updatedAddress:(NSString *)currentAddress
{
   self.addressLabel.text = currentAddress;
   NSLog(@"Current Address In Home VC:%@",self.addressLabel.text);
}

//-(void)didFailedLocationUpdate
//{
//    [self gotoLocationServicesMessageViewController];
//}

#pragma mark - prepare Segue -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   [self addAllDetailsToAppointmentClass];
   switch ([segueArray indexOfObject:segue.identifier]) {
         
      case pickupVC:
      {
         PickUpViewController *pickController = [segue destinationViewController];
         pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
         pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
         pickController.pickUpAddressDelegate = self;

         [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                   subType:kCATransitionFromTop
                                                   forView:self.navigationController.view
                                              timeDuration:0.3];
      }
         
         break;
         
      default:
         break;
   }
   
}

#pragma mark - GetAddress From Pick UP VC Delegate Method -

-(void)getAddressFromPickUpAddressVC:(NSDictionary *)addressDetails
{
   _isAddressManuallyPicked = YES;
   
   NSString *addressText = flStrForStr(addressDetails[@"address"]);
//   if ([addressDetails[@"address2"] rangeOfString:addressText].location == NSNotFound) {
//      
//      addressText = [addressText stringByAppendingString:@","];
//      addressText = [addressText stringByAppendingString:flStrForStr(addressDetails[@"address2"])];
//      
//   } else {
//      
//      if([addressDetails[@"address2"]length])
//      {
//         addressText = flStrForStr(addressDetails[@"address2"]);
//      }
//      
//   }
   
   self.addressLabel.text = addressText;
   _currentLatitude = [addressDetails[@"lat"] floatValue];
   _currentLongitude = [addressDetails[@"lon"] floatValue];
   
   
   [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:addressDetails waitUntilDone:YES];
}


#pragma mark - UIButton Actions -


- (IBAction)lockOrUnlockButtonAction:(id)sender {
   
   if (![sender isSelected]) {
      
      _isAddressManuallyPicked = YES;
      [sender setSelected:YES];
      
   }
   else {
      
      _isAddressManuallyPicked = NO;
      [sender setSelected:NO];
      
   }
   
}

- (IBAction)searchAddressButtonAction:(id)sender
{
   [self disableAllButtonInteraction];
   [self performSegueWithIdentifier:@"toPickupAddressVC" sender:self];
   
}


- (IBAction)currentLocationButtonAction:(id)sender {
   
   CLLocation *location = mapView_.myLocation;
   _currentLatitude = location.coordinate.latitude;
   _currentLongitude = location.coordinate.longitude;
   
   if (location) {
      
      GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
      [mapView_ animateWithCameraUpdate:zoomCamera];
   }
   
}


- (IBAction)bookNowButtonAction:(id)sender {
   
   if(self.arrayOfProvidersAround.count > 0){
      
      NSArray *array =  self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"];
      
      if(array.count == 0){
         
         [UIHelper showMessage:LS(@"No providers are currently online") withTitle:LS(@"Message")delegate:self];
         return;
      }
   }
   else{
      
      [UIHelper showMessage:LS(@"Provider Types are unavailable!") withTitle:LS(@"Message")delegate:self];
      return;
   }
   
   
   if(self.addressLabel.text.length == 0 && self.arrayOfProviderTypes.count == 0) {
      
      [UIHelper showMessage:LS(@"Please mention the Job Location!") withTitle:LS(@"Message")delegate:self];
   }
   
   
   [ud setValue:[NSString stringWithFormat:@"%ld",(long)selectedTypeButtonTag] forKey:@"selectedProviderTypeTag"];
   [ud synchronize];
   
   
   [self disableAllButtonInteraction];
   
   ProviderBookingViewController *bookingVC = (ProviderBookingViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"confirmBookingVC"];
   
   [self addAllDetailsToAppointmentClass];
   
   apLocaion.bookingType = iServeBookingTypeNowWithDispatch;
   bookingVC.providerTypeDetails = [[NSMutableDictionary alloc]initWithDictionary: self.arrayOfProviderTypes[selectedTypeButtonTag]];
   bookingVC.arrayOfProviderDetailsFromHomeVC = self.arrayOfProvidersAround[selectedTypeButtonTag][@"mas"];
   
   
   [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                             subType:kCATransitionFromTop
                                             forView:self.navigationController.view
                                        timeDuration:0.3];

   [self.navigationController pushViewController:bookingVC animated:NO];

}

- (IBAction)bookLaterButtonAction:(id)sender
{
   if(self.arrayOfProviderTypes.count == 0 || self.arrayOfProviderTypes == nil)
   {
      [UIHelper showMessage:LS(@"Provider Types are unavailable!") withTitle:LS(@"Message")delegate:self];
      return;
   }

   scheduleDate = [NSDate dateWithTimeIntervalSinceNow:3600];
   [self.datePicker setMinimumDate:scheduleDate];
   

   [self.datePicker setDate:scheduleDate];
   
   //DATE PICKER Showing Animation
   self.datePickerViewBottomConstraints.constant = -25;
   [UIView animateWithDuration:0.5f animations:^{
      
      [self.view layoutIfNeeded];
      
   }];

}


-(void)changeButtonState:(id)sender
{
   UIButton *mBtn = (UIButton *)sender;
   for (UIView *button in mBtn.superview.subviews)
   {
      if ([button isKindOfClass:[UIButton class]])
      {
         [(UIButton *)button setSelected:NO];
      }
   }
   mBtn.selected = YES;
}

- (IBAction)showMapViewButtonAction:(id)sender
{
   NSLog(@"show map providers");
   
   [self changeButtonState:sender];
}

- (IBAction)showListViewButtonAction:(id)sender {
   
   NSLog(@"show list providers");
   
   if(self.arrayOfProviderTypes.count == 0 || self.arrayOfProviderTypes == nil){
      
      [UIHelper showMessage:LS(@"Provider Types are unavailable!") withTitle:LS(@"Message")delegate:self];
      return;
      
   }
   
   [self disableAllButtonInteraction];
   [self changeButtonState:sender];
   
   ProvidersListViewController *providerListVC = (ProvidersListViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"providersListVC"];
   
   [self addAllDetailsToAppointmentClass];
   
   providerListVC.arrayOfProviderTypes = self.arrayOfProviderTypes;
   providerListVC.arrayOfProvidersArround = self.arrayOfProvidersAround;
   providerListVC.selectedTypeButtonTag = selectedTypeButtonTag;
   
   [AnimationsWrapperClass UIViewAnimationAnimationCurve:UIViewAnimationCurveEaseInOut
                                transition:UIViewAnimationTransitionFlipFromLeft
                                   forView:self.navigationController.view
                              timeDuration:0.5];
   
   [self.navigationController pushViewController:providerListVC animated:YES];
   
   [UIView commitAnimations];
   
}

- (IBAction)swipeGestureAction:(id)sender
{
   UISwipeGestureRecognizer *swipeGesture = (UISwipeGestureRecognizer *)sender;
   
   if(swipeGesture.direction == UISwipeGestureRecognizerDirectionUp)
   {
      if(isCategoryDescriptionViewShowing == NO)
      {
         [self showCategoryDescriptionView];
      }

   }
   else if (swipeGesture.direction == UISwipeGestureRecognizerDirectionDown)
   {
      if(isCategoryDescriptionViewShowing)
      {
         [self hideCategoryDescriptionView];
      }

   }
   
}

- (IBAction)leftArrowButtonAction:(id)sender
{
   if(self.scrollView.contentOffset.x-(screenSize.size.width/3) > 0)
   {
      [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x-(screenSize.size.width/3), 0) animated:YES];
   }
   else
   {
      [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
   }
}

- (IBAction)rightArrowButtonAction:(id)sender
{
   if((self.scrollView.contentOffset.x + (screenSize.size.width/3) + self.scrollView.frame.size.width) < self.scrollView.contentSize.width)
   {
      [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentOffset.x + (screenSize.size.width/3), 0) animated:YES];
   }
   else
   {
      [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width-self.scrollView.frame.size.width, 0) animated:YES];
   }
}

- (IBAction)datePickerCancelButtonAction:(id)sender {
   
   self.datePickerViewBottomConstraints.constant = -300;
   [UIView animateWithDuration:0.5f animations:^{
      
      [self.view layoutIfNeeded];
      
   }];
   
}


- (IBAction)datePickerDoneButtonAction:(id)sender {
   
   scheduleDate = self.datePicker.date;
   NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
   
   [dateFormat setDateFormat:@"d MMM YYYY hh:mm a"];
   
   [dateFormat setAMSymbol:@"AM"];
   [dateFormat setPMSymbol:@"PM"];
   
   [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
   
   [self datePickerCancelButtonAction:nil];
   [self showBookLaterScreen];
   
}

-(void)showBookLaterScreen
{
   if(self.arrayOfProviderTypes.count == 0 || self.arrayOfProviderTypes == nil)
   {
      [UIHelper showMessage:LS(@"Provider Types are unavailable!") withTitle:LS(@"Message")delegate:self];
      return;
   }
   
   [self disableAllButtonInteraction];
   ProvidersListViewController *providerListVC = (ProvidersListViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"providersListVC"];
   
   [self addAllDetailsToAppointmentClass];
   
   providerListVC.arrayOfProviderTypes = self.arrayOfProviderTypes;
   providerListVC.arrayOfProvidersArround = self.arrayOfProvidersAround;
   providerListVC.isFromBoookLater = YES;
   providerListVC.selectedTypeButtonTag = selectedTypeButtonTag;
   providerListVC.dateFromHomeVC = scheduleDate;
   
   [self.navigationController pushViewController:providerListVC animated:YES];
}




#pragma mark - UIScrollView Delegate -

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   CGFloat xOffset = scrollView.contentOffset.x;
   
   if(xOffset <= 0 && scrollView.contentSize.width > scrollView.frame.size.width)
   {
      self.leftArrowButton.hidden = YES;
      self.rightArrowButton.hidden = NO;
   }
   else if(xOffset > 0  )
   {
      if((xOffset+scrollView.frame.size.width) < scrollView.contentSize.width)
      {
         self.leftArrowButton.hidden = NO;
         self.rightArrowButton.hidden = NO;
      }
      else
      {
         self.leftArrowButton.hidden = NO;
         self.rightArrowButton.hidden = YES;
      }
   }
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//   
//}

@end
