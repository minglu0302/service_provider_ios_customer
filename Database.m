//
//  Database.m
//  Towtray
//
//  Created by -Tony Lu on 17/06/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "Database.h"
#import "SelectedAddress.h"
#import "ManageAddress.h"
#import <GooglePlaces/GooglePlaces.h>

static Database *database = nil;

@implementation Database

+(instancetype)sharedInstance
{
    if (!database) {
        
        database = [[self alloc] init];
    }
    return database;
}


#pragma mark - Card Methods -

-(void)makeDataBaseEntry:(NSDictionary *)dictionary
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    CardDetails *entity = [NSEntityDescription insertNewObjectForEntityForName:@"CardDetails" inManagedObjectContext:context];
    
    [entity setExpMonth:flStrForObj([dictionary objectForKey:@"exp_month"]]);
    [entity setExpYear:flStrForObj([dictionary objectForKey:@"exp_year"])];
    [entity setIdCard:flStrForObj([dictionary objectForKey:@"id"])];
    [entity setCardtype:flStrForObj([dictionary objectForKey:@"type"])];
    [entity setLast4:flStrForObj([dictionary objectForKey:@"last4"])];
        
}

+(NSArray *)getParticularCardDetail:(NSString *)cardId{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@",cardId];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *result =[context executeFetchRequest:fetch error:&fetchError];
    
    return result;

}

+ (NSArray *)getCardDetails;
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
   
    return result;
    
}

+ (BOOL)DeleteCard:(NSString*)Campaign_id
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}


/**
 *  On logout it will delete all cards
 */

+ (void)DeleteAllCard
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"CardDetails" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    //  NSPredicate *pred = [NSPredicate predicateWithFormat:@"idCard == %@", Campaign_id];
    //[fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
}

     
     
#pragma mark - Previously Selected Address -
 //For Previously Selected Address
 
 - (void)makeDataBaseEntryForAddress:(GMSAutocompletePrediction *)dictionary
{
   
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    SelectedAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"SelectedAddress" inManagedObjectContext:context];
    
    NSDictionary *dict = @{
                           @"placeId":dictionary.placeID,
                           @"add1":dictionary.attributedPrimaryText.string,
                           @"add2":dictionary.attributedSecondaryText.string,
                           @"fullAdd":dictionary.attributedFullText.string
                         };
    [entity setPreviouslySelectedAddress:dict];
    
}
 
 
 + (NSArray *)getAddressDetails
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SelectedAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    [fetchRequest setEntity:entity];
    
    NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
    return result;
}
 
 + (void)DeleteAllAddress
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SelectedAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    
    NSError *fetchError;
    NSError *error;
    
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }
    
    BOOL isSaved = [context save:&error];
    if (isSaved) {
        
    }
    
}
     
#pragma mark - Manage Address -
//For Manage Address
- (void)makeDataBaseEntryForManageAddress:(NSDictionary *)dictionary
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    ManageAddress *entity = [NSEntityDescription insertNewObjectForEntityForName:@"ManageAddress" inManagedObjectContext:context];

    [entity setAddressId:flStrForStr([dictionary objectForKey:@"aid"]]);
    [entity setAddressLine1:flStrForStr([dictionary objectForKey:@"address1"])];
    [entity setFlatNumber:flStrForStr([dictionary objectForKey:@"suite_num"])];
    [entity setTagAddress:flStrForStr([dictionary objectForKey:@"tag_address"])];
    [entity setLatitude:flStrForObj([dictionary objectForKey:@"lat"])];
    [entity setLongitude:flStrForObj([dictionary objectForKey:@"long"])];
}

+ (NSArray *)getManageAddressDetails
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     NSManagedObjectContext *context = [appDelegate managedObjectContext];
     
     NSEntityDescription *entity = [NSEntityDescription entityForName:@"ManageAddress" inManagedObjectContext:context];
     
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
     [fetchRequest setEntity:entity];
     
     NSArray *result = [context executeFetchRequest:fetchRequest error:nil];
     return result;
}
     
+ (NSDictionary *)getParticularManageAddressDetail:(NSString *)addressId
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSEntityDescription *entity=[NSEntityDescription entityForName:@"ManageAddress" inManagedObjectContext:context];
    
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"addressId == %@",addressId];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    
    ManageAddress *address = [context executeFetchRequest:fetch error:&fetchError][0];
    
   
    NSDictionary *addressDetails = @{
                                     @"aid":address.addressId,
                                     @"address1":address.addressLine1,
                                     @"tag_address":address.tagAddress,
                                     @"suite_num":address.flatNumber,
                                     @"lat":address.latitude,
                                     @"long":address.longitude
                                   };

    return  addressDetails;
}
     
+ (BOOL)DeleteParticularManageAddress:(NSString *)addressId
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];

    NSEntityDescription *entity=[NSEntityDescription entityForName:@"ManageAddress" inManagedObjectContext:context];

    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:entity];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"addressId == %@",addressId];
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        [context deleteObject:product];
    }

    if ([context save:&error]) {
        return YES;
    }
    else {
        return NO;
    }
    return NO;
}

+ (void)DeleteAllManageAddress
{
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
     NSManagedObjectContext *context = [appDelegate managedObjectContext];
     
     NSEntityDescription *entity = [NSEntityDescription entityForName:@"ManageAddress" inManagedObjectContext:context];
     
     NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
     [fetch setEntity:entity];
     
     NSError *fetchError;
     NSError *error;
     
     NSArray *fetchedProducts=[context executeFetchRequest:fetch error:&fetchError];
     
     for (NSManagedObject *product in fetchedProducts) {
         [context deleteObject:product];
     }
     
     BOOL isSaved = [context save:&error];
     if (isSaved) {
         
     }
 
}

@end
