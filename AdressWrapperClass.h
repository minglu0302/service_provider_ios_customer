//
//  AdressWrapperClass.h
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
@import EventKit;

@interface AdressWrapperClass : NSObject <WebServiceHandlerDelegate>

@property (strong ,nonatomic)NSMutableArray *arrayOfAddresses;

+(instancetype) sharedInstance;

-(void)updateAddressInDatabase:(NSMutableArray *)addressInfo;
-(void)sendRequestToGetAddress;
-(void)addReminder:(NSDate *)startDate andEndDate:(NSDate *)endDate Title:(NSString *)title Message:(NSString *)message BookingId:(NSString *)bid;
-(void)removeReminder:(NSString *)eventId;

@end
