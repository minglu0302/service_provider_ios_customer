//
//  TWTSupportViewController.m
//  Towtray
//
//  Created by -Tony Lu on 24/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SupportViewController.h"
#import "SupportTableCell.h"
#import "SupportDetailsViewController.h"
#import "SupportWebViewController.h"

@interface SupportViewController ()


@end

@implementation SupportViewController

#pragma mark - UILife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.supportTableView.hidden=YES;
    self.listOfItemsArray=[[NSMutableArray alloc]init];
    self.detailsOfListArray=[[NSMutableArray alloc]init];
    [self sendRequestToGetSupportDetails];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - WebServiceCall -

-(void)sendRequestToGetSupportDetails
{
    NSDictionary *params = @{
                             @"support":@"support",
                             @"ent_lan":[NSString stringWithFormat:@"%td",[[NSUserDefaults standardUserDefaults]integerForKey:iServeUserSelectedLanguageNumber]]
                            };
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];

    [[WebServiceHandler sharedInstance] sendRequestToGetSupportDetails:params andDelegate:self];
}

#pragma mark - WebServiceDelegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    if (errFlag == 1)
    {
        [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
        return;
    }
    if(errFlag == 0 && requestType == RequestTypeGetSupportDetails && errNum == 33)
    {
        
        self.listOfItemsArray = [response[@"support"]mutableCopy];
        self.supportTableView.hidden = NO;
        [self.supportTableView reloadData];
        
    }
}

#pragma mark - TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.listOfItemsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SupportTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"supportTableViewCell"];
    cell.backgroundSupportCellView.layer.borderWidth = 1.0f;
    cell.backgroundSupportCellView.layer.borderColor = [UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1].CGColor;
    cell.cellLabel.text = flStrForStr(self.listOfItemsArray[indexPath.row][@"tag"]);
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.detailsOfListArray = self.listOfItemsArray[indexPath.row][@"childs"];
    self.titleString = [flStrForStr(self.listOfItemsArray[indexPath.row][@"tag"]) capitalizedString];
    if (self.detailsOfListArray.count == 0)
    {
        self.webUrlLink = [NSString stringWithFormat:@"%@%@",BASE_URL_SUPPORT,self.listOfItemsArray[indexPath.row][@"link"]];
        [self performSegueWithIdentifier:@"supportWebViewVC" sender:indexPath];
    }
    else
    {
        
        [self performSegueWithIdentifier:@"supportDetailsVC" sender:indexPath];
        
    }
}
#pragma mark - Prepare Segue -
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSIndexPath *path = (NSIndexPath *)sender;
    if ([[segue identifier] isEqualToString:@"supportDetailsVC"])
    {
        SupportDetailsViewController *CLVC = (SupportDetailsViewController*)[segue destinationViewController];
        CLVC.detailsArray = [self.detailsOfListArray mutableCopy];
        CLVC.senderString = self.titleString;
    }
    else if ([[segue identifier] isEqualToString:@"supportWebViewVC"])
    {
        SupportWebViewController *webView = (SupportWebViewController*)[segue destinationViewController];
        self.webUrlLink = [NSString stringWithFormat:@"%@",self.listOfItemsArray[path.row][@"link"]];
        webView.title = [flStrForStr(self.listOfItemsArray[path.row][@"tag"]) capitalizedString];//NSLocalizedString(@"Learn More", @"Learn More");
//        NSLog(@"webUrlLink: %@", self.webUrlLink);
        webView.weburlString = self.webUrlLink;
    }
    
}
@end
