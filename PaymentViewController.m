//
//  TWTPaymentVC.m
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PaymentViewController.h"
#import "PaymentCell.h"
#import "AddCardViewController.h"
#import "PaymentDetailsViewController.h"


@interface PaymentViewController ()
{
    NSMutableArray *arrDBResult;
    NSMutableArray *arrayContainingCardInfo;
    NSInteger selectedIndexPath;
}

@end

@implementation PaymentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.isFromProviderBookingVC)
    {
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
    }
    else
    {
        [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
    }

    arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    [self.paymentTableView reloadData];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITABLEVIEW DELEGATE -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDBResult.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"paymentCell";
    PaymentCell *cell = (PaymentCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    ;
    
    if(cell == nil)
    {
        cell =[[PaymentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //Add Card Row
    if(indexPath.row == arrDBResult.count)
    {
         cell.cardDetailView.hidden = YES;
         cell.addCardView.hidden = NO;
        
        [cell.contentView bringSubviewToFront:cell.addCardView];
        
    }
    //Card Details Row
    else
    {
        cell.addCardView.hidden = YES;
        cell.cardDetailView.hidden = NO;
        
        [cell.contentView bringSubviewToFront:cell.cardDetailView];
        
        CardDetails *fav = arrDBResult[indexPath.row];
        NSString *str = @"**** **** **** ";
        str = [str stringByAppendingString:fav.last4];
        cell.cardLast4Number.text = flStrForObj(str);
        cell.cardImage.image = [UIHelper requestForCardTypeImageName:fav.cardtype];
        
        
    }
    //set Top-Bottom-Left-Right Border
    if(indexPath.row == arrDBResult.count)
    {
        if (arrDBResult.count == 0)
        {
            cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top"];
        }
        else
        {
            cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_bottom"];
        }
    }
    else if(indexPath.row == 0)
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top"];
    }
    else
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_middle"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.isFromProviderBookingVC == NO)
    {
        if(arrDBResult.count == indexPath.row)
        {
            [self gotoAddCardVC];
        }
        else
        {
            CardDetails *fav = arrDBResult[indexPath.row];
            NSMutableDictionary *getDetailsfromDB = [[NSMutableDictionary alloc]init];
            [getDetailsfromDB setObject:fav.expMonth forKey:@"exp_month"];
            [getDetailsfromDB setObject:fav.expYear forKey:@"exp_year"];
            [getDetailsfromDB setObject:fav.cardtype forKey:@"type"];
            [getDetailsfromDB setObject:fav.last4 forKey:@"last4"];
            [getDetailsfromDB setObject:fav.idCard forKey:@"id"];
            [self cardDetailsClicked:getDetailsfromDB];
        }
    }
    else
    {
        if(arrDBResult.count == indexPath.row)
        {
            [self gotoAddCardVC];
            
        }
        else
        {
            CardDetails *event = [arrDBResult objectAtIndex:indexPath.row];
            [[NSUserDefaults standardUserDefaults] setObject:[event idCard] forKey:@"idOfSelectedCard"];
            selectedIndexPath = indexPath.row;
            
            if (self.callback) {
                
                self.callback(event.last4,event.idCard,event.cardtype,selectedIndexPath);
            }

            [self backButton:nil];
        }

    }
}


#pragma mark - UIButton Actions -

-(void)cardDetailsClicked:(NSDictionary *)getDict
{
    [self performSegueWithIdentifier:@"toPaymentDetailVC" sender:getDict];
}

- (IBAction)backButton:(id)sender
{
    if(self.isFromProviderBookingVC){
        
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                  subType:kCATransitionFromBottom
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController popViewControllerAnimated:NO];

    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)gotoAddCardVC
{
    AddCardViewController *vc = (AddCardViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"addCardVC"];
    vc.isComingFromPayment = 2;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:navBar animated:YES completion:nil];

}

//-(void)addInDataBase
//{
//    Database *db = [Database sharedInstance];
//    {
//        for (int i =0; i<arrayContainingCardInfo.count; i++)
//        {
//            [db makeDataBaseEntry:arrayContainingCardInfo[i]];
//        }
//    }
//    
//}

//-(void)cardIsAdded:(NSNotification *)notification
//{
//    arrDBResult = [[NSMutableArray alloc]init];
//    
//    [arrDBResult addObjectsFromArray:[Database getCardDetails]];
//    [self.paymentTableView reloadData];
//    
//}
//-(void)cardIsDeleted:(NSNotification *)notification
//{
//    arrDBResult = [[NSMutableArray alloc]init];
//    
//    [arrDBResult addObjectsFromArray:[Database getCardDetails]];
//    [self.paymentTableView reloadData];
//}


//#pragma mark - WebService call -

/**
 *  Service To get Card Details (R395)
 */
//-(void)sendServicegetCardDetail
//{
//    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
//    
//    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
//    if([reachability isNetworkAvailable])
//    {
//        NSDictionary *params = @{
//                                @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken],
//                                @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
//                                @"ent_date_time":[UIHelper getCurrentDateTime],
//                                };
//        
//        [[WebServiceHandler sharedInstance] sendRequestToGetCardDetails:params andDelegate:self];
//    }
//    else{
//        
//        [[ProgressIndicator sharedInstance]hideProgressIndicator];
//        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
//    }
//    
//}

//#pragma mark - WebService Response Delegate -
//-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
//{
//    [[ProgressIndicator sharedInstance]hideProgressIndicator];
//    
//    if (error)
//    {
//        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
//        return;
//    }
//    NSInteger errFlag = [response[@"errFlag"] integerValue];
//    NSInteger errNum = [response[@"errNum"] integerValue];
//    
//    switch (errFlag) {
//        case 1:
//        {
//            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
//            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
//                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
//            }
//            
//            else if (requestType == RequestTypeGetCardDetails )
//            {
//                if(errNum != 51 && errNum != 52)
//                {
//                    [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
//                }
//                
//            }
//            else
//            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
//            }
//
//        }
//        break;
//            
//        case 0:
//        {
//            arrayContainingCardInfo = [[NSMutableArray alloc]initWithArray:response[@"cards"]];
//            if (arrDBResult.count != arrayContainingCardInfo.count)
//            {
//                [[CardAddOrDeleteClass sharedInstance]updateCardsInDatabase:arrayContainingCardInfo];
//                arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
//                [self.paymentTableView reloadData];
//            }
//            break;
//            
//        }
//        default:
//            
//            break;
//    }
//}

#pragma mark - Prepare Segue -
   
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"toPaymentDetailVC"])
    {
        PaymentDetailsViewController *PDVC = [segue destinationViewController];
        PDVC.containingDetailsOfCard = sender;
    }
    else
    {
        AddCardViewController *addCardVc = [segue destinationViewController];
        addCardVc.isComingFromPayment = 1;
    }
    
}

@end
