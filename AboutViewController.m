//
//  TWTAboutVC.m
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutWebViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

#pragma mark - UILife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.aboutTitleArray = [[NSMutableArray alloc]init];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSString *str = [NSString stringWithFormat:@"%@ %@",LS(@"version"),version];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];
    
    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_SemiBold size:8] range:NSMakeRange(8,version.length)];
    
    [attributedString addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(8,version.length)];
    
    [self.versionLabel setAttributedText:attributedString];

}
-(void)viewWillDisappear:(BOOL)animated{

    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}

#pragma mark - IBAction

- (IBAction)rateButton:(id)sender
{
//    NSString *itunesURL = @"https://itunes.apple.com/us/app/iserve-for-on-demand-services/id1150492942?ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (IBAction)likeButton:(id)sender
{
    [self.aboutTitleArray removeAllObjects];
    [self.aboutTitleArray addObject:NSLocalizedString(@"Like",@"Like")];
    [self.aboutTitleArray addObject:facebookPageLink];
    [self performSegueWithIdentifier:@"webviewcontroller" sender:_aboutTitleArray];
}

- (IBAction)legalButton:(id)sender
{
    [self.aboutTitleArray removeAllObjects];
    [self.aboutTitleArray addObject:LS(@"Legal")];
    [self.aboutTitleArray addObject:[NSString stringWithFormat:legalURL]];
    [self performSegueWithIdentifier:@"webviewcontroller" sender:self];
}

- (IBAction)webButton:(id)sender
{
    NSString *websiteURL = webSiteURL;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteURL]];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"webviewcontroller"])
    {
        AboutWebViewController *controller = [segue destinationViewController];
        controller.recvArray = self.aboutTitleArray;
    }
}
@end
