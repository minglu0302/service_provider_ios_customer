//
//  TWTAmSlideTableViewCell.h
//  Towtray
//
//  Created by -Tony Lu on 19/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmSlideTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *eachRowTitleButton;
@property (weak, nonatomic) IBOutlet UILabel *eachRowTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *divider;
//@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;

@end
