//
//  ReminderWrapperClass.m
//  iServe_Customer
//
//  Created by Apple on 05/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ReminderWrapperClass.h"

static ReminderWrapperClass *reminderWrapperClass = nil;

@implementation ReminderWrapperClass

+(instancetype) sharedInstance
{
    if (!reminderWrapperClass) {
        
        reminderWrapperClass = [[self alloc] init];
    }
    return reminderWrapperClass;
    
}

-(void)addReminder:(NSDate *)startDate andEndDate:(NSDate *)endDate Title:(NSString *)title Message:(NSString *)message BookingId:(NSString *)bid
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [EKEvent eventWithEventStore:store];
         //        event.timeZone =  [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.title = title;
         event.notes = message;
         
         
         NSArray *alarms = @[[EKAlarm alarmWithRelativeOffset: - 60.0f *60* 1.0f]];
         event.alarms = alarms;
         
         event.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.startDate = startDate;
         
         event.endDate = endDate;  //set 1 hour meeting
         event.calendar = [store defaultCalendarForNewEvents];
         NSError *err = nil;
         [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
         NSString *eventID = [NSString stringWithFormat:@"%@", event.eventIdentifier];
         NSLog(@"Event Id:%@",eventID);
         //         [self sendEventIdToServer:bid andEventID:eventID];
         
     }];
    
}

-(void)removeReminder:(NSString *)eventId
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [store eventWithIdentifier:eventId];
         
         if(event)
         {
             NSError *err = nil;
             [store removeEvent:event span:EKSpanThisEvent commit:YES error:&err];
         }
     }];
    
}

#pragma mark - Web Service Call -

-(void)sendEventIdToServer:(NSString *)bid andEventID:(NSString *)eventId
{
    if([[PMDReachabilityWrapper sharedInstance] isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":bid,
                                 @"ent_event_id":eventId,
                                 @"ent_user_type":@"2"
                                 };
        
        [[WebServiceHandler sharedInstance]sendEventToServer:params andDelegate:self];
        
    }
}


#pragma mark - Web service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            
        }
            break;
            
        case 0:
        {
            if (requestType == RequestTypeSendEventId)
            {
                
            }
            
        }
            break;
            
        default:
            break;
    }
    
}

@end
