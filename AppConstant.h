//
//  AppConstant.h
//  Twotray
//
//  Created by -Tony Lu on 12/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#ifndef AppConstant_h
#define AppConstant_h
#define APP_NAME LS(@"Goclean Service")
#define LS(str) NSLocalizedString(str, nil)

typedef enum {
    
    iServeProviderAcceptedBooking = 2,
    iServeProviderOnMyWay = 5,
    iServeProviderArrived = 21,
    iServeJobStarted = 6,
    iServeTimerStarted = 15,
    iServeTimerStopped = 16,
    iServeBookingComplete = 7,
    iServeProviderCancelledBooking = 10,
    iServeCustomerCancelledBooking = 9,
    
}iServeBookingNotificationType;

typedef enum CancelBookingReasons :NSUInteger{
    
    iServeBookedByMistake = 1,
    iServeChangedMyMind,
    iServeProviderDidNotShowUp,
    iServeMyReasonIsNotListed,
    iServeNoReason,
    
}CancelBookingReasons;

typedef enum {
    
    iServeBookingTypeNowWithDispatch = 1,
    iServeBookingTypeNowWithOutDispatch = 2,
    iServeBookingTypeLater = 3,
    
}iServeBookingType;

typedef enum {
    
    feeTypeFixed = 0,
    feeTypeHourly = 1,
    feeTypeMilleage = 2,
    
}FeeType;

static NSString *const iServeGoogleMapKey                       = @"AIzaSyBCGghZXXRx0aDc8zJJdd8Ego6-wnIPQdQ";
static NSString *const iServeGoogleServerMapsAPIKey             = @"AIzaSyBty4t6xUH-VXt0uuwfiRo6mgG42rhTOSk";

static NSString *const webSiteURL                               = @"http://www.goclean-service.com";
static NSString *const imgLinkForSharing                        = @"http://www.goclean-service.com/pics/share.png";

static NSString *const iServeDefaultDateFormat                  = @"yyyy-MM-dd HH:mm:ss";
static NSString *const iServeStripeTestKey                      = @"pk_live_GaJtmpWcB2oPYVo4vj1IP2re";

static NSString *const iServeLocationServicesChangedNameKey     = @"CLChanged";
static NSString *const iServeNetworkErrormessage                = @"No network connection";
static NSString *const iServeSocketDisconnectedAndConnected     = @"socketDisconnectedAndConnected";

static NSString *const iServeCheckUserSessionToken              = @"sessionToken";
static NSString *const iServeDeviceId                           = @"deviceId";
static NSString *const iServePubNubChannelkey                   = @"userChannel";
static NSString *const iServeAPIKey                             = @"apiKey";

static NSString *const iServePublishkey                         = @"publishKey";
static NSString *const iServeSubscribekey                       = @"subscribeKey";
static NSString *const iServeServerchnkey                       = @"serverChannel";
static NSString *const iServePushTokenKey                       = @"pushToken";

static NSString *const iServeSignupProfileImage                 = @"signupProfileImage";
static NSString *const iServeSegueTag                           = @"segueTag";
static NSString *const iServeSignupProfileImageTag              = @"signupProfileImageTag";

static NSString *const iServeProvidersTypesArrayKey             = @"providerTypesArray";
static NSString *const iServeCustomerID                         = @"customerId";

static NSString *const iServeUserProfilepic                     = @"profilePic";
static NSString *const iServeUserFirstName                      = @"userFirstName";
static NSString *const iServeUserLastName                       = @"userLastName";
static NSString *const iServeUserEmail                          = @"userEmail";
static NSString *const iServeUserMobile                         = @"userMobile";
static NSString *const iServeUserPassword                       = @"userPassword";
static NSString *const iServeUserCountryCode                    = @"userCountryCode";
static NSString *const iServeReferralCode                       = @"referralCode";
static NSString *const iServeUserLanguages                      = @"userLanguages";
static NSString *const iServeUserSelectedLanguageNumber         = @"selectedLanguageNumber";
static NSString *const iServeUserFBPassword                     = @"userFBPassword";


static NSString *const iServeCouponkey                          = @"coupon";

static NSString *const iServeUserCurrentLat                     = @"userLatitude";
static NSString *const iServeUserCurrentLong                    = @"userLongitude";
static NSString *const iServeUserCurrentCity                    = @"userCity";
static NSString *const iServeUserCurrentCountry                 = @"userCountry";
static NSString *const iServeUserCurrentArea                    = @"userArea";
static NSString *const iServeUserCurrentAddress                 = @"userCurrentAddress";
static NSString *const iServeUserCurrentZipCode                 = @"userZipCode";

static NSString *const iServeCurrentCountryCurrencySymbol       = @"currencySymbol";
static NSString *const iServeStripeKey                          = @"stripeKey";

static NSString *const iServeNewCardAddedNameKey                = @"cardAdded";
static NSString *const iServeCardDeletedNameKey                 = @"cardDeleted";

static NSString *const iServeProfileImageChanged                = @"userPicChange";
static NSString *const iServeUserNameChanged                    = @"userNameChange";

static BOOL const iServeBookLater                               = YES;

static double const  iServeDistanceMetric                       = 1608.34;
static NSString *const iServeDistanceParameter                  = @"Miles";
static NSString *const iServeMPHorKMPH                          = @"MPH";

static NSString *const iServeAllOngoingBookings                 = @"bookings";

static NSString *const iServeAllMessages                        = @"messages";
static NSString *const iServeAllSelectedServices                = @"selectedServices";
static NSString *const iServeAllRemoveMessages                  = @"removeMessages";
static NSString *const iServeFCMTokenKey                        = @"fcmToken";


static NSString *const facebookPageLink                         = @"https://www.facebook.com/Go-Clean-1799689986953665/";
static NSString *const iTunesLink                               = @"https://itunes.apple.com/us/app/goclean-service/id1331343919?ls=1&mt=8";
static NSString *const legalURL                                 = BASE_IP @"PrivacyPolicy.html";


#endif /* AppConstant_h */
