//
//  UploadImagesToAmazonServer.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadImagesToAmazonServer : NSObject

+(instancetype) sharedInstance;

-(void)uploadProfileImageToServer:(UIImage *)profileImage
                         andEmail:(NSString *)userEmail
                          andType:(NSInteger)type
                   withCompletion:(void (^)(NSString *))complete;

-(void)uploadJobImagesToServer:(NSArray *)jobImagesarray andBid:(NSString *)bid;

-(void)downloadImageFromAmazon;

-(void)deleteImageFromAmazon:(NSString *)imageURL;


@end
