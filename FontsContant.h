//
//  FontsContant.h
//  MODA
//
//  Created by -Tony Lu on 3/23/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#ifndef FontsContant_h

#define FontsContant_h

#define     OpenSans_Regular                @"OpenSans"
#define     OpenSans_SemiBold               @"OpenSans-Semibold"
#define     OpenSans_Light                  @"OpenSans-Light"

#define     Roboto_Light                    @"Roboto-Light"
#define     Roboto_Bold                     @"Roboto-Bold"
#define     Roboto_Regular                  @"Roboto-Regular"

#define     Palatino_Italic                 @"Palatino-Regular"


#endif /* FontsContant_h */
