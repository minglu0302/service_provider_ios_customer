//
//  TWTPaymentVC.h
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ChooseCardCallback)(NSString *cardNo,NSString *cardID,NSString *type,NSInteger arrIndex);

@interface PaymentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;
@property (nonatomic,copy)  ChooseCardCallback callback;
@property (assign,nonatomic) BOOL isFromProviderBookingVC;
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;


- (IBAction)backButton:(id)sender;

@end
