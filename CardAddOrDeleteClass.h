//
//  CardAddOrDeleteClass.h
//  iServe_AutoLayout
//
//  Created by Apple on 24/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardAddOrDeleteClass : NSObject

@property (strong ,nonatomic)NSMutableArray *arrayOfCards;

+(instancetype) sharedInstance;

-(void)updateCardsInDatabase:(NSMutableArray *)cardsInfo;

@end
