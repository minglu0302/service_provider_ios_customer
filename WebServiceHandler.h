//
//  WebServiceHandler.h
//  MODA
//
//  Created by -Tony Lu on 2/19/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>


/*
 @protocol WebServiceHandlerDelegate
 @abstract protocol to be iplemented by the calling class to get the response
 */
@protocol WebServiceHandlerDelegate <NSObject>

/*
 @method didFinishLoadingRequest
 @abstract Method called when web service request is complete
 @param requestType - Request Type for this request
 @param response - id Response of this request. Can be nil in case of an error.
 @param error - NSError - error object in case of any error. Nil in case of success.
 */
@required
- (void) didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError*)error;

@end

enum RequestType
{
    eLogin=1,
    eSignUp,
    eUploadImage,
    eLatLongparser
};



@interface WebServiceHandler : NSObject
{
    NSMutableData *responseData;
    NSURLConnection *urlConnection;
    id target;
    SEL selector;
    NSTimer *timeOut;
}

/**
 MODA Request Type Constants
 */

/// MODA Type application/json
extern NSString * const MODARequesttypeJSON;

/// MODA Type application/x-www-form-urlencoded
extern NSString * const MODARequestTypeFormURLEncoded;

/// MODA Type application/xml
extern NSString * const MODARequestTypeXML;

/// MODA Type text/xml
extern NSString * const MODARequestTypeTextXML;

/// MODA Type text/xml
extern NSString * const MODARequestTypeTextHTML;


/*
 @method getPhotosWithDelegate
 @abstract Method to get photos from the service
 @param delegate - calling class object to recieve the response. Response is delivered WebServiceHandlerDelegate protocol method didFinishLoadingRequest
 @result void
 */
+(id)sharedInstance;

- (void)placeWebserviceRequestWithString:(NSMutableURLRequest *)string Target:(id)_target Selector:(SEL)_selector;

//App Version

- (void)sendRequestToUpdateAppVersion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//SignUp
- (void)sendRequestToverifyEmail:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToverifyMobileNumber:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToverifyReferralCode:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

//OTP
- (void)sendRequestToGetOTP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToVerifyOTP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToSignUP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//SignIn
- (void)sendRequestToSignIN:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToForgetPassword:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendOTPToVerifyPhoneNumber:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToChangePassword:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

//Profile
- (void)sendRequestToGetProfileData:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
- (void)sendRequestToUpdateProfileData:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//signOut
- (void)sendRequestToSignOut:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Check Promocode
- (void)sendRequestToCheckCoupon:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Live Booking
- (void)sendRequestToLiveBooking:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToCancelBooking:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetServiceGroupsDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetCancelReasons:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Booking History
- (void)sendRequestToGetAllOngoingAppointmentDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetAllAppointments:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetParticularApptStatus:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToUpdateReview:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetProviderDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToUpdateDispute:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//ChatVC
- (void)sendRequestToGetChatMessages:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Payment (card)
- (void)sendRequestToAddCard:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToGetCardDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToDeleteCard:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//My Address
- (void)sendRequestToAddAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

- (void)sendRequestToDeleteAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;
- (void)sendRequestToGetAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Support
- (void)sendRequestToGetSupportDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;


//Store Event
- (void)sendEventToServer:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate;

@end
