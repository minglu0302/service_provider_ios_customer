//
//  PickAddressFromMapViewController.h
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickAddressFromMapViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *topAddressView;
@property (weak, nonatomic) IBOutlet UILabel *selectedAddressLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchAddressButton;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property BOOL isFromProviderBookingVC;

- (IBAction)confirmLocationButtonAction:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)searchAddressButtonAction:(id)sender;
- (IBAction)currentLocationButtonAction:(id)sender;

@end
