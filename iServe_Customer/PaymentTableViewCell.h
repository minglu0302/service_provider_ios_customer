//
//  PaymentTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/20/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardOrCashLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;

-(void)setPaymentDetails:(NSDictionary *)bookingDetails;

@end
