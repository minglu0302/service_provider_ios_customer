//
//  AppointmentLocation.m
//  privMD
//
//  Created by Surender Rathore on 11/04/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import "AppointmentLocation.h"

@implementation AppointmentLocation


static AppointmentLocation *appointmentLocaiton;

+ (id)sharedInstance {
	if (!appointmentLocaiton) {
		appointmentLocaiton  = [[self alloc] init];
	}
	
	return appointmentLocaiton;
}

@end
