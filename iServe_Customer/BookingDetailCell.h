//
//  BookingDetailCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bookingDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *divider;
@property (weak, nonatomic) IBOutlet UIView *bookingDetailView;
@property (weak, nonatomic) IBOutlet UIImageView *providerImage;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookingAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *providerTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *amountLabelHeightConstraint;

-(void)showEachBookingRow:(NSDictionary *)bookingDetail;

@end
