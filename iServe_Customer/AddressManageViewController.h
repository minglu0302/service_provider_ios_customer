//
//  AddressManageViewController.h
//  Uberx
//
//  Created by -Tony Lu on 19/09/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>



/**
 *  Getting Address From DataBase Delegate Method
 */
@protocol AddressManageDelegate <NSObject>

@optional

-(void)getAddressFromDatabase:(NSDictionary *)addressDetails;

@end


@interface AddressManageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,WebServiceHandlerDelegate>


/**
 *  Address Manage TableView Used to List Addresses From Database
 */
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
@property (weak, nonatomic) IBOutlet UITableView *manageAddressTableView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

/**
 *  Id of the above Getting address From Database Delegate Method
 */
@property (nonatomic,assign) id addressManageDelegate;

/**
 *  Dictionary Contain the Address From Database
 */
@property(strong,nonatomic) NSDictionary *getAddress;

/**
 *  Used to check from which controller to coming to this Controller
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationbackButton;
@property BOOL isFromProviderBookingVC;

/**
 *  Add New Address Button Action
 *
 *  @param sender  add new Button Identifications
 */
- (IBAction)addNewAddressButtonAction:(id)sender;
- (IBAction)navigationbackButtonAction:(id)sender;
- (IBAction)removeAddressButtonAction:(id)sender;



@end
