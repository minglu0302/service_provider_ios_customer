//
//  JobDetailsTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "JobDetailsTableViewCell.h"

@implementation JobDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
