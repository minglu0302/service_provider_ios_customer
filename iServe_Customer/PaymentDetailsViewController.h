//
//  PaymentDetailsViewController.h
//  privMD
//
//  Created by -Tony Lu on 05/03/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^OfferApplyCallback)();

@interface PaymentDetailsViewController : UIViewController<UIActionSheetDelegate,UITextFieldDelegate,UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UILabel *cardNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *expLabel;
@property (strong, nonatomic) IBOutlet UITextField *expTextField;
@property (strong, nonatomic) IBOutlet UITextField *cvvTextField;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@property (strong, nonatomic) NSDictionary *containingDetailsOfCard;

- (IBAction)deleteButtonClicked:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)editButtonAction:(id)sender;


@end
