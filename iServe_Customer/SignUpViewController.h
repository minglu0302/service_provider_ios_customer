//
//  SignUpViewController.h
//  iServe app
//  Developed by Raghavendra
//  Created by -Tony Lu on 06/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "FBLoginHandler.h"

@interface SignUpViewController : UIViewController<UITextFieldDelegate,WebServiceHandlerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIScrollViewDelegate,FBLoginHandlerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UIButton *countryImageButton;

//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;


/**
 *  Phone number text field Outlet
 */
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *referralCodeTextField;


/**
 *  Signup profile image view outlet
 */
@property (weak, nonatomic) IBOutlet UIImageView *signUpProfileImageView;
/**
 *  Accept  Button outlet's
 *
 */
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
/**
 *  Navigation left button outlet's it shifted navigationleftButtonto -16 left side on navigation bar
 *
 *
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
/**
 *  It's having all ui element's for register view controller
 *
 *
 */
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

/**
 *   Passwordtextfield outlet's
 */
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;

/**
 *  Emailtextfield outlet's
 */
@property (weak, nonatomic) IBOutlet UITextField *emailTextfield;
/**
 *    Lastname textfield outlet's
 */
@property (weak, nonatomic) IBOutlet UITextField *lastnameTextfield;
/**
 *   FirstnameTextfiled outlet's
 */
@property (weak, nonatomic) IBOutlet UITextField *firstnameTextfield;
/**
 *  Checkmark button are used for enabling the term's and condition's for sign up
 *
 *
 */
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkMarkButton;


@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
/*
 *   Signup Next Button Outlet
 *
 */
@property (weak, nonatomic) IBOutlet UIButton *signUpNextButton;

@property (weak, nonatomic) IBOutlet UIButton *signupWithFacebookButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *passwordTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *passwordBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordBackgroundViewHeightConstraint;

/**
 *  Image picker Variable
 */
@property (strong,nonatomic) UIImagePickerController *imgpicker;
/**
 *  Checking offset for scroll view content
 */
@property (nonatomic,assign) CGFloat minOffsetY;
/**
 *  Checking offset for scroll view content
 */
@property (nonatomic,assign) CGFloat maxOffsetY;
/**
 *   this variable assign offset value for scroll view
 */
@property (nonatomic,assign) CGPoint previousPoint;
/*
 *
 *   Keyboard height variable
 */
@property (nonatomic,assign)  NSInteger keyboardHeight;

/*
 *
 *     Active textfield for check which textfield is active
 */
@property (nonatomic,strong) UITextField *activeTextfield;
/*
 *
 *  Signup dictionary for storing the signup parameter's
 */
@property (nonatomic,strong) NSDictionary *signupParameters;

/**
 *  storing the data from facebook
 */
@property (nonatomic,strong) NSDictionary *fBData;
@property BOOL isFromSignIn;


- (IBAction)imageButtonAction:(id)sender;

/**
 *  Signup Next Button Action
 *
 *  @param sender
 */

- (IBAction)signUpNextButton:(id)sender;
/**
 *  Back Button Action
 *
 *  @param sender
 */

- (IBAction)backButtonClick:(id)sender;
/*
 *
 *  Check mark button action for attaching and detaching the uiimage on click action's
 *
 *  @param IBAction
 *
 *  @return
 */
- (IBAction)checkMarkButton:(id)sender;
/*
 *
 *  It will go terms and condition's view controller for on click terms and condition button
 *
 *  @param IBAction
 *
 *  @return
 */

- (IBAction)termsConditionButton:(id)sender;
- (IBAction)countryButton:(id)sender;
- (IBAction)signUpTapGestureAction:(id)sender;
- (IBAction)signupWithFacebookButtonAction:(id)sender;

@end
