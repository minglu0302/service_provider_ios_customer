//
//  MyAppTimerClass.h
//  UBER
//
//  Created by -Tony Lu on 30/08/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyAppTimerClass : NSObject

@property(nonatomic,weak) NSTimer *pubnubStreamTimer;

+ (id)sharedInstance;

-(void)startPublishTimer;

-(void)stopPublishTimer;


@end
