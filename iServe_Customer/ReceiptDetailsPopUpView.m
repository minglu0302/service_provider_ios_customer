//
//  RecieptDetailsPopUpView.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ReceiptDetailsPopUpView.h"
#import "InvoicePopUpView.h"
#import "ItemDetailsTableViewCell.h"
#import "PaymentTableViewCell.h"
#import "SignatureTableViewCell.h"
#import "RateAndCommentTableViewCell.h"
#import "NotesFromProviderTableViewCell.h"

static ReceiptDetailsPopUpView *receiptPopUp = Nil;

@implementation ReceiptDetailsPopUpView

#pragma mark - Initial Methods -

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self =  [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance {
    
    if (!receiptPopUp) {
        
        receiptPopUp  = [[self alloc] initWithFrame:CGRectZero];
        receiptPopUp.frame = [[UIScreen mainScreen]bounds];
        
    }
    receiptPopUp.subTotalValue = 0.0;
    return receiptPopUp;
}

#pragma mark - Custom Methods -

-(void)setTopViewFrame
{
    CGSize screenSize = [[UIScreen mainScreen]bounds].size;
    
    if (screenSize.height < 568)
    {
        self.topViewTopConstraint.constant = 50;
        self.topViewBottomConstraint.constant = 50;
    }
    else if(screenSize.height < 667)
    {
        self.topViewTopConstraint.constant = 80;
        self.topViewBottomConstraint.constant = 80;
    }
    else if(screenSize.height < 736)
    {
        self.topViewTopConstraint.constant = 100;
        self.topViewBottomConstraint.constant = 100;
    }
    else
    {
        self.topViewTopConstraint.constant = 120;
        self.topViewBottomConstraint.constant = 120;
    }
    
    [self layoutIfNeeded];
    
}


#pragma mark - UITableView Footer methods -

//Add Rating And Comment in Table Footer
-(void)setTableFooter
{
//    NSString *cellIdentifier = @"rateAndCommentCell";
//    RateAndCommentTableViewCell *rateAndCommentcell = (RateAndCommentTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
//    
//    if (rateAndCommentcell == nil)
//    {
//        rateAndCommentcell = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:nil options:nil] objectAtIndex:4];
//        rateAndCommentcell.frame = CGRectMake(0, 0, self.tableView.frame.size.width,50);
//        
//    }
//    
//    rateAndCommentcell.ratingView.markFont = [UIFont systemFontOfSize:30];
//    rateAndCommentcell.ratingView.highlightColor = UIColorFromRGB(0xffd200);
//    rateAndCommentcell.ratingView.baseColor = UIColorFromRGB(0xcccccc);
//    rateAndCommentcell.ratingView.userInteractionEnabled = NO;
//    
//    rateAndCommentcell.commentbackgroundView.layer.borderColor = UIColorFromRGB(0xebebeb).CGColor;
//    
//    rateAndCommentcell.ratingView.value = [self.bookingDetails[@"star_rating"]floatValue];
//    rateAndCommentcell.commentlabel.text = self.bookingDetails[@"rev"];
//    
//    CGFloat height = [self measureHeightLabel:  rateAndCommentcell.commentlabel];
//    CGRect frame =  rateAndCommentcell.frame;
//    
//    if(rateAndCommentcell.commentlabel.text.length == 0)
//    {
//         rateAndCommentcell.commentlabel.text = LS(@"Not Commented");
//         rateAndCommentcell.commentBackgroundViewHeightConstraint.constant = height*2;
//         frame.size.height = 70+(height*2);
//    }
//    else
//    {
//        rateAndCommentcell.commentBackgroundViewHeightConstraint.constant = height+5;
//        frame.size.height = height+70;
//    }
//
//    rateAndCommentcell.frame = frame;
    
    
        NSString *cellIdentifier = @"notesFromProviderCell";
        NotesFromProviderTableViewCell *notesFromProviderCell = (NotesFromProviderTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        if (notesFromProviderCell == nil)
        {
            notesFromProviderCell = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:nil options:nil] objectAtIndex:4];
            notesFromProviderCell.frame = CGRectMake(0, 0, self.tableView.frame.size.width,90);
            
        }
    
    
        notesFromProviderCell.notesFromProviderBackgroundView.layer.borderColor = UIColorFromRGB(0xebebeb).CGColor;
    
        notesFromProviderCell.noteFromProviderLabel.text = self.bookingDetails[@"fdata"][@"pro_note"];
    
        CGFloat height = [self measureHeightLabel:notesFromProviderCell.noteFromProviderLabel];
        CGRect frame =  notesFromProviderCell.frame;
    
        if(notesFromProviderCell.noteFromProviderLabel.text.length == 0)
        {
             notesFromProviderCell.noteFromProviderLabel.text = LS(@"No Notes From Provider");
             notesFromProviderCell.notesFromProviderBackgroundViewHeightConstraint.constant = height*2;
             frame.size.height = 35+(height*2);
        }
        else
        {
            notesFromProviderCell.notesFromProviderBackgroundViewHeightConstraint.constant = height+5;
            frame.size.height = height+35;
        }
    
        notesFromProviderCell.frame = frame;
    
        [self.tableView setTableFooterView:notesFromProviderCell];
}

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

-(void)setVisitFeeDetails:(ItemDetailsTableViewCell *)itemsCell
{
    NSArray *feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
    NSString *currencySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];
    
    CGFloat amountValue;
    
    switch ([feeTypesArray indexOfObject:self.bookingDetails[@"fdata"][@"fee_type"]])
    {
            //Feixed Based
        case feeTypeFixed:
        {
            itemsCell.itemLabel.text = LS(@"Fixed Fee");
            amountValue = [self.bookingDetails[@"fixed_price"]floatValue];
            itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,amountValue];
            
        }
            break;
            
            //Hourly Based And Milleage Based
        case feeTypeHourly:
        {
            itemsCell.itemLabel.text = LS(@"Visit Fee");
            amountValue = [self.bookingDetails[@"visit_fees"]floatValue];
            itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,amountValue];
            
        }
            
            break;
            
        default:
        {
            itemsCell.itemLabel.text = LS(@"Minimum Fee");
            amountValue = [self.bookingDetails[@"min_fees"]floatValue];
            itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,amountValue];
            
        }
            break;
    }
    self.subTotalValue = amountValue;
}


#pragma mark - UIButton Actions -

- (IBAction)closeButtonAction:(id)sender {
    
    [[InvoicePopUpView sharedInstance] removeRecieptPopUp];
    receiptPopUp = nil;
}

#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 7;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return self.arrayOfServiceDetails.count;
            break;
        case 2:
            return 2;
            break;
        case 3:
            return 3;
            break;
        default:
            return 1;
            break;
    }
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1 && self.arrayOfServiceDetails.count > 0)
    {
        return 30;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 6:
            return 50;
            break;
        default:
            return 40;
            break;
    }

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width,1)];
    
    if(section == 1 && self.arrayOfServiceDetails.count > 0)
    {
            headerView.backgroundColor = UIColorFromRGB(0xebebeb);
            headerView.frame = CGRectMake(0, 0,tableView.frame.size.width, 30);
            
            UILabel *sectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(8, 0, headerView.frame.size.width, headerView.frame.size.height)];
            sectionTitle.font = [UIFont fontWithName:OpenSans_SemiBold size:11];
            sectionTitle.textColor = UIColorFromRGB(0x444444);
            sectionTitle.text = @"SERVICE DETAILS";
            
            [headerView addSubview:sectionTitle];
    }
    else
    {
        headerView.backgroundColor = UIColorFromRGB(0xffffff);
            
    }
    
    return headerView;

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    
    switch (section) {
        case 0:
        case 1:
        {
            if(self.arrayOfServiceDetails.count > 0)
            {
                footerView.backgroundColor = UIColorFromRGB(0xebebeb);
            }
            else
            {
                footerView.backgroundColor = UIColorFromRGB(0xffffff);
            }
        }
            break;
        case 4:
            footerView.backgroundColor = UIColorFromRGB(0xffffff);
            break;
        default:
            footerView.backgroundColor = UIColorFromRGB(0xebebeb);
            break;
    }
    return footerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section) {
        case 5:
        {
            cellIdentifier = @"paymentCell";
            PaymentTableViewCell *paymentCell = (PaymentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (paymentCell == nil)
            {
                paymentCell = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:nil options:nil] objectAtIndex:2];
                paymentCell.frame = CGRectMake(0, 0, tableView.frame.size.width,40);
                
            }
            [paymentCell setPaymentDetails:self.bookingDetails];
            
            return paymentCell;

        }
            break;
            
        case 6:
        {
            cellIdentifier = @"signatureCell";
            SignatureTableViewCell *cell = (SignatureTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil)
            {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:nil options:nil] objectAtIndex:3];
                cell.frame = CGRectMake(0, 0, tableView.frame.size.width,50);
            }
            
            [cell.activityIndicator startAnimating];
            
            [cell.signatureImageView sd_setImageWithURL:[NSURL URLWithString:self.bookingDetails[@"fdata"][@"sign_url"]]
                                      placeholderImage:nil
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                 
                                                 [cell.activityIndicator stopAnimating];
                                                 
                                             }];

            
            return cell;

        }
            break;
        default:
        {
            cellIdentifier = @"itemsCell";
            ItemDetailsTableViewCell *itemsCell = (ItemDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (itemsCell == nil)
            {
                itemsCell = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptDetailsPopUpView" owner:nil options:nil] objectAtIndex:1];
                itemsCell.frame = CGRectMake(0, 0, tableView.frame.size.width,40);
                
            }
            
            NSString *currencySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];
            
            if(indexPath.section == 0)
            {
                if(indexPath.row == 0)
                {
                    [self setVisitFeeDetails:itemsCell];
                }
                else if (indexPath.row == 1)
                {
                    itemsCell.itemLabel.text = LS(@"Time Fee");
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"time_fees"]floatValue]];
                    self.subTotalValue = self.subTotalValue + [self.bookingDetails[@"fdata"][@"time_fees"]floatValue];
                }
            }
            else if(indexPath.section == 1)
            {
                //Show Service Details
//                if(self.arrayOfServiceDetails.count > 0 && indexPath.row < self.arrayOfServiceDetails.count)
//                {
                    itemsCell.itemLabel.text = self.arrayOfServiceDetails[indexPath.row][@"sname"];
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.arrayOfServiceDetails[indexPath.row][@"sprice"]floatValue]];
                    
                    self.subTotalValue = self.subTotalValue + [self.arrayOfServiceDetails[indexPath.row][@"sprice"]floatValue];

//                }
            }
            else if(indexPath.section == 2)
            {  //Show Material Fee
                if (indexPath.row == 0)
                {
                    itemsCell.itemLabel.text = LS(@"Material Fee");
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"mat_fees"]floatValue]];
                    
                    self.subTotalValue = self.subTotalValue + [self.bookingDetails[@"fdata"][@"mat_fees"]floatValue];

                }//Show Misc Fee
                else if (indexPath.row == 1)
                {
                    itemsCell.itemLabel.text = LS(@"Misc Fee");
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"misc_fees"]floatValue]];
                    
                    self.subTotalValue = self.subTotalValue + [self.bookingDetails[@"fdata"][@"misc_fees"]floatValue];
                }
                
            }
            else if (indexPath.section == 3)
            {
                if (indexPath.row == 0)
                {
                    itemsCell.itemLabel.text = LS(@"Subtotal");
//                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,self.subTotalValue];
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"sub_total_cust"]floatValue]];
                    
                }
                else if (indexPath.row == 1)
                {
                    itemsCell.itemLabel.text = LS(@"Provider discount");
                    itemsCell.amountLabel.text = [NSString stringWithFormat:@"-%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"pro_disc"]floatValue]];
                }                else if (indexPath.row == 2)
                {
//                    if([self.bookingDetails[@"fdata"][@"coupon_per"] length] > 0)
//                    {
                        //2-Fixed Discound
                        //1-Percentage Discount
                        if([self.bookingDetails[@"fdata"][@"coupon_type"]integerValue] == 1 )
                        {
                            itemsCell.itemLabel.text = [NSString stringWithFormat:@"Discount Coupon(%@%%)",self.bookingDetails[@"fdata"][@"coupon_per"]];
                            
                            itemsCell.amountLabel.text = [NSString stringWithFormat:@"-%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"coupon_discount"]floatValue]];
                           
                        }
                        else
                        {
                            itemsCell.itemLabel.text = LS(@"Discount Coupon");
                            itemsCell.amountLabel.text = [NSString stringWithFormat:@"-%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"coupon_discount"]floatValue]];
                        }
//                    }
//                    else
//                    {
//                        itemsCell.itemLabel.text = LS(@"Discount Coupon");
//                        itemsCell.amountLabel.text = [NSString stringWithFormat:@"-%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"coupon_discount"]floatValue]];
//                    }
                }

            }
            else if (indexPath.section == 4)
            {
                itemsCell.itemLabel.text = LS(@"Total");
                itemsCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",currencySymbol,[self.bookingDetails[@"fdata"][@"total_pro"]floatValue]];
            }
            
            return itemsCell;

        }
            break;
    }
   
}

@end
