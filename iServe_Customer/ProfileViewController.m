//
//  ProfileViewController.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 6/29/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProfileViewController.h"
#import "LeftMenuVC.h"
#import "CountryNameTableViewController.h"
#import "UploadImagesToAmazonServer.h"
#import "AMSlideMenuLeftTableViewController.h"
#import "ChangePasswordViewController.h"
#import "AppDelegate.h"

@interface ProfileViewController ()<AMSlideMenuDelegate,UIKeyboardDelegates,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSString* hasRegistered;
    UIImagePickerController *imagePicker;
    UIImage *pickedImage;
    Logout *logout;
    NSMutableArray *previousInformation;
    NSArray *arrayOfLanguages;
    NSInteger selectedLanguageIndexValue;
    NSUserDefaults *ud;
    
    UIImage *defaultProfileImage;

}

@property (weak, nonatomic) IBOutlet UISwitch *notificationBtn;

- (void) setNotificationSwitch;
- (void) toggleNotifications: (UISwitch*) sender;


@end

@implementation ProfileViewController


#pragma mark - UILife Cycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ud = [NSUserDefaults standardUserDefaults];
    
    [self disableTextFieldInterActions];
   
    logout = [Logout sharedInstance];
    [self downloadProfileImage];
    [self sendServiceForGettingProfileData];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    self.countryCodeLabel.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    self.countryImageView.image = [UIImage imageNamed:imagePath];
    
    AMSlideMenuMainViewController *AMSLider = [AMSlideMenuMainViewController getInstanceForVC:self];
    AMSLider.slideMenuDelegate = self;
    
    previousInformation = [[NSMutableArray alloc]init];
    
    NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"register_profile_default_image"],0.8);

    defaultProfileImage = [UIImage imageWithData:data];
    
    [self setNotificationSwitch];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;

}

- (void)viewDidAppear:(BOOL)animated
{
    [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width,CGRectGetHeight(self.logoutBackGroundView.frame)+CGRectGetMinY(self.logoutBackGroundView.frame)+30);

}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = nil;

}

-(void)viewDidDisappear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}

-(void)downloadProfileImage{
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@",[ud objectForKey:iServeUserProfilepic]];
    
    if([[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:strImageUrl])
    {
        [[SDImageCache sharedImageCache] removeImageForKey:strImageUrl withCompletion:nil];
    }

    
    [self.activityIndicator startAnimating];
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                             placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                        
                                        [self.activityIndicator stopAnimating];
                                        
                                    }];

}


#pragma mark - ImagePickerDelegate -

-(void)deSelectProfileImage {
    
    self.profileImageView.image =  defaultProfileImage;
    pickedImage = [UIImage imageNamed:@"register_profile_default_image"];
    
}

-(void)cameraButtonClicked
{
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        [UIHelper showMessage:LS(@"Camera is not available") withTitle:LS(@"Message")delegate:self];
    }
}

-(void)libraryButtonClicked
{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate =self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    self.profileImageView.image = pickedImage;
    pickedImage = [self imageWithImage:pickedImage scaledToSize:CGSizeMake(90,90)];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.5);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



#pragma mark - AlertViewDelegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (alertView.tag) {
            
        case 100:
            //Update Profile
            if (buttonIndex==[alertView cancelButtonIndex])
            {
                [self uploadImageToAmazon];
                [self editButtonAction:nil];
            }
        
            [self.view endEditing:YES];
            break;
            
        case 200:
            //LogOut
            if (buttonIndex == [alertView cancelButtonIndex])
            {
                [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Logging out...", @"Logging out...")];
                [logout sendRequestToLogOut];
                [self.view endEditing:YES];
            }
            break;
            
        default:
            break;
    }
    
}


#pragma mark - UIButton Actions -


- (IBAction)profileImageButtonAction:(id)sender {
    
    [self.view endEditing:YES];
        
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:LS(@"Select Picture") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Take Photo") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take Photo button tapped.
      
        [self cameraButtonClicked];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Choose From Library") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose From Library button tapped.
        
        [self libraryButtonClicked];
    }]];
    
    if (![UIImagePNGRepresentation(self.profileImageView.image) isEqualToData:UIImagePNGRepresentation(defaultProfileImage)] && ![self.profileImageView.image isEqual:[UIImage imageNamed:@"register_profile_default_image"]])
    {
        [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Remove Profile Photo") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Choose From Library button tapped.
            
            [self deSelectProfileImage];
        }]];
        
    }

    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];


}


- (IBAction)logoutButtonAction:(id)sender {
    
//    if([self.logoutButton.titleLabel.text isEqualToString:@"LOGOUT"])
//    {
        //Logout Process
        UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:LS(@"Message")
                                                                                  message:LS(@"Do you wish to logout?")
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addAction:[UIAlertAction actionWithTitle:LS(@"NO")
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {
                                                              
        }]];
        
        
        [alertController addAction:[UIAlertAction actionWithTitle:LS(@"YES")
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction *action) {
            //YES Button Action
            [self.view endEditing:YES];
            [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Logging out...", @"Logging out...")];
            [logout sendRequestToLogOut];
                                                              
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
//    }
//    else
//    {
        //Change Password Process
//        ChangePasswordViewController *changePasswordVC = (ChangePasswordViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"changePasswordVC"];
//    
//        
//        changePasswordVC.phoneNumber = self.phoneNumberTextField.text;
//        changePasswordVC.isFromProfileVC = YES;
//        
//        
//        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
//                                                  subType:kCATransitionFromTop
//                                                  forView:self.navigationController.view
//                                             timeDuration:0.3];
//        
//        [self.navigationController pushViewController:changePasswordVC animated:NO];

        
//    }


}

- (IBAction)editButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    if(self.editButton.isSelected)
    {
        self.editButton.selected = NO;
        [self.editButton setTitle:LS(@"Edit") forState:UIControlStateNormal];
        [self disableTextFieldInterActions];
        [self uploadImageToAmazon];
    }
    else
    {
        self.editButton.selected = YES;
        [self enableTextFieldInterActions];
        [self.editButton setTitle:LS(@"Save") forState:UIControlStateNormal];
    }

}

- (IBAction)profileTapGestureAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)changePasswordButtonAction:(id)sender
{
    //Change Password Process
    ChangePasswordViewController *changePasswordVC = (ChangePasswordViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"changePasswordVC"];
    
    
    changePasswordVC.phoneNumber = self.phoneNumberTextField.text;
    changePasswordVC.isFromProfileVC = YES;
    
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:changePasswordVC animated:NO];

}

- (IBAction)languageButtonAction:(id)sender
{
    [self.view endEditing:YES];
    
    [self.languagePickerView selectRow:selectedLanguageIndexValue inComponent:0 animated:YES];
    
    self.languagePickerBackgroundViewBottomConstraint.constant = 0;
    
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.view layoutIfNeeded];
        
    }];

}

- (IBAction)languagePickerCanelButtonAction:(id)sender
{
    self.languagePickerBackgroundViewBottomConstraint.constant = -250;
    
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.view layoutIfNeeded];
        
    }];

}

- (IBAction)languagePickerDoneButtonAction:(id)sender
{
    NSInteger selectedRow = [self.languagePickerView selectedRowInComponent:0];
    
    if(selectedLanguageIndexValue != selectedRow)
    {
        selectedLanguageIndexValue = selectedRow;
        
        [self.languageButton setTitle:[arrayOfLanguages[selectedLanguageIndexValue][@"lan_name"]capitalizedString] forState:UIControlStateNormal];
      
        
        [ud setInteger:selectedLanguageIndexValue forKey:iServeUserSelectedLanguageNumber];
        
        [ud synchronize];
    }

    [self languagePickerCanelButtonAction:nil];

}

-(void)disableTextFieldInterActions{
    
    self.profileImageBackGroundButton.userInteractionEnabled = NO;
    self.firstNameTextField.userInteractionEnabled = NO;
    self.lastNameTextField.userInteractionEnabled = NO;
    self.emailTextField.userInteractionEnabled = NO;
    self.phoneNumberTextField.userInteractionEnabled = NO;
    self.countryCodeButton.userInteractionEnabled = NO;
    
    self.logoutBackGroundView.hidden = NO;
//    [self.logoutButton setTitle:@"LOGOUT" forState:UIControlStateNormal];

}


-(void)enableTextFieldInterActions{
    
    self.profileImageBackGroundButton.userInteractionEnabled = YES;
    self.firstNameTextField.userInteractionEnabled = YES;
    self.lastNameTextField.userInteractionEnabled = YES;
    
    self.logoutBackGroundView.hidden = YES;
//    [self.logoutButton setTitle:@"Change Password" forState:UIControlStateNormal];
    
}

#pragma mark - UIPicker Delegate -

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrayOfLanguages.count;
}

//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
//    
//    return [NSString stringWithFormat:@"%@",[arrayOfLanguages[row][@"lan_name"]capitalizedString]];
//}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* pickerLabel = (UILabel*)view;
    
    if (!pickerLabel)
    {
        pickerLabel = [[UILabel alloc] init];
        
        pickerLabel.font = [UIFont fontWithName:OpenSans_Regular size:15];
        
        pickerLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    [pickerLabel setText:[arrayOfLanguages[row][@"lan_name"]capitalizedString]];
    
    return pickerLabel;
}



#pragma mark - KeyBoardHideAndShow -

-(void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

- (void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}

#pragma mark - TextField Delegates -


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextfield = textField;
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag) {
            
        case 0:
            if(textField == self.firstNameTextField)
            {
                [self.lastNameTextField becomeFirstResponder];
            }
            
            break;
        case 1:
            if(textField == self.lastNameTextField)
            {
                [self.phoneNumberTextField resignFirstResponder];
            }
            
            break;
            
        default:
            [textField resignFirstResponder];
            break;
    }
    return YES;
}


#pragma mark - AMSliderDelegatesMethod -

- (void)leftMenuWillOpen
{
    [self.view endEditing:YES];
//    if([self.editButton.titleLabel.text isEqual:@"Save"])
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
//                                                        message:@"Do you want to update the profile?"
//                                                       delegate:self
//                                              cancelButtonTitle:@"YES"
//                                              otherButtonTitles:@"NO",nil];
//        alert.tag=100;
//        [alert show];
//    }
}

#pragma mark - WebServiceCall -

-(void)uploadImageToAmazon
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow  withMessage:LS(@"Updating Profile..")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        if(pickedImage){
            
            [[UploadImagesToAmazonServer sharedInstance] deleteImageFromAmazon:[ud objectForKey:iServeUserProfilepic]];
            
            [[UploadImagesToAmazonServer sharedInstance] uploadProfileImageToServer:pickedImage
                                                                           andEmail:self.emailTextField.text
                                                                            andType:1
                                                                     withCompletion:^(NSString *imageURL) {
                                                                         [self sendServiceForUpdatingProfileData:imageURL];
                                                                     }];
        }
        else {
            
            [self sendServiceForUpdatingProfileData:[ud objectForKey:iServeUserProfilepic]];
        }
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}


-(void)sendServiceForUpdatingProfileData:(NSString *)imageURL
{
//    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow  withMessage:LS(@"Updating Profile..")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
//        if(pickedImage){
//            
//            [[UploadImagesToAmazonServer sharedInstance] uploadProfileImageToServer:pickedImage andEmail:self.emailTextField.text andType:1];
//        }
        
        NSDictionary *params =  @{
                                  kTWTSessionTokenkey:flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                                  kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                  kTWTFirstName:flStrForStr((self.firstNameTextField.text)),
                                  kTWTLastName:flStrForStr((self.lastNameTextField.text)),
                                  @"ent_cid":[ud objectForKey:iServeCustomerID],
                                  kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                  @"ent_ppic":flStrForStr(imageURL)
                                };
        
        NSLog(@"data update :%@", imageURL);
        
        [[WebServiceHandler sharedInstance] sendRequestToUpdateProfileData:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}

-(void)sendServiceForGettingProfileData
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow  withMessage:@"Loading..."];
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetProfileData:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}


#pragma mark - WebServiceDelegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    self.responseDict = (NSDictionary *)response;
    
    if (error)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
           
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [logout deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                 [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
            
        case 0:
        {
            switch (requestType) {
                    
                case RequestTypeGetProfileData:
                {
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    self.firstNameTextField.text = flStrForStr(self.responseDict[@"fName"]);
                    self.lastNameTextField.text = flStrForStr(self.responseDict[@"lName"]);
                    self.emailTextField.text = flStrForStr(self.responseDict[@"email"]);
                    self.phoneNumberTextField.text = flStrForStr(self.responseDict[@"phone"]);
                    
                    [ud setObject:flStrForStr(self.responseDict[@"pPic"]) forKey:iServeUserProfilepic];
                    [self downloadProfileImage];
                    
                    self.countryCodeLabel.text = flStrForStr(self.responseDict[@"country_code"]);
                    NSString *countryCode = [self.countryCodeLabel.text substringFromIndex:1];
                    
                    
                    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
                    
                    NSString *countryName = [[dictionaryWithCode allKeysForObject:countryCode] objectAtIndex:0];
                    
                    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@",countryName];
                    self.countryImageView.image = [UIImage imageNamed:imagePath];
                    
                    previousInformation = [[NSMutableArray alloc]init];
                    [previousInformation addObject:self.firstNameTextField.text];
                    [previousInformation addObject:self.lastNameTextField.text];
                    
                    if([response[@"Languages"]count] > 0)
                    {
                        if(![ud objectForKey:iServeUserLanguages] || ![[ud objectForKey:iServeUserLanguages] isEqualToArray:response[@"Languages"]])
                        {
                            [ud setObject:response[@"Languages"]  forKey:iServeUserLanguages];
                            [ud setInteger:0 forKey:iServeUserSelectedLanguageNumber];
                            [ud synchronize];
                        }
                        
                        arrayOfLanguages = [ud objectForKey:iServeUserLanguages];
                        selectedLanguageIndexValue = [ud integerForKey:iServeUserSelectedLanguageNumber];
                        [self.languageButton setTitle:[arrayOfLanguages[selectedLanguageIndexValue][@"lan_name"]capitalizedString] forState:UIControlStateNormal];
                        [self.languagePickerView reloadAllComponents];
                        
                    }

                    
                }
                    break;
                    
                case RequestTypeUpdateProfileData:
                {
                    if(!pickedImage){
                        
                        [[ProgressIndicator sharedInstance]hideProgressIndicator];
                    }
                    
                    [ud setObject:flStrForStr(self.firstNameTextField.text) forKey:iServeUserFirstName];
                    [ud setObject:flStrForStr(self.lastNameTextField.text) forKey:iServeUserLastName];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:iServeUserNameChanged object:nil userInfo:nil];
                    
                }
                    break;
                default:
                    break;
            }
            
        }
            break;
            
            
        default:
            break;
    }
}

#pragma mark - Notification Switch

- (void)setNotificationSwitch {
    [self.notificationBtn setOn:NO animated:NO];
    
    hasRegistered = [[NSUserDefaults standardUserDefaults] objectForKey:@"MyApp_Notifications"];
    
    if(hasRegistered)
    {
        if ([hasRegistered isEqualToString:@"SEND"]){
            [self.notificationBtn setOn:YES animated:NO];
        }
    }
    
    [self.notificationBtn addTarget:self action:@selector(toggleNotifications:) forControlEvents:UIControlEventValueChanged];
}


-(void)toggleNotifications: (UISwitch*) sender{
    
    AppDelegate* delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if(sender.isOn){
        [delegate registerPushNotification];
    }else{
        [delegate unregisterAppForNotifications];
    }
}

@end
