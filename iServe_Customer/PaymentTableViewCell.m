//
//  PaymentTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/20/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PaymentTableViewCell.h"

@implementation PaymentTableViewCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setPaymentDetails:(NSDictionary *)bookingDetails
{
    if([bookingDetails[@"fdata"][@"payment_type"]integerValue] == 1){
    
        self.cardOrCashLabel.text = LS(@"CASH");
        self.cardImageView.image = [UIImage imageNamed:@"payment_cash_icon"];
        
    }
    else{
        
        self.cardOrCashLabel.text = LS(@"CARD");
        self.cardImageView.image = [UIHelper requestForCardTypeImageName:bookingDetails[@"fdata"][@"paymentType"]];
    
    }
    
}


@end
