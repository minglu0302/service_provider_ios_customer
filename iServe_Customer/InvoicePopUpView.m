//
//  InvoicePopUpView.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "InvoicePopUpView.h"
#import "ReceiptDetailsPopUpView.h"

static InvoicePopUpView *invoicePopUpView = nil;

@interface InvoicePopUpView ()<UITextViewDelegate,WebServiceHandlerDelegate>
{
    ReceiptDetailsPopUpView *receiptPopUp;
    UIWindow *window;
}

@end

@implementation InvoicePopUpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self =  [[[NSBundle mainBundle] loadNibNamed:@"InvoicePopUpView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance
{
    if (!invoicePopUpView)
    {
        invoicePopUpView  = [[self alloc] initWithFrame:CGRectZero];
        invoicePopUpView.frame = [[UIScreen mainScreen]bounds];
        
    }
    invoicePopUpView.scrollView.hidden = NO;
    invoicePopUpView.needHelpBackGroundView.hidden = YES;
    
    [invoicePopUpView setRatingViewPropertries];
    
    return invoicePopUpView;
}

-(void)showBookingDetails:(NSDictionary *)bookingDetails
{
    self.providerNameLabel.text = [NSString stringWithFormat:@"%@ %@",bookingDetails[@"fName"],bookingDetails[@"lName"]];
    self.bookingId = [bookingDetails[@"bid"] integerValue];
    
    self.bookingIdLabel.text = bookingDetails[@"bid"];
    
    NSString *myString = bookingDetails[@"apptDate"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@\n%@",[dateFormatter stringFromDate:yourDate],bookingDetails[@"apptTime"]];
    
    self.addressLabel.text = bookingDetails[@"addr1"];
    
    self.providerTypeLabel.text = bookingDetails[@"cat_name"];
    
    self.totalCostLabel.text = [NSString stringWithFormat:@"%@ %.2f",[[NSUserDefaults standardUserDefaults]objectForKey:iServeCurrentCountryCurrencySymbol],[bookingDetails[@"fdata"][@"total_pro"] floatValue]];
    
    [self.activityIndicator startAnimating];
    
    NSLog(@"local image url: %@", bookingDetails[@"pPic"]);
    
    [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:bookingDetails[@"pPic"]]
                              placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         [self.activityIndicator stopAnimating];
                                         
                                     }];
    [self setFromPastOrderProperties:bookingDetails];
}

-(void)setFromPastOrderProperties:(NSDictionary *)bookingDetails
{
    if(self.isFromPastOrder)
    {
        [self showPastOrderPopup:bookingDetails];
    }
    else
    {
        self.needHelpButton.hidden = NO;
        self.cancelReasonBackgroundView.hidden = YES;
        self.rateAndCommentView.hidden = NO;
        self.leaveACommentButton.hidden = NO;
        self.recieptButton.hidden = NO;
        
        [self.submitButton setTitle:LS(@"SUBMIT") forState:UIControlStateNormal];
        
        self.ratingView.value = 5.0;
        self.ratingView.userInteractionEnabled = YES;
        
        self.rateYourServiceLabel.text = LS(@"Rate Your service");
        
//        self.topViewHeightConstraint.constant = 380;//Normal Height of Top View
    }
    
}

-(void)setRatingViewPropertries
{
    self.ratingView.markFont = [UIFont systemFontOfSize:30];
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
}

-(void)closeWithAnimation
{
    invoicePopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         invoicePopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         
                         dispatch_async(dispatch_get_main_queue(),^{
                             
                             NSInteger bookingID = invoicePopUpView.bookingId;
                             
                             [invoicePopUpView removeFromSuperview];
                             invoicePopUpView = nil;
                             
                             AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                             AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
                             
                             UINavigationController *naviVC = menu.currentActiveNVC;
                             
                             if([naviVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
                             {
                                 ShowOnGoingBookingsViewController *onGoingVC = [ShowOnGoingBookingsViewController getSharedInstance];
                                 if(onGoingVC.bookingId == bookingID)
                                 {
                                     [onGoingVC navigationBackButtonAction:nil];
                                 }
                             }
                             else if([naviVC.topViewController isKindOfClass:[HomeViewController class]])
                             {
                                 [[HomeViewController getSharedInstance]showInvoiceRatingPopUP];
                             }
                             
                             
                             
                         });
                     }];
    
}

-(void)showPastOrderPopup:(NSDictionary *)bookingDetails
{
    self.needHelpButton.hidden = YES;
    [self.submitButton setTitle:LS(@"CLOSE") forState:UIControlStateNormal];
    self.rateAndCommentView.hidden = YES;
//    self.topViewHeightConstraint.constant = 325;
    self.recieptButton.hidden = YES;
    self.cancelReasonBackgroundView.hidden = NO;
    
    self.cancelReasonTitleLabel.text = LS(@"Cancellation Reason");
    self.cancelReasonLabel.text = [NSString stringWithFormat:@"%@",bookingDetails[@"cancel_reason"]];
    
    self.totalCostLabel.text = [NSString stringWithFormat:@"%@ %.2f",[[NSUserDefaults standardUserDefaults]objectForKey:iServeCurrentCountryCurrencySymbol],[bookingDetails[@"cancel_amount"] floatValue]];
    
    switch (self.bookingStatus)
    {
        case 3:
        {
            NSLog(@"PROVIDER REJECTED");
        }
            break;
        case 4:
        {
            NSLog(@"CUSTOMER CANCELLED WITHOUT FEES");
        }
            break;
        case 7:
        {
            NSLog(@"BOOKING COMPLETED");
            
            if([bookingDetails[@"disputed"]integerValue] == 1)
            {
                self.cancelReasonTitleLabel.text = LS(@"Dispute Reason");
                self.cancelReasonLabel.text = [NSString stringWithFormat:@"%@",bookingDetails[@"dispute_msg"]];
                
//                float height = [self measureHeightLabel:self.cancelReasonLabel] + 8 + 325;
//                self.topViewHeightConstraint.constant = height;
                
            }
            else
            {
                self.ratingView.value = [bookingDetails[@"star_rating"]floatValue];
                self.ratingView.userInteractionEnabled = NO;
                self.rateAndCommentView.hidden = NO;
                self.cancelReasonBackgroundView.hidden = YES;
                self.rateYourServiceLabel.text = LS(@"Your Rating");
                self.recieptButton.hidden = NO;
                self.leaveACommentButton.hidden = YES;
//                self.topViewHeightConstraint.constant = 325;
            }
            
            //(height of topview - height of rating background View)
            
            self.totalCostLabel.text = [NSString stringWithFormat:@"%@ %.2f",[[NSUserDefaults standardUserDefaults]objectForKey:iServeCurrentCountryCurrencySymbol],[bookingDetails[@"fdata"][@"total_pro"] floatValue]];

        }
            break;
        case 8:
        {
            NSLog(@"BOOKING REQUEST TIMEOUT OR PROVIDER DID NOT RESPOND");
        }
            break;
        case 9:
        {
            NSLog(@"CUSTOMER CANCELLED AND FEE CHARGED");
        }
            break;
        case 10:
        {
            NSLog(@"PROVIDER CANCELLED");
        }
            break;
            
        default:
            NSLog(@"");
            break;
    }

}

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

-(void)setTopViewFrame
{
    CGSize screenSize = [[UIScreen mainScreen]bounds].size;
    
    if (screenSize.height < 568)
    {
        if(!self.isFromPastOrder)
        {
            self.topViewTopConstraint.constant = 55;
            self.topViewBottomConstraint.constant = 55;
        }
        else {
            
            self.topViewTopConstraint.constant = 75;
            self.topViewBottomConstraint.constant = 75;
        }
        
    }
    else if(screenSize.height < 667)
    {
        if(!self.isFromPastOrder)
        {
            self.topViewTopConstraint.constant = 90;
            self.topViewBottomConstraint.constant = 90;
        }
        else {
            
            self.topViewTopConstraint.constant = 105;
            self.topViewBottomConstraint.constant = 105;
        }
        
    }
    else if(screenSize.height < 736)
    {
        if(!self.isFromPastOrder)
        {
            self.topViewTopConstraint.constant = 135;
            self.topViewBottomConstraint.constant = 135;
        }
        else {
            
            self.topViewTopConstraint.constant = 155;
            self.topViewBottomConstraint.constant = 155;
        }
        
    }
    else
    {
        if(!self.isFromPastOrder)
        {
            self.topViewTopConstraint.constant = 165;
            self.topViewBottomConstraint.constant = 165;
        }
        else {
            
            self.topViewTopConstraint.constant = 185;
            self.topViewBottomConstraint.constant = 185;
        }
        
    }
    [self layoutIfNeeded];
    
}




#pragma mark - UIbutton Actions -

- (IBAction)recieptButtonAction:(id)sender
{
    window = [UIApplication sharedApplication].keyWindow;
    receiptPopUp = [ReceiptDetailsPopUpView sharedInstance];
    receiptPopUp.bookingDetails = self.invoiceBookingDetails;
    receiptPopUp.arrayOfServiceDetails = self.invoiceBookingDetails[@"services"];
    [receiptPopUp setTopViewFrame];
    [receiptPopUp.tableView reloadData];
    
    
//    if(self.isFromPastOrder)
//    {
        [receiptPopUp setTableFooter];
//    }
    
    //Set Appointment date
    NSString *myString = self.invoiceBookingDetails[@"apptDate"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM-dd-yyyy";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    receiptPopUp.appointmentDateLabel.text = [NSString stringWithFormat:@"Appointment Date: %@ %@",[dateFormatter stringFromDate:yourDate],self.invoiceBookingDetails[@"apptTime"]];
    
    [window addSubview:receiptPopUp];
    
}
- (void)removeRecieptPopUp
{
    [receiptPopUp removeFromSuperview];
    receiptPopUp = nil;
}

- (IBAction)submitButtonAction:(id)sender
{
    if(self.isFromPastOrder)
    {
        [self closeWithAnimation];
    }
    else
    {
        [self sendRequestToSubmitReview];
    }
}

- (IBAction)needHelpButtonAction:(id)sender
{
    invoicePopUpView.topView.hidden = YES;
    invoicePopUpView.commentBackGroundView.hidden = YES;
    invoicePopUpView.needHelpBackGroundView.hidden = NO;
    [self.needHelpTextView becomeFirstResponder];
    
    [AnimationsWrapperClass CATransitionForViewsinSameVCAnimationType:kCATransitionPush
                                                              subType:kCATransitionFromRight
                                                     timeFunctionType:kCAMediaTimingFunctionEaseInEaseOut
                                                              forView:self.needHelpBackGroundView
                                                         timeDuration:0.5];
    
}


- (IBAction)needHelpBackButtonAction:(id)sender
{
    [invoicePopUpView endEditing:YES];
    if(_commentTextView.text.length > 0)
    {
        [self.leaveACommentButton setTitle:LS(@"Edit Comment") forState:UIControlStateNormal];
    }
    else
    {
        [self.leaveACommentButton setTitle:LS(@"Leave a Comment") forState:UIControlStateNormal];
    }
    invoicePopUpView.topView.hidden = NO;
    invoicePopUpView.commentBackGroundView.hidden = YES;
    invoicePopUpView.needHelpBackGroundView.hidden = YES;
    
    
    [AnimationsWrapperClass CATransitionForViewsinSameVCAnimationType:kCATransitionPush
                                                              subType:kCATransitionFromLeft
                                                     timeFunctionType:kCAMediaTimingFunctionEaseInEaseOut
                                                              forView:self.scrollView
                                                         timeDuration:0.5];
    
    
}

- (IBAction)needHelpSubmitButtonAction:(id)sender
{
    [invoicePopUpView endEditing:YES];
    if(self.needHelpTextView.text.length == 0)
    {
       [UIHelper showMessage:LS(@"Please enter your message") withTitle:LS(@"Message")delegate:nil];
    }
    else
    {
        [self sendDisputeRquest];
    }
}

- (IBAction)leaveACommentButtonAction:(id)sender
{
    invoicePopUpView.topView.hidden = YES;
    invoicePopUpView.commentBackGroundView.hidden = NO;
    invoicePopUpView.needHelpBackGroundView.hidden = YES;
    [self.commentTextView becomeFirstResponder];
    
    
    [AnimationsWrapperClass CATransitionForViewsinSameVCAnimationType:kCATransitionPush
                                                              subType:kCATransitionFromRight
                                                     timeFunctionType:kCAMediaTimingFunctionEaseInEaseOut
                                                              forView:self.commentBackGroundView
                                                         timeDuration:0.5];
    
}

- (IBAction)commentBackButtonAction:(id)sender
{
    [invoicePopUpView endEditing:YES];
    [self needHelpBackButtonAction:nil];
}

- (IBAction)commentDoneButtonAction:(id)sender
{
    [self needHelpBackButtonAction:nil];
}


#pragma mark - TextView Delegate methods -

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        if([textView isEqual:self.commentTextView])
        {
            [self commentDoneButtonAction:nil];
        }
        else if ([textView isEqual:self.needHelpTextView])
        {
            [self needHelpSubmitButtonAction:nil];
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - Web Service Call -

-(void)sendRequestTogetBookingDetails
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_inv":[NSString stringWithFormat:@"1"],
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetParticularApptStatus:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        //        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:[UIHelper getCurrentController]];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
        
    }
    
}
-(void)sendDisputeRquest
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_dispute_msg":flStrForStr(self.needHelpTextView.text),
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToUpdateDispute:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
        
    }
    
}

-(void)sendRequestToSubmitReview
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Submiting Review...", @"Submiting Review...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSString *reviewMessage = self.commentTextView.text;
        if(reviewMessage.length == 0 ||[reviewMessage isEqualToString:@"Leave a comment"]){
            reviewMessage = @"";
        }
        
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_rating_num":[NSNumber numberWithFloat:self.ratingView.value],
                                 @"ent_review_msg":flStrForStr(reviewMessage),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToUpdateReview:params andDelegate:invoicePopUpView];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
        
    }
    
}

#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:nil];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeUpdateReview)
            {
                //remove Invoice Popup
                [self closeWithAnimation];
                //                invoicePopUpView = nil;
                
                
            }
            else if(requestType == RequestTypeGetParticularApptDetails)
            {
                self.invoiceBookingDetails = response;
                [self showBookingDetails:response];
            }
            else if (requestType == RequestTypeUpdateDispute)
            {
                 [self closeWithAnimation];
            }
            
            
        }
            break;
        default:
            break;
    }
    
    
}

@end
