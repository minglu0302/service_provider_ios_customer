//
//  AddressManageViewController.m
//  Uberx
//
//  Created by -Tony Lu on 19/09/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//



#import "AddressManageViewController.h"
#import "Addresscell.h"
#import "PickAddressFromMapViewController.h"
#import "ManageAddress.h"

@interface AddressManageViewController ()
{
    NSInteger value;
    BOOL isright;
    NSInteger selectedIndex;
    CGRect screenSize;
    NSMutableArray *arrDBResult;

}

@end

@implementation AddressManageViewController

@synthesize getAddress,manageAddressTableView;

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    screenSize = [[UIScreen mainScreen]bounds];
    
    manageAddressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.manageAddressTableView.allowsMultipleSelectionDuringEditing = NO;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.isFromProviderBookingVC)
    {
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
    }
    else
    {
        [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
    }
    
//    [self sendRequestToGetAddress];
    
    arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getManageAddressDetails]];
    [self.manageAddressTableView reloadData];

    
    if (self.isFromProviderBookingVC) {
        
        self.title = LS(@"Select Address");
    }
    else{
        
       self.title = LS(@"Manage Addresses");

    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}

#pragma mark - UITableview Delegates -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrDBResult.count == 0)
    {
        manageAddressTableView.backgroundView = self.messageLabel;
        manageAddressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.messageLabel setHidden:NO];
        
        [self.messageLabel setHidden:NO];
        return 0;
        
    }
    else
    {
        [self.messageLabel setHidden:YES];
        return arrDBResult.count;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    
    if (self.isFromProviderBookingVC)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    else
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell.removeAddressButton setTag:indexPath.row];
    [cell bringSubviewToFront:cell.removeAddressButton];
    
    
    ManageAddress *manageAddress = arrDBResult[indexPath.row];
    
    if([manageAddress.flatNumber length] > 0){
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@, %@",manageAddress.flatNumber,manageAddress.addressLine1]];
    }
    else{
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@",manageAddress.addressLine1]];
    }
    
   if([[manageAddress.tagAddress uppercaseString] isEqualToString:@"HOME"])
   {
       cell.tagImageView.image = [UIImage imageNamed:@"home_icn"];
       cell.tagLabel.text = LS(@"HOME");
   }
   else if ([[manageAddress.tagAddress uppercaseString] isEqualToString:@"OFFICE"])
   {
       cell.tagImageView.image = [UIImage imageNamed:@"office__icn"];
       cell.tagLabel.text = LS(@"OFFICE");
   }
   else
   {
       cell.tagImageView.image = [UIImage imageNamed:@"other_icn"];
       cell.tagLabel.text = LS(@"OTHER");
   }
    
    [cell layoutIfNeeded];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *tableidentifier = @"adressManageCell";
    
    Addresscell *cell = (Addresscell *)[tableView dequeueReusableCellWithIdentifier:tableidentifier];
    
    ManageAddress *manageAddress = arrDBResult[indexPath.row];
    
    if([manageAddress.flatNumber length] > 0){
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@, %@",manageAddress.flatNumber,manageAddress.addressLine1]];
    }
    else{
        
        [cell.addressLabel setText:[NSString stringWithFormat:@"%@",manageAddress.addressLine1]];
    }
    
    float height = [self measureHeightLabel:cell.addressLabel];
    
    return 6+height+8;
}


- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-115  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ManageAddress *manageAddress = arrDBResult[indexPath.row];
    
    NSDictionary *dict = @{
                           @"address1":manageAddress.addressLine1,
                           @"address2":@"",
                           @"suite_Number":manageAddress.flatNumber,
                           @"tag_Address":manageAddress.tagAddress,
                           @"latitude":manageAddress.latitude,
                           @"longitude":manageAddress.longitude,
                         };
    
    if (self.isFromProviderBookingVC)
    {
        [_addressManageDelegate getAddressFromDatabase:dict];
        [self navigationbackButtonAction:nil];
        
    }
}

//-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewRowAction *deleteButton = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"DELETE" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
//                                    {
//                                        selectedIndex =indexPath.row;
//                                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Remove Address?" message:@"Are you sure you want to remove this address ?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
//                                        alert.tag=40;
//                                        [alert show];
//
//                                        NSLog(@"Action to perform with Button 1");
//                                    }];
//    
//    deleteButton.backgroundColor = APP_COLOR;
//    //arbitrary color
//    
//    return @[deleteButton];
//}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    // you need to implement this method too or nothing will work:
//    
//}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}

//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//    }
//}

#pragma mark - Alert view Delegates -

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if(alertView.tag == 40)
//    {
//        if (buttonIndex == 1)
//        {
//            [self sendRequestToDeleteAddress];
//        }
//    }
//}
//
#pragma mark - UIButton Actions -

- (IBAction)addNewAddressButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"toPickUpAddressFromMapVC" sender:sender];
}

- (IBAction)navigationbackButtonAction:(id)sender {
    
    if (self.isFromProviderBookingVC)
    {
//         [self dismissViewControllerAnimated:YES completion:nil];
        
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                  subType:kCATransitionFromBottom
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController popViewControllerAnimated:NO];
    }
    else {
        
        [self.navigationController popViewControllerAnimated:YES];

    }
    
}

- (IBAction)removeAddressButtonAction:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    selectedIndex = mBtn.tag;
    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Remove Address?" message:@"Are you sure you want to remove this address ?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
//    alert.tag = 40;
//    [alert show];
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:LS(@"Remove Address?")
                                                                              message:LS(@"Are you sure you wish to remove this address?")
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:LS(@"NO")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
            if(self.isFromProviderBookingVC)
            {
                [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
            }
               
                                                      }]];

    
    [alertController addAction:[UIAlertAction actionWithTitle:LS(@"YES")
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction *action) {
            //Delete Address
            if(self.isFromProviderBookingVC)
            {
                [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
            }
                                                    
            [self sendRequestToDeleteAddress];
                                                          
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
    


}

#pragma mark - Web Service Call -

-(void)sendRequestToGetAddress
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_cust_id":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID]
                                };
        
        [[WebServiceHandler sharedInstance]sendRequestToGetAddress:params andDelegate:self];
        
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}

-(void)sendRequestToDeleteAddress{
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Deleting...", @"Deleting...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
         ManageAddress *manageAddress = arrDBResult[selectedIndex];
         NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_cust_id":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID],
                                 @"ent_addressid":flStrForObj(manageAddress.addressId),
                                };
        
        [[WebServiceHandler sharedInstance]sendRequestToDeleteAddress:params andDelegate:self];
        
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}


#pragma mark - Web service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message") delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            
            else if (requestType == RequestTypeGetAddress)
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message") delegate:self];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message") delegate:self];
            }
            
        }
            break;
            
        case 0:
        {
           
//            addressArray = [[NSMutableArray alloc]initWithArray:response[@"addlist"]];
//         
//            [manageAddressTableView reloadData];
            
            if (requestType == RequestTypeDeleteAddress)
            {
                ManageAddress *manageAddress = arrDBResult[selectedIndex];
                [Database DeleteParticularManageAddress:manageAddress.addressId];
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
                arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getManageAddressDetails]];
                [self.manageAddressTableView reloadData];
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];

            }
            
        }
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toPickUpAddressFromMapVC"])
    {
        PickAddressFromMapViewController *pickUpAddressFromMapVC = [segue destinationViewController];
        pickUpAddressFromMapVC.isFromProviderBookingVC = self.isFromProviderBookingVC;
        
    }

}

@end
