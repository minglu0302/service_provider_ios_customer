//
//  SignInViewController.h
//  iServe app
//  Developed by Raghavendra
//  Created by -Tony Lu on 06/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBLoginHandler.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate,WebServiceHandlerDelegate,FBLoginHandlerDelegate>

/**
 *     Password Label Outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
/**
 *  Email Label outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
/**
 *  forgot password Button outlet's
 *
 */
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;

/**
 *  sign in button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *signinButton;

/**
 *  navigation back button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
/**
 *  emailtextfield outlets
 */
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
/**
 *  passwordtextfield outlets
 */
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

/**
 *  check for current text field value in textfield delegates method .
 */
@property (nonatomic,strong) UITextField *activeTextField;
/**
 *  ScrollView Outlet
 */
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/**
 *  ScrollView ContentView Outlet
 */
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;

/**
 *  Login With Facebook Button Outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *loginWithFacebookButton;

/**
 *  storing the data from facebook
 */
@property (nonatomic,strong) NSDictionary *fbData;

/**
 storing From Change Password View Controller or Not
 */
@property BOOL isFromChangePassVC;
/**
 *   It will check the email field is valid or not
 *
 *  @param emailToValidate
 *
 *  @return
 */
-(BOOL) emailValidationCheck: (NSString *) emailToValidate;


/**
 *  This service Used for login the signin service
 */
-(void)sendServiceForLogin;

/**
 *  This service Used for create the Object of Class
 */
+ (instancetype)getSharedInstance;
/**
 *  This method call when user press back button
 *
 *  @param sender
 */
- (IBAction)backButtonClick:(id)sender;
/**
 *  This method will called when user press the sign in button and it check all ui text field data.
 *
 *  @param IBAction
 *
 *  @return
 */
- (IBAction)signInButtonClicked:(id)sender;
/**
 *  This method called when user want's reset the password and it display popup Controller for resetting the previous password.
 *
 *  @param IBAction
 *
 *  @return
 */
- (IBAction)forgotPasswordButtonClicked:(id)sender;


/**
 *  This method called when user want's login with Facebook and it facebook page for login to the Customer.
 *
 *  @param IBAction
 *
 *  @return
 */

- (IBAction)loginWithFacebookButtonAction:(id)sender;

/**
 *
 *  Sign In Tab Gesture Action used to close the KeyBoard
 *  @param sender
 */
- (IBAction)signInTabGesture:(id)sender;

@end
