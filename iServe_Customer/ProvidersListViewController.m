//
//  ProvidersListViewController.m
//  iServe_AutoLayout
//
//  Created by Imma Web Pvt Ltd on 02/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProvidersListViewController.h"
#import "ProviderDetailsTableViewCell.h"
#import "PickUpViewController.h"
#import "ProviderDetailsViewController.h"

@interface ProvidersListViewController ()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,SocketWrapperDelegate,PickUpAddressDelegate>
{
    NSMutableArray *providerTypesTitleArray;
    CGRect screenSize;
//    NSInteger selectedTypeButtonTag;
    CGFloat width;
    CGFloat divider;
    
    NSMutableArray *arrayOfProvidersTypeButtons;
    NSMutableArray *arrayOfProvidersDetailsTable;
    NSMutableArray *arrayOfMessageLabels;
    
    AppointmentLocation *apLocation;
    AMSlideMenuMainViewController *AMSLider;
    
    SocketIOWrapper *socketWrapper;
    NSTimer *publishToSocketTimer;
    NSUserDefaults *ud;
    
    BOOL showingScheduleVisitViews;
    NSString *selectedDate;
    BOOL isComingTocheckProviderTypes;
    NSDate *scheduleDate;
    
    NSArray *feeTypesArray;
}

@end

@implementation ProvidersListViewController

@synthesize selectedTypeButtonTag;

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    apLocation = [AppointmentLocation sharedInstance];
    screenSize = [[UIScreen mainScreen]bounds];
    ud = [NSUserDefaults standardUserDefaults];
    [self loadViewInitially];
    
    feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.isFromBoookLater)
    {
        scheduleDate = self.dateFromHomeVC;
        [self showScheduleVisitViews];
    }
    else
    {
        if(showingScheduleVisitViews == NO)
            [self showNormalViews];
    }

    AMSLider = [AMSlideMenuMainViewController getInstanceForVC:self];
    [AMSLider enableSlidePanGestureForLeftMenu];
   
    socketWrapper = [SocketIOWrapper sharedInstance];
    socketWrapper.socketDelegate = self;
  
    [self publishesToSocket];

    publishToSocketTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(publishesToSocket) userInfo:nil repeats:YES];
    
    if(!self.arrayOfProviderTypes)
        self.arrayOfProviderTypes = [[NSMutableArray alloc]init];
    if(!self.arrayOfProvidersArround)
        self.arrayOfProvidersArround = [[NSMutableArray alloc]init];

    self.addressLabel.text = apLocation.pickUpAddress;
   
    [self changeButtonState:self.showListViewButton];
    
    isComingTocheckProviderTypes = NO;
   
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
    
    [publishToSocketTimer invalidate];
    publishToSocketTimer = nil;
    
    [self datePickerCancelButtonAction:nil];
    
//    scheduleDate = nil;
    
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appD setSocketDelegateToAppDelegate];
}

#pragma mark - SIOSocket -
/**
 *  publishSocketStream to receiving providers Type and Details near around customer current location
 */
-(void)publishesToSocket {

    NSDictionary *message = @{
                              @"btype":[NSNumber numberWithInt:3],
                              @"email": [ud objectForKey:iServeUserEmail],
                              @"lat": apLocation.pickupLatitude,
                              @"long": apLocation.pickupLongitude,
                            };
    
    NSMutableDictionary *publishMessage = [[NSMutableDictionary alloc]initWithDictionary:message];
    
    if(showingScheduleVisitViews)
    {
        [publishMessage setObject:[NSNumber numberWithInt:4] forKey:@"btype"];
        [publishMessage setObject:flStrForStr(selectedDate) forKey:@"dt"];
    }
    else
    {
        [publishMessage setObject:[NSNumber numberWithInt:3] forKey:@"btype"];
    }
    
    NSLog(@"Published Message:%@",publishMessage);
    
    [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:publishMessage];
}


//Socket Response with Provider Details
-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message{
    
    NSLog(@"ProviderListVC message: %@",message);
    
    if([channelName isEqualToString:@"UpdateCustomer"]){
        
        if ([[message objectForKey:@"flag"] intValue] == 0) //will get Provider Types From Server
        {
            //Update Providers Type Details
            [self socketResponseWithProviderTypes:message];
           
            //Update Providers Arround Details
            [self socketResponseWithProvidersArround:message];
            
        }
        else{
            //Maintain If No Providers Type And No providers
            //Update Providers Type Details
            [self socketResponseWithProviderTypes:message];
        }
    }
    else if([channelName isEqualToString:@"CustomerStatus"]){//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10)
        {
            //Booking Response
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"Message"])//Chat Message Response
    {
        if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
    }

    
}


-(void)socketResponseWithProvidersArround:(NSDictionary *)messageDict {
    
    [self performSelector:@selector(hideProgressBar) withObject:nil afterDelay:0.6];
    if (!self.arrayOfProviderEmails) {//For the first time when the array is not initialised copy all the data to the array
        
        self.arrayOfProviderEmails = [[NSMutableArray alloc] init];
        self.arrayOfProvidersArround = messageDict[@"msg"][@"masArr"];
        
        
        for (int masArrIndex = 0;masArrIndex < self.arrayOfProvidersArround.count; masArrIndex++)
        {
            
            //Storing Each and Every Providers Channel now email will change to channel
            NSArray *channelsData = self.arrayOfProvidersArround[masArrIndex][@"mas"];
            
            for(NSDictionary *dict in channelsData)
            {
                
                [self.arrayOfProviderEmails addObject:dict[@"e"]];//change to Channels
                
            }
        }
        
       
//        [self reloadProviderDetailsTable:arrayOfProvidersDetailsTable[selectedTypeButtonTag]];
        [self reloadAllProviderDetailsTableView];
        
    }
    else {
        
        NSArray *arrayOfNewProvidersAround = messageDict[@"msg"][@"masArr"];
        NSMutableArray *arrayNewChannels = [[NSMutableArray alloc] init];
        
        //Storing Each and Every Providers Channel now email will change to channel
        for (int masArrIndex = 0;masArrIndex < arrayOfNewProvidersAround.count; masArrIndex++) {
            
            NSArray *channelsData = arrayOfNewProvidersAround[masArrIndex][@"mas"];
            
            for(NSDictionary *dict in channelsData){
                
                [arrayNewChannels addObject:dict[@"e"]];//change to Channels
                
            }
        }
        
        NSArray *previousProvidersDetails = self.arrayOfProvidersArround;
        self.arrayOfProvidersArround = [arrayOfNewProvidersAround mutableCopy]; //update the providers Arround Array
        
        
        if (self.arrayOfProviderEmails.count > arrayNewChannels.count) {//when Providers count from server changes
            
            NSMutableArray *channelToRemove = [[NSMutableArray alloc] init];
            
            //need to remove channels From New Providers Channel
            for(NSString *channel in self.arrayOfProviderEmails){
                
                if (![arrayNewChannels containsObject:channel]) {
                    
                    //remove channels
                    [channelToRemove addObject:channel];
                }
            }
            
            [self.arrayOfProviderEmails removeObjectsInArray:channelToRemove];
//            [self reloadProviderDetailsTable:arrayOfProvidersDetailsTable[selectedTypeButtonTag]];
            if(self.arrayOfProvidersArround.count > 0)
                [self reloadAllProviderDetailsTableView];
            
        }
        else if (self.arrayOfProviderEmails.count < arrayNewChannels.count) {//when Provider count from server changes
            
            //Need to add New channels to Providers channel Array
            NSMutableArray *channelsToAdd = [[NSMutableArray alloc] init];
            NSMutableArray *markerToAdd = [[NSMutableArray alloc] init];
            
            for(NSString *channel in arrayNewChannels){
                
                if (![self.arrayOfProviderEmails containsObject:channel]) {
                    
                    //add channel and subscribe here
                    [channelsToAdd addObject:channel];
                    [markerToAdd addObjectsFromArray:arrayNewChannels];
                }
            }
            
            //Showing a newly Added Providers On MapView and there Distance also
            [self.arrayOfProviderEmails addObjectsFromArray:channelsToAdd];
           
//            [self reloadProviderDetailsTable:arrayOfProvidersDetailsTable[selectedTypeButtonTag]];
             if(self.arrayOfProvidersArround.count > 0)
                 [self reloadAllProviderDetailsTableView];
            
        }
       else if (![self.arrayOfProvidersArround isEqualToArray:previousProvidersDetails]){
            
            //Different Providers With Same Count
//            [self reloadProviderDetailsTable:arrayOfProvidersDetailsTable[selectedTypeButtonTag]];
             if(self.arrayOfProvidersArround.count > 0)
                 [self reloadAllProviderDetailsTableView];
            
        }
        
    }
}

-(void)socketResponseWithProviderTypes:(NSDictionary *)messageDict {
    
    NSArray *arrayOffNewProviderTypes = messageDict[@"msg"][@"types"];
    
    if (arrayOffNewProviderTypes.count > 0) {
        
        if(!self.arrayOfProviderTypes) {
            
            //For the first time when the array is not initialised copy all the data to the array
            self.arrayOfProviderTypes = [arrayOffNewProviderTypes mutableCopy];
            [self showProviderTypeScrollView];
            
        }
        else{
            if (arrayOffNewProviderTypes.count != self.arrayOfProviderTypes.count) {
                
                //when array count from server changes
                [self clearAndUpdateProviderTypesDetails:arrayOffNewProviderTypes];
                
            }
            else if (arrayOffNewProviderTypes.count == _arrayOfProviderTypes.count){ //when array count from server is same
                
                if ([arrayOffNewProviderTypes isEqualToArray:_arrayOfProviderTypes]) {
                    
                    //Providers Types Remains Same then dont do anything
                    [ud setObject:self.arrayOfProviderTypes forKey:iServeProvidersTypesArrayKey];
                    [ud synchronize];
                    
                }
                else { //if the cotent is not same just copy the new array from the server and update scroller
                    
                    [self clearAndUpdateProviderTypesDetails:arrayOffNewProviderTypes];
                }
            }
        }
        isComingTocheckProviderTypes = NO;
        
    }
    else {//If there is No Providers Type Available
        
        //removing ScrollView from the View
        if (!isComingTocheckProviderTypes)
        {
            isComingTocheckProviderTypes = YES;
            
            [self.arrayOfProviderTypes removeAllObjects];
            self.arrayOfProviderTypes = nil;
            
            [self.arrayOfProvidersArround removeAllObjects];
            self.arrayOfProvidersArround = nil;
            
            [self.arrayOfProviderEmails removeAllObjects];
            self.arrayOfProviderEmails = nil;
            
            selectedTypeButtonTag = 0;

            dispatch_async(dispatch_get_main_queue(),^{
                
//                [[ProgressIndicator sharedInstance]showMessage:@"Sorry! we are not operational in your region at the moment, please write to info@goclean-service.com" On:self.view];
                [UIHelper showMessage:LS(@"Sorry! we are not operational in your region at the moment, please write to info@goclean-service.com") withTitle:LS(@"Message")delegate:self];

            });

            
            [ud setObject:[NSArray array] forKey:iServeProvidersTypesArrayKey];
            [ud synchronize];
        
            [self hideAllViewsAndShowNoProviderTypesMessage];
            
        }
    }
    
}


-(void)clearAndUpdateProviderTypesDetails:(NSArray *)arrayOffNewProviderTypes{
    
    [self.arrayOfProviderTypes removeAllObjects];
    self.arrayOfProviderTypes = nil;
    
    [self.arrayOfProvidersArround removeAllObjects];
    self.arrayOfProvidersArround = nil;
   
    [self.arrayOfProviderEmails removeAllObjects];
    self.arrayOfProviderEmails = nil;
    
//    selectedTypeButtonTag = 0;
    
    self.arrayOfProviderTypes = [arrayOffNewProviderTypes mutableCopy];
    
    [self hideAllViewsAndShowNoProviderTypesMessage];

    [self showProviderTypeScrollView];
    
    [ud setObject:self.arrayOfProviderTypes forKey:iServeProvidersTypesArrayKey];
    [ud synchronize];
    
}


#pragma mark - UIButton Actions -

- (IBAction)showMapViewButtonAction:(id)sender
{
    [self changeButtonState:sender];
    
    [AnimationsWrapperClass UIViewAnimationAnimationCurve:UIViewAnimationCurveEaseInOut
                                               transition:UIViewAnimationTransitionFlipFromRight
                                                  forView:self.navigationController.view
                                             timeDuration:0.5];

    [self.navigationController popViewControllerAnimated:NO];
    
}

- (IBAction)showListViewButtonAction:(id)sender
{
    [self changeButtonState:sender];
}

- (IBAction)addressButtonAction:(id)sender
{
    PickUpViewController *pickController = (PickUpViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"pickUpLocationVC"];
    
    pickController.latitude =  [NSString stringWithFormat:@"%f",apLocation.pickupLatitude.floatValue];
    pickController.longitude = [NSString stringWithFormat:@"%f",apLocation.pickupLongitude.floatValue];
    pickController.pickUpAddressDelegate = self;
    
      
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];

    [self.navigationController pushViewController:pickController animated:NO];
    
}

- (IBAction)scheduleVisitButtonAction:(id)sender
{
    [AnimationsWrapperClass CATransitionForViewsinSameVCAnimationType:kCATransitionPush
                                                              subType:kCATransitionFromRight
                                                     timeFunctionType:kCAMediaTimingFunctionEaseInEaseOut
                                                              forView:self.providerDetailsBackgroundView
                                                         timeDuration:0.5];
    
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        self.providerDetailsTablebackgroundviewBottomConstraint.constant = -80;
        self.bottomView.hidden = YES;
       
        [self.view layoutIfNeeded];
    });

    [self.arrayOfProvidersArround removeAllObjects];
    self.arrayOfProvidersArround = nil;
    
    [self.arrayOfProviderEmails removeAllObjects];
    self.arrayOfProviderEmails = nil;
    
    [self reloadAllProviderDetailsTableView];
    
    [self showScheduleVisitViews];
}

- (IBAction)typeButtonAction:(id)sender
{
        UIButton *typeBtn = (UIButton *) sender;
        [self changeButtonState:sender];

        selectedTypeButtonTag = typeBtn.tag;
    
    
        //Scrolling Provider Details ScrollView
        if(self.scrollView.contentOffset.x != selectedTypeButtonTag*CGRectGetWidth(self.scrollView.frame))
        {
            [self.scrollView setContentOffset:CGPointMake(selectedTypeButtonTag*CGRectGetWidth(self.scrollView.frame), 0) animated:NO];
        }
        
        //Scrolling Provider Type ScrollView
        if(self.arrayOfProviderTypes.count > 3)
        {
            NSInteger setNumber = selectedTypeButtonTag/3;
            
            [self.topScrollView setContentOffset:CGPointMake(setNumber * CGRectGetWidth(self.scrollView.frame) , 0) animated:NO];
        }
    
        if(self.typeDividerLeadingConstraint.constant != selectedTypeButtonTag*width+2)
            self.typeDividerLeadingConstraint.constant = selectedTypeButtonTag*width+2;

        UITableView *tableView = arrayOfProvidersDetailsTable[selectedTypeButtonTag];
        [self reloadProviderDetailsTable:tableView];

}

- (IBAction)navigationLeftButtonAction:(id)sender
{
    if(self.isFromBoookLater)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(showingScheduleVisitViews)
    {
        
        [AnimationsWrapperClass CATransitionForViewsinSameVCAnimationType:kCATransitionPush
                                                                  subType:kCATransitionFromLeft
                                                         timeFunctionType:kCAMediaTimingFunctionEaseInEaseOut
                                                                  forView:self.providerDetailsBackgroundView
                                                             timeDuration:0.5];
        
        if(self.datePickerViewBottomConstraints.constant == 0)
        {
            [self datePickerCancelButtonAction:nil];
        }
        
        [self.arrayOfProvidersArround removeAllObjects];
        self.arrayOfProvidersArround = nil;
        
        [self.arrayOfProviderEmails removeAllObjects];
        self.arrayOfProviderEmails = nil;
        
        [self reloadAllProviderDetailsTableView];

        [self showNormalViews];
    }
    else
        [AMSLider openLeftMenu];
}

- (IBAction)calenderButtonAction:(id)sender
{
    [self.datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    [self.datePicker setDate:scheduleDate];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        //DATE PICKER Showing Animation
        self.datePickerViewBottomConstraints.constant = -30;
        [UIView animateWithDuration:0.5f animations:^{
            
            [self.view layoutIfNeeded];
            
        }];
    }else{
        //DATE PICKER Showing Animation
        self.datePickerViewBottomConstraints.constant = 0;
        [UIView animateWithDuration:0.5f animations:^{
            
            [self.view layoutIfNeeded];
            
        }];
    }


}

- (IBAction)datePickerCancelButtonAction:(id)sender {
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        self.datePickerViewBottomConstraints.constant = -255;
        [UIView animateWithDuration:0.5f animations:^{
            
            [self.view layoutIfNeeded];
            
        }];
    }else{
        self.datePickerViewBottomConstraints.constant = -200;
        [UIView animateWithDuration:0.5f animations:^{
            
            [self.view layoutIfNeeded];
            
        }];
    }
    
}


- (IBAction)datePickerDoneButtonAction:(id)sender {
    
    scheduleDate = self.datePicker.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"d MMM YYYY hh:mm a"];
    
    [dateFormat setAMSymbol:@"AM"];
    [dateFormat setPMSymbol:@"PM"];
    
    if(![self.selectedDateLabel.text isEqualToString:[dateFormat stringFromDate:scheduleDate]])
    {
        self.selectedDateLabel.text = [dateFormat stringFromDate:scheduleDate];
        [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        selectedDate = [dateFormat stringFromDate:scheduleDate];
        
        dispatch_async(dispatch_get_main_queue(),^{
            [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
        });
        
        [self publishesToSocket];
    }
    
    [self datePickerCancelButtonAction:nil];
    
}


#pragma mark - Custom Methods -

- (void)changeDate:(UIDatePicker *)sender
{
    NSLog(@"New Date: %@", sender.date);
}

-(void)showScheduleVisitViews
{
    showingScheduleVisitViews = YES;
    
    [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"back_btn_off"] forState:UIControlStateNormal];
    [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on"] forState:UIControlStateHighlighted];
    
    self.topAddressBackgroundView.hidden = NO;
    self.scheduleVisitBackgroundView.hidden = YES;
    
    self.availableLabel.hidden = YES;
    self.calenderBackGroundView.hidden = NO;
    
    dispatch_async(dispatch_get_main_queue(),^{
        self.providerDetailsTablebackgroundviewBottomConstraint.constant = -80;
        self.bottomView.hidden = YES;
    });

//    [UIView animateWithDuration:0.5f animations:^{
//        
//        [self.view layoutIfNeeded];
//    }];
    
    if(scheduleDate == nil)
    {
        scheduleDate = [NSDate dateWithTimeIntervalSinceNow:3600];
        [self.datePicker setMinimumDate:scheduleDate];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"d MMM YYYY hh:mm a"];
    
    [dateFormat setAMSymbol:@"AM"];
    [dateFormat setPMSymbol:@"PM"];
    
    self.selectedDateLabel.text = [dateFormat stringFromDate:scheduleDate];

    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    selectedDate = [dateFormat stringFromDate:scheduleDate];
    
    
    [self.datePicker setDate:scheduleDate];
    
}


-(void)showNormalViews
{
    showingScheduleVisitViews = NO;
    
    self.topAddressBackgroundView.hidden = NO;
    self.scheduleVisitBackgroundView.hidden = YES;
    
    [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_off"] forState:UIControlStateNormal];
    [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_on"] forState:UIControlStateHighlighted];
    
    self.availableLabel.hidden = NO;
    self.calenderBackGroundView.hidden = YES;

    scheduleDate = nil;
    
    dispatch_async(dispatch_get_main_queue(),^{
        if(self.arrayOfProviderTypes.count > 0)
        {
            self.providerDetailsTablebackgroundviewBottomConstraint.constant = 2;
            self.bottomView.hidden = NO;
        }
        else
        {
            self.providerDetailsTablebackgroundviewBottomConstraint.constant = -80;
            self.bottomView.hidden = YES;
        }
    });

    
//    [UIView animateWithDuration:0.5f animations:^{
//      
//        [self.view layoutIfNeeded];
//    }];

    
}

-(void)loadViewInitially
{
    if (self.arrayOfProviderTypes.count > 0)
    {
        self.providerTypesScrollerBackgroundView.hidden = YES;
        self.providerDetailsTablesbackGroundView.hidden = YES;
        self.providerAvaliableMessageView.hidden = YES;
        
        [self.topScrollView setContentOffset:CGPointMake(0, 0)];
        [self showProviderTypeScrollView];
    }
    else{
        
        self.arrayOfProviderTypes = nil;
        self.arrayOfProvidersArround = nil;
        self.arrayOfProviderEmails = nil;
        
        [self hideAllViewsAndShowNoProviderTypesMessage];
    }

}

-(void)changeButtonState:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    for (UIView *button in mBtn.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    mBtn.selected = YES;
    
}

-(void)showProviderTypeScrollView
{
    if (self.arrayOfProviderTypes.count == 0) {
        
        [UIHelper showMessage:LS(@"Sorry! we are not operational in your region at the moment, please write to info@goclean-service.com") withTitle:LS(@"Message")delegate:self];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(),^{
            
            if (self.providerTypesScrollerBackgroundView.hidden == YES)
            {
                self.providerTypesScrollerBackgroundView.hidden = NO;
                [self createDynamicProviderDetailsTableViews];
                
                //Creating Provider Types ScrollView
                if(self.arrayOfProviderTypes.count == 1)
                {
                    divider = 1;
                    width = screenSize.size.width/2;
                    self.typeDividerWidthConstraint.constant = (width-4);
                }
                else if(self.arrayOfProviderTypes.count == 2)
                {
                    divider = 2;
                    width = screenSize.size.width/divider;
                    self.typeDividerWidthConstraint.constant = (width-4);
                }
                else{
                    divider = 3;
                    width = screenSize.size.width/divider;
                    self.typeDividerWidthConstraint.constant = (width-4);
                }
                
                if(arrayOfProvidersTypeButtons.count > 0){
                    
                    [self removeAllProviderTypesInScrollView];
                }

                
                arrayOfProvidersTypeButtons = [[NSMutableArray alloc]init];
                
                UIButton *selectedBtn;
                UIButton *firstCatBtn;
                
                for (int i=0; i < self.arrayOfProviderTypes.count; i++)
                {
                    UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                    
                    buttonCategory.frame = CGRectMake(width*i,0,width,40);
                    buttonCategory.tag = i;
                    
                    [arrayOfProvidersTypeButtons addObject:buttonCategory];
                    [buttonCategory addTarget:self action:@selector(typeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if(i==0)
                    {
                        firstCatBtn = buttonCategory;
                    }
                    if ((selectedTypeButtonTag == i) && (selectedTypeButtonTag < self.arrayOfProviderTypes.count)){
                        
                        selectedBtn = buttonCategory;
                    }
                    
                    
                    buttonCategory.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:11];
                    [buttonCategory setTitleColor:UIColorFromRGB(0xc2c2c2) forState:UIControlStateNormal];
                    [buttonCategory setTitleColor:APP_COLOR forState:UIControlStateSelected];
                    
                    buttonCategory.titleLabel.numberOfLines = 2;
                    buttonCategory.titleLabel.minimumScaleFactor = 0.5;

                    
                    [buttonCategory setTitle:[[self.arrayOfProviderTypes objectAtIndex:i][@"cat_name"] uppercaseString] forState:UIControlStateNormal];
                    
                    [self.topScrollContentView addSubview:buttonCategory];
                    
                }
                
                if(self.arrayOfProviderTypes.count > 3)
                {
                  
                    self.topScrollContentViewWidthConstraint.constant = (screenSize.size.width/3)*self.arrayOfProviderTypes.count  - screenSize.size.width;
            
                }
                else
                {
                    self.topScrollContentViewWidthConstraint.constant = 0;
                }
                
                if(selectedBtn)
                {
                    [self typeButtonAction:selectedBtn];
                }
                else {
                    [self typeButtonAction:firstCatBtn];
                }
                
            }
        });
    }

}

-(void)createDynamicProviderDetailsTableViews
{
    //Creating Provider Details tableViews
    if(arrayOfProvidersDetailsTable.count > 0)
    {
        [self removeAllProviderDetailstableView];
    }

    arrayOfProvidersDetailsTable = [[NSMutableArray alloc]init];
    arrayOfMessageLabels = [[NSMutableArray alloc]init];
    
    if (self.providerDetailsTablesbackGroundView.hidden == YES)
    {
        self.providerDetailsTablesbackGroundView.hidden = NO;
        self.providerAvaliableMessageView.hidden = NO;
        
        if(showingScheduleVisitViews)
        {
            self.providerDetailsTablebackgroundviewBottomConstraint.constant = -80;
            self.bottomView.hidden = YES;
        }
        else
        {
            self.providerDetailsTablebackgroundviewBottomConstraint.constant = 2;
            self.bottomView.hidden = NO;
        }
        [self.view layoutIfNeeded];
        
        self.messageLableForAll.hidden = YES;
        
        for (int i=0; i < self.arrayOfProviderTypes.count; i++)
        {
                UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(screenSize.size.width*i,0,screenSize.size.width,self.scrollView.frame.size.height) style:UITableViewStylePlain];
                table.tag = 3+i;
                table.separatorStyle = UITableViewCellSeparatorStyleNone;
                table.backgroundColor = [UIColor clearColor];
            
                UILabel *messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenSize.size.width*i,0,screenSize.size.width,self.scrollView.frame.size.height)];
                messageLabel.tag = i;
                messageLabel.textAlignment = NSTextAlignmentCenter;
                messageLabel.numberOfLines = 0;
                messageLabel.text = LS(@"Unfortunately there are no available \nproviders at this time");
                messageLabel.font = [UIFont fontWithName:Palatino_Italic size:14];
                table.backgroundView = messageLabel;
            
                [arrayOfMessageLabels addObject:messageLabel];
                [arrayOfProvidersDetailsTable addObject:table];
        
                [self.scrollContentView addSubview:table];
                [self.scrollContentView layoutIfNeeded];
            
                table.dataSource = self;
                table.delegate = self;

            
        }
        
        if(self.arrayOfProviderTypes.count > 1)
        {
           
            self.scrollContentViewWidthConstraint.constant = (screenSize.size.width)*self.arrayOfProviderTypes.count  - screenSize.size.width;
            
        }
        else
        {
            self.scrollContentViewWidthConstraint.constant = 0;

        }
    }

}

-(void)reloadProviderDetailsTable:(UITableView *)tableView
{
    dispatch_async(dispatch_get_main_queue(),^{
        [tableView reloadData];
    });
}

-(void)reloadAllProviderDetailsTableView
{
    for(UITableView *tableView in arrayOfProvidersDetailsTable)
    {
        dispatch_async(dispatch_get_main_queue(),^{
            [tableView reloadData];
        });
    }

}

-(void)removeAllProviderTypesInScrollView
{
    for(UIButton *typeButton in self.topScrollContentView.subviews)
    {
        if([typeButton isKindOfClass:[UIButton class]])
            [typeButton removeFromSuperview];
    }
    
}

-(void)removeAllProviderDetailstableView
{
    for(UITableView *tableView in self.scrollContentView.subviews)
    {
        tableView.dataSource = nil;
        tableView.delegate = nil;
        [tableView removeFromSuperview];
    }
    for(UILabel *messageLabel in self.scrollContentView.subviews)
    {
        [messageLabel removeFromSuperview];
    }

    
}

-(void)hideAllViewsAndShowNoProviderTypesMessage
{
    self.providerTypesScrollerBackgroundView.hidden = YES;
    self.providerDetailsTablesbackGroundView.hidden = YES;
    self.providerAvaliableMessageView.hidden = YES;
    
    
    self.bottomView.hidden = YES;
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        self.providerDetailsTablebackgroundviewBottomConstraint.constant = -80;
        [self.view layoutIfNeeded];
    });
    
    self.messageLableForAll.hidden = NO;
}

-(void)hideProgressBar
{
    dispatch_async(dispatch_get_main_queue(),^{
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
    });
}

#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    UILabel *messagelabel = arrayOfMessageLabels[tableView.tag-3];
    
    NSString *str = [self.arrayOfProviderTypes[selectedTypeButtonTag][@"cat_name"]capitalizedString];
    if([str hasSuffix:@"s"])
    {
        str = [str substringToIndex:[str length]-1];
    }

    if(self.arrayOfProvidersArround.count > 0)
    {
        if([self.arrayOfProvidersArround[tableView.tag-3][@"mas"] count] > 0)
        {
            if(selectedTypeButtonTag == tableView.tag-3)
            {
//                self.availableLabel.text = [NSString stringWithFormat:LS(@"\"%@s available for booking right away are shown below\""),str];
                self.availableLabel.text = @"Providers who are available are shown below";
            }
            
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [messagelabel setHidden:YES];
            return 1;
        }
        else
        {
            if(selectedTypeButtonTag == tableView.tag-3)
            {
//                self.availableLabel.text = [NSString stringWithFormat:LS(@"\"No %@s are available now\""),[self.arrayOfProviderTypes[selectedTypeButtonTag][@"cat_name"]capitalizedString]];
                self.availableLabel.text = @"No Providers are available now";
                
            }
            
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            [messagelabel setHidden:NO];
            return 0;
        }
    }
    else
    {
        if(selectedTypeButtonTag == tableView.tag-3)
        {
//            self.availableLabel.text = [NSString stringWithFormat:LS(@"\"No %@s are available now\""),[self.arrayOfProviderTypes[selectedTypeButtonTag][@"cat_name"]capitalizedString]];
            self.availableLabel.text = @"No Providers are available now";

        }
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [messagelabel setHidden:NO];
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayOfProvidersArround[tableView.tag-3][@"mas"] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 75;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"providerDetailsCell";
    
    ProviderDetailsTableViewCell *providersDetailCell = (ProviderDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (providersDetailCell == nil)
    {
        providersDetailCell = [[[NSBundle mainBundle] loadNibNamed:@"ProviderDetailsTableViewCell" owner:nil options:nil] objectAtIndex:0];
    }
    
    
    [providersDetailCell showProviderDetails:self.arrayOfProvidersArround[tableView.tag-3][@"mas"][indexPath.row] and:self.arrayOfProviderTypes[tableView.tag-3]];
    
    return providersDetailCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ProviderDetailsViewController *proDetailsVC = (ProviderDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"providerDetailsVC"];
    
    ProviderDetailsTableViewCell *providersDetailCell = (ProviderDetailsTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    proDetailsVC.providerId = providersDetailCell.providerId;
    proDetailsVC.isLaterBooking = showingScheduleVisitViews;
    
    if(showingScheduleVisitViews)
    {
        proDetailsVC.scheduleDate = scheduleDate;
        proDetailsVC.selectedDate = selectedDate;
    }
    
    
    NSDictionary *proDetails = @{
                                 @"miles":providersDetailCell.milesAwayLabel.text,
                                 @"rating":[NSNumber numberWithFloat:providersDetailCell.ratingView.value],
                                 @"image":providersDetailCell.providerImageView.image,
                                 @"email":providersDetailCell.providerEmailId,
                                 };
    proDetailsVC.providerDetailsFromPreviousController = proDetails;
    
    NSMutableDictionary *temp = [[NSMutableDictionary alloc]initWithDictionary: self.arrayOfProviderTypes[selectedTypeButtonTag]];
    
    if(providersDetailCell.isPriceSetByProvider)
    {
        
        switch ([feeTypesArray indexOfObject:temp[@"ftype"]])
        {
                //Feixed Based
            case feeTypeFixed:
            {
                
                [temp setObject:[NSNumber numberWithDouble:providersDetailCell.providerAmountValue] forKey:@"fixed_price"];
                
            }
                break;
                
                //Hourly Based And Milleage Based
            case feeTypeHourly:
            {
                [temp setObject:[NSNumber numberWithDouble:providersDetailCell.providerAmountValue] forKey:@"price_min"];
            }
                
                break;
                
            default:
            {
                [temp setObject:[NSNumber numberWithDouble:providersDetailCell.providerAmountValue] forKey:@"price_mile"];
            }
                break;
                
        }
        
    }
    
    proDetailsVC.providerTypeDetails = temp.mutableCopy;
    
    [ud setValue:[NSString stringWithFormat:@"%td",selectedTypeButtonTag] forKey:@"selectedProviderTypeTag"];
    [ud synchronize];
    
    
    [self.navigationController pushViewController:proDetailsVC animated:YES];

}


#pragma mark - UISCrollViewDelegate Methods -

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat xContentOffset = scrollView.contentOffset.x;
    switch (scrollView.tag) {
        case 1:
        {
            //Provider Types ScrollView
            
            
        }
            break;
        case 2:
        {
            //Provider Details ScrollView
            xContentOffset = xContentOffset/divider;
            
            if(xContentOffset > 0)
            {
                self.typeDividerLeadingConstraint.constant = xContentOffset+2;
            }

        }
            break;
        default:
            break;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    CGFloat xContentOffset = scrollView.contentOffset.x;
    switch (scrollView.tag) {
        case 1:
        {
            //Provider Types ScrollView
            
//            if(xContentOffset >=0)
//            {
//                selectedTypeButtonTag = lround((xContentOffset)/width);
//                [self typeButtonAction:arrayOfProvidersTypeButtons[selectedTypeButtonTag]];
//
//            }

            
        }
            break;
        case 2:
        {
            //Provider Details ScrollView
            selectedTypeButtonTag = lround((self.typeDividerLeadingConstraint.constant-2)/width);
            [self typeButtonAction:arrayOfProvidersTypeButtons[selectedTypeButtonTag]];
            
        }
            break;
        default:
            break;
    }

}

#pragma mark - GetAddress From Pick UP VC Delegate Method -

-(void)getAddressFromPickUpAddressVC:(NSDictionary *)addressDetails
{
    NSString *addressText = flStrForStr(addressDetails[@"address"]);
//    if ([addressDetails[@"address2"] rangeOfString:addressText].location == NSNotFound) {
//        
//        addressText = [addressText stringByAppendingString:@","];
//        addressText = [addressText stringByAppendingString:flStrForStr(addressDetails[@"address2"])];
//        
//    } else {
//        
//        if([addressDetails[@"address2"]length])
//        {
//            addressText = flStrForStr(addressDetails[@"address2"]);
//        }
//        
//    }
    
    apLocation.pickupLatitude = [NSNumber numberWithFloat:[addressDetails[@"lat"] floatValue]];
    apLocation.pickupLongitude = [NSNumber numberWithFloat:[addressDetails[@"lon"] floatValue]];
    apLocation.pickUpAddress = addressText;

}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if([segue.identifier isEqualToString:@"fromListToSearchaddressVC"])
//    {
//        PickUpViewController *pickController = [segue destinationViewController];
//        pickController.latitude =  [NSString stringWithFormat:@"%f",apLocation.pickupLatitude.floatValue];
//        pickController.longitude = [NSString stringWithFormat:@"%f",apLocation.pickupLongitude.floatValue];
//        
//       
//        
//        
//        };
//    }

}

@end
