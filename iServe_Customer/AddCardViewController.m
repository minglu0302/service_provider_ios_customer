//
//  TWTAddCardViewController.m
//  iServePassenger
//
//  Created by -Tony Lu on 12/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AddCardViewController.h"
#import "HelpViewController.h"
#import "CardIO.h"

@interface AddCardViewController ()<STPPaymentCardTextFieldDelegate,CardIOPaymentViewControllerDelegate,WebServiceHandlerDelegate>
{
    STPPaymentCardTextField *paymentTextField;
    CGRect screenSize;
}
@end

@implementation AddCardViewController

#pragma mark - UILifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    screenSize = [[UIScreen mainScreen]bounds];
    
    //Setting Payment Textfield Properties
    [self setPaymentTextFieldProperties];
    
    [self.paymentBackGroundView addSubview:paymentTextField];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated{
    
    if((_isComingFromPayment ==1)||(_isComingFromPayment == 2))
    {
        self.navigationBackButton.hidden = NO;
        self.skipButton.hidden = YES;
        
    }
    else
    {
        self.navigationBackButton.hidden = YES;
        self.skipButton.hidden = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [paymentTextField becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
     [paymentTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setPaymentTextFieldProperties{
    
    paymentTextField = [[STPPaymentCardTextField alloc] init];
    
    paymentTextField.delegate = self;
    paymentTextField.cursorColor = [UIColor blueColor];
    paymentTextField.borderColor = [UIColor clearColor];
    paymentTextField.borderWidth = 0;
    paymentTextField.font = [UIFont fontWithName:OpenSans_SemiBold size:12];
    paymentTextField.textColor = [UIColor blackColor];
    paymentTextField.cornerRadius = 2.0;
    
    paymentTextField.frame = CGRectMake(0, 0, screenSize.size.width-110,40);

}

#pragma mark - UIBUtton Actions -

-(void)showMessage:(NSString *)message andTitle:(NSString *)title
{
    [[[UIAlertView alloc]initWithTitle:title
                               message:message
                              delegate:nil
                     cancelButtonTitle:LS(@"OK")
                     otherButtonTitles:nil, nil]show];
}

- (IBAction)scancardButtonAction:(id)sender {
    
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.hideCardIOLogo=YES;
    scanViewController.disableManualEntryButtons = YES;
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    scanViewController.title = LS(@"Scan Card");
    scanViewController.navigationController.navigationBarHidden = NO;
    [self presentViewController:scanViewController animated:YES completion:nil];

}

- (IBAction)navigationBackButtonAction:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doneButtonClicked:(id)sender
{
    if (![paymentTextField isValid]) {
        
        [self showMessage:LS(@"Please Enter Valid Card Details") andTitle:LS(@"Message")];
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        
        NSError *error = [NSError errorWithDomain:StripeDomain
                                             code:STPInvalidRequestError
                                         userInfo:@{
                                                    NSLocalizedDescriptionKey:LS(@"Please specify a Stripe Publishable Key")
                                                    }];
        if (error)
        {
            [self showMessage:[error localizedDescription] andTitle:LS(@"Message")];
        }
        return;
        
    }
    
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Adding Card...", @"Adding Card...")];

    //Creating token
    [[STPAPIClient sharedClient] createTokenWithCard:paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              
                                              if (error) {
                                                  
                                                  [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                                  [self showMessage:[error localizedDescription] andTitle:LS(@"Message")];
                                              }
                                              else{
                                                  
                                                  NSLog(@"%@",token.tokenId);
                                                  [self sendServiceAddCardsDetails:token.tokenId];
                                              }
                                              
                                          }];

}

- (IBAction)skipbuttonAction:(id)sender{
    
    [self gotoMenuVC];
}

#pragma mark- Card IO Delegate -

-(void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)cardInfo inPaymentViewController:(CardIOPaymentViewController *)paymentViewController{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    STPCardParams *cardParameters = [[STPCardParams alloc]init];
    
    cardParameters.number =  cardInfo.cardNumber;
    cardParameters.expMonth = cardInfo.expiryMonth;
    cardParameters.expYear = cardInfo.expiryYear;
    cardParameters.cvc = cardInfo.cvv;
    
    if([STPCardValidator validationStateForCard:cardParameters] ==  STPCardValidationStateValid){
        
        paymentTextField.cardParams = cardParameters;
        [self doneButtonClicked:nil];
        
    }
    else{
        
        [self showMessage:LS(@"Please Enter Valid Card Details") andTitle:LS(@"Message")];
        
    }
    
}

-(void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController{
    
     [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Web Services -

-(void)sendServiceAddCardsDetails:(NSString *)token
{
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken],
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_token":token,
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 };
        [[WebServiceHandler sharedInstance] sendRequestToAddCard:params andDelegate:self];
    }
    else{
        
         [[ProgressIndicator sharedInstance]hideProgressIndicator];
         [self showMessage:iServeNetworkErrormessage andTitle:LS(@"oops!")];
    }
   
}

#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [self showMessage:[error localizedDescription] andTitle:LS(@"Message")];
        return;
    }
    NSInteger errFlag = [response[@"errFlag"] integerValue];

    switch (errFlag) {
        case 1:
        {
            [self showMessage:response[@"errMsg"] andTitle:@"Message"];
            switch (_isComingFromPayment) {
                case 2:
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }
                    break;
                case 3:
                {
                    [self gotoMenuVC];

                }
                    break;

                default:
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }

                break;
            }
           
            
        }
        break;
            
        case 0:
        {
            self.arrayContainingCardInfo = response[@"cards"];
            
            if (_arrayContainingCardInfo || self.arrayContainingCardInfo.count){
                
                [[CardAddOrDeleteClass sharedInstance]updateCardsInDatabase:self.arrayContainingCardInfo];
            }
           
            switch (_isComingFromPayment) {
                case 2:
                {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }

                    break;
                    
                case 3:
                {
                    [self gotoMenuVC];
                }
                    break;
                default:
                {
                    [self.navigationController popViewControllerAnimated:YES];

                }
                    break;
            }

        }
        break;

        default:
            
            break;
    }
    
}

-(void)gotoMenuVC
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    LeftMenuVC *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"menuVC"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

@end
