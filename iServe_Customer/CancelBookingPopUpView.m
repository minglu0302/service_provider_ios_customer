//
//  CancelBookingPopUpView.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CancelBookingPopUpView.h"
#import "ShowOnGoingBookingsViewController.h"
#import "CancellationReasonsTableViewCell.h"
#import "SocketIOWrapper.h"

#define reasonButtonHeight 39

static CancelBookingPopUpView *cancelReasonPopUp = nil;

@interface CancelBookingPopUpView () <WebServiceHandlerDelegate,UIGestureRecognizerDelegate,UIAlertViewDelegate>

@end

@implementation CancelBookingPopUpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self =  [[[NSBundle mainBundle] loadNibNamed:@"CancelBookingPopUpView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance {
    
    if (!cancelReasonPopUp) {
        
        cancelReasonPopUp  = [[self alloc] initWithFrame:CGRectZero];
        cancelReasonPopUp.frame = [[UIScreen mainScreen]bounds];
        
    }
    
    cancelReasonPopUp.bookingCancelReason = @"";
    cancelReasonPopUp.selectedCancelReasonIndex = -1;
   
    return cancelReasonPopUp;
}

-(void)closeWithAnimation
{
    cancelReasonPopUp.backgroundView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         cancelReasonPopUp.backgroundView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         
                         dispatch_async(dispatch_get_main_queue(),^{
                             [cancelReasonPopUp removeFromSuperview];
                             cancelReasonPopUp = nil;
                         });
                     }];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self performSelector:@selector(updateAmSlideLeftMenu) withObject:nil afterDelay:0.6];
}
-(void)updateAmSlideLeftMenu
{
    [[AMSlideMenuMainViewController getInstanceForVC:[[[UIApplication sharedApplication]delegate]window].rootViewController] disableSlidePanGestureForLeftMenu];
}

/*-(void)showCancellReasonsScrollView
{
    for (int i=0; i < self.arrayOfCancelReasons.count; i++)
    {
        UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
        
        buttonCategory.frame = CGRectMake(0,(reasonButtonHeight + 1)*i,self.scrollView.frame.size.width,reasonButtonHeight);
        buttonCategory.tag = i;
    
        [buttonCategory addTarget:self action:@selector(cancelReasonButtonaction:) forControlEvents:UIControlEventTouchUpInside];
        
        buttonCategory.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:13.5];
        [buttonCategory setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
        
        [buttonCategory setImage:[UIImage imageNamed:@"cancellation_reason_correct_icon"] forState:UIControlStateSelected];
        
        [buttonCategory setTitle:self.arrayOfCancelReasons[i] forState:UIControlStateNormal];
        
        [self.scrollcontentView addSubview:buttonCategory];
        
        if(i < self.arrayOfCancelReasons.count - 1)
        {
            UIView *divider = [[UIView alloc]initWithFrame:CGRectMake(0, i+reasonButtonHeight*(i+1), self.scrollView.frame.size.width, 1)];
            divider.backgroundColor = UIColorFromRGB(0xebebeb);
            [self.scrollcontentView addSubview:divider];
        }
        
    }
    
    if(self.arrayOfCancelReasons.count > 4)
    {
        self.scrollContentViewHeightConstraint.constant = (reasonButtonHeight +1)*self.arrayOfCancelReasons.count  - self.scrollView.frame.size.height;
    }
    else
    {
        self.scrollContentViewHeightConstraint.constant = 0;
    }
}

-(void)cancelReasonButtonaction:(id)sender
{
    UIButton *reasonBtnType = (UIButton *)sender;
    if(reasonBtnType.selected == NO)
    {
        for (UIButton *button in reasonBtnType.superview.subviews) {
            
            if ([button isKindOfClass:[UIButton class]])
            {
                [(UIButton *)button setSelected:NO];
                button.userInteractionEnabled = YES;
                button.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
                button.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
            }
        }
        reasonBtnType.titleEdgeInsets = UIEdgeInsetsMake(0,-25, 0, 0);
        reasonBtnType.imageEdgeInsets = UIEdgeInsetsMake(0,230,0,0);
        reasonBtnType.selected = YES;
        reasonBtnType.userInteractionEnabled = NO;
        cancelReasonPopUp.bookingCancelReason = reasonBtnType.titleLabel.text;//[UIHelper getBookingCancelReason:reasonBtnType.tag];
    }

}*/

#pragma mark - Custom Methods -

-(void)setTopViewFrame
{
    CGSize screenSize = [[UIScreen mainScreen]bounds].size;
    
    if (screenSize.height < 568)
    {
        self.topViewTopConstraint.constant = 60;
        self.topViewBottomConstraint.constant = 60;
    }
    else if(screenSize.height < 667)
    {
        self.topViewTopConstraint.constant = 80;
        self.topViewBottomConstraint.constant = 80;
    }
    else if(screenSize.height < 736)
    {
        self.topViewTopConstraint.constant = 120;
        self.topViewBottomConstraint.constant = 120;
    }
    else
    {
        self.topViewTopConstraint.constant = 150;
        self.topViewBottomConstraint.constant = 150;
    }
    
    [self layoutIfNeeded];
    
}


#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfCancelReasons.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width-60, 0)];
    
    label.text = self.arrayOfCancelReasons[indexPath.row];
    label.font = [UIFont fontWithName:OpenSans_Regular size:15];
    
    float height = [UIHelper measureHeightLabel:label];
    [tableView layoutIfNeeded];
    
    return height+((height/20)*2)+20;

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cancellationReasonsCell";
    
    CancellationReasonsTableViewCell *cancelReasonCell = (CancellationReasonsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cancelReasonCell == nil)
    {
        cancelReasonCell = [[[NSBundle mainBundle] loadNibNamed:@"CancelBookingPopUpView" owner:nil options:nil] objectAtIndex:1];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width-60, 0)];
        
        label.text = self.arrayOfCancelReasons[indexPath.row];
        label.font = [UIFont fontWithName:OpenSans_Regular size:15];
        
        float height = [UIHelper measureHeightLabel:label];

        height = height+((height/20)*2)+20;
        
        cancelReasonCell.frame = CGRectMake(0, 0, tableView.frame.size.width,height);
    }
    
    cancelReasonCell.reasonTitleLabel.text = self.arrayOfCancelReasons[indexPath.row];
    
    if(cancelReasonPopUp.selectedCancelReasonIndex == indexPath.row){
        
        [cancelReasonCell.reasonButton setBackgroundImage:[UIImage imageNamed:@"cancellation_reason_correct_icon"] forState:UIControlStateNormal];
        cancelReasonPopUp.bookingCancelReason = self.arrayOfCancelReasons[indexPath.row];
    }
    else {
        
        [cancelReasonCell.reasonButton setBackgroundImage:[UIImage imageNamed:@"cancellation_reason_unselect_icon"] forState:UIControlStateNormal];
    }
    
    
    return cancelReasonCell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cancelReasonPopUp.bookingCancelReason = self.arrayOfCancelReasons[indexPath.row];
    cancelReasonPopUp.selectedCancelReasonIndex = indexPath.row;
    [self.tableView reloadData];
}



#pragma mark - UIButton Actions -

- (IBAction)submitButtonAction:(id)sender
{
    if(cancelReasonPopUp.bookingCancelReason.length != 0)
    {
        [cancelReasonPopUp sendRequestToCancelBooking];
        
    }
    else{
        
        [UIHelper showMessage:LS(@"Please select a cancellation reason") withTitle:LS(@"Message")delegate:nil];
        
    }
}

- (IBAction)closeButtonAction:(id)sender {
    
    [self closeWithAnimation];
}


//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch  *)touch
//{
////    if ([touch.view isDescendantOfView:self.topView] || [touch.view isDescendantOfView:self.bottomView])
////    {
////        return NO;
////    }
////    else{
////        
////        [self closeWithAnimation];
////        return YES;
////    }
//    
//}

#pragma mark - Web Service Call -

- (void)sendRequestToCancelBooking
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Booking Cancel...", @"Booking Cancel...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_reason":flStrForStr(cancelReasonPopUp.bookingCancelReason),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToCancelBooking:params andDelegate:self];
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}

- (void)sendRequestToGetCancelReasons
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_lan":[NSString stringWithFormat:@"%td",[[NSUserDefaults standardUserDefaults]integerForKey:iServeUserSelectedLanguageNumber]],
                                 @"ent_user_type":[NSString stringWithFormat:@"%d",2],
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetCancelReasons:params andDelegate:self];
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:nil];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeCancelBooking)
            {
                
//                NSString *customerId = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID];
//                NSDictionary *message = @{
//                                          @"cid":[NSNumber numberWithInteger:customerId.integerValue],
//                                          @"bid":[NSNumber numberWithInteger:self.bookingId],
//                                          @"dt":[UIHelper getCurrentDateTime],
//                                          @"can_reason":flStrForStr(cancelReasonPopUp.bookingCancelReason),
//                                        };
//                
//                NSLog(@"Cancel Booking Parameters:%@",message);
//                
//                [[SocketIOWrapper sharedInstance] publishMessageToChannel:@"cancelBooking" withMessage:message];
                
                [self closeWithAnimation];
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
                [[ShowOnGoingBookingsViewController getSharedInstance]removeReminderFromCalendar];
                [[ShowOnGoingBookingsViewController getSharedInstance]navigationBackButtonAction:nil];
            }
            else if (requestType == RequestTypeGetCancelReasons)
            {
                self.arrayOfCancelReasons = response[@"data"];
//                [self showCancellReasonsScrollView];
                [self.tableView reloadData];
            }
        }
        default:
            break;
    }
    
}

@end
