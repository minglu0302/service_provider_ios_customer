//
//  AppDelegate.h
//  Twotray
//
//  Created by -Tony Lu on 12/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AFNetworkActivityIndicatorManager.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import <AWSS3/AWSS3.h>
#import "SocketIOWrapper.h"

@class GTMOAuth2Authentication;

/**
 * UIKeyboard Delegate Methods
 */
@protocol UIKeyboardDelegates <NSObject>

@optional

-(void)keyboardWillShown:(__unused NSNotification *)inputViewNotification;
-(void)keyboardWillHide:(__unused NSNotification *)inputViewNotification;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate,SocketWrapperDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (readonly, assign, nonatomic) AFNetworkReachabilityStatus networkStatus;

/**
 *  Id of UIKeyboard Delegate Methods
 */
@property (nonatomic,assign) id keyboardDelegate;



- (void)setSocketDelegateToAppDelegate;
- (void)playNotificationSound;
- (void)registerPushNotification;
- (void)registerAppForNotifications;
- (void)unregisterAppForNotifications;

@end

