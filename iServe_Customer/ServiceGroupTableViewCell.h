//
//  ServiceGroupTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceGroupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *serviceNotAddedView;
@property (weak, nonatomic) IBOutlet UILabel *serviceGroupNameLabel;

@property (weak, nonatomic) IBOutlet UIView *serviceAddedView;
@property (weak, nonatomic) IBOutlet UILabel *serviceGroupNameWhenServiceAdded;
@property (weak, nonatomic) IBOutlet UILabel *serviceLabel;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;

-(void)showEachRowDetails:(NSMutableArray *)selectedServiceDetails groupDetails:(NSArray *)serviceGroup indexValue:(NSIndexPath *)indexPath;

@end
