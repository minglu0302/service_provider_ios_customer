//
//  ProfileViewController.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 6/29/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UITextFieldDelegate,WebServiceHandlerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIButton *profileImageBackGroundButton;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UIButton *countryCodeButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *logoutBackGroundView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;

@property (weak, nonatomic) IBOutlet UIView *languagePickerBackgroundView;
@property (weak, nonatomic) IBOutlet UIPickerView *languagePickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *languagePickerBackgroundViewBottomConstraint;


@property NSDictionary *responseDict;
@property (strong, nonatomic) UITextField *activeTextfield;

- (IBAction)profileImageButtonAction:(id)sender;
- (IBAction)logoutButtonAction:(id)sender;
- (IBAction)editButtonAction:(id)sender;
- (IBAction)profileTapGestureAction:(id)sender;
- (IBAction)changePasswordButtonAction:(id)sender;
- (IBAction)languageButtonAction:(id)sender;
- (IBAction)languagePickerCanelButtonAction:(id)sender;
- (IBAction)languagePickerDoneButtonAction:(id)sender;

@end
