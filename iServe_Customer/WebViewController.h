//
//  TWTWebViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
/**
 *  Topview Constraints outlet
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topview;
/**
 *  WebView outlet
 */
@property (weak, nonatomic) IBOutlet UIWebView *webview;
/**
 *  Sender String for getting the url
 */
@property(nonatomic,strong) NSString *senderString;

/**
 *  Back button action
 */
@property (weak, nonatomic) IBOutlet UIButton *backButton;
/**
 *  Back Button Action for going to root view controller
 *
 *  @param sender 
 */
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
- (IBAction)backButton:(id)sender;

@end
