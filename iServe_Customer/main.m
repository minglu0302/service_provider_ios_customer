
//
//  main.m
//  Twotray
//
//  Created by -Tony Lu on 12/05/16.
//  Copyright © 2016 -Tony Lu. All rights reiserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char * argv[]){
    @autoreleasepool{
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
