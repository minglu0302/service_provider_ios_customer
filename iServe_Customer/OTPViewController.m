//
//  RPOtpViewController.m
//  iServePassenger
//
//  Created by -Tony Lu on 22/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "OTPViewController.h"
#import "AddCardViewController.h"
#import "UploadImagesToAmazonServer.h"

@interface OTPViewController ()<UIKeyboardDelegates>
{
    NSString *verificationCode;
    NSString *signupPhoneno;
    NSString *profilePicURL;
}
@end

@implementation OTPViewController

#pragma mark - UILife Cycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    profilePicURL = @"";
    
    signupPhoneno = [NSString stringWithFormat:@"%@%@",[self.signUpDictionary objectForKey:iServeUserCountryCode],[self.signUpDictionary objectForKey:iServeUserMobile]];
    
    [self getOTP:signupPhoneno];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width,CGRectGetHeight(self.resendSMSButton.frame)+CGRectGetMinY(self.resendSMSButton.frame)+30);
    [self.firstNumber becomeFirstResponder];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = nil;
}

#pragma mark - UITapGestureRecognizer


- (IBAction)otpTabGesture:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)resendSMSButton:(id)sender
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Sending OTP...", @"Sending OTP...")];
    
    [self getOTP:signupPhoneno];
}



#pragma mark - WebServiceCall -

-(void)getOTP:(NSString *)mobileNumber
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTMobile:(mobileNumber)
                                };
        [[WebServiceHandler sharedInstance] sendRequestToGetOTP:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}

-(void)verifyOTP:(NSString *)otpNumber
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Verifying OTP...", @"Verifying OTP...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTMobile:signupPhoneno,
                                 @"ent_code":otpNumber,
                                 @"ent_user_type":@"2",
                                 @"ent_service_type":@"1"
                                };
        [[WebServiceHandler sharedInstance] sendRequestToVerifyOTP:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}


#pragma mark - WebServiceDelegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message") delegate:self];
        return;
    }
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    
    if (errFlag == 1)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
        
        if(requestType == RequestTypeVerifyOTP)
        {
            _firstNumber.text = _secondNumber.text = _thirdNumber.text = _fourthNumber.text =  @"";
            [_firstNumber becomeFirstResponder];
        }
    }
    else if (errFlag==0)
    {
        switch (requestType)
        {
            case RequestTypeGetOTP:
            {
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                verificationCode = [NSString stringWithFormat:@"%@",response[@"code"]];
                NSLog(@"response %@",verificationCode);
                
            }
                break;
                
            case RequestTypeVerifyOTP:
            {
                [self uploadProfileImageToAmazon];
            }
                break;
            case RequestTypeSignUp:
            {
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
//                NSString *password = self.signUpDictionary[iServeUserPassword];
                NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
                
                [ud setObject:flStrForStr(response[@"CustId"]) forKey:iServeCustomerID];
                [ud setObject:self.signUpDictionary[iServeUserFirstName] forKey:iServeUserFirstName];
                [ud setObject:self.signUpDictionary[iServeUserLastName] forKey:iServeUserLastName];
                [ud setObject:flStrForStr(response[@"token"]) forKey:iServeCheckUserSessionToken];
                [ud setObject:flStrForStr(response[@"apiKey"]) forKey:iServeAPIKey];
                [ud setObject:flStrForStr(response[@"coupon"]) forKey:iServeCouponkey];
                [ud setObject:flStrForStr(response[@"email"])  forKey:iServeUserEmail];
                [ud setObject:self.signUpDictionary[iServeUserPassword] forKey:iServeUserPassword];
                [ud setObject:flStrForStr(response[@"currency"]) forKey:iServeCurrentCountryCurrencySymbol];
                [ud setObject:flStrForStr(profilePicURL) forKey:iServeUserProfilepic];
                
                
                
                if(response[@"stripe_pub_key"])
                {
                    [ud setObject:response[@"stripe_pub_key"] forKey:iServeStripeKey];
                    [Stripe setDefaultPublishableKey:response[@"stripe_pub_key"]];
                }

                
                if([response[@"Languages"]count] > 0)
                {
                    [ud setObject:response[@"Languages"]  forKey:iServeUserLanguages];
                    [ud setInteger:0 forKey:iServeUserSelectedLanguageNumber];
                }

                [ud synchronize];
                [self gotoNextController];
                
            }
                break;
                
            default:
                break;
        }
    }
}


-(void)gotoNextController{
    
    [self performSegueWithIdentifier:@"TWTAddCardViewController" sender:nil];
}

#pragma mark - Sign UP -

-(void)uploadProfileImageToAmazon
{
    [[ProgressIndicator sharedInstance] changePIMessage:NSLocalizedString(@"Signing up...", @"Signing up...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        [[UploadImagesToAmazonServer sharedInstance]uploadProfileImageToServer:self.signUpDictionary[iServeUserProfilepic]
                                                                      andEmail:self.signUpDictionary[iServeUserEmail]
                                                                       andType:0
                                                                withCompletion:^(NSString *imageURL)
         {
             profilePicURL = imageURL;
             [self signUp];
         }];
        
    }
    
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}

-(void)signUp
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        //Upload Profile Image To Amazon
//        [[UploadImagesToAmazonServer sharedInstance]uploadProfileImageToServer:self.signUpDictionary[iServeUserProfilepic] andEmail:self.signUpDictionary[iServeUserEmail] andType:0];
        

        NSDictionary *temp = @{
                                 kTWTFirstName:flStrForStr(self.signUpDictionary[iServeUserFirstName]),
                                 kTWTLastName:flStrForStr(self.signUpDictionary[iServeUserLastName]),
                                 kTWTEmail:flStrForStr(self.signUpDictionary[iServeUserEmail]),
                                 @"ent_country_code":flStrForStr(self.signUpDictionary[iServeUserCountryCode]),
                                 kTWTPhone:flStrForStr([self.signUpDictionary objectForKey:iServeUserMobile]),
                                 kTWTPassword:flStrForStr(self.signUpDictionary[iServeUserPassword]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 kTWTSignupAccessToken:@"",
                                 kTWTSignupTermscond:@"1",
                                 kTWTSignupPricing:@"1",
                                 kTWTDeviceType:flStrForStr([Utilities getDeviceType]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 @"ent_profile_pic":flStrForStr(profilePicURL),
                                 @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                 @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                                 @"ent_dev_model":[UIHelper getModelName],
                                 @"ent_manf":@"APPLE",
                                 kTWTLatitude:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLat]),
                                 kTWTLongitude:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLong]),
                                 kTWTPushToken:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeFCMTokenKey]),
                                 @"ent_fbid":flStrForStr(self.signUpDictionary[iServeUserFBPassword])
                                 
                                };
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:temp];
        
        //Adding Referral Code
        if([self.signUpDictionary[iServeReferralCode]length] > 0){
            
            [params setObject:self.signUpDictionary[iServeReferralCode] forKey:@"ent_referral_code"];
        }
        if(flStrForStr(self.signUpDictionary[iServeUserFBPassword]).length > 0){
            
            [params setObject:[NSNumber numberWithInt:2] forKey:@"ent_signup_type"];
        }
        else {
            
            [params setObject:[NSNumber numberWithInt:1] forKey:@"ent_signup_type"];
        }
 
        [[WebServiceHandler sharedInstance] sendRequestToSignUP:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}

#pragma mark - UIButton Actions -

- (IBAction)navigationBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)registerButtonAction:(id)sender {
    
    if (_firstNumber.text.length != 0 && _secondNumber.text.length != 0 && _thirdNumber.text.length != 0 && _fourthNumber.text.length != 0 ) {
        
        NSString *code = [NSString stringWithFormat:@"%@%@%@%@",_firstNumber.text,_secondNumber.text,_thirdNumber.text,_fourthNumber.text];
        
//        if ([code isEqualToString:verificationCode]) {
        
//            [self signUp];
            [self verifyOTP:code];
//        }
//        else {
        
//            _firstNumber.text = _secondNumber.text = _thirdNumber.text = _fourthNumber.text =  @"";
//            [_firstNumber becomeFirstResponder];
//            [UIHelper showMessage:LS(@"Seems like you have entered wrong verification code") withTitle:LS(@"Message")delegate:self];
//        }
        
    }
    else {
        
        [UIHelper showMessage:LS(@"Please enter the code sent to your registered mobile number") withTitle:LS(@"Message")delegate:self];
    }
    
}

#pragma mark - UITextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self findNextResponder:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL shouldProcess = NO; //default to reject
    BOOL shouldMoveToNextField = NO; //default to remaining on the current field
    
    NSUInteger insertStringLength = [string length];
    if(insertStringLength == 0){ //backspace
        shouldProcess = YES; //Process if the backspace character was pressed
    }
    else {
        if([[textField text] length] == 0) {
            shouldProcess = YES; //Process if there is only 1 character right now
        }
    }
    
    //here we deal with the UITextField on our own
    if(shouldProcess){
        //grab a mutable copy of what's currently in the UITextField
        NSMutableString* mstring = [[textField text] mutableCopy];
        if([mstring length] == 0){
            //nothing in the field yet so append the replacement string
            [mstring appendString:string];
            
            shouldMoveToNextField = YES;
        }
        else{
            //adding a char or deleting?
            if(insertStringLength > 0){
                [mstring insertString:string atIndex:range.location];
            }
            else {
                //delete case - the length of replacement string is zero for a delete
                [mstring deleteCharactersInRange:range];
            }
        }
        
        //set the text now
        [textField setText:mstring];
        
        
        if (shouldMoveToNextField) {
            //MOVE TO NEXT INPUT FIELD HERE
            
            [self findNextResponder:textField];
        }
    }
    
    //always return no since we are manually changing the text field
    return NO;
}

-(void)findNextResponder:(UITextField *)textField {
    
    switch (textField.tag) {
            
        case 0:
            if(textField == self.firstNumber)
            {
                [self.secondNumber becomeFirstResponder];
            }
            
            break;
        case 1:
            if(textField == self.secondNumber)
            {
                [self.thirdNumber becomeFirstResponder];
            }
            
            break;
        case 2:
            if (textField == self.thirdNumber)
            {
                [self.fourthNumber becomeFirstResponder];
            }
            
            break;
        case 3:
            if (textField == self.fourthNumber)
            {
                [textField resignFirstResponder];
                [self registerButtonAction:nil];
                
            }
            break;
            
        default:
            [textField resignFirstResponder];
            break;
    }
    
    
}

#pragma mark - KeyBoardHideAndShow -
/**
 *   This method will called when the user wants the keyboard's
 */
-(void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

-(void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}



#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"TWTAddCardViewController"])
    {
        AddCardViewController *addCardVC = (AddCardViewController*)[segue destinationViewController];
        addCardVC.isComingFromPayment = 3;
        
    }
    
}

@end
