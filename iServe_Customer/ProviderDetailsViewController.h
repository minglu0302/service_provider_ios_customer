//
//  ProviderDetailsViewController.h
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderDetailsViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;

//Header View
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewWidthConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *bookButton;


@property BOOL isFromBookingFlowScreen;
@property BOOL isLaterBooking;
@property (strong, nonatomic) NSDate *scheduleDate;
@property (strong, nonatomic) NSString *selectedDate;
@property NSInteger providerId;
@property NSDictionary *providerDetailsFromPreviousController;
@property NSMutableDictionary *providerTypeDetails;


+ (instancetype) getSharedInstance;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)pageIndicatorAction:(id)sender;
- (IBAction)bookButtonAction:(id)sender;

@end
