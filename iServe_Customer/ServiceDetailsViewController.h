//
//  ServiceDetailsViewController.h
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Getting Selected Service Details
 */
@protocol SelectedServiceDetailsDelegate <NSObject>

@optional

-(void)getSelectedServiceDetailsFromServiceDetailVC:(NSMutableArray *)selectedServices;

@end

@interface ServiceDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;


@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,assign) id selectedServiceDetailDelegate;

@property (strong , nonatomic) NSArray *arrayOfServices;
@property (strong , nonatomic) NSMutableArray *arrayOfSelectedServices;
@property (strong , nonatomic) NSMutableArray *arrayOfSelectedServicesForAllGroups;
@property (strong , nonatomic) NSString *isMultipleSelection;

@property NSInteger serviceGroupNumber;

- (IBAction)addButtonAction:(id)sender;
- (IBAction)navigationLeftButtonaction:(id)sender;

@end
