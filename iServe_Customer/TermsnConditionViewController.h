//
//  TermsnConditionViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 13/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsnConditionViewController : UIViewController

/**
 *  Navigation back button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationBackbutton;
/**
 *  Terms condition button action outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *termsCondition;
/**
 *  Privacy policy button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *privacyPolicy;
/**
 *  Title string for printing the title in navigation bar
 */
@property (nonatomic,strong) NSString *titlestring;
/**
 *  Back Button action for go back to root view controller
 *
 *  @param sender
 */
- (IBAction)backButton:(id)sender;
/**
 *  Terms Condition button action
 *
 *  @param sender
 */
- (IBAction)termsConditionButton:(id)sender;
/**
 *  Privacy policy button action
 *
 *  @param sender 
 */
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
- (IBAction)privacyPolicyButton:(id)sender;

@end
