//
//  ProviderBookingViewController.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderBookingViewController.h"
#import "AddressDetailTableViewCell.h"
#import "PromoCodeTableViewCell.h"
#import "JobDetailsTableViewCell.h"
#import "JobPhotosTableViewCell.h"
#import "PaymetDetailTableViewCell.h"
#import "FeeEstimateTableViewCell.h"
#import "AddressManageViewController.h"
#import "PaymentViewController.h"
#import "UploadImagesToAmazonServer.h"
#import "AMSlideMenuLeftTableViewController.h"
#import "ServiceDetailsTableViewCell.h"
#import "ServiceGroupViewController.h"
#import "AdressWrapperClass.h"

#define MAX_SEC 150

@interface ProviderBookingViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AddressManageDelegate,SocketWrapperDelegate,WebServiceHandlerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UIScrollViewDelegate,ServiceDetailsDelegate,UIKeyboardDelegates>
{
    
    NSArray *sectionTitleArray;
    AppointmentLocation *ap;
    UITextView *activeTextView;
    UITextField *activeTextField;
    NSString *jobDetails;
    UIImage *pickedImage;
    UIImagePickerController *imagePicker;
    NSMutableArray *jobImagesArray;
    
    JobPhotosTableViewCell *jobPhotosCell;
    PromoCodeTableViewCell *promoCodeCell;
    
    FeeEstimateTableViewCell *feeEstimateCell;
    UIWindow *window;
    SocketIOWrapper *socketWrapper;
    NSString *patientEmail;
    NSString *selectedProviderType;
    NSInteger remainingSeconds;
    NSTimer *bookingTimer;
    
    NSString *customerId;
    NSString *bid;
    
    NSDictionary *currentlyBookingSentedProviderDetails;
    NSInteger elapsedSeconds;
    BOOL isBookingSend;
    NSTimer *publishToSocketTimer;
    
    //Booking Queue
    NSMutableArray *updatedProvidersArray;
    NSMutableArray *alreadyRequestSentedArray;
    UIButton *deleteJobPhotoButton;
    
    
    //Fee Estimate
    FeeDetailsClass *feeDetails;
    NSArray *feeTypesArray;
    NSString *discountType;
    NSInteger discountAmount;
    NSInteger numberOfHourValue;
    
    NSUserDefaults *ud;
    JobDetailsTableViewCell *jobDetailCell;
    
    UIBackgroundTaskIdentifier bgTask;
    UIApplication *app;
    CGRect screenSize;
    
    UILabel *navigationTitleLabel;
    NSMutableAttributedString *navigationAttributedTitle;
    
    NSArray *arrayOfServiceGroups;
    NSMutableArray *arrayOfSelectedServices;
    NSMutableArray *arrayOfServiceAddedIndexValues;
    
    BOOL isServiceMandatory;
    BOOL isMandatoryServicesSelected;
    BOOL isServiceGroupAvailableOrNot;
    
    double pickupLat;
    double pickupLong;
    NSString *pickupAddress;
    
    CGFloat servicesTotalAmount;
    
    BOOL isShowingRequestingScreen;
    BOOL isServiceCategoryAvailable;
    
    UIAlertController *actionSheet;
    
}

@end

static ProviderBookingViewController *providerBookingVC = nil;


@implementation ProviderBookingViewController

@synthesize requestingView;

#pragma mark - Initial Methods -

+ (instancetype) getSharedInstance
{
    return providerBookingVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    screenSize = [[UIScreen mainScreen]bounds];
    remainingSeconds = MAX_SEC;
    elapsedSeconds = 0;
    
    ud = [NSUserDefaults standardUserDefaults];
    
    // Do any additional setup after loading the view from its nib.
    updatedProvidersArray = [[NSMutableArray alloc]init];
    
    if(self.arrayOfProviderDetailsFromHomeVC.count > 0)
    {
        updatedProvidersArray = self.arrayOfProviderDetailsFromHomeVC;
    }
    
    alreadyRequestSentedArray = [[NSMutableArray alloc]init];
    selectedProviderType = [ud objectForKey:@"selectedProviderTypeTag"];
    
    sectionTitleArray = @[
                          LS(@"JOB LOCATION"),
                          LS(@"PROMO CODE"),
                          LS(@"SERVICE DETAILS"),
                          LS(@"JOB DETAILS"),
                          LS(@"JOB PHOTOS"),
                          LS(@"PAYMENT"),
                          LS(@"FEE ESTIMATE")
                        ];
    
    ap = [AppointmentLocation sharedInstance];
    jobImagesArray = [[NSMutableArray alloc]init];
    
    providerBookingVC = self;
    window = [[UIApplication sharedApplication] keyWindow];
    socketWrapper = [SocketIOWrapper sharedInstance];
    
    arrayOfSelectedServices = [[NSMutableArray alloc]init];
    arrayOfServiceAddedIndexValues = [[NSMutableArray alloc]init];
    customerId = [ud objectForKey:iServeCustomerID];
    patientEmail = [ud objectForKey:iServeUserEmail];
    
    feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
    numberOfHourValue = 1;
    discountType = @"";
    discountAmount = 0;
    servicesTotalAmount = 0;
    
    app = [UIApplication sharedApplication];
    
    self.ratingView.markFont = [UIFont systemFontOfSize:18];
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    
    if([self.providerTypeDetails[@"IsGroupContain"]integerValue]== 1)
    {
        isServiceGroupAvailableOrNot = YES;
        [self sendServiceToGetServiceGroupDetails];
    }
    
    pickupAddress = ap.pickUpAddress;
    pickupLat = ap.pickupLatitude.doubleValue;
    pickupLong = ap.pickupLongitude.doubleValue;
    
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setFeeEstimateDetails];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;
    
    socketWrapper.socketDelegate = self;
    
    
    if(ap.bookingType == 1)
    {
        isServiceCategoryAvailable = YES;
        //Views For Dispatch Queue
        //Timer To publish To Socket
        publishToSocketTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(publishingToSocket) userInfo:nil repeats:YES];
        
        self.headerView.hidden = YES;
        CGRect frame = self.headerView.frame;
        frame.size.height = 0;
        self.headerView.frame = frame;
        [self.tableView layoutIfNeeded];
        
        self.title = LS(@"Confirmation");
        //        self.navigationTitleLabel.textAlignment = NSTextAlignmentLeft;
        
        self.tableViewTopConstraint.constant = 0;
        
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_off"] forState:UIControlStateNormal];
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_on"] forState:UIControlStateHighlighted];
        
        [self setRequestButtonTitle:[self.providerTypeDetails[@"cat_name"] capitalizedString]];
    }
    else
    {
        //Views For withOut Dispatch Queue and Later Booking
        [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [UIImage new];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.view.backgroundColor = [UIColor clearColor];
        self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
        
        self.title = @"";
        
        //Show Navigation Title
        navigationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
        navigationTitleLabel.backgroundColor = [UIColor clearColor];
        navigationTitleLabel.numberOfLines = 0;
        navigationTitleLabel.textAlignment = NSTextAlignmentCenter;
        navigationTitleLabel.adjustsFontSizeToFitWidth = YES;
        
        
        self.tableViewTopConstraint.constant = -64;
        
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"back_btn_off"] forState:UIControlStateNormal];
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on"] forState:UIControlStateHighlighted];
        
        self.providerImageView.image = self.providerDetailsFromPreviousController[@"image"];
        self.milesLabel.text = self.providerDetailsFromPreviousController[@"miles"];
        self.ratingView.value = [self.providerDetailsFromPreviousController[@"rating"]floatValue];
        self.providerNameLabel.text = self.providerDetailsFromPreviousController[@"name"];
        
        [self setRequestButtonTitle: [self.providerNameLabel.text capitalizedString]];
        [self setNavigationTitle];
        
        if(ap.bookingType == 2)
        {
            isServiceCategoryAvailable = YES;
            //Timer To publish To Socket
            publishToSocketTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(publishingToSocket) userInfo:nil repeats:YES];
            
        }
        
    }
    [self.tableView setContentOffset:CGPointMake(0,0) animated:NO];
    [self.tableView reloadData];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(ap.bookingType != 1)
    {
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setBackgroundImage:nil
                                                      forBarMetrics:UIBarMetricsDefault];
        self.navigationController.view.backgroundColor = [UIColor whiteColor];
        self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    }
    [self stopTimer];
    [publishToSocketTimer invalidate];
    publishToSocketTimer = nil;
    
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appD setSocketDelegateToAppDelegate];
    
    appD.keyboardDelegate = nil;
    
}


#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return sectionTitleArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //For Service Details Section No Of Rows
    if(section == 2)
    {
        
        //To Show Total Service Fee
        if(arrayOfServiceAddedIndexValues.count > 0)
            return arrayOfServiceAddedIndexValues.count + 1;
        //            numberOfRows = numberOfRows + 1;
        else
            return 0;
    }
    
    //For Fee Details Section
    if(section == sectionTitleArray.count-1)
    {
        NSInteger numberOfrows = 0;
        switch ([feeTypesArray indexOfObject:self.providerTypeDetails[@"ftype"]])
        {
            case feeTypeFixed:
            {
                numberOfrows = 2;
                //If any Services Available And Selected Any Service
                if(isServiceGroupAvailableOrNot && [arrayOfServiceAddedIndexValues count] > 0)
                {
                    numberOfrows = numberOfrows + 1;
                }
                
                //is Any Discount Added
                if(discountType.length>0)
                {
                    numberOfrows = numberOfrows + 1;
                }
                
                //For Slot Later Booking
                if(ap.bookingType == 3)
                {
                    numberOfrows = numberOfrows + 1;
                }
                return numberOfrows;
            }
                break;
                
            default:
            {
                numberOfrows = 3;
                
                //If any Services Available And Selected Any Service
                if(isServiceGroupAvailableOrNot && [arrayOfServiceAddedIndexValues count] > 0)
                {
                    numberOfrows = numberOfrows + 1;
                }
                
                //is Any Discount Added
                if(discountType.length>0)
                {
                    numberOfrows = numberOfrows + 1;
                }
                //For Slot Later Booking
                if(ap.bookingType == 3)
                {
                    numberOfrows = numberOfrows + 1;
                }
                
                return numberOfrows;
                
            }
                break;
        }
    }
    return 1;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    float width = tableView.bounds.size.width;
    float Height = 24;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, Height)];
    view.backgroundColor = [UIColor whiteColor];
    
    //For Service Details Section Header When No Services Available
    if(isServiceGroupAvailableOrNot == NO && section == 2)
    {
        view.frame = CGRectMake(0, 0, 0, 0);
        return view;
    }
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0,(screenSize.size.width-20)/2,22)];
    label.text = sectionTitleArray[section];
    label.textColor = APP_COLOR;
    label.font = [UIFont fontWithName:OpenSans_Regular size:11];
    
    UILabel *divider = [[UILabel alloc] initWithFrame:CGRectMake(10,22,width-20,0.5)];
    divider.backgroundColor = UIColorFromRGB(0xcccccc);
    
    [view addSubview:label];
    [view addSubview:divider];
    
    //For Service Details Section Header When Services Available
    if(isServiceGroupAvailableOrNot && section == 2)
    {
        UIButton *addServices = [UIButton buttonWithType:UIButtonTypeCustom];
        addServices.frame = CGRectMake(20+((screenSize.size.width-20)/2), 0, ((screenSize.size.width-20)/2)-15, 22);
        
        [addServices setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        [addServices setTitleColor:UIColorFromRGB(0xF5070B) forState:UIControlStateNormal];
        [addServices setTitleColor:UIColorFromRGB(0xffcccc) forState:UIControlStateHighlighted];
        addServices.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:10];
        [addServices addTarget:self action:@selector(addServicesButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        //If Services Added Or Not
        if([arrayOfServiceAddedIndexValues count] > 0)
        {
            [addServices setTitle:LS(@"EDIT SERVICE") forState:UIControlStateNormal];
            addServices.hidden = NO;
        }
        else
        {
            [addServices setTitle:LS(@"+ ADD SERVICE") forState:UIControlStateNormal];
            addServices.hidden = YES;
        }
        
        [view addSubview:addServices];
    }
    
    
    return view;
    
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,5)];
    view.backgroundColor = UIColorFromRGB(0xf6f7f8);
    
    //For Service Details Section Footer When No Services Available
    if( section == 2)
    {
        if(isServiceGroupAvailableOrNot == NO)
        {
            view.frame = CGRectMake(0, 0, 0, 0);
            return view;
        }
        else
        {
            if([arrayOfServiceAddedIndexValues count] == 0)
            {
                //If No Services Selected
                view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,55)];
                view.backgroundColor = UIColorFromRGB(0xffffff);
                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width,20)];
                label.backgroundColor = [UIColor whiteColor];
                label.text = LS(@"Any Service Required ?");
                label.textAlignment = NSTextAlignmentCenter;
                label.font = [UIFont fontWithName:OpenSans_Regular size:14];
                label.textColor = UIColorFromRGB(0x333333);
                
                UIButton *addServicesButton = [UIButton buttonWithType:UIButtonTypeCustom];
                addServicesButton.frame = CGRectMake((tableView.bounds.size.width/2)-(tableView.bounds.size.width/6), 20, tableView.bounds.size.width/3, 25);
                
                [addServicesButton setBackgroundImage:[UIImage imageNamed:@"home_book_now_btn_off"] forState:UIControlStateNormal];
                [addServicesButton setBackgroundImage:[UIImage imageNamed:@"home_book_now_btn_on"] forState:UIControlStateHighlighted];
                
                [addServicesButton setTitle:LS(@"+ ADD SERVICE") forState:UIControlStateNormal];
                addServicesButton.titleLabel.font = [UIFont fontWithName:OpenSans_Regular size:12];
                [addServicesButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
                [addServicesButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
                
                [addServicesButton addTarget:self action:@selector(addServicesButtonAction) forControlEvents:UIControlEventTouchUpInside];
                
                UIView *divider = [[UIView alloc] initWithFrame:CGRectMake(0, 50, tableView.bounds.size.width,5)];
                divider.backgroundColor = UIColorFromRGB(0xf6f7f8);
                
                [view addSubview:label];
                [view addSubview:addServicesButton];
                [view addSubview:divider];
                
                return view;
            }
            
        }
    }
    
    
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    //For Service Details Section Footer Height When No Services Available
    
    if( section == 2)
    {
        if(isServiceGroupAvailableOrNot == NO)
        {
            return 1;
        }
        else
        {
            if(arrayOfServiceAddedIndexValues.count == 0)
            {
                return 55;
            }
            else
            {
                return 5;
            }
            
        }
        
    }
    
    if(section == sectionTitleArray.count-1){
        return 1;
    }
    
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    //For Service Details Section Header Height When No Services Available
    if(isServiceGroupAvailableOrNot == NO && section == 2)
    {
        return 1;
    }
    
    return 24;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
        {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width-100, 0)];
            label.text = [NSString stringWithFormat:@"%@",pickupAddress];
            label.font = [UIFont fontWithName:OpenSans_Regular size:12];
            
            float height1 = [UIHelper measureHeightLabel:label];
            [tableView layoutIfNeeded];
            
            return 10+height1+10;
        }
            break;
        case 2:
            return 25;
            break;
        case 3:
            return 60;
            break;
            
        case 4:
            return 105;
            break;
        case 6:
        {
            return 25;
        }
            break;
        default:
            return 50;
            break;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier;
    
    switch (indexPath.section) {
        case 0:
        {
            cellIdentifier = @"addressDetailCell";
            AddressDetailTableViewCell *addressCell = (AddressDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            
            if(addressCell == nil)
            {
                addressCell =[[AddressDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            addressCell.addressLabel.text = [NSString stringWithFormat:@"%@",pickupAddress];
            [addressCell.changeAddressButton.layer setBorderColor:[APP_COLOR CGColor]];
            [addressCell.contentView layoutIfNeeded];
            return addressCell;
        }
            break;
            
        case 1:
        {
            cellIdentifier = @"promoCodeCell";
            promoCodeCell = (PromoCodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(promoCodeCell == nil)
            {
                promoCodeCell =[[PromoCodeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            [promoCodeCell.applyPromocodeButton.layer setBorderColor:[APP_COLOR CGColor]];
            
            return promoCodeCell;
            
        }
            break;
            
        case 2:
        {
            NSString *cellIdentifier = @"serviceDetailCell";
            
            ServiceDetailsTableViewCell *serviceDetailsCell = (ServiceDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (serviceDetailsCell == nil)
            {
                serviceDetailsCell = [[ServiceDetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            if(indexPath.row == arrayOfServiceAddedIndexValues.count)
            {
                serviceDetailsCell.serviceNameLabel.text = LS(@"Total Services Fee");
                
                CGFloat totalAmount = 0.0;
                for(int i=0; i<arrayOfServiceAddedIndexValues.count; i++)
                {
                    totalAmount = totalAmount + [arrayOfServiceAddedIndexValues[i][@"fixed_price"]floatValue];
                }
                
                serviceDetailsCell.serviceAmountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],totalAmount];
                
            }
            else
            {
                serviceDetailsCell.serviceNameLabel.text = arrayOfServiceAddedIndexValues[indexPath.row][@"sname"];
                
                serviceDetailsCell.serviceAmountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],[arrayOfServiceAddedIndexValues[indexPath.row][@"fixed_price"]floatValue]];
                
                serviceDetailsCell.serviceNameLabel.font = [UIFont fontWithName:OpenSans_Regular size:12.0];
                serviceDetailsCell.serviceAmountLabel.font = [UIFont fontWithName:OpenSans_Regular size:12.0];
                
                
            }
            
            
            return serviceDetailsCell;
            
        }
            break;
        case 3:
        {
            cellIdentifier = @"jobDetailCell";
            jobDetailCell = (JobDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(jobDetailCell == nil)
            {
                jobDetailCell =[[JobDetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            return jobDetailCell;
            
        }
            break;
            
        case 4:
        {
            cellIdentifier = @"jobPhotosCell";
            
            jobPhotosCell = (JobPhotosTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(jobPhotosCell == nil)
            {
                jobPhotosCell =[[JobPhotosTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            return jobPhotosCell;
            
        }
            break;
            
        case 5:
        {
            cellIdentifier = @"paymentDetailCell";
            PaymetDetailTableViewCell *paymentCell = (PaymetDetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(paymentCell == nil)
            {
                paymentCell =[[PaymetDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            [paymentCell setPaymentDetails];
            return paymentCell;
            
        }
            break;
            
        case 6:
        {
            cellIdentifier = @"feeEstimateCell";
            feeEstimateCell = (FeeEstimateTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(feeEstimateCell == nil)
            {
                feeEstimateCell =[[FeeEstimateTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            feeEstimateCell.providerTypeDetails = self.providerTypeDetails;
            feeEstimateCell.arrayOfSelectedServices = arrayOfServiceAddedIndexValues;
            feeEstimateCell.bookingType = ap.bookingType;
            if(ap.bookingType == 3)
            {
                feeEstimateCell.slotPrice = [self.selectedSlotDictionary[@"slotPrice"]floatValue];
            }
            [feeEstimateCell setFeeEstimates:indexPath];
            
            return feeEstimateCell;
        }
            break;
        default:
            return nil;
            break;
            
    }
    
}

#pragma mark - UIButton Actions -

- (IBAction)changeAddressButtonAction:(id)sender {
    
    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddressManageViewController *addressVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"myAddress"];
    
    addressVC.isFromProviderBookingVC = 1;
    addressVC.addressManageDelegate = self;
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:addressVC animated:NO];
    
}

- (IBAction)changePaymentButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    
    //    UIActionSheet *actionSheet;
    //
    //    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Change Payment Type"
    //                                              delegate:self
    //                                     cancelButtonTitle:@"Cancel"
    //                                destructiveButtonTitle:nil
    //                                     otherButtonTitles:@"CASH",@"CARD",nil];
    //
    //    actionSheet.tag = 2;
    //    [actionSheet showInView:self.view];
    
    actionSheet = [UIAlertController alertControllerWithTitle:LS(@"Change Payment Type") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"CASH") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // CASH button tapped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
        [self cashButtonSelected];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"CARD") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // CARD button tapped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
        [self cardButtonSelected];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (IBAction)applyPromoCodeButtonAction:(id)sender {
    
    if([promoCodeCell.applyPromocodeButton.titleLabel.text isEqualToString:@"CLEAR"]){
        
        [promoCodeCell.applyPromocodeButton setTitle:LS(@"APPLY") forState:UIControlStateNormal];
        promoCodeCell.promocodeTextField.text = @"";
        //        [promoCodeCell.promocodeTextField becomeFirstResponder];
        [self removeDiscountDetails];
    }
    else{
        
        if(promoCodeCell.promocodeTextField.text.length > 0){
            
            [promoCodeCell.promocodeTextField resignFirstResponder];
            [self sendServiceToCheckPromoCode:promoCodeCell.promocodeTextField.text];
        }
        else{
            [self performSelector:@selector(showAlertView) withObject:nil afterDelay:0.6];
            
            //            [promoCodeCell.promocodeTextField becomeFirstResponder];
        }
    }
    
}

- (IBAction)changeHourValueButtonAction:(id)sender {
    
    feeEstimateCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:5]];
    [self.hourPicker selectRow:numberOfHourValue-1 inComponent:0 animated:YES];
    
    self.pickerBackgroundViewBottomConstraint.constant = 0;
    
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.view layoutIfNeeded];
        
    }];
    
}

- (IBAction)addNewPhotoButtonAction:(id)sender {
    
    [self.view endEditing:YES];
    
    //    UIActionSheet *actionSheet;
    //    actionSheet.tag = 1;
    //
    //    actionSheet = [[UIActionSheet alloc] initWithTitle:@"Add Job Photos"
    //                                                  delegate:self
    //                                         cancelButtonTitle:@"Cancel"
    //                                    destructiveButtonTitle:nil
    //                                         otherButtonTitles:@"Take Photo",@"Choose From Library",nil];
    //
    //    actionSheet.tag = 1;
    //    [actionSheet showInView:self.view];
    
    actionSheet = [UIAlertController alertControllerWithTitle:LS(@"Add Job Photos") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Take Photo") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take Photo button tapped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
        [self cameraButtonClicked];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Choose From Library") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose From Library button tapped.
        [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
        [self libraryButtonClicked];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (IBAction)navigationBackButtonAction:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(),^{
        [self stopTimer];
        [publishToSocketTimer invalidate];
        publishToSocketTimer = nil;
        
        AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appD setSocketDelegateToAppDelegate];
        
        if(ap.bookingType == 1)
        {
            [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                      subType:kCATransitionFromBottom
                                                      forView:self.navigationController.view
                                                 timeDuration:0.3];
            
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    });
    
}

- (IBAction)requestingButtonAction:(id)sender
{
    if(isServiceMandatory && !isMandatoryServicesSelected)//If any mandatory services are available
    {
        [UIHelper showMessage:LS(@"Please add mandatory services to confirm your booking!") withTitle:LS(@"Message")delegate:nil];
        return;
    }
    else if (isServiceCategoryAvailable == NO &&(ap.bookingType == 1|| ap.bookingType == 2))
    {
        [UIHelper showMessage:@"Sorry we are not operational in the selected location please select other location!" withTitle:@"Message" delegate:nil];
        return;
    }
    
    isShowingRequestingScreen = YES;
    if(ap.bookingType == 1|| ap.bookingType == 2)
    {
        requestingView = [ProviderRequestingView sharedInstance];
        [window addSubview:requestingView];
        
        self.requestingButton.userInteractionEnabled = NO;
        requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
        
        [UIView animateWithDuration:0.5
                              delay:0.2
                            options: UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
    }
    [self sendLiveBookingRequest];
    //    [self startBackgroundTask];
    
}

- (IBAction)cancelBookingButtonAction:(id)sender {
    
    [self stopTimer];
    [self sendServiceToCancelBooking];
}

- (IBAction)deleteJobPhotoButtonAction:(id)sender {
    
    deleteJobPhotoButton = (UIButton *)sender;
    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message"
    //                                                    message:@"Are you sure you want to remove this photo ?"
    //                                                   delegate:self
    //                                          cancelButtonTitle:@"YES"
    //                                          otherButtonTitles:@"NO",nil];
    //    alert.tag = 1;
    //    [alert show];
    
    actionSheet = [UIAlertController  alertControllerWithTitle:LS(@"Message")
                                                       message:LS(@"Are you sure you wish to remove this photo?")
                                                preferredStyle:UIAlertControllerStyleAlert];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"YES")
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      
                                                      //Delete Job photos
                                                      
                                                      [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
                                                      [jobImagesArray removeObjectAtIndex:deleteJobPhotoButton.tag];
                                                      [jobPhotosCell reloadCollectionView:jobImagesArray];
                                                      
                                                      //                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                      
                                                  }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"NO")
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
                                                      
                                                  }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    
    
}

- (IBAction)pickerCancelButtonAction:(id)sender
{
    self.pickerBackgroundViewBottomConstraint.constant = -400;
    
    [UIView animateWithDuration:0.5f animations:^{
        
        [self.view layoutIfNeeded];
        
    }];
    
}

- (IBAction)pickerDonebuttonAction:(id)sender
{
    NSNumber *selectedRow = [NSNumber numberWithInteger:[self.hourPicker selectedRowInComponent:0]+1];
    if(numberOfHourValue != selectedRow.integerValue)
    {
        numberOfHourValue = selectedRow.integerValue;
        feeDetails.numberOfHourValue = selectedRow;
        [self reloadParticularSectionInTableView:6];
    }
    
    [self pickerCancelButtonAction:nil];
}

- (IBAction)longPressActionForJobPhotos:(id)sender {
    
    //    UILongPressGestureRecognizer *gesture = (UILongPressGestureRecognizer *)sender;
    //
    //    if(gesture.state == UIGestureRecognizerStateBegan)
    //    {
    //        count = 1;
    //        NSLog(@"Long Pressing Begin");
    //    }
    //    else{
    //        count++;
    //        if(count >2 && jobImagesArray.count>0)
    //        {
    //            longPress = YES;
    //            [jobPhotosCell reloadCollectionView:jobImagesArray and:longPress];
    //        }
    //    }
    
}

- (IBAction)removeLongGestureAnimation:(id)sender
{
    //    longPress = NO;
}

#pragma mark - AlertViewDelegate -

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    switch (alertView.tag) {
            
        case 1:
            //Delete Job photos
            if (buttonIndex == [alertView cancelButtonIndex])
            {
                [jobImagesArray removeObjectAtIndex:deleteJobPhotoButton.tag];
                [jobPhotosCell reloadCollectionView:jobImagesArray];
            }
            break;
        case 2:
            if(buttonIndex == [alertView cancelButtonIndex])
            {
                [self performSelector:@selector(setMenuSettings) withObject:nil afterDelay:0.6];
            }
            
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Custom Methods -

-(void)reloadParticularSectionInTableView:(NSInteger)sectionValue{
    
    NSRange range = NSMakeRange(sectionValue, 1);
    NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];
    
}

-(void)removeRequestingScreen{
    
    self.requestingButton.userInteractionEnabled = YES;
    requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         requestingView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         
                         dispatch_async(dispatch_get_main_queue(),^{
                             [requestingView removeFromSuperview];
                             requestingView = nil;
                         });
                     }];
    
}

-(void)setRequestButtonTitle:(NSString *)typeName
{
    if(ap.bookingType == 1)
    {
        if([[typeName substringToIndex:1] isEqualToString:@"A"] ||[[typeName substringToIndex:1] isEqualToString:@"E"]||
           [[typeName substringToIndex:1] isEqualToString:@"I"] || [[typeName substringToIndex:1] isEqualToString:@"O"]||
           [[typeName substringToIndex:1] isEqualToString:@"U"]){
            
            [self.requestingButton setTitle:@"BOOK" forState:UIControlStateNormal];//[NSString stringWithFormat:@"Get me an %@",typeName]
        }
        else{
            
            [self.requestingButton setTitle:@"BOOK" forState:UIControlStateNormal];
        }
        
    }
    else
    {
        [self.requestingButton setTitle:[NSString stringWithFormat:LS(@"Confirm and book %@"),[typeName componentsSeparatedByString:@" "][0]] forState:UIControlStateNormal];
    }
    
}

-(void)setFeeEstimateDetails
{
    feeDetails = [FeeDetailsClass sharedInstance];
    
    feeDetails.feeType = [NSNumber numberWithInteger:[feeTypesArray indexOfObject:self.providerTypeDetails[@"ftype"]]];
    
    feeDetails.discountType = discountType;
    feeDetails.numberOfHourValue = [NSNumber numberWithInteger:numberOfHourValue];
    feeDetails.discount = [NSNumber numberWithFloat:discountAmount];
    
    feeDetails.visitFee = [NSNumber numberWithFloat:0];
    feeDetails.hourlyFee = [NSNumber numberWithFloat:0];
    feeDetails.discountFee = [NSNumber numberWithFloat:0];
    feeDetails.totalFee = [NSNumber numberWithFloat:0];
    feeDetails.servicesAmount = [NSNumber numberWithFloat:servicesTotalAmount];
    if(ap.bookingType == 3)
    {
        feeDetails.slotAmount = [NSNumber numberWithFloat:[self.selectedSlotDictionary[@"slotPrice"]floatValue]];
    }
    else
    {
        feeDetails.slotAmount = [NSNumber numberWithFloat:0.0];
    }
    
}
-(void)removeDiscountDetails
{
    discountType = @"";
    discountAmount = 0;
    [self setFeeEstimateDetails];
    [self reloadParticularSectionInTableView:6];
}

-(void)goToBookingVC{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] openContentViewControllerForMenu:AMSlideMenuLeft atIndexPath:indexPath];
    
}

-(void)setNavigationTitle
{
    NSInteger amountValue;
    NSString *title;
    
    switch ([feeTypesArray indexOfObject:self.providerTypeDetails[@"ftype"]])
    {
            //Feixed Based
        case feeTypeFixed:
        {
            amountValue = [self.providerTypeDetails[@"fixed_price"]floatValue];
            
        }
            break;
            
            //Hourly Based And Milleage Based
        case feeTypeHourly:
        {
            amountValue = [self.providerTypeDetails[@"price_min"]floatValue];
            
        }
            
            break;
            
        default:
        {
            amountValue = [UIHelper convertMilesToKilometer:[self.providerTypeDetails[@"price_mile"]floatValue]];
            
        }
            break;
            
    }
    
    
    title = [NSString stringWithFormat:@"%@\n%@",self.providerNameLabel.text,self.milesLabel.text];
    
    navigationAttributedTitle = [[NSMutableAttributedString alloc]initWithString:title];
    
    [navigationAttributedTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_Regular size:15] range:NSMakeRange(0,self.providerNameLabel.text.length)];
    [navigationAttributedTitle addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(0,self.providerNameLabel.text.length)];
    
    [navigationAttributedTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_Regular size:11] range:NSMakeRange(self.providerNameLabel.text.length,title.length-self.providerNameLabel.text.length)];
    [navigationAttributedTitle addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(self.providerNameLabel.text.length,title.length-self.providerNameLabel.text.length)];
    
    
    [navigationTitleLabel setAttributedText:navigationAttributedTitle];
    
}

-(void)addServicesButtonAction
{
    [self performSegueWithIdentifier:@"toServiceGroupVC" sender:self];
}

//-(void)hideRequestingButton
//{
//    self.requestingButtonBottomConstraint.constant = -40;
//
//    [UIView animateWithDuration:0.5f animations:^{
//
//        [self.view layoutIfNeeded];
//    }];
//
//}
//
//-(void)showRequestingButton
//{
//    self.requestingButtonBottomConstraint.constant = 6;
//
//    [UIView animateWithDuration:0.5f animations:^{
//
//        [self.view layoutIfNeeded];
//    }];
//
//}
-(void)showAlertView
{
    [UIHelper showMessage:LS(@"Please Enter Promocode") withTitle:LS(@"Message")delegate:nil];
}

//-(void)showMessage:(NSString *)message withTitle:(NSString *)title
//{
//    dispatch_async(dispatch_get_main_queue(),^{
//
//        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        alertView.tag = 2;
//        [alertView show];
//    });
//}

-(void)setMenuSettings
{
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
}



#pragma mark - UIImagePickerDelegate -

-(void)cameraButtonClicked
{
    imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        [UIHelper showMessage:LS(@"Camera is not available") withTitle:LS(@"Message")delegate:nil];
    }
}

-(void)libraryButtonClicked
{
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [jobImagesArray addObject:pickedImage];
    [jobPhotosCell reloadCollectionView:jobImagesArray];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - Change Payment Methods -

-(void)cashButtonSelected
{
    [ud setObject:@"CASH" forKey:@"paymentType"];
    [ud synchronize];
    [self reloadParticularSectionInTableView:5];
    
}

-(void)cardButtonSelected
{
    PaymentViewController *paymentVC = (PaymentViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"paymentVC"];
    paymentVC.callback = ^(NSString *cardNo ,NSString *cardIdentity, NSString *type,NSInteger selectedcardIndex){
        
        [ud setObject:@"CARD" forKey:@"paymentType"];
        [ud setObject:cardIdentity forKey:@"selectedCardId"];
        [ud synchronize];
        
        [self reloadParticularSectionInTableView:5];
        
    };
    paymentVC.isFromProviderBookingVC = YES;
    
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:paymentVC animated:NO];
    
}


#pragma mark - UITextField Delegates -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    activeTextView = nil;
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([promoCodeCell.applyPromocodeButton.titleLabel.text isEqualToString:@"CLEAR"]){
        
        [promoCodeCell.applyPromocodeButton setTitle:LS(@"APPLY") forState:UIControlStateNormal];
        [self removeDiscountDetails];
    }
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(BOOL)textFieldShouldClear:(UITextField *)textField{
    
    [promoCodeCell.applyPromocodeButton setTitle:LS(@"APPLY") forState:UIControlStateNormal];
    [self removeDiscountDetails];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    if([promoCodeCell.applyPromocodeButton.titleLabel.text isEqualToString:@"CLEAR"])
        [promoCodeCell.promocodeTextField resignFirstResponder];
    else
        [self applyPromoCodeButtonAction:nil];
    
    return YES;
}

#pragma mark - TextView Delegate methods -

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    activeTextView = textView;
    activeTextField = nil;
    if ([textView.text isEqualToString:@"Provide details of your job requirement (optional)"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    
    if(textView.text.length == 0)
    {
        textView.textColor = [UIColor lightGrayColor];
        textView.text = LS(@"Provide details of your job requirement (optional)");
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            
            textView.textColor = [UIColor lightGrayColor];
            textView.text = LS(@"Provide details of your job requirement (optional)");
        }
        
        return NO;
    }
    
    return YES;
}

#pragma mark - KeyBoard Methods -

- (void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.tableView.scrollIndicatorInsets = ei;
        self.tableView.contentInset = ei;
    }];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    
}

- (void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.tableView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.tableView.scrollIndicatorInsets = ei;
    self.tableView.contentInset = ei;
    
    if(activeTextField)
    {
        if(ap.bookingType == 1)
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        }
        else
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        }
    }
    else if(activeTextView)
    {
        if(isServiceGroupAvailableOrNot)
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        }
        else
        {
            [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        }
        
    }
    //    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
    
}

#pragma mark - UIPicker Delegate -

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    switch (component) {
        case 0:
            return 15;
            break;
            
        default:
            return 1;
            break;
            
    }
    
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    switch (component) {
            
        case 0:
        {
            NSNumber *selectedRow = [NSNumber numberWithInteger:row+1];
            return [NSString stringWithFormat:@"%@",selectedRow.stringValue];
        }
            break;
            
        default:
            return LS(@"Hours");
            break;
    }
    
}

#pragma mark - SIOSocket Booking Dispatch Queue -

/**
 *  Publish SocketStream to receiving pro near around Customer current location
 */
-(void)publishingToSocket
{
    //    if(pickupLat != 0)
    //    {
    NSDictionary *message = @{
                              @"btype":[NSNumber numberWithInt:3],
                              @"email":patientEmail,
                              @"lat": [NSString stringWithFormat:@"%lf",pickupLat],
                              @"long": [NSString stringWithFormat:@"%lf",pickupLong],
                              };
    
    [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:message];
    //    }
}


//Socket Response Delegate Methods
-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message{
    
    NSLog(@"Booking Screen message: %@",message);
    
    if([channelName isEqualToString:@"CustomerStatus"])
    {
        //Booking Status Response
        
        if([message[@"st"]integerValue] == 2)
        {//If Provider Accepts Booking
            
            NSLog(@"Socket BookingAccepted:%@",message);
            
            AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appD setSocketDelegateToAppDelegate];
            
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
            
        }
        else if([message[@"st"]integerValue] == 3)
        {//if provider Rejects Booking
            
            //if provider Rejects Booking
            //Checking This Provider Already in Requested Queue
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pid = %d",[message[@"proid"]integerValue]];
            NSArray *results = [alreadyRequestSentedArray filteredArrayUsingPredicate:predicate];
            
            if(results.count > 0)
            {
                return;
            }
            
            if(ap.bookingType == 1)
            {
                [self stopTimer];
                [self maintainAlreadyRequestSentQueue];
            }
            else
            {
                NSDictionary *message = @{
                                          @"proid":self.providerDetailsFromPreviousController[@"pid"],
                                          };
                
                NSLog(@"Make Provider Free Parameters:%@",message);
                
                [socketWrapper publishMessageToChannel:@"makefree" withMessage:message];
                [self stopTimer];
                [self showScreenWhenProviderNotAcceptedBooking:LS(@"Provider is busy")];
                
            }
            
        }
        
    }
    else if ([channelName isEqualToString:@"UpdateCustomer"]){//Updated Providers Type and List Details Response
        
        if ([message[@"msg"][@"types"]count] > 0) //success
        {
            isServiceCategoryAvailable = YES;
            if(ap.bookingType == 1)
            {
                [self socketResponseWithProvidersArround:message];
            }
            else if(isShowingRequestingScreen == NO && ap.bookingType == 2)
            {
                //Update Providers Arround Details
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"e = %@",self.providerDetailsFromPreviousController[@"email"]];
                NSArray *results = [message[@"msg"][@"masArr"][selectedProviderType.integerValue][@"mas"] filteredArrayUsingPredicate:predicate];
                
                if(results.count == 0)
                {
                    [UIHelper showMessage:@"Unfortunately selected provider went offline" withTitle:LS(@"Message")delegate:nil];
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        [actionSheet dismissViewControllerAnimated:NO completion:nil];
                        [self stopTimer];
                        [publishToSocketTimer invalidate];
                        publishToSocketTimer = nil;
                        
                        AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        [appD setSocketDelegateToAppDelegate];
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                        
                        [[AMSlideMenuMainViewController getInstanceForVC:self] openContentViewControllerForMenu:AMSlideMenuLeft atIndexPath:indexPath];
                        
                        //                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    });
                    
                }
                
            }
        }
        else
        {
            if(isServiceCategoryAvailable == YES)
            {
                [UIHelper showMessage:@"Sorry we are not operational in the selected location please select other location!" withTitle:@"Message" delegate:nil];
            }
            isServiceCategoryAvailable = NO;
        }
        
    }
    
}

-(void)parseBookingAcceptedResponse
{
    dispatch_async(dispatch_get_main_queue(),^{
        
        [[LiveBookingStatusUpdateClass sharedInstance]sendRequestToGetAllonGoingBookings];
        NSLog(@"BookingAccepted");
        
        NSString *providerName;
        
        if(ap.bookingType == 1)
        {
            providerName = currentlyBookingSentedProviderDetails[@"n"];
        }
        else
        {
            providerName = [self.providerNameLabel.text componentsSeparatedByString:@" "][0];
        }
        
        
        //        [UIHelper showMessage:[NSString stringWithFormat:LS(@"Hey “%@” your job has been accepted by “%@” and he will be on the way shortly"),[ud objectForKey:iServeUserFirstName],providerName] withTitle:LS(@"Message")delegate:nil];
        
        [UIHelper showMessage:[NSString stringWithFormat:LS(@"Your job has been accepted by “%@” and they will be on their way to you shortly"),providerName] withTitle:LS(@"Message")delegate:nil];
        
        [ud removeObjectForKey:@"paymentType"];
        [ud removeObjectForKey:@"selectedCardId"];
        [ud synchronize];
        
        
        
        [[UploadImagesToAmazonServer sharedInstance] uploadJobImagesToServer:jobImagesArray andBid:bid];
        [self removeRequestingScreen];
        [self navigationBackButtonAction:nil];
        [self goToBookingVC];
        //        [self stopBackgroundTask];
    });
    
}

//Getting Providers List From Socket
-(void)socketResponseWithProvidersArround:(NSDictionary *)messageDict{
    
    updatedProvidersArray = [[NSMutableArray alloc] init];
    
    if([messageDict[@"msg"][@"masArr"] count] > 0){
        
        updatedProvidersArray = messageDict[@"msg"][@"masArr"][selectedProviderType.integerValue][@"mas"];
        NSLog(@"Total Providers:%@",updatedProvidersArray);
        [self removeAlreadyBookedProviderFromQueue];
        
    }
    NSLog(@"Updated Providers:%@",updatedProvidersArray);
    NSLog(@"alreadyRequested Providers:%@",alreadyRequestSentedArray);
}

//Already Request Sent Providers Details
-(void)maintainAlreadyRequestSentQueue{
    
    [alreadyRequestSentedArray addObject:currentlyBookingSentedProviderDetails];
    
    [self removeAlreadyBookedProviderFromQueue];
    
    [self checkRemainingSecondsAndSendBookingAgain];
}


//Checking Remaining Time
-(void)checkRemainingSecondsAndSendBookingAgain
{
    remainingSeconds = remainingSeconds - elapsedSeconds;//calculating Remaining Seconds
    
    elapsedSeconds = 0;
    
    if(remainingSeconds > 0 ){//Checking remaining seconds more than 0
        
        if(updatedProvidersArray.count > 0){//checking any Providers Available
            
            currentlyBookingSentedProviderDetails = updatedProvidersArray[0];
            [self sendBookingThroughSocket:currentlyBookingSentedProviderDetails[@"pid"]];//need ProID
            
        }
        else{
            //No providers Available
            //            [self runTimerWhenNoProvidersAvailable];
            [self showScreenWhenProviderNotAcceptedBooking:LS(@"Providers around you seem to be busy.Please try again later")];
            
        }
        
    }
    else{
        
        [self runTimerWhenNoProvidersAvailable];
        
    }
    
}

//Send Booking Request Through Socket
-(void)sendBookingThroughSocket:(NSString *)pid{
    
    [self stopTimer];
    [self startTimer];
    isBookingSend = YES;
    
    NSInteger bookingTypeValue;
    if(ap.bookingType == 2)
    {
        bookingTypeValue = 3;
    }
    else if(ap.bookingType == 3)
    {
        bookingTypeValue = 2;
    }
    else
    {
        bookingTypeValue = 1;
    }
    
    NSDictionary *message = @{
                              @"cid":[NSNumber numberWithInteger:customerId.integerValue],
                              @"bid":[NSNumber numberWithInteger:bid.integerValue],
                              @"proid":[NSNumber numberWithInteger:pid.integerValue],
                              @"dt":[UIHelper getCurrentDateTime],
                              @"btype":[NSNumber numberWithInteger:bookingTypeValue]
                              };
    
    NSLog(@"Booking Send to:%@",currentlyBookingSentedProviderDetails);
    NSLog(@"Booking Parameters:%@",message);
    
    [socketWrapper publishMessageToChannel:@"LiveBooking" withMessage:message];
    
}

//get Elapsed Time
-(void)updateEachAndEverySeconds{
    
    elapsedSeconds = elapsedSeconds + 1;
    
    if(ap.bookingType == 1)
    {
        if(isBookingSend == YES){//currently sended booking
            
            if(elapsedSeconds == 30){
                
                [self stopTimer];
                [self maintainAlreadyRequestSentQueue];
            }
            
            NSLog(@"Elapsed Seconds:%ld",(long)elapsedSeconds);
            NSLog(@"Booking Send to:%@",currentlyBookingSentedProviderDetails);
            
        }
        else if(remainingSeconds <= 150 && remainingSeconds > 0){//currently No booking Sent run upto remaining seconds
            
            [self checkRemainingSecondsAndSendBookingAgain];
            NSLog(@"No Updated Providers Available");
            NSLog(@"Elapsed Seconds:%zd",remainingSeconds);
        }
        else{
            
            [self showScreenWhenProviderNotAcceptedBooking:LS(@"Providers around you seem to be busy.Please try again later")];
        }
        
    }
    else
    {
        if(elapsedSeconds == 30){
            
            [self stopTimer];
            [self showScreenWhenProviderNotAcceptedBooking:LS(@"Provider is busy")];
            
        }
        
        NSLog(@"Elapsed Seconds:%ld",(long)elapsedSeconds);
    }
    
    
}


-(void)startTimer{
    
    bookingTimer = [NSTimer scheduledTimerWithTimeInterval:1
                                                    target:self
                                                  selector:@selector(updateEachAndEverySeconds)
                                                  userInfo:nil
                                                   repeats:YES];
    
}

-(void)stopTimer{
    
    [bookingTimer invalidate];
    bookingTimer = nil;
    
}

//Run Timer When No Providers Available and seconds not more than 150
-(void)runTimerWhenNoProvidersAvailable{
    
    if(bookingTimer == nil){
        
        [self startTimer];
    }
    isBookingSend = NO;
}

//Remove Already Requested Providers From Booking Queue
-(void)removeAlreadyBookedProviderFromQueue{
    
    for(int i=0;i<alreadyRequestSentedArray.count;i++){
        
        for(int j=0;j<updatedProvidersArray.count;j++){//Remove already book sented provider Detail
            
            if([alreadyRequestSentedArray[i][@"e"] isEqualToString:updatedProvidersArray[j][@"e"]]){
                [updatedProvidersArray removeObjectAtIndex:j];
            }
        }
    }
    
}

-(void)showScreenWhenProviderNotAcceptedBooking:(NSString *)message
{
    //    dispatch_async(dispatch_get_main_queue(),^{
    
    [UIHelper showMessage:message withTitle:LS(@"Message")delegate:nil];
    //    });
    
    [ud removeObjectForKey:@"paymentType"];
    [ud removeObjectForKey:@"selectedCardId"];
    [ud synchronize];
    
    
    //    [self stopBackgroundTask];
    [self removeRequestingScreen];
    [self navigationBackButtonAction:nil];
    
}

#pragma mark - WebService Request -

//Send Appointment Now
-(void)sendLiveBookingRequest
{
    //Checking paymentType Cash Or Card
    NSInteger paymentTypeValue;
    if([[ud objectForKey:@"paymentType"] isEqualToString:@"CARD"])
    {
        paymentTypeValue = 2;
    }
    else
    {
        paymentTypeValue = 1;
    }
    
    jobDetails = jobDetailCell.jobDetailTextView.text;
    
    if(jobDetails.length == 0 ||[jobDetails isEqualToString:@"Provide details of your job requirement (optional)"]){
        jobDetails = @"";
    }
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSInteger bookingTypeValue;
        if(ap.bookingType == 2)
        {
            bookingTypeValue = 3;
        }
        else if(ap.bookingType == 3)
        {
            bookingTypeValue = 2;
        }
        else
        {
            bookingTypeValue = 1;
        }
        
        NSMutableArray *listOfSelectedServicesArray = [[NSMutableArray alloc]init];
        
        if(isServiceGroupAvailableOrNot)//If services are added
        {
            for(int i=0; i<arrayOfServiceAddedIndexValues.count; i++)
            {
                NSMutableDictionary *serviceDict = [[NSMutableDictionary alloc]init];
                [serviceDict setObject:arrayOfServiceAddedIndexValues[i][@"sname"] forKey:@"sname"];
                [serviceDict setObject:arrayOfServiceAddedIndexValues[i][@"sid"] forKey:@"sid"];
                [serviceDict setObject:arrayOfServiceAddedIndexValues[i][@"fixed_price"] forKey:@"sprice"];
                
                [listOfSelectedServicesArray addObject:serviceDict];
            }
            
        }
        
        NSDictionary *temp = @{
                               @"ent_sess_token":flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                               @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                               @"ent_custid":customerId,
                               @"ent_btype":[NSNumber numberWithInteger:bookingTypeValue],
                               @"ent_a1":pickupAddress,
                               @"ent_lat":[NSNumber numberWithDouble:pickupLat],
                               @"ent_long":[NSNumber numberWithDouble:pickupLong],
                               @"ent_cat_id":self.providerTypeDetails[@"type_id"],
                               @"ent_dtype":[NSNumber numberWithInt:2],
                               @"ent_date_time":[UIHelper getCurrentDateTime],
                               @"ent_job_imgs":[NSNumber numberWithInteger:jobImagesArray.count],
                               @"ent_job_details":jobDetails,
                               @"ent_coupon":flStrForObj(promoCodeCell.promocodeTextField.text),
                               @"ent_discount":feeDetails.discountFee,
                               @"ent_hourly_amt":feeDetails.hourlyFee,
                               @"ent_total":feeDetails.totalFee,
                               @"ent_pymt":[NSNumber numberWithInteger:paymentTypeValue],
                               @"ent_cat_name":self.providerTypeDetails[@"cat_name"],
                               @"ent_services":listOfSelectedServicesArray
                               };
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:temp];
        
        //Adding Selected Card Id
        if(paymentTypeValue == 2)
        {
            [params setObject:flStrForObj([ud objectForKey:@"selectedCardId"]) forKey:@"ent_card_id"];
        }
        
        if(bookingTypeValue == 2)
        {
            [params setObject:flStrForObj(self.selectedSlotDictionary[@"sid"]) forKey:@"ent_slot_id"];
            [params setObject:flStrForObj(self.providerDetailsFromPreviousController[@"pid"]) forKey:@"ent_proid"];
            
        }
        
        if(ap.bookingType == 3)//For Later Booking
        {
            [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Requesting...", @"Requesting...")];
        }
        
        
        [[WebServiceHandler sharedInstance] sendRequestToLiveBooking:params andDelegate:self];
    }
    else
    {
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}

-(void)sendServiceToCancelBooking
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 
                                 @"ent_bid":[NSNumber numberWithInteger:bid.integerValue],
                                 @"ent_date_time":[UIHelper getCurrentDateTime]
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToCancelBooking:params andDelegate:self];
    }
    else{
        
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}


-(void)sendServiceToCheckPromoCode :(NSString *)promoCode
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Validating Promo Code...", @"Validating Promo Code...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_coupon":promoCode,
                                 @"ent_lat":[NSNumber numberWithDouble:pickupLat],
                                 @"ent_long":[NSNumber numberWithDouble:pickupLong],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToCheckCoupon:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}

-(void)sendWithoutDispatchOrLaterLiveBooking
{
    [self stopTimer];
    [self startTimer];
    isBookingSend = YES;
    
    
    NSDictionary *message = @{
                              @"cid":[NSNumber numberWithInteger:customerId.integerValue],
                              @"bid":[NSNumber numberWithInteger:bid.integerValue],
                              @"proid":self.providerDetailsFromPreviousController[@"pid"],
                              @"dt":[UIHelper getCurrentDateTime],
                              @"btype":[NSNumber numberWithInteger:3],
                              };
    
    NSLog(@"Booking Parameters:%@",message);
    
    [socketWrapper publishMessageToChannel:@"LiveBooking" withMessage:message];
    
}

-(void)sendServiceToGetServiceGroupDetails
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([ud objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_catid":self.providerTypeDetails[@"type_id"]
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetServiceGroupsDetails:params andDelegate:self];
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
    }
    
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:nil];
        if(requestType == RequestTypeLiveBooking)
        {
            [self removeRequestingScreen];
            
        }
        return;
        
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
                
            }
            else if(requestType == RequestTypeCheckCoupon)
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
                [promoCodeCell.promocodeTextField becomeFirstResponder];
                promoCodeCell.promocodeTextField.text = @"";
            }
            else if(requestType == RequestTypeCancelBooking)
            {
                [self removeRequestingScreen];
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
                
            }
            else if(requestType == RequestTypeLiveBooking)
            {
                [self removeRequestingScreen];
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
            }
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeCancelBooking){
                
                [self removeRequestingScreen];
                [self navigationBackButtonAction:nil];
                [UIHelper showMessage:LS(@"You have cancelled the booking.") withTitle:LS(@"Message")delegate:nil];
                
            }
            else if (requestType == RequestTypeCheckCoupon)
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
                [promoCodeCell.applyPromocodeButton setTitle:LS(@"CLEAR") forState:UIControlStateNormal];
                
                discountType = feeDetails.discountType = response[@"dtype"];
                discountAmount = [response[@"dis"]integerValue];
                feeDetails.discount = [NSNumber numberWithInteger:discountAmount];
                
                [self reloadParticularSectionInTableView:6];
                
            }
            else if (requestType == RequestTypeGetServiceGroupDetails)
            {
                if([response[@"data"]count] > 0)
                {
                    arrayOfServiceGroups = [[NSArray alloc]initWithArray:response[@"data"]];
                    
                    isServiceMandatory = NO;
                    for(int i=0; i<arrayOfServiceGroups.count; i++)
                    {
                        if([arrayOfServiceGroups[i][@"cmand"]integerValue] == 1)
                        {
                            isServiceMandatory = YES;
                            isMandatoryServicesSelected = NO;
                            break;
                        }
                    }
                    
                    arrayOfSelectedServices = [[NSMutableArray alloc]init];
                    
                    for(int i=0; i<arrayOfServiceGroups.count;i++)
                    {
                        NSArray *serviceArray =[[NSArray alloc]init];
                        [arrayOfSelectedServices addObject:serviceArray];
                    }
                    
                }
                
            }
            else{
                
                bid = [response objectForKey:@"bid"];
                
                if(ap.bookingType == 1)
                {
                    //Dispatch queue
                    [self checkRemainingSecondsAndSendBookingAgain];
                }
                else if(ap.bookingType == 2)
                {
                    //Without Dispatch Booking
                    [self sendWithoutDispatchOrLaterLiveBooking];
                }
                else
                {
                    //For Later Booking
                    [[LiveBookingStatusUpdateClass sharedInstance]sendRequestToGetAllonGoingBookings];
                    NSLog(@"BookingAccepted");
                    
                    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appD setSocketDelegateToAppDelegate];
                    
                    //                    NSTimeInterval start = [self.selectedSlotDictionary[@"stime"] doubleValue];
                    
                    //                    NSDate * startData = [NSDate dateWithTimeIntervalSince1970:start];
                    
                    //                    NSDate *FromDate = [self convertGMTtoLocalTimeConversion:startData];
                    
                    //                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                    //                    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                    
                    //                    NSString *dateRepresentation = [dateFormatter stringFromDate:dateFromString];
                    //                    NSLog(@"Date formated with local time zone: %@",dateRepresentation);
                    
                    //                    NSTimeInterval end = [self.selectedSlotDictionary[@"etime"] doubleValue];
                    //                    NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];
                    
                    //   NSDate *toDate = [self convertGMTtoLocalTimeConversion:endDate];
                    
                    //                    AddRemindersAndEvents *eventObj = [AddRemindersAndEvents instance];
                    //                    [eventObj eventStore];
                    //                    [eventObj setStartingDate:startData];
                    //                    [eventObj setEndingDate:endDate];
                    
                    //                    [eventObj setReminderTitle:[NSString stringWithFormat:@"You have a confirmed booking with \"%@\" on iServe. Location:  \"%@\" at %@",self.providerDetailsFromPreviousController[@"name"],pickupAddress,self.selectedSlotDictionary[@"from"]]];
                    
                    //                    [eventObj setReminderMessage:@"Booking reminder on iServe"];
                    //                    [eventObj updateAuthorizationStatusToAccessEventStore];
                    [self addEventInCalendar];
                    
                    [[UploadImagesToAmazonServer sharedInstance] uploadJobImagesToServer:jobImagesArray andBid:bid];
                    
//                    NSDictionary *message = @{
//                                              @"cid":[NSNumber numberWithInteger:customerId.integerValue],
//                                              @"bid":[NSNumber numberWithInteger:bid.integerValue],
//                                              @"proid":self.providerDetailsFromPreviousController[@"pid"],
//                                              @"stime":flStrForObj(self.selectedSlotDictionary[@"stime"]),
//                                              @"etime":flStrForObj(self.selectedSlotDictionary[@"etime"])
//                                              };
//                    
//                    NSLog(@"Later Booking Parameters:%@",message);
//                    
//                    [socketWrapper publishMessageToChannel:@"LaterBooking" withMessage:message];
                    
                    [ud removeObjectForKey:@"paymentType"];
                    [ud removeObjectForKey:@"selectedCardId"];
                    [ud synchronize];
                    
                    
                    [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        [self stopTimer];
                        [publishToSocketTimer invalidate];
                        publishToSocketTimer = nil;
                        
                        AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        [appD setSocketDelegateToAppDelegate];
                        
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                        
                        [[AMSlideMenuMainViewController getInstanceForVC:self] openContentViewControllerForMenu:AMSlideMenuLeft atIndexPath:indexPath];
                        
                        //                        [self.navigationController popToRootViewControllerAnimated:YES];
                        
                    });
                    
                    
                }
                
            }
            
        }
            break;
        default:
        {
            [self removeRequestingScreen];
            [self navigationBackButtonAction:self];
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
            
        }
            break;
    }
    
    
}

-(void)addEventInCalendar
{
    NSArray * arr = [self.selectedDate componentsSeparatedByString:@" "];
    NSLog(@"Array values are : %@",arr);
    
    
    NSString *tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"from"]];
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
    
    
    [dateFormat setLocale:locale];
    
    
    [dateFormat setDateFormat:@"YYYY-MM-dd hh:mm a"];
    NSDate *startDate = [dateFormat dateFromString:tempStringDate];
    
    
    tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"to"]];
    
    NSDate *endDate = [dateFormat dateFromString:tempStringDate];
    
    
    [[AdressWrapperClass sharedInstance] addReminder:startDate
                                          andEndDate:endDate
                                               Title:@"Booking reminder on iServe"
                                             Message:[NSString stringWithFormat:@"You have a confirmed booking with \"%@\" on iServe. Location:  \"%@\" at %@",self.providerDetailsFromPreviousController[@"name"],pickupAddress,self.selectedSlotDictionary[@"from"]]
                                           BookingId:bid];
    
}

-(void)addReminder:(NSDate *)startDate andEndDate:(NSDate *)endDate Title:(NSString *)title Message:(NSString *)message
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent
                          completion:^(BOOL granted, NSError *error)
     {
         if (!granted)
         {
             return;
         }
         EKEvent *event = [EKEvent eventWithEventStore:store];
         //        event.timeZone =  [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.title = title;
         event.notes = message;
         
         
         
         //         //Converting GMt to Current Locale
         //         NSArray * arr = [self.selectedDate componentsSeparatedByString:@" "];
         //         NSLog(@"Array values are : %@",arr);
         //
         //
         //         NSString *tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"from"]];
         //
         //
         //
         //         NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
         //
         //         NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:[NSString stringWithFormat:@"%@_POSIX",[NSLocale currentLocale].localeIdentifier]];
         //
         //
         //         [dateFormat setLocale:locale];
         //
         //         //        [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
         //
         //         [dateFormat setDateFormat:@"YYYY-MM-dd hh:mm a"];
         //         NSDate *startDate = [dateFormat dateFromString:tempStringDate];
         //
         //
         //         tempStringDate = [NSString stringWithFormat:@"%@ %@",arr[0],self.selectedSlotDictionary[@"to"]];
         //
         //         NSDate *endDate = [dateFormat dateFromString:tempStringDate];
         //
         //         //        NSTimeInterval end = [self.selectedSlotDictionary[@"etime"] doubleValue];
         //         //        NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:end];
         
         NSArray *alarms = @[[EKAlarm alarmWithRelativeOffset: - 60.0f *60* 1.0f]];
         event.alarms = alarms;
         
         event.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
         event.startDate = startDate;
         
         event.endDate = endDate;  //set 1 hour meeting
         event.calendar = [store defaultCalendarForNewEvents];
         NSError *err = nil;
         [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
         NSString *eventID = [NSString stringWithFormat:@"%@", event.eventIdentifier];
         NSLog(@"Event Id:%@",eventID);
         //        self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
     }];
    
}



#pragma mark - Class Delegate Method -

-(void)getAddressFromDatabase:(NSDictionary *)addressDetails{
    
    if([addressDetails[@"suite_Number"]length] > 0){
        
        pickupAddress = [NSString stringWithFormat:@"%@, %@,%@",addressDetails[@"suite_Number"],addressDetails[@"address1"],addressDetails[@"address2"]];
    }
    else{
        
        pickupAddress = [NSString stringWithFormat:@"%@,%@",addressDetails[@"address1"],addressDetails[@"address2"]];
    }
    
    pickupLat = [[addressDetails objectForKey:@"latitude"]doubleValue];
    pickupLong = [[addressDetails objectForKey:@"longitude"]doubleValue];
    
    [self reloadParticularSectionInTableView:0];
    
}

-(void)getSelectedServiceDetails:(NSMutableArray *)selectedServiceDetails;
{
    arrayOfSelectedServices = [[NSMutableArray alloc]initWithArray:selectedServiceDetails copyItems:YES];
    servicesTotalAmount = 0;
    //Getting Service Added Index Values
    arrayOfServiceAddedIndexValues = [[NSMutableArray alloc]init];
    for(int i=0; i<arrayOfSelectedServices.count; i++)
    {
        if([arrayOfSelectedServices[i]count] > 0)
        {
            //            [arrayOfServiceAddedIndexValues addObject:[NSNumber numberWithInt:i]];
            [arrayOfServiceAddedIndexValues addObjectsFromArray:arrayOfSelectedServices[i]];
            
            for(int j=0; j<[arrayOfSelectedServices[i]count];j++)
            {
                servicesTotalAmount = servicesTotalAmount + [arrayOfSelectedServices[i][j][@"fixed_price"]floatValue];
            }
        }
    }
    feeDetails.servicesAmount = [NSNumber numberWithFloat:servicesTotalAmount];
    
    //    [self reloadParticularSectionInTableView:6];
    //
    //    [self.tableView beginUpdates];
    //    [self.tableView endUpdates];
    //
    //    [self reloadParticularSectionInTableView:2];
    [self.tableView reloadData];
    
    NSInteger mandatoryGroupCount = 0;
    NSInteger normalServiceSelectedCount = 0;
    NSInteger serviceSelectedManadatoryGroupsCount = 0;
    
    for(int i=0; i<arrayOfServiceGroups.count; i++)
    {
        //If Mandatory Group Available
        if([arrayOfServiceGroups[i][@"cmand"]integerValue] == 1)
        {
            mandatoryGroupCount = mandatoryGroupCount + 1;
            if([arrayOfSelectedServices[i] count] > 0)
            {
                serviceSelectedManadatoryGroupsCount = serviceSelectedManadatoryGroupsCount + 1;
            }
        }
        else
        {
            //Not a Mandatory Group Services Selected or Not
            if([arrayOfSelectedServices[i] count] > 0)
            {
                normalServiceSelectedCount = normalServiceSelectedCount + 1;
            }
            
        }
        
    }
    
    //Show Or Hide Done Button When Mandatory Group Available
    if(mandatoryGroupCount > 0)
    {
        if(serviceSelectedManadatoryGroupsCount == mandatoryGroupCount)
        {
            isMandatoryServicesSelected = YES;
            //            [self showRequestingButton];
        }
        else
        {
            isMandatoryServicesSelected = NO;
            //            [self hideRequestingButton];
        }
    }
    //    else
    //    {
    //        [self showRequestingButton];
    //    }
    
    
}

#pragma mark - Prepare Segue -
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toServiceGroupVC"])
    {
        ServiceGroupViewController *serviceGroupVC = [segue destinationViewController];
        serviceGroupVC.arrayOfServiceGroups = [[NSArray alloc]initWithArray:arrayOfServiceGroups copyItems:YES];
        serviceGroupVC.arrayOfSelectedServices = [[NSMutableArray alloc]initWithArray:arrayOfSelectedServices copyItems:YES];
        serviceGroupVC.serviceDetailDelegate = self;
    }
    
}

#pragma mark - UIscrollView Delegates -

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(ap.bookingType != 1)
    {
        if(scrollView.tag == 1)
        {
            if(scrollView.contentOffset.y >= 64)
            {
                //[UIView animateWithDuration:0.5f animations:^{
                
                self.navigationController.navigationBar.translucent = NO;
                [self.navigationController.navigationBar setBackgroundImage:nil
                                                              forBarMetrics:UIBarMetricsDefault];
                self.navigationController.view.backgroundColor = [UIColor whiteColor];
                self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
                //                self.title = self.providerNameLabel.text;
                
                [navigationTitleLabel setAttributedText:navigationAttributedTitle];
                self.navigationItem.titleView = navigationTitleLabel;
                
                //}];
            }
            else
            {
                //[UIView animateWithDuration:0.5f animations:^{
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                              forBarMetrics:UIBarMetricsDefault];
                self.navigationController.view.backgroundColor = [UIColor clearColor];
                self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
                //                self.title = @"";
                navigationTitleLabel.text = @"";
                self.navigationItem.titleView = navigationTitleLabel;
                
                //}];
            }
            
        }
    }
}

@end
