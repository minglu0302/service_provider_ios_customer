//
//  JobPhotosPoppUpCollectionViewCell.h
//  OnTheWay_Customer
//
//  Created by Apple on 05/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPhotosPoppUpCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *jobPhoto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
