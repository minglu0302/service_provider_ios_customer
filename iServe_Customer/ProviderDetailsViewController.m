//
//  ProviderDetailsViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderDetailsViewController.h"
#import "DetailsTableViewCell.h"
#import "ProviderJobPhotosTableViewCell.h"
#import "ProviderReviewsTableViewCell.h"
#import "ProviderBookingViewController.h"
#import "SlotsTableViewCell.h"


@interface ProviderDetailsViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,WebServiceHandlerDelegate,SocketWrapperDelegate>
{
    NSMutableArray *sectionTitleArray;
    CGRect screenSize;
    NSMutableArray *providerDetails;
    AppointmentLocation *apLocation;
    
    SlotsTableViewCell *slotsCell;
    ProviderJobPhotosTableViewCell *jobPhotosCell;
    
    NSMutableArray *arrayOfSlotsButtons;
    NSInteger selectedSlotButtonTag;
    
    UILabel *navigationTitleLabel;
    NSMutableAttributedString *navigationAttributedTitle;
    
    SocketIOWrapper *socketWrapper;
    NSTimer *publishToSocketTimer;
    NSUserDefaults *ud;
    NSString *selectedProviderType;
}
@end

static ProviderDetailsViewController *providerDetailVC = nil;

@implementation ProviderDetailsViewController

#pragma mark - Initial Methods -

+ (instancetype) getSharedInstance
{
    return providerDetailVC;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    screenSize = [[UIScreen mainScreen]bounds];

    apLocation = [AppointmentLocation sharedInstance];
    ud = [NSUserDefaults standardUserDefaults];
    
    providerDetailVC = self;
    
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    self.ratingView.markFont = [UIFont systemFontOfSize:18];
    
    selectedProviderType = [ud objectForKey:@"selectedProviderTypeTag"];

    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView setContentOffset:CGPointMake(0, 0)];
    //Navigation Bar To Transparent
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    //Show Navigation Title
    navigationTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    navigationTitleLabel.backgroundColor = [UIColor clearColor];
    navigationTitleLabel.numberOfLines = 0;
    navigationTitleLabel.textAlignment = NSTextAlignmentCenter;
    navigationTitleLabel.adjustsFontSizeToFitWidth = YES;
   
    [self setNavigationTitle];
   
    
    //Creating Section Titles Array
    sectionTitleArray = [[NSMutableArray alloc]init];
    
//    if(self.isLaterBooking)
//    {
        [sectionTitleArray addObject:LS(@"SLOTS")];
//    }
    
    [sectionTitleArray addObject:LS(@"ABOUT ME")];
    [sectionTitleArray addObject:LS(@"AREAS OF EXPERTISE")];
    [sectionTitleArray addObject:LS(@"LANGUAGES")];
    [sectionTitleArray addObject:LS(@"JOB PHOTOS")];
    [sectionTitleArray addObject:LS(@"REVIEWS")];

    
    if(self.isFromBookingFlowScreen)
    {
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_off"] forState:UIControlStateNormal];
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_on"] forState:UIControlStateHighlighted];
        
        self.bottomView.hidden = YES;
        self.tableViewBottomConstraint.constant = -48;

    }
    else
    {
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"back_btn_off"] forState:UIControlStateNormal];
        [self.navigationBackButton setBackgroundImage:[UIImage imageNamed:@"back_btn_on"] forState:UIControlStateHighlighted];
        
        self.bottomView.hidden = NO;
        self.tableViewBottomConstraint.constant = 2;
        
        if(!self.isLaterBooking)
        {
            socketWrapper = [SocketIOWrapper sharedInstance];
            socketWrapper.socketDelegate = self;
            
            [self publishesToSocket];
            
            publishToSocketTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(publishesToSocket) userInfo:nil repeats:YES];
        }



    }
    
    self.providerImageView.image = self.providerDetailsFromPreviousController[@"image"];
    
    self.ratingView.value = [self.providerDetailsFromPreviousController[@"rating"]floatValue];
    
     dispatch_async(dispatch_get_main_queue(),^{
         [self sendRequestToGetProviderDetails];
     });
}


-(void)viewWillDisappear:(BOOL)animated
{
    //Clear All Navigation Transparent Property
    [publishToSocketTimer invalidate];
    publishToSocketTimer = nil;
    
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appD setSocketDelegateToAppDelegate];

    
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setBackgroundImage:nil
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SIOSocket -
/**
 *  publishSocketStream to receiving providers Type and Details near around customer current location
 */
-(void)publishesToSocket {
    
    NSDictionary *message = @{
                              @"btype":[NSNumber numberWithInt:3],
                              @"email": [ud objectForKey:iServeUserEmail],
                              @"lat": apLocation.pickupLatitude,
                              @"long": apLocation.pickupLongitude,
                            };
    
    
    NSLog(@"Published Message:%@",message);
    
    [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:message];
}


//Socket Response with Provider Details
-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message{
    
    NSLog(@"ProviderListVC message: %@",message);
    
    if([channelName isEqualToString:@"UpdateCustomer"] && self.isLaterBooking == NO){
        
        if ([[message objectForKey:@"flag"] intValue] == 0) //will get Provider Types From Server
        {
            //Update Providers Arround Details
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"e = %@",self.providerDetailsFromPreviousController[@"email"]];
            NSArray *results = [message[@"msg"][@"masArr"][selectedProviderType.integerValue][@"mas"] filteredArrayUsingPredicate:predicate];
            
            if(results.count == 0)
            {
                [UIHelper showMessage:@"Unfortunately selected provider went offline" withTitle:LS(@"Message")delegate:nil];
                
                AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appD setSocketDelegateToAppDelegate];

                dispatch_async(dispatch_get_main_queue(),^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }

        }
        else{
            //Maintain If No Providers Type And No providers
            [UIHelper showMessage:@"Unfortunately selected provider went offline" withTitle:LS(@"Message")delegate:nil];
            
            AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appD setSocketDelegateToAppDelegate];

            dispatch_async(dispatch_get_main_queue(),^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }
    else if([channelName isEqualToString:@"CustomerStatus"]){//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10)
        {
            //Booking Response
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"Message"])//Chat Message Response
    {
        if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
    }

    
}


#pragma mark - UIButton Actions -

- (IBAction)navigationBackButtonAction:(id)sender
{
    if(self.isFromBookingFlowScreen)
    {
        
        [AnimationsWrapperClass UIViewAnimationAnimationCurve:UIViewAnimationCurveEaseInOut
                                                   transition:UIViewAnimationTransitionCurlDown
                                                      forView:self.navigationController.view
                                                 timeDuration:0.75];


        [self.navigationController popViewControllerAnimated:NO];
        
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)pageIndicatorAction:(id)sender
{
    
}

- (IBAction)bookButtonAction:(id)sender
{
    ProviderBookingViewController *bookingVC = (ProviderBookingViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"confirmBookingVC"];
    
    NSDictionary *proDetails = @{
                                 @"miles":self.milesLabel.text,
                                 @"rating":[NSNumber numberWithFloat:self.ratingView.value],
                                 @"image":self.providerImageView.image,
                                 @"name":self.providerNameLabel.text,
                                 @"pid":[NSNumber numberWithInteger:self.providerId],
                                 @"email":self.providerDetailsFromPreviousController[@"email"]
                                };
    
    bookingVC.providerDetailsFromPreviousController = proDetails;
    
    bookingVC.providerTypeDetails = self.providerTypeDetails;
    
    if(self.isLaterBooking)
    {
        bookingVC.selectedDate = self.selectedDate;
        apLocation.bookingType = iServeBookingTypeLater;
        if([providerDetails[0]count] > 0)
        {
            bookingVC.selectedSlotDictionary = providerDetails[0][selectedSlotButtonTag];
        }
    }
    else
    {
        apLocation.bookingType = iServeBookingTypeNowWithOutDispatch;
    }
    
    
    [self.navigationController pushViewController:bookingVC animated:YES];
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-30 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}

-(void)showSlotsScrollViewContents
{
    arrayOfSlotsButtons = [[NSMutableArray alloc]init];

    double eachSlotButtonWidth = (screenSize.size.width-36)/4;

    for (int i=0; i < [providerDetails[0]count]; i++)
    {
        //Creating Of Slot Button
        UIButton *slotButton = [UIButton buttonWithType:UIButtonTypeCustom];
        slotButton.frame = CGRectMake(5+(eachSlotButtonWidth*i),0,eachSlotButtonWidth,slotsCell.scrollView.frame.size.height);
        
//        slotButton.layer.cornerRadius = 2.0;
//        slotButton.layer.borderWidth = 1.0;
//        slotButton.layer.borderColor = APP_COLOR.CGColor;
        
        slotButton.tag = i;
        [arrayOfSlotsButtons addObject:slotButton];
        
        [slotButton addTarget:self action:@selector(slotButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (i == 0)
        {
            selectedSlotButtonTag = 0;
            [self slotButtonAction:slotButton];
        }
        
        [slotButton setTitle:[NSString stringWithFormat:@"%@\nto\n%@\n%@ %.2f",providerDetails[0][i][@"from"],providerDetails[0][i][@"to"],[ud objectForKey:iServeCurrentCountryCurrencySymbol],[providerDetails[0][i][@"slotPrice"] floatValue]] forState:UIControlStateNormal];
        slotButton.titleLabel.font = [UIFont fontWithName:OpenSans_SemiBold size:10];
        [slotButton setTitleColor:APP_COLOR forState:UIControlStateSelected];
        [slotButton setTitleColor:UIColorFromRGB(0x121212) forState:UIControlStateNormal];
        
      
        slotButton.titleLabel.numberOfLines = 0;
        slotButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [slotsCell.scrollContentView addSubview:slotButton];
        
        
    }
    if([providerDetails[0]count] > 4){
    
        slotsCell.scrollContentViewWidthConstraint.constant = eachSlotButtonWidth*[providerDetails[0]count]  - screenSize.size.width+20;
    }
    else
    {
        slotsCell.scrollContentViewWidthConstraint.constant = 0;
    }

}

/**
 *  Each Slot Button Action
 *
 *  @param sender Button Properties
 */
-(void)slotButtonAction:(UIButton *)sender {
    
    //Selected Slot Button
    UIButton *srcBtn = (UIButton *) sender;
    
    //unselect all Slot Button
    for (UIView *button in srcBtn.superview.subviews) {
        
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    
    //selecting Particular Selected Slot Button
    srcBtn.selected = YES;
    
    
    if(selectedSlotButtonTag != srcBtn.tag)
    {
        //If Selected Other Slot Button
        
        selectedSlotButtonTag = srcBtn.tag;
      
    }
    
    
}

-(void)setNavigationTitle
{
    NSArray *feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
    NSString *currncySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];
    
    NSInteger amountValue;
    NSString *title;
    
    NSInteger number;
    if(self.providerTypeDetails[@"ftype"])
    {
        number = [feeTypesArray indexOfObject:self.providerTypeDetails[@"ftype"]];
    }
    else
    {
         number = [feeTypesArray indexOfObject:self.providerTypeDetails[@"fee_type"]];
    }

    switch (number)
    {
            //Feixed Based
        case feeTypeFixed:
        {
            amountValue = [self.providerTypeDetails[@"fixed_price"]floatValue];
            
            if(self.isFromBookingFlowScreen)
            {
                self.milesLabel.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
            }
            else
            {
                self.milesLabel.text = [NSString stringWithFormat:@"%@ / %@ %td",self.providerDetailsFromPreviousController[@"miles"],currncySymbol,amountValue];
            }

        }
            break;
            
            //Hourly Based And Milleage Based
        case feeTypeHourly:
        {
            amountValue = [self.providerTypeDetails[@"price_min"]floatValue];
            if(self.isFromBookingFlowScreen)
            {
                 self.milesLabel.text = [NSString stringWithFormat:@"%@ %td Per Hr",currncySymbol,amountValue];
            }
            else
            {
                 self.milesLabel.text = [NSString stringWithFormat:@"%@ / %@ %td Per Hr",self.providerDetailsFromPreviousController[@"miles"],currncySymbol,amountValue];
            }

        }
            
            break;
            
        default:
        {
            amountValue = [UIHelper convertMilesToKilometer:[self.providerTypeDetails[@"price_mile"]floatValue]];
            if(self.isFromBookingFlowScreen)
            {
                 self.milesLabel.text = [NSString stringWithFormat:@"%@ %td Per Km",currncySymbol,amountValue];
            }
            else
            {
                 self.milesLabel.text = [NSString stringWithFormat:@"%@ / %@ %td Per Km",self.providerDetailsFromPreviousController[@"miles"],currncySymbol,amountValue];
            }

           

        }
            break;
            
    }
    
    
    
    title = [NSString stringWithFormat:@"%@\n%@",self.providerNameLabel.text,self.milesLabel.text];

    navigationAttributedTitle = [[NSMutableAttributedString alloc]initWithString:title];
    
    [navigationAttributedTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_Regular size:15] range:NSMakeRange(0,self.providerNameLabel.text.length)];
    [navigationAttributedTitle addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(0,self.providerNameLabel.text.length)];
    
    [navigationAttributedTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_Regular size:10] range:NSMakeRange(self.providerNameLabel.text.length,title.length-self.providerNameLabel.text.length)];
    [navigationAttributedTitle addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(self.providerNameLabel.text.length,title.length-self.providerNameLabel.text.length)];

    
    [navigationTitleLabel setAttributedText:navigationAttributedTitle];

}

#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return providerDetails.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     //For Slots Number Of Rows When Not Later Booking
    if(self.isLaterBooking == NO && section == 0)
    {
        return 0;
    }
    //For Review Section
    if(section == providerDetails.count-1)
    {
        return [providerDetails[section] count];
    }
    else
    {
        //Other Sections
        return 1;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    float width = tableView.bounds.size.width;
    float Height = 24;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, Height)];
    view.backgroundColor = [UIColor whiteColor];
    
    //For Slots Section Header When Not Later Booking
    if(self.isLaterBooking == NO && section == 0)
    {
        view.frame = CGRectMake(0, 0, 0, 1);
        view.backgroundColor = UIColorFromRGB(0xf6f7f8);
        return view;
    }

    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,0,width-10,22)];
    label.text = sectionTitleArray[section];
    label.textColor = APP_COLOR;
    label.font = [UIFont fontWithName:OpenSans_Regular size:11];
    
    UILabel *divider = [[UILabel alloc] initWithFrame:CGRectMake(10,22,width-20,0.5)];
    divider.backgroundColor = UIColorFromRGB(0xcccccc);
    
    [view addSubview:label];
    [view addSubview:divider];
    
    
    return view;

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view;
    
    //For Review Section
    if(section == sectionTitleArray.count-1){
        
        if([providerDetails[section] count] == 0)
        {
            //If No Reviews
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,50)];
            view.backgroundColor = UIColorFromRGB(0xffffff);
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width,50)];
            label.text = LS(@"No Reviews");
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont fontWithName:OpenSans_Regular size:15];
            label.textColor = UIColorFromRGB(0x333333);
            
            [view addSubview:label];
            return view;
        }
    }

    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,5)];
    view.backgroundColor = UIColorFromRGB(0xf6f7f8);
    
    //For Slots Section Footer When Not Later Booking
    if(self.isLaterBooking == NO && section == 0)
    {
        view.frame = CGRectMake(0, 0, 0, 0);
        return view;
    }

    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
     //For Slots Section Footer Height When Not Later Booking
    if(self.isLaterBooking == NO && section == 0)
    {
        return 1;
    }
    
    //For Review Section
    if(section == sectionTitleArray.count-1){
        
        if([providerDetails[section] count] == 0)
        {
            return 50;
        }
        return 1;
    }
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
     //For Slots Section Header Height When Not Later Booking
    if(self.isLaterBooking == NO && section == 0)
    {
        
        return 1;
    }
    return 24;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
            return 70;
            break;
        case 1:
        case 2:
        case 3:
        {
            cellIdentifier = @"detailsCell";
            DetailsTableViewCell *detailsCell = (DetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(detailsCell == nil)
            {
                detailsCell =[[DetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            
            if(indexPath.section == 0)
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            else if(indexPath.section == 1)
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            else
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            
            float height = [self measureHeightLabel:detailsCell.detailLabel];
            return 10+height;
            
        }
            break;
            
        case 4:
        {
            return 90;
        }
        case 5:
        {
            cellIdentifier = @"providerReviewCell";
            ProviderReviewsTableViewCell *reviewcell = (ProviderReviewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(reviewcell == nil)
            {
                reviewcell =[[ProviderReviewsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            reviewcell.reviewLabel.text = providerDetails[indexPath.section][indexPath.row][@"review"];
            
            float height = [self measureHeightLabel:reviewcell.reviewLabel];
            return 70+height;
            
            
        }
        default:
            return 0;
            break;
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    switch (indexPath.section)
    {
        case 0:
        {
            cellIdentifier = @"slotsCell";
            
            if(slotsCell == nil)
            {
                slotsCell = (SlotsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
                if(slotsCell == nil)
                {
                    slotsCell =[[SlotsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                }
                [slotsCell.scrollBackgroundView layoutIfNeeded];
                [slotsCell.scrollView layoutIfNeeded];
                [slotsCell.scrollContentView layoutIfNeeded];
                
                [self showSlotsScrollViewContents];
                
            }
            
            return slotsCell;
        }
            break;
        case 1:
        case 2:
        case 3:
        {
            cellIdentifier = @"detailsCell";
            DetailsTableViewCell *detailsCell = (DetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(detailsCell == nil)
            {
                detailsCell =[[DetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            if(indexPath.section == 0)
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            else if(indexPath.section == 1)
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            else
            {
                detailsCell.detailLabel.text = providerDetails[indexPath.section];
            }
            
            return detailsCell;

        }
            break;
            
        case 4:
        {
            cellIdentifier = @"providerJobPhotosCell";
            
            if(jobPhotosCell == nil)
            {
                jobPhotosCell = (ProviderJobPhotosTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
                if(jobPhotosCell == nil)
                {
                    jobPhotosCell =[[ProviderJobPhotosTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                }
            
                [jobPhotosCell reloadCollectionView:providerDetails[indexPath.section] and:self.providerId];

                return jobPhotosCell;
            }
            return jobPhotosCell;
        }
            break;
      
        default:
        {
            cellIdentifier = @"providerReviewCell";
            ProviderReviewsTableViewCell *reviewcell = (ProviderReviewsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(reviewcell == nil)
            {
                reviewcell =[[ProviderReviewsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            }
            
            reviewcell.reviewerImageView.layer.borderColor = APP_COLOR.CGColor;
            [reviewcell showReviewDetails:providerDetails[indexPath.section][indexPath.row]];
            
            if(indexPath.row == [providerDetails[indexPath.section]count]-1)
            {
                reviewcell.divider.hidden = YES;
            }
            else
            {
                reviewcell.divider.hidden = NO;
            }
            
            return reviewcell;

        }
            break;
    }
}

#pragma mark - UIAScrollView Delegates -

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.tag == 1)
    {
        
        if(scrollView.contentOffset.y >= 64)
        {
            //[UIView animateWithDuration:0.5f animations:^{
            
                self.navigationController.navigationBar.translucent = NO;
                [self.navigationController.navigationBar setBackgroundImage:nil
                                                              forBarMetrics:UIBarMetricsDefault];
                self.navigationController.view.backgroundColor = [UIColor whiteColor];
                self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
//                self.title = [NSString stringWithFormat:@"%@\n$20 Per Hour",self.providerNameLabel.text];
            
//                navigationTitleLabel.text = [NSString stringWithFormat:@"%@\n$20 Per Hour",self.providerNameLabel.text];
            
                [navigationTitleLabel setAttributedText:navigationAttributedTitle];
                self.navigationItem.titleView = navigationTitleLabel;
            
            //}];
        }
        else
        {
//            [UIView animateWithDuration:0.5f animations:^{
                self.navigationController.navigationBar.translucent = YES;
                [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                              forBarMetrics:UIBarMetricsDefault];
                self.navigationController.view.backgroundColor = [UIColor clearColor];
                self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
//                 self.title = @"";
            
                navigationTitleLabel.text = @"";
                self.navigationItem.titleView = navigationTitleLabel;

//            }];
        }
        
    }
}

#pragma mark - Web Service Call -

-(void)sendRequestToGetProviderDetails
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *temp = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_pro_id":[NSNumber numberWithInteger:self.providerId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                             };
        NSMutableDictionary *params = [[NSMutableDictionary alloc]initWithDictionary:temp];
        
        if(self.isLaterBooking)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            
            [dateFormat setDateFormat:@"MM-dd-YYYY"];
            NSString *selectedDate = [dateFormat stringFromDate:self.scheduleDate];

            [params setObject:selectedDate forKey:@"ent_slot_date"];
        }
        
        [[WebServiceHandler sharedInstance] sendRequestToGetProviderDetails:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}


#pragma mark - Web Service Response Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error || !response)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }

            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeGetProviderDetails)
            {
                providerDetails = [[NSMutableArray alloc]init];
//                if(self.isLaterBooking)
//                {
                [providerDetails addObject:response[@"slots"]];
//                }
                [providerDetails addObject:response[@"about"]];
                [providerDetails addObject:response[@"expertise"]];
                [providerDetails addObject:response[@"languages"]];
                [providerDetails addObject:response[@"job_images"]];
                [providerDetails addObject:response[@"reviews"]];
                
                NSString *providerImageUrl = response[@"pPic"];
                if([providerImageUrl isEqual:[NSNull null]]|| providerImageUrl == nil)
                {
                    providerImageUrl = @"";
                }

                
                [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:providerImageUrl]
                                          placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                     
//                                                     [self.activityIndicator stopAnimating];
                                                     
                                                 }];

                
                if([response[@"num_of_job_images"]integerValue] > 0)
                {
                    for(int i=0; i<[response[@"num_of_job_images"]integerValue]; i++)
                    {
                        NSString *jobPhotoURL = [NSString stringWithFormat:@"https://%@.s3.amazonaws.com/JobImages/%td_%d.png",Bucket,self.providerId,i];
                    
                        if([[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:jobPhotoURL])
                        {
                            [[SDImageCache sharedImageCache] removeImageForKey:jobPhotoURL withCompletion:nil];
                            NSLog(@"Removed jobPhoto SDImageCache");
                        }

                    }
                }
                
                self.providerNameLabel.text = [NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
                [self.bookButton setTitle:[NSString stringWithFormat:@"BOOK %@",[response[@"fName"]uppercaseString]] forState:UIControlStateNormal];
                self.ratingView.value = [response[@"rating"]floatValue];
                [self setNavigationTitle];
                [self.tableView reloadData];
              
            }
        }
            break;
        default:
            break;
    }
}


@end
