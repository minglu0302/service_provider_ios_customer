//
//  SignUpViewController.m
//  iServe app
//  Developed by Raghavendra
//  Created by -Tony Lu on 06/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SignUpViewController.h"
#import "OTPViewController.h"
#import "CountryNameTableViewController.h"


@interface SignUpViewController()<UIKeyboardDelegates,GetCurrentLocationDelegate>
{
    UIImage *pickedImage;
    BOOL isFBSignUp;//is Facebook Login
    BOOL isAlreadyPop;//is Controller Already Popped
    BOOL isMobileNumberValid;//MobileNumber Validate Or Not
    BOOL isReferralCodeValid;//MobileNumber Validate Or Not
    BOOL isFromValidation;
    NSMutableArray *arrayContainingCardInfo;
    GetCurrentLocation *getCurrentLocation;
    NSString *facebookId;
}

@end


@implementation SignUpViewController

#pragma mark - UILife Cycle -

-(void)viewDidLoad
{
    [super viewDidLoad];
    
//    getCurrentLocation = [GetCurrentLocation sharedInstance];
//    [getCurrentLocation getLocation];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    self.countryCodeLabel.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    self.countryImageView.image = [UIImage imageNamed:imagePath];
    
    facebookId = @"";
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;

    
    isAlreadyPop = NO;
    if(self.isFromSignIn)
    {
        [self didFacebookUserLoginWithDetails:self.fBData];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if((self.signUpNextButton.frame.size.height+self.signUpNextButton.frame.origin.y) > self.mainScrollView.frame.size.height)
    {
        self.scrollContentViewHeightConstraint.constant = (self.signUpNextButton.frame.size.height+self.signUpNextButton.frame.origin.y) - self.mainScrollView.frame.size.height;
    }
    else
    {
        self.scrollContentViewHeightConstraint.constant = 0;
    }
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width,CGRectGetHeight(self.signUpNextButton.frame)+CGRectGetMinY(self.signUpNextButton.frame)+15);
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = nil;
}

-(void)signUpValidation
{
    if(![self.activeTextfield isEqual:self.phoneNumberTextField] && ![self.activeTextfield isEqual:self.referralCodeTextField])
    {
        [self.activeTextfield resignFirstResponder];
    }
    
    
    NSString *signupFirstName = flStrForStr(self.firstnameTextfield.text);
    NSString *signupEmail = flStrForStr(self.emailTextfield.text);
    NSString *signupPassword = flStrForStr(self.passwordTextfield.text);
    NSString *phoneNumber =flStrForStr(self.phoneNumberTextField.text);
    
    if ((unsigned long)signupFirstName.length == 0)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [UIHelper showMessage:LS(@"Enter First Name") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Enter Email Id") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else if ([self emailValidationCheck:signupEmail] == 0 )
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Invalid Email Id") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else if ((unsigned long)phoneNumber.length == 0)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Enter Phone Number") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Enter Password") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else if ([self checkPasswordStrength:self.passwordTextfield.text] == 0 && isFBSignUp == NO)
    {
        [self.passwordTextfield becomeFirstResponder];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Please ensure your password has atleast a capital letter, a small letter and a number consisting of minimum 7 characters.") withTitle:LS(@"Message")delegate:self];
        });
        
        return;
    }
    else if(isMobileNumberValid == NO)
    {
        if([self.activeTextfield isEqual:self.phoneNumberTextField])
        {
            [self.activeTextfield resignFirstResponder];
        }

    }
    else if(isReferralCodeValid == NO && self.referralCodeTextField.text.length > 0)
    {
        if([self.activeTextfield isEqual:self.referralCodeTextField])
        {
             [self.activeTextfield resignFirstResponder];
        }
        
    }
    else if (!self.checkMarkButton.isSelected)
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [UIHelper showMessage:LS(@"Please accept the Terms and Conditions") withTitle:LS(@"Message")delegate:self];
        });
        return;
    }
    else
    {
        [self sendServiceForUserSignUp];
    }
    
}
-(void)closeKeyboard
{
     [self.activeTextfield resignFirstResponder];
}

-(void)showAlertMessage:(NSString *)message
{
    
}


#pragma mark - ImagePickerDelegate -

//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    switch (buttonIndex)
//    {
//        case 0:
//        {
//            [self cameraButtonClicked];
//            break;
//        }
//        case 1:
//        {
//            [self libraryButtonClicked];
//            break;
//        }
//        case 2:
//        {
//            [self deSelectProfileImage];
//            break;
//        }
//        default:
//            break;
//    }
//    
//}

-(void)deSelectProfileImage {
    
    self.signUpProfileImageView.image =  [UIImage imageNamed:@"register_profile_default_image"];
    pickedImage = nil;
    
}

-(void)cameraButtonClicked
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        [UIHelper showMessage:LS(@"Camera is not available") withTitle:LS(@"Message")delegate:self];
    }
}

-(void)libraryButtonClicked
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else {
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    pickedImage = [self imageWithImage:pickedImage scaledToSize:CGSizeMake(90,90)];
    self.signUpProfileImageView.image = pickedImage;
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 1.5);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UIButton Actions -

- (IBAction)signUpNextButton:(id)sender
{
    [self signUpValidation];
}

- (IBAction)backButtonClick:(id)sender
{
    isAlreadyPop = YES;
    if(self.isFromSignIn)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                  subType:kCATransitionFromBottom
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController popViewControllerAnimated:NO];

    }
    
}

- (IBAction)checkMarkButton:(id)sender
{
    if(self.checkMarkButton.isSelected)
    {
        self.checkMarkButton.selected = NO;
        
    }
    else
    {
        self.checkMarkButton.selected=YES;
        
    }
}

- (IBAction)termsConditionButton:(id)sender
{
    [self performSegueWithIdentifier:@"Terms&Condition" sender:self];
}


- (IBAction)countryButton:(id)sender
{
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
    {
        self.countryImageView.image=flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        self.countryCodeLabel.text = countryCode;
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

- (IBAction)signUpTapGestureAction:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)signupWithFacebookButtonAction:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    handler.isSignInOrSignUp = 2;
    [handler setDelegate:self];
    [handler loginWithFacebook:self];
    
}

- (IBAction)imageButtonAction:(id)sender
{
    [self.view endEditing:YES];
//    UIActionSheet *actionSheet;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:LS(@"Select Picture") message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Cancel") style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Take Photo") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take Photo button tapped.
        
        [self cameraButtonClicked];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Choose From Library") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose From Library button tapped.
        
        [self libraryButtonClicked];
    }]];
    
    
    
//    if (![self.signUpProfileImageView.image isEqual:[UIImage imageNamed:@"register_profile_default_image"]])
    if (![UIImagePNGRepresentation(self.signUpProfileImageView.image) isEqualToData:UIImagePNGRepresentation([UIImage imageNamed:@"register_profile_default_image"])])
    {
        [actionSheet addAction:[UIAlertAction actionWithTitle:LS(@"Remove Profile Photo") style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // Choose From Library button tapped.
            
            [self deSelectProfileImage];
        }]];
    
    }
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];

}

#pragma mark - KeyBoardHideAndShow -

-(void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.mainScrollView.scrollIndicatorInsets = ei;
        self.mainScrollView.contentInset = ei;
    }];
}

-(void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.mainScrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.mainScrollView.scrollIndicatorInsets = ei;
    self.mainScrollView.contentInset = ei;
}

#pragma mark - TextField Delegates -


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextfield = textField;
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == 3)
    {
        isMobileNumberValid = NO;
    }
    else if(textField.tag == 5)
    {
        isReferralCodeValid = NO;
    }

    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField.tag == 3)
    {
        isMobileNumberValid = NO;
    }
    else if(textField.tag == 5)
    {
        isReferralCodeValid = NO;
    }

    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(isAlreadyPop == NO)
    {
        switch (textField.tag) {
                
            case 2: //Email TexttField
                if ([self emailValidationCheck:self.emailTextfield.text])
                {
                    [self.emailTextfield resignFirstResponder];
                    [self validateEmailAndPostalCode];
                }
                
                break;
            case 3://Phone Number TexttField
                if (self.phoneNumberTextField.text.length > 0 && isMobileNumberValid == NO)
                {
                    [self validateMobileNumber];
                }
                
                break;
            case 4://Password TexttField
            {
                [self.passwordTextfield resignFirstResponder];
                
            }
                break;
            case 5://Referral TexttField
            {
                if (self.referralCodeTextField.text.length > 0 && isReferralCodeValid == NO)
                {
                    [self validateReferralCode];
                }
                
            }
                break;
  
            default:
                break;
        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag) {
            
        case 0:
            if(textField == self.firstnameTextfield)
            {
                [self.lastnameTextfield becomeFirstResponder];
            }
            
            break;
        case 1:
            if(textField == self.lastnameTextfield)
            {
                [self.emailTextfield becomeFirstResponder];
            }
            
            break;
        case 2:
            if (textField == self.emailTextfield)
            {
                if ([self emailValidationCheck:self.emailTextfield.text] == 0)
                {
                    [UIHelper showMessage:LS(@"Please enter a valid email") withTitle:LS(@"Message")delegate:self];
                    [self.emailTextfield becomeFirstResponder];
                }
                else{
                    
                    [self.phoneNumberTextField becomeFirstResponder];
                }
                
            }
            
            break;
        case 3:
            if (textField == self.phoneNumberTextField)
            {
                [self.passwordTextfield becomeFirstResponder];
            }
            break;
        case 4:
            if (textField == self.passwordTextfield)
            {
//                [textField resignFirstResponder];
                [self.referralCodeTextField becomeFirstResponder];
            }
            break;
        case 5:
            if (textField == self.referralCodeTextField)
            {
                [textField resignFirstResponder];
                [self signUpValidation];
            }
            break;
  
        default:
            [textField resignFirstResponder];
            break;
    }
    return YES;
}

#pragma mark - Local Validations -

-(BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

- (BOOL)phoneNumberValidationCheck: (NSString *)phoneNumberToValidate
{
    NSString *phoneReg = @"[0-9]{10}";
    NSPredicate *phoneValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneReg];
    return [phoneValidation evaluateWithObject:phoneNumberToValidate];
}

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    int strength = 0;
    
    if (len == 0 || len < 7)
    {
        return 0;
    }
    else if (len >= 7)
    {
        strength++;
    }
    else if (len <= 10)
    {
        strength +=2;
    }
    else
    {
        strength +=3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    NSAssert(regex, @"Unable to create regular expression");
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    BOOL didValidate = 0;
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    return didValidate;
}

#pragma mark - Facebook SignUp Delegates -

- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo
{
    NSLog(@"FB Data =  %@", userInfo);
    
    self.fBData = userInfo;
    
    if (self.fBData == nil)
    {
        [self.firstnameTextfield becomeFirstResponder];
        return;
    }
    
    self.firstnameTextfield.text = self.fBData[@"first_name"];
    self.lastnameTextfield.text = self.fBData[@"last_name"];
    
//    self.passwordBackgroundViewHeightConstraint.constant = 0;
//    self.passwordBackgroundView.hidden = YES;
//    self.passwordTextfield.text = [self.fBData objectForKey:@"id"];
    facebookId = [self.fBData objectForKey:@"id"];

    
    isFBSignUp = YES;
    
    if([self.fBData[@"email"] length])
    {
        self.emailTextfield.text = self.fBData[@"email"];
        [self validateEmailAndPostalCode];
    }
    else
    {
        self.emailTextfield.text = @"";
        [self.emailTextfield becomeFirstResponder];
    }
   
    
    /**
     *     Storing the facebook profile image in picked image
     *
     */
    
    NSString *imageURLForLarge = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",[self.fBData objectForKey:@"id"]];
    
    [self.activityIndicator startAnimating];
    
    [self.signUpProfileImageView sd_setImageWithURL:[NSURL URLWithString:imageURLForLarge]
                                   placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl){
                                              if(!error)
                                              {
                                                  [self.activityIndicator stopAnimating];
                                                  pickedImage = self.signUpProfileImageView.image ;
                                                  [self imageWithImage:pickedImage scaledToSize:CGSizeMake(90,90)];
                                              }
                                          }];
    
}


- (void)didFailWithError:(NSError *)error
{
    [UIHelper showMessage:[error localizedDescription] withTitle:LS(@"Message")delegate:self];
}

-(void)didUserCancelLogin
{
    
}

#pragma mark - WebServiceCall -

- (void)validateEmailAndPostalCode
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Validating Email...", @"Validating Email...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        
        {
            NSDictionary *params = @{
                                     kTWTEmail:flStrForStr(self.emailTextfield.text),
                                     kTWTZipCode:flStrForStr([Utilities getZipCode]),
                                     kTWTEnterUserTypekey:flStrForStr([Utilities getUserType]),
                                     kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                     };
            
            [[WebServiceHandler sharedInstance] sendRequestToverifyEmail:params andDelegate:self];
        }
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}

- (void)validateMobileNumber
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Validating Mobile Number...", @"Validating Mobile Number...")];
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTMobile:flStrForStr(self.phoneNumberTextField.text),
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToverifyMobileNumber:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}
- (void)validateReferralCode
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Validating Referral Code...", @"Validating Referral Code...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_coupon":flStrForStr(self.referralCodeTextField.text),
                                 @"ent_lat":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLat]),
                                 @"ent_long":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLong]),
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToverifyReferralCode:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}

- (void)sendServiceForUserSignUp
{
//    if(pickedImage)
//    {
//        [[UploadImagesToAmazonServer sharedInstance]uploadProfileImageToServer:pickedImage andEmail:self.emailTextfield.text andType:0];
//    }
    self.signupParameters = @{
                              iServeUserFirstName:flStrForStr(self.firstnameTextfield.text),
                              iServeUserLastName:flStrForStr(self.lastnameTextfield.text),
                              iServeUserEmail:flStrForStr(self.emailTextfield.text),
                              iServeUserMobile:flStrForStr(self.phoneNumberTextField.text),
                              iServeUserPassword:flStrForStr(self.passwordTextfield.text),
                              iServeUserCountryCode:flStrForStr(self.countryCodeLabel.text),
                              iServeReferralCode:flStrForStr(self.referralCodeTextField.text),
                              iServeUserProfilepic:self.signUpProfileImageView.image,
                              iServeUserFBPassword:flStrForStr(facebookId)
                            };
    
    [self performSegueWithIdentifier:@"TWTOTPViewController" sender:nil];
}

-(void)sendServiceForFacebookLogin:(NSDictionary *)userData
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Login...", @"Login...")];
        
        NSDictionary *params = @{
                                 kTWTEmail:flStrForStr(userData[@"email"]),
                                 kTWTPassword:flStrForStr(userData[@"id"]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
//                                 kTWTPushToken:flStrForStr([Utilities getPushToken]),
                                 kTWTDeviceType:flStrForStr([Utilities getDeviceType]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                 @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                                 @"ent_dev_model":[UIHelper getModelName],
                                 @"ent_manf":@"APPLE",
                                 kTWTPushToken:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeFCMTokenKey]),
                                 @"ent_signup_type":[NSNumber numberWithInt:2],
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToSignIN:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}



#pragma mark - WebServiceDelegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    
    if (errFlag == 1)
    {
        switch (requestType)
        {
            case RequestTypeVerifyEmail:
            {
                if(isFBSignUp == NO)
                {
                    [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                    self.emailTextfield.text = @"";
                    [self.emailTextfield becomeFirstResponder];
                    
                }
                else
                {
//                    if([response[@"errNum"] integerValue] == 2)
//                    {
//                         [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
//                        self.emailTextfield.text = @"";
//                        [self.emailTextfield becomeFirstResponder];
//                    }
//                    else {
                        isFBSignUp = NO;
                        [self performSelector:@selector(sendServiceForFacebookLogin:)
                                   withObject:self.fBData
                                   afterDelay:0.5];
//                    }
                    
                }
                
            }
                break;
            case RequestTypeVerifyMobileNumber:
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                self.phoneNumberTextField.text = @"";
                [self.phoneNumberTextField becomeFirstResponder];
            }
                break;
                
            case RequestTypeVerifyReferralCode:
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                self.referralCodeTextField.text = @"";
                [self.referralCodeTextField becomeFirstResponder];
                
            }
                break;
                
            case RequestTypeSignIn:
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                self.emailTextfield.text = @"";
                [self.emailTextfield becomeFirstResponder];
            }
   
            default:
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
                break;
        }
        
    }
    else if(errFlag == 0 && requestType == RequestTypeVerifyEmail)
    {
        if(isFBSignUp)
        {
            self.phoneNumberTextField.text = @"";
            [self.phoneNumberTextField becomeFirstResponder];
        }
    }
    else if(errFlag == 0 && requestType == RequestTypeVerifyMobileNumber)
    {
//        if(isFBSignUp)
//        {
//            [self signUpValidation];
//        }
        isMobileNumberValid = YES;
    }
    else if(errFlag == 0 && requestType == RequestTypeVerifyReferralCode)
    {
        if(isFBSignUp)
        {
            [self signUpValidation];
        }
        isReferralCodeValid = YES;
    }

    else if(errFlag == 0 && requestType == RequestTypeSignIn)
    {
        // Save information
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        
        [ud setObject:flStrForStr(response[@"CustId"]) forKey:iServeCustomerID];
        [ud setObject:flStrForStr(response[@"fname"]) forKey:iServeUserFirstName];
        [ud setObject:flStrForStr(response[@"lname"]) forKey:iServeUserLastName];
        [ud setObject:flStrForStr(response[@"token"]) forKey:iServeCheckUserSessionToken];
        [ud setObject:flStrForStr(response[@"apiKey"]) forKey:iServeAPIKey];
        [ud setObject:flStrForStr(response[@"coupon"]) forKey:iServeCouponkey];
        [ud setObject:flStrForStr(response[@"email"])  forKey:iServeUserEmail];
        [ud setObject:self.fBData[@"id"] forKey:iServeUserPassword];
        [ud setObject:flStrForStr(response[@"currency"]) forKey:iServeCurrentCountryCurrencySymbol];
        [ud setObject:flStrForStr(response[@"profilePic"]) forKey:iServeUserProfilepic];

        [ud synchronize];
        
        if(response[@"stripe_pub_key"])
        {
            [Stripe setDefaultPublishableKey:response[@"stripe_pub_key"]];
        }

        
        arrayContainingCardInfo = [[NSMutableArray alloc ] initWithArray:response[@"cards"]];
        
        if (arrayContainingCardInfo || arrayContainingCardInfo.count){
            
             [[CardAddOrDeleteClass sharedInstance]updateCardsInDatabase:arrayContainingCardInfo];
        }
        
        if([response[@"Languages"]count] > 0)
        {
            [ud setObject:response[@"Languages"]  forKey:iServeUserLanguages];
            [ud setInteger:0 forKey:iServeUserSelectedLanguageNumber];
            [ud synchronize];
            
        }

        
        [[LiveBookingStatusUpdateClass sharedInstance]sendRequestToGetAllonGoingBookings];
        [[AdressWrapperClass sharedInstance] sendRequestToGetAddress];
        
        [self performSegueWithIdentifier:@"fromSignUptoMainVC" sender:nil];
    }
    
}


#pragma mark - prepareForSegue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"TWTOTPViewController"])
    {
        OTPViewController *vc = [segue destinationViewController];
        vc.signUpDictionary = self.signupParameters;
    }
}


@end
