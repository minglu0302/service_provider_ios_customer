//
//  TWTSecWebViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 19/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportWebViewController : UIViewController<UIWebViewDelegate>
/**
 *  Navigation back button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationBackbutton;
/**
 *  Webview outlet
 */
@property (weak, nonatomic) IBOutlet UIWebView *webview;
/**
 *  TopView constraints outlet
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topview;
/**
 *  Weburl string Varialbe
 */
@property(nonatomic,strong) NSString *weburlString;
/**
 *
 *  Back button action for going to root view controller
 *  @param sender 
 */
- (IBAction)backButton:(id)sender;

@end
