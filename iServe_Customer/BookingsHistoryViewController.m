
//  BookingsViewController.m
//  UBER
//
//  Created by -Tony Lu on 05/02/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import "BookingsHistoryViewController.h"
#import "LeftMenuVC.h"
#import "BookingHistorySectionHeaderCell.h"
#import "BookingDetailCell.h"
#import "ShowOnGoingBookingsViewController.h"
#import "InvoicePopUpView.h"

static BookingsHistoryViewController *bookingHistoryVC = nil;

@interface BookingsHistoryViewController ()<WebServiceHandlerDelegate,SocketWrapperDelegate>
{
    NSArray *onGoingBookings;
    NSMutableArray *pastBookings;
    SocketIOWrapper *socketWrapper;
    UIWindow *window;
    InvoicePopUpView *invoicePopUp;
    NSInteger pageIndex;
}

@end

@implementation BookingsHistoryViewController


#pragma mark - Initial Methods -
+ (instancetype) getSharedInstance
{
    return bookingHistoryVC;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    bookingHistoryVC = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    pastBookings = [[NSMutableArray alloc]init];
    pageIndex = 0;
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.isFromInvoice != YES)
    {
        pageIndex = 0;
        [self hideLoadMoreButton];
        [self sendRequestToGetBookingDetails];
    }
    
    socketWrapper = [SocketIOWrapper sharedInstance];
    socketWrapper.socketDelegate = self;
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] enableSlidePanGestureForLeftMenu];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [[AMSlideMenuMainViewController getInstanceForVC:self] disableSlidePanGestureForLeftMenu];
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appD setSocketDelegateToAppDelegate];
}

#pragma mark - UITableView Delegate -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(onGoingBookings.count > 0 ||pastBookings.count >0)
    {
        [self.messageLabel setHidden:YES];
        return 2;
    }
    [self.messageLabel setHidden:NO];
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return onGoingBookings.count;
            break;
        default:
            return pastBookings.count;
            break;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *cellIdentifier = @"sectionCell";
    BookingHistorySectionHeaderCell *sectionCell = (BookingHistorySectionHeaderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    switch (section) {
        case 0:
            
            if(onGoingBookings.count > 0){
                
                sectionCell.sectionTitle.text = LS(@"UPCOMING BOOKINGS");
            }
            else{
                sectionCell.sectionTitle.text = @"";
            }
            
            
            break;
        default:
            
            if(pastBookings.count > 0){
                
                sectionCell.sectionTitle.text = LS(@"PAST BOOKINGS");
            }
            else{
                sectionCell.sectionTitle.text = @"";
            }
            
            break;
    }
    
    return sectionCell.contentView;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
        {
            if(onGoingBookings.count> 0){
                return 45;
            }
            return 1;
        }
            break;
            
        default:
        {
            if(pastBookings.count> 0){
                return 45;
            }
            return 1;
        }
            break;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-108, 0)];
    label.font = [UIFont fontWithName:OpenSans_Regular size:11.0];
    float height;
    
    switch (indexPath.section)
    {
        case 0:
        {
            label.text = onGoingBookings[indexPath.row][@"addrLine1"];
            height = [self measureHeightLabel:label] + 100;
        }
            break;
            
        default:
        {
            label.text = pastBookings[indexPath.row][@"addrLine1"];
            height = [self measureHeightLabel:label] + 100 + 15;//15 for amount Label
        }
            break;
    }
    
    return height;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"bookingDetailCell";
    BookingDetailCell *bookingDetailCell = (BookingDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if(bookingDetailCell == nil)
    {
        bookingDetailCell =[[BookingDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if(indexPath.section == 0)
    {
        [bookingDetailCell showEachBookingRow:onGoingBookings[indexPath.row]];
        bookingDetailCell.amountLabelHeightConstraint.constant = 0;
        [bookingDetailCell layoutIfNeeded];
        
    }
    else
    {
        [bookingDetailCell showEachBookingRow:pastBookings[indexPath.row]];
        bookingDetailCell.amountLabelHeightConstraint.constant = 15;//Show amount Label
        [bookingDetailCell layoutIfNeeded];
        bookingDetailCell.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[[NSUserDefaults standardUserDefaults]objectForKey:iServeCurrentCountryCurrencySymbol],[pastBookings[indexPath.row][@"amount"] floatValue]];
    }
    
    return bookingDetailCell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            [self performSegueWithIdentifier:@"toAcceptVC" sender:indexPath];
        }
            break;
            
        default:
        {
//            if([pastBookings[indexPath.row][@"statCode"] integerValue] == 7)
//            {
                window = [[[UIApplication sharedApplication]delegate]window];
                
                invoicePopUp = [InvoicePopUpView sharedInstance];
                invoicePopUp.bookingStatus = [pastBookings[indexPath.row][@"statCode"] integerValue];
            
                invoicePopUp.isFromPastOrder = YES;
                [invoicePopUp setTopViewFrame];

                [window addSubview:invoicePopUp];
                
                invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                [UIView animateWithDuration:0.5
                                      delay:0.2
                                    options: UIViewAnimationOptionBeginFromCurrentState
                                 animations:^{
                                     
                                     invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                                     
                                 }
                                 completion:^(BOOL finished){
                                     
//                                     invoicePopUp.isFromPastOrder = YES;
                                     invoicePopUp.bookingId = [pastBookings[indexPath.row][@"bid"] integerValue];
                                      [invoicePopUp sendRequestTogetBookingDetails];
                                 }];

                
                
                
               
                
//            }
//            else
//            {
//                [UIHelper showMessage:[UIHelper getBookingStatusString:[pastBookings[indexPath.row][@"statCode"] integerValue]] withTitle:LS(@"Message") delegate:self];
//            }
        }
            break;
    }
    
}

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

#pragma mark - Custom Methods -

-(void)hideLoadMoreButton
{
    self.loadMoreBackGroundView.hidden = YES;
    //    CGRect frame = self.loadMoreBackGroundView.frame;
    //    frame.size.height = 0;
    //    self.loadMoreBackGroundView.frame = frame;
    //    [self.tableView layoutIfNeeded];
}
-(void)showLoadMoreButton
{
    self.loadMoreBackGroundView.hidden = NO;
    //    CGRect frame = self.loadMoreBackGroundView.frame;
    //    frame.size.height = 35;
    //    self.loadMoreBackGroundView.frame = frame;
    //    [self.loadMoreBackGroundView layoutIfNeeded];
    //    [self.loadMoreButton layoutIfNeeded];
    //    [self.tableView layoutIfNeeded];
}


#pragma mark - WebService Call -

-(void)sendRequestToGetBookingDetails
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_page_index":[NSString stringWithFormat:@"%td",pageIndex],//[NSNumber numberWithInteger:pageIndex],
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetAllAppointments:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
        
    }
    
}

- (IBAction)loadMoreButtonAction:(id)sender
{
    pageIndex = pageIndex+1;
    [self sendRequestToGetBookingDetails];
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else if ((errNum == 65 && requestType == RequestTypeGetAllApptDetails)||(errNum == 30 && requestType == RequestTypeGetAllApptDetails))
            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                self.tableView.backgroundView = self.messageLabel;
                [self.tableView reloadData];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }

            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeGetAllApptDetails)
            {
                if(pageIndex == 0)
                {
                    pastBookings = [[NSMutableArray alloc]init];
                    onGoingBookings = response[@"ongoing_appts"];
                }
                
                [pastBookings addObjectsFromArray:response[@"past_appts"]];
                self.tableView.backgroundView = self.messageLabel;
                [self.tableView reloadData];
                
                if([response[@"lastcount"]integerValue] == 1)
                {
                    if(self.loadMoreBackGroundView.hidden == NO)
                    {
                        [self hideLoadMoreButton];
                    }
                }
                else
                {
                    if(self.loadMoreBackGroundView.hidden == YES)
                    {
                        [self showLoadMoreButton];
                    }
                    
                }
                
            }
            
        }
            break;
        default:
        {
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
        }
            break;
    }
    
}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSIndexPath *indexPath = (NSIndexPath *)sender;
    if([segue.identifier isEqualToString:@"toInvoiceVC"])
    {
//        InvoiceViewController *invoiceVC = [segue destinationViewController];
//        invoiceVC.bookingDetails = pastBookings[indexPath.row];
//        self.isFromInvoice = YES;
        
    }
    else
    {
        ShowOnGoingBookingsViewController *showOnGoingBookingVC = [segue destinationViewController];
        showOnGoingBookingVC.status = [onGoingBookings[indexPath.row][@"statCode"] integerValue];
        showOnGoingBookingVC.bookingId = [onGoingBookings[indexPath.row][@"bid"] integerValue];
        self.isFromInvoice = NO;
    }
}

#pragma mark - Socket Response -



-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message
{
    NSLog(@"Booking History message: %@",message);
    
    if([channelName isEqualToString:@"CustomerStatus"]){//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10){//Booking Response
            
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"Message"])//Chat Message Response
    {
        if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
    }
    
}

@end
