//
//  PickUpViewController.h
//  DoctorMapModule
//
//  Created by -Tony Lu on 04/02/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Getting Address From DataBase Delegate Method
 */
@protocol PickUpAddressDelegate <NSObject>

@optional

-(void)getAddressFromPickUpAddressVC:(NSDictionary *)addressDetails;

@end



@interface PickUpViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,assign) BOOL isComingFromMapVCFareButton;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBarController;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) NSMutableArray *mAddress;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (weak, nonatomic) IBOutlet UIButton *navigationbackButton;
/**
 *  Id of the above Getting address From PickUpVC Delegate Method
 */
@property (nonatomic,assign) id pickUpAddressDelegate;

- (IBAction)navigationBackButtonAction:(id)sender;


@end
