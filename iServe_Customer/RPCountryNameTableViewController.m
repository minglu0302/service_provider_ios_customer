//
//  TableViewController.m
//  Sup
//
//  Created by Rahul Sharma on 3/5/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import "RPCountryNameTableViewController.h"
#import "CountryPicker.h"
#import "RPCountryPickerCell.h"


@interface RPCountryNameTableViewController ()

@end


@implementation RPCountryNameTableViewController
@synthesize details;
@synthesize countrycode;
@synthesize oncomplete;
@synthesize countryNameFirstString;


#pragma marks - table view life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    countryNameFirstString = [[NSMutableArray alloc]init];
    self.globalyAccesableArray = [[NSMutableArray alloc] init];
    details = [CountryPicker countryNames];
    countrycode = [CountryPicker countryCodes];
    for (int i=0; i<[details count]-1; i++)
    {
        if([[details[i] substringToIndex:1] isEqualToString:[details[i+1] substringToIndex:1]])
        {
            
        }
        else
        {
            [countryNameFirstString addObject:[details[i] substringToIndex:1]];
        }
        if(i==[details count]-2)
        {
            [countryNameFirstString addObject:[details[i] substringToIndex:1]];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.hidesBackButton = NO;
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:255/255.0f green:209/255.0f blue:18/255.0f alpha:1.0f]];
    [self.navigationItem setTitle:NSLocalizedString(@"COUNTRY CODE", @"COUNTRY CODE")];
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
  //  [self queryGooglePlaces:searchBar.text];
    return YES;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
   
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}


-(IBAction)cancelView:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

/**
 *  used this method for Sorting an array
 *
 *  @param index used for the generated array
 *
 *  @return list of shorted characters
 */
- (NSArray *)shortArray:(int)index{
    
    NSString *charater = countryNameFirstString[index];
    
    NSPredicate *samplePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF beginswith[c] '%@'",charater]];
    
    NSArray *listOfCharacters = [details filteredArrayUsingPredicate:samplePredicate];
    return listOfCharacters;
}


#pragma mark - Table view data source and delegate methods

/*used for returning the number of sections used in the table view
 *@param take values and generate sections
 *
 * @return number of sections
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
            return 1;
    }
    return [countryNameFirstString count];
}



/**
 *  used for giving the title for header in section
 *
 *  @param tableView view will be generated
 *  @param section   number of sections
 *
 *  @return generate the first letter title for section
 */
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return nil;
    }
    return [countryNameFirstString objectAtIndex:section];
}


/**
 *  filter all countries begin with searched text given by user
 *
 *  @param searchText string provided by the user
 *  @param scope button pressed by user
 */
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"SELF beginswith[c] '%@'",searchText]];
    if (self.searchResults == nil)
    {
        self.searchResults = [[NSArray alloc]init];
    }
    self.searchResults = [details filteredArrayUsingPredicate:resultPredicate];
}
/**
 *  number of rows are going to be in the table
 *
 *  @param section numbers of section in whole view
 *
 *  @return number of counts of the sections
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.searchResults count];
    }
    else
    {
        return [[self shortArray:(int)section] count];
    }
}



- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return countryNameFirstString;
}



- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    
    return [countryNameFirstString indexOfObject:title];
}



-(BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *countryNameList;
    countryNameList = [[NSMutableArray alloc] init];
    NSString *stringCountryCode;
    static NSString *CellIdentifier = @"myCell";
    RPCountryPickerCell *cell = (RPCountryPickerCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[RPCountryPickerCell alloc ]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        [countryNameList addObjectsFromArray:self.searchResults];
        
        stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[countryNameList
                                                                              objectAtIndex:indexPath.row]];
        [self.globalyAccesableArray removeAllObjects];
        [self.globalyAccesableArray addObjectsFromArray:self.searchResults];
        
    }
    else
    {
        
        countryNameList = [[self shortArray:(int)indexPath.section] mutableCopy];
        stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[countryNameList
                                                                              objectAtIndex:indexPath.row]];
        [self.globalyAccesableArray removeAllObjects];
        [self.globalyAccesableArray addObjectsFromArray:countryNameList];
    }
    
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@",stringCountryCode];
    self.flagCountry = [UIImage imageNamed:imagePath];
    cell.imageFlag.image = self.flagCountry;
    cell.labelCountryName.text = [NSString stringWithFormat:@"%@",countryNameList[indexPath.row]];
    NSDictionary *countrycodes = [CountryPicker dictionaryWithCountryCodes];
    cell.labelCountryCode.text = [NSString stringWithFormat:@"+ %@", countrycodes[[NSString stringWithFormat:@"%@",stringCountryCode]]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (oncomplete)
    {
        if(tableView != self.searchDisplayController.searchResultsTableView )
        {
            _globalyAccesableArray = [[self shortArray:(int)indexPath.section] mutableCopy];
        }
        NSString *stringCountryCode = [[CountryPicker countryCodesByName] objectForKey:[ self.globalyAccesableArray objectAtIndex:indexPath.row]];
        
        NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", stringCountryCode];
        NSLog(@"%@",stringCountryCode);
        UIImage *flagImage  = [UIImage imageNamed:imagePath];
        NSDictionary *countrycodes = [CountryPicker dictionaryWithCountryCodes];
        NSString *code = countrycodes[[NSString stringWithFormat:@"%@",stringCountryCode]];
        
        NSDictionary *countryNames = [CountryPicker countryNamesByCode];
        NSString *countryName = countryNames[[NSString stringWithFormat:@"%@",stringCountryCode]];
        
        oncomplete(code,flagImage,countryName);
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 32;
}
- (IBAction)backButton:(id)sender
{
     [self dismissViewControllerAnimated:YES completion:nil];
}
@end
