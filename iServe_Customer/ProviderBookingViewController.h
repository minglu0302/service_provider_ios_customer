//
//  ProviderBookingViewController.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ProviderRequestingView.h"

@interface ProviderBookingViewController : UIViewController

//Navigation
//@property (strong, nonatomic) IBOutlet UIView *navigationTitleView;
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;


//HeaderView
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewWidthConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTopConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *requestingButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *requestingButtonBottomConstraint;

//Hours Picker
@property (weak, nonatomic) IBOutlet UIView *pickerBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIPickerView *hourPicker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerBackgroundViewBottomConstraint;


@property (strong, nonatomic) NSMutableDictionary *providerTypeDetails;
@property NSDictionary *providerDetailsFromPreviousController;
@property (strong, nonatomic) NSDictionary *selectedSlotDictionary;
@property (strong, nonatomic) NSString *selectedDate;
@property (strong, nonatomic) NSMutableArray *arrayOfProviderDetailsFromHomeVC;

@property (strong, nonatomic) ProviderRequestingView *requestingView;



+ (instancetype) getSharedInstance;
- (void)parseBookingAcceptedResponse;
- (IBAction)changeAddressButtonAction:(id)sender;
- (IBAction)changePaymentButtonAction:(id)sender;
- (IBAction)applyPromoCodeButtonAction:(id)sender;
- (IBAction)changeHourValueButtonAction:(id)sender;
- (IBAction)addNewPhotoButtonAction:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)requestingButtonAction:(id)sender;
- (IBAction)cancelBookingButtonAction:(id)sender;
- (IBAction)deleteJobPhotoButtonAction:(id)sender;
- (IBAction)pickerCancelButtonAction:(id)sender;
- (IBAction)pickerDonebuttonAction:(id)sender;
- (IBAction)longPressActionForJobPhotos:(id)sender;
- (IBAction)removeLongGestureAnimation:(id)sender;

@end
