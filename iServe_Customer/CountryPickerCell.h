//
//  CountryPickerCell.h
//  Sup
//
//  Created by -Tony Lu on 3/11/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryPickerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageFlag;
@property (weak, nonatomic) IBOutlet UILabel *labelCountryName;
@property (weak, nonatomic) IBOutlet UILabel *labelCountryCode;

@end
