//
//  AcceptedViewController.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"

@interface ShowOnGoingBookingsViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
@property (strong, nonatomic) IBOutlet UIView *navigationTitleView;
@property (weak, nonatomic) IBOutlet UILabel *bookingIdLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewheightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;


//Accepted Booking

@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *circularView;

@property (weak, nonatomic) IBOutlet UILabel *step1Label;
@property (weak, nonatomic) IBOutlet UILabel *step2Label;
@property (weak, nonatomic) IBOutlet UILabel *step3Label;
@property (weak, nonatomic) IBOutlet UILabel *step4Label;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

//ProviderDetails

@property (weak, nonatomic) IBOutlet UIView *providerDetailView;
@property (weak, nonatomic) IBOutlet UIView *innerView;
@property (weak, nonatomic) IBOutlet UIView *divider1;
@property (weak, nonatomic) IBOutlet UIView *divider2;
@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIButton *viewProfileButton;
@property (weak, nonatomic) IBOutlet UIView *callAndMessageBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *providerDetailsViewBottomConstraint;

//OnTheWay

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property NSInteger status;
@property NSInteger bookingId;
@property NSString *providerPhoneNumber;
@property NSString *providerChannelName;
@property BOOL isNetworkComeBackRefresh;

+ (instancetype) getSharedInstance;
- (void)sendRequestToCancelBooking:(NSString *)message;
- (void)changeViews;
- (void)sendRequestToGetBookingDetails:(NSInteger)tag;
- (void)removeReminderFromCalendar;


- (IBAction)viewProfileButtonAction:(id)sender;
- (IBAction)callButtonAction:(id)sender;
- (IBAction)messageButtonAction:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)cancelBookingButtonAction:(id)sender;
- (IBAction)swipeGestureAction:(id)sender;

@end
