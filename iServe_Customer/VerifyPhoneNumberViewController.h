//
//  VerifyPhoneNumberViewController.h
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyPhoneNumberViewController : UIViewController<UITextFieldDelegate>

@property(weak, nonatomic) IBOutlet UITextField *firstNumber;
@property(weak, nonatomic) IBOutlet UITextField *secondNumber;
@property(weak, nonatomic) IBOutlet UITextField *thirdNumber;
@property(weak, nonatomic) IBOutlet UITextField *fourthNumber;
@property (weak, nonatomic) IBOutlet UIButton *resendOTPButton;
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (strong ,nonatomic) NSString *phoneNumber;
@property (strong ,nonatomic) NSString *verificationCode;

- (IBAction)resendOTPButtonAction:(id)sender;
- (IBAction)navigationLeftButtonAction:(id)sender;

@end
