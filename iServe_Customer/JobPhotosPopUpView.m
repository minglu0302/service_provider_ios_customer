//
//  JobPhotosPopUpView.m
//  OnTheWay_Customer
//
//  Created by Apple on 05/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "JobPhotosPopUpView.h"
#import "JobPhotosPoppUpCollectionViewCell.h"

static JobPhotosPopUpView *jobPhotosPopUpView = nil;

@interface JobPhotosPopUpView ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
{
    UIWindow *window;
}

@end

@implementation JobPhotosPopUpView

#pragma mark - Initial Methods -

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self =  [[[NSBundle mainBundle] loadNibNamed:@"JobPhotosPopUpView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance
{
    if (!jobPhotosPopUpView)
    {
        jobPhotosPopUpView  = [[self alloc] initWithFrame:CGRectZero];
        jobPhotosPopUpView.frame = [[UIScreen mainScreen]bounds];
        
        [jobPhotosPopUpView.collectionView registerNib:[UINib nibWithNibName:@"JobPhotosPoppUpCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"JobPhotosPopUpCollectionViewCell"];
        
        [[jobPhotosPopUpView.jobPhoto imageView] setContentMode: UIViewContentModeScaleAspectFit];
        
    }
    
    
    return jobPhotosPopUpView;
}

#pragma mark - UIButton Actions -

- (IBAction)closeButtonAction:(id)sender
{
    jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         
                         dispatch_async(dispatch_get_main_queue(),^{
                             [jobPhotosPopUpView removeFromSuperview];
                             jobPhotosPopUpView = nil;
                         });
                     }];
    
    
    
}

#pragma mark - Custom Methods -

-(void)reloadCollectionView:(NSMutableArray *)jobPhotos and:(NSIndexPath *)selectedJobPhotoIndexPath
{
    self.selectedIndexPath = selectedJobPhotoIndexPath;
    self.arrayOfJobPhotos = jobPhotos.mutableCopy;
    [self.collectionView reloadData];
    
    [self.collectionView selectItemAtIndexPath:selectedJobPhotoIndexPath
                                      animated:YES
                                scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    [self collectionView:self.collectionView didSelectItemAtIndexPath:selectedJobPhotoIndexPath];
    
}
-(void)scrollCollectionview:(NSString *)xoffset
{
    [self.collectionView setContentOffset:CGPointMake(xoffset.floatValue, 0) animated:YES];
}

#pragma mark - UICollectionView Methods -

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.arrayOfJobPhotos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    cellIdentifier = @"JobPhotosPopUpCollectionViewCell";
    
    JobPhotosPoppUpCollectionViewCell *jobPhotosCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (jobPhotosCell == nil)
    {
        jobPhotosCell = [[[NSBundle mainBundle] loadNibNamed:@"JobPhotosPopUpView" owner:nil options:nil] objectAtIndex:1];
        jobPhotosCell.frame = CGRectMake(0, 0,collectionView.frame.size.width,collectionView.frame.size.height);
        
    }
    
    //    NSString *jobPhotoURL = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/jobImages/%td_%td.png",Bucket,self.providerId,indexPath.row];
    
    if(self.isFromBookingScreen)
    {
        jobPhotosCell.jobPhoto.image = self.arrayOfJobPhotos[indexPath.row];
    }
    else
    {
        [jobPhotosCell.activityIndicator startAnimating];
        
        [jobPhotosCell.jobPhoto sd_setImageWithURL:[NSURL URLWithString:self.arrayOfJobPhotos[indexPath.row]]
                                  placeholderImage:[UIImage imageNamed:@"profile_last_default_image"]
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                             
                                             [jobPhotosCell.activityIndicator stopAnimating];
                                             
                                             if(self.selectedIndexPath == indexPath)
                                             {
                                                 [self.jobPhoto setImage:image forState:UIControlStateNormal];
                                             }
                                             
                                         }];
        
    }
    
    
    
    
    
    if(self.selectedIndexPath.row == indexPath.row)
    {
        //        jobPhotosCell.jobPhoto.layer.borderWidth = 1.5;
        jobPhotosCell.backgroundColor = APP_COLOR;//APP_COLOR;
        //        jobPhotosCell.jobPhoto.layer.borderColor = APP_COLOR.CGColor;
        //        jobPhotosCell.jobPhoto.clipsToBounds = YES;
        
    }
    else
    {
        //        jobPhotosCell.jobPhoto.layer.borderWidth = 0.0;
        jobPhotosCell.backgroundColor = [UIColor clearColor];
        //        jobPhotosCell.jobPhoto.layer.borderColor = [UIColor clearColor].CGColor;
        //        jobPhotosCell.jobPhoto.clipsToBounds = YES;
        
    }
    
    return jobPhotosCell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100,100);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        if(self.arrayOfJobPhotos.count>1)
        {
            self.jobPhotosLabel.text = [NSString stringWithFormat:LS(@"Job Photos")];
        }
        else
        {
            self.jobPhotosLabel.text = [NSString stringWithFormat:LS(@"Job Photo")];
        }
        
        self.jobPhotoNumberLabel.text = [NSString stringWithFormat:@"%td of %td",indexPath.row+1,self.arrayOfJobPhotos.count];
        
        
        JobPhotosPoppUpCollectionViewCell *jobPhotosCell = (JobPhotosPoppUpCollectionViewCell *)[self collectionView:self.collectionView cellForItemAtIndexPath:indexPath ];
        
        [self.jobPhoto setImage:jobPhotosCell.jobPhoto.image forState:UIControlStateNormal];
        
        self.selectedIndexPath = indexPath;
        
        [self.collectionView reloadData];
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
}

@end
