//
//  ProvidersListViewController.h
//  iServe_AutoLayout
//
//  Created by Imma Web Pvt Ltd on 02/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProvidersListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *navTabButtons;

//Navigation Views
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (strong, nonatomic) IBOutlet UIView *navigationTitleView;
@property (weak, nonatomic) IBOutlet UIButton *showMapViewButton;
@property (weak, nonatomic) IBOutlet UIButton *showListViewButton;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *topAddressBackgroundView;


//Show Message Views
@property (weak, nonatomic) IBOutlet UILabel *messageLableForAll;
@property (weak, nonatomic) IBOutlet UIView *providerAvaliableMessageView;
@property (weak, nonatomic) IBOutlet UILabel *availableLabel;


//Calender
@property (weak, nonatomic) IBOutlet UIView *calenderBackGroundView;
@property (weak, nonatomic) IBOutlet UILabel *selectedDateLabel;


//Provider Type Scroll View
@property (weak, nonatomic) IBOutlet UIView *providerTypesScrollerBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *typeBottomView;
@property (weak, nonatomic) IBOutlet UIScrollView *topScrollView;
@property (weak, nonatomic) IBOutlet UIView *topScrollContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topScrollContentViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeDividerWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeDividerLeadingConstraint;



//Providers List scroll View
@property (weak, nonatomic) IBOutlet UIView *providerDetailsBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *providerDetailsTablesbackGroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *providerDetailsTablebackgroundviewBottomConstraint;


//Date Picker
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerViewBottomConstraints;


//Bottom View
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *scheduleVisitButton;
@property (weak, nonatomic) IBOutlet UIView *scheduleVisitBackgroundView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewBottomConstraint;


@property (strong,nonatomic) NSMutableArray *arrayOfProvidersArround;
@property (strong,nonatomic) NSMutableArray *arrayOfProviderTypes;
@property (strong,nonatomic) NSMutableArray *arrayOfProviderEmails;
@property BOOL isFromBoookLater;
@property NSInteger selectedTypeButtonTag;
@property (strong,nonatomic) NSDate *dateFromHomeVC;



- (IBAction)showMapViewButtonAction:(id)sender;
- (IBAction)showListViewButtonAction:(id)sender;
- (IBAction)addressButtonAction:(id)sender;
- (IBAction)scheduleVisitButtonAction:(id)sender;
- (IBAction)typeButtonAction:(id)sender;
- (IBAction)navigationLeftButtonAction:(id)sender;
- (IBAction)calenderButtonAction:(id)sender;
- (IBAction)datePickerDoneButtonAction:(id)sender;
- (IBAction)datePickerCancelButtonAction:(id)sender;

@end
