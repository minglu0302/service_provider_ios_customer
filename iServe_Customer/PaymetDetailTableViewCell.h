//
//  PaymetDetailTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymetDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cardTypeImage;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet UIButton *changePaymentButton;

-(void)setPaymentDetails;

@end
