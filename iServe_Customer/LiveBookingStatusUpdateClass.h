//
//  LiveBookingStatusUpdateClass.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/27/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShowOnGoingBookingsViewController.h"
#import "BookingsHistoryViewController.h"


@interface LiveBookingStatusUpdateClass : NSObject

+(instancetype) sharedInstance;

-(void)profileGotRejected:(NSDictionary *)response;
-(void)sendRequestToGetAllonGoingBookings;
-(void)didRecieveBookingStatusFromPushOrSocket:(NSDictionary *)response;
-(void)didRecieveChatMessageFromPushorSocket:(NSDictionary *)response;

@end
