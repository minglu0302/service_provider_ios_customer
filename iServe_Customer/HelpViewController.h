//
//  HelpViewController.h
//  iServe app
//
//  Created by -Tony Lu on 06/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>

@interface HelpViewController : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewConstraint;

@property (nonatomic,strong) CALayer *newlayer;
@property (nonatomic, strong) AVPlayer *avplayer;
@property (nonatomic,strong) NSString *avplayerTagString;
/**
 *  Bottom view outlet's
 */
@property (weak, nonatomic) IBOutlet UIView *bottomView;
/**
 *  Splash Image view  outlet's
 */
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
/**
 *  Signin Button outlet's
 */
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
/**
 *  Register Button outlet's
 */
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
/**
 *  Sign in Button action
 *
 *  @param sender
 */
- (IBAction)signinButton:(id)sender;
/**
 *  Register Button action
 *
 *  @param sender 
 */
- (IBAction)registerButton:(id)sender;

@end
