//
//  DetailsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

@end
