//
//  ProviderJobPhotosTableViewCell.m
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderJobPhotosTableViewCell.h"
#import "ProviderJobPhotosCollectionViewCell.h"
#import "JobPhotosPopUpView.h"
#import "ProviderDetailsViewController.h"

@interface ProviderJobPhotosTableViewCell ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *arrayOfDownloadedJobPhotos;
    UIWindow *window;
    //    NSInteger totalJobPhotosCount;
    JobPhotosPopUpView *jobPhotosPopUpView;
    NSInteger providerId;
}

@end

@implementation ProviderJobPhotosTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)reloadCollectionView:(NSArray *)arrayOfJobImagesURL and:(NSInteger)pid
{
    providerId = pid;
    arrayOfDownloadedJobPhotos = [[NSMutableArray alloc]initWithArray:arrayOfJobImagesURL];
    //    for(int i=0; i<noOfJobImages;i++)
    //    {
    //        NSString *jobPhotoURL = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/jobImages/%td_%d.png",Bucket,providerId,i];
    //
    //        [arrayOfDownloadedJobPhotos addObject:jobPhotoURL];
    //    }
    
    //    totalJobPhotosCount = arrayOfDownloadedJobPhotos.count;
    [self.collectionView reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    self.collectionView.backgroundView = self.messageLabel;
    
    if(arrayOfDownloadedJobPhotos.count > 0 )
    {
        [self.messageLabel setHidden:YES];
        return 1;
    }
    [self.messageLabel setHidden:NO];
    return 0;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return arrayOfDownloadedJobPhotos.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    
    cellIdentifier = @"providerJobPhotosCollectionCell";
    ProviderJobPhotosCollectionViewCell *jobPhotosCell;
    
    
    jobPhotosCell = (ProviderJobPhotosCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    [jobPhotosCell.activityIndicator startAnimating];
    
    [jobPhotosCell.jobPhoto sd_setImageWithURL:[NSURL URLWithString:arrayOfDownloadedJobPhotos[indexPath.row]]
                              placeholderImage:[UIImage imageNamed:@"profile_last_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         
                                         [jobPhotosCell.activityIndicator stopAnimating];
                                         
                                     }];
    return jobPhotosCell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    jobPhotosPopUpView = [JobPhotosPopUpView sharedInstance];
    jobPhotosPopUpView.providerId = providerId;
    
    jobPhotosPopUpView.isFromBookingScreen = NO;
    [jobPhotosPopUpView reloadCollectionView:arrayOfDownloadedJobPhotos and:indexPath];
    window = [[UIApplication sharedApplication] keyWindow];
    [window addSubview:jobPhotosPopUpView];
    
    jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
}



@end
