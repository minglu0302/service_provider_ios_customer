//
//  ServiceGroupViewController.h
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Getting Selected Service Details
 */
@protocol ServiceDetailsDelegate <NSObject>

@optional

-(void)getSelectedServiceDetails:(NSMutableArray *)selectedServiceDetails;

@end



@interface ServiceGroupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;


@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong , nonatomic) NSArray *arrayOfServiceGroups;
@property (strong , nonatomic) NSMutableArray *arrayOfSelectedServices;
@property (strong , nonatomic) NSMutableArray *arrayOfPreviouslySelectedServices;

@property (nonatomic,assign) id serviceDetailDelegate;


- (IBAction)navigationLeftButtonAction:(id)sender;
- (IBAction)doneButtonAction:(id)sender;
- (IBAction)changeServicesButtonAction:(id)sender;

@end
