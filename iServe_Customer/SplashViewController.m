//
//  RPSplashViewController.m
//  iServePassenger
//
//  Created by -Tony Lu on 4/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SplashViewController.h"
#import "HelpViewController.h"
#import "LeftMenuVC.h"
#import "CheckPushEnabledOrNot.h"

@interface SplashViewController ()
{
    GetCurrentLocation *getCurrentLocation;
}

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if (screenSize.height < 568)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone4_Screen"]];
    }
    else if(screenSize.height < 667)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone5_Screen"]];
    }
    else if(screenSize.height < 736)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone6_Screen"]];
    }
    else
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone6Plus_Screen"]];
    }
    
    if((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        NSLog(@"iphone x image");
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhoneX_Screen"]];
    }
    
    self.navigationController.navigationBarHidden = YES;
    
//    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
//        // Remove navigation bar bottom shadow line in iOS 11
//        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
//        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
//    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
//    NSLocale *theLocale = [NSLocale currentLocale];
//    [[NSUserDefaults standardUserDefaults] setObject:[theLocale objectForKey:NSLocaleCurrencySymbol] forKey:iServeCurrentCountryCurrencySymbol];
//    NSLog(@"Currency Symbol:%@",[theLocale objectForKey:NSLocaleCurrencySymbol]);
    
   /* NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    NSString *currencyCode = [formatter currencyCode];
    NSLog(@"Currency Code:%@",currencyCode);*/
    
    if(TARGET_IPHONE_SIMULATOR)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"13.028869"
                                                  forKey:iServeUserCurrentLat];
        [[NSUserDefaults standardUserDefaults] setObject:@"77.589638"
                                                  forKey:iServeUserCurrentLong];
        [[NSUserDefaults standardUserDefaults] setObject:@"3Embed Software Pvt. Ltd ,10th Cross RT Nagar"
                                                  forKey:iServeUserCurrentAddress];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(removeSplash) userInfo:Nil repeats:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)removeSplash
{
    getCurrentLocation = [GetCurrentLocation sharedInstance];
    [getCurrentLocation getLocation];
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken])
    {
        [[LiveBookingStatusUpdateClass sharedInstance]sendRequestToGetAllonGoingBookings];
        [Stripe setDefaultPublishableKey:[[NSUserDefaults standardUserDefaults] objectForKey:iServeStripeKey]];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        LeftMenuVC *menuVC = [storyboard instantiateViewControllerWithIdentifier:@"menuVC"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:menuVC, nil];
        
    }
    else
    {
        HelpViewController *helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
        
        [[self navigationController ] pushViewController:helpVC animated:NO];
        
    }
}


@end
