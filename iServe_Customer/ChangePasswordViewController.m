//
//  ChangePasswordViewController.m
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "SignInViewController.h"
#import "ForgotPasswordViewController.h"

@interface ChangePasswordViewController ()<WebServiceHandlerDelegate>
{
    NSString *oldPassword;
}
@end

@implementation ChangePasswordViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.isFromProfileVC)
    {
        oldPassword = [[NSUserDefaults standardUserDefaults] objectForKey:iServeUserPassword];
        self.passwordLabel.hidden = YES;
        self.oldPasswordLabel.hidden = NO;
        self.confirmPasswordLabel1.hidden = YES;
        self.neewPasswordLabel.hidden = NO;
        self.passwordTextField.placeholder = @"Old Password";
        self.confirmPasswordTextField1.placeholder = @"New Password";
        self.bottomViewHeightConstraint.constant = 55;
        self.confirmPasswordTextField1.returnKeyType = UIReturnKeyNext;
    }
    else
    {
        self.passwordLabel.hidden = NO;
        self.oldPasswordLabel.hidden = YES;
        self.confirmPasswordLabel1.hidden = NO;
        self.neewPasswordLabel.hidden = YES;
        self.passwordTextField.placeholder = @"Password";
        self.confirmPasswordTextField1.placeholder = @"Confirm Password";
        self.bottomViewHeightConstraint.constant = 0;
        self.confirmPasswordTextField1.returnKeyType = UIReturnKeyDone;
    }
    
    
    if(self.isFromProfileVC)
    {
        [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_off"] forState:UIControlStateNormal];
        [self.navigationLeftButton  setBackgroundImage:[UIImage imageNamed:@"current_booking_cross_icon_on"] forState:UIControlStateHighlighted];
    }
    else
    {
        [self.navigationLeftButton setBackgroundImage:[UIImage imageNamed:@"back_btn_off"] forState:UIControlStateNormal];
        [self.navigationLeftButton  setBackgroundImage:[UIImage imageNamed:@"back_btn_on"] forState:UIControlStateHighlighted];

    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.passwordTextField becomeFirstResponder];
}

#pragma mark - UIButton Actions -

- (IBAction)changePasswordButtonAction:(id)sender
{
    NSString *password = self.passwordTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField1.text;
    NSString  *neewPassword = self.confirmPasswordTextField2.text;
   
    
    if(self.isFromProfileVC)
    {
        confirmPassword = self.confirmPasswordTextField2.text;
        neewPassword = self.confirmPasswordTextField1.text;
        
        if((unsigned long)password.length == 0)
        {
            [UIHelper showMessage:NSLocalizedString(@"Please enter old password", @"Please enter old password") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
            [self.passwordTextField becomeFirstResponder];
        }
        else if (![oldPassword isEqualToString:self.passwordTextField.text])
        {
            [UIHelper showMessage:LS(@"Please enter valid old password") withTitle:LS(@"Message")delegate:nil];
            self.passwordTextField.text = @"";
            self.confirmPasswordTextField1.text = @"";
            self.confirmPasswordTextField2.text = @"";
            [self.passwordTextField becomeFirstResponder];
        }
        else if([self checkPasswordStrength:neewPassword] == 0)
        {
            [UIHelper showMessage:LS(@"Please ensure your new password has 1 capital letter, 1 small letter and 1 number atleast and in total should be 7 characters long") withTitle:LS(@"Message")delegate:nil];

            self.confirmPasswordTextField1.text = @"";
            self.confirmPasswordTextField2.text = @"";
            [self.confirmPasswordTextField1 becomeFirstResponder];
            
        }
        else if(![neewPassword isEqualToString:confirmPassword])
        {
            [UIHelper showMessage:NSLocalizedString(@"New password and confirm password should be same", @"New password and confirm password should be same") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        }
        else
        {
            [self.view endEditing:YES];
            [self.activeTextField resignFirstResponder];
            [self sendRequestToChangePassword:self.confirmPasswordTextField1.text];
        }

    }
    else if((unsigned long)password.length == 0)
    {
        [UIHelper showMessage:NSLocalizedString(@"Please enter password", @"Please enter password") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        [self.passwordTextField becomeFirstResponder];
    }
    else if([self checkPasswordStrength:password] == 0)
    {
        [UIHelper showMessage:LS(@"Please ensure your password has atleast a capital letter, a small letter and a number consisting of minimum 7 characters.") withTitle:LS(@"Message")delegate:nil];
        self.passwordTextField.text = @"";
        self.confirmPasswordTextField1.text = @"";
        [self.passwordTextField becomeFirstResponder];
        
    }
    else if(![password isEqualToString:confirmPassword])
    {
        [UIHelper showMessage:NSLocalizedString(@"Password and confirm password should be same", @"Password and confirm password should be same") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
    }
    else
    {
        [self.view endEditing:YES];
        [self.activeTextField resignFirstResponder];
        [self sendRequestToChangePassword:self.passwordTextField.text];
    }
    

}

- (IBAction)navigationLeftButtonAction:(id)sender
{
    if(self.isFromProfileVC)
    {
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                                  subType:kCATransitionFromBottom
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];
        
        [self.navigationController popViewControllerAnimated:NO];

    }
    else
    {
        for (UIViewController* viewController in self.navigationController.viewControllers)
        {
            if ([viewController isKindOfClass:[SignInViewController class]] )
            {
                [self.navigationController popToViewController:viewController animated:YES];
                return;
            }
        }

    }
    
}

#pragma mark - Password Validation -

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    int strength = 0;
    
    if (len == 0 || len < 7)
    {
        return 0;
    }
    else if (len >= 7)
    {
        strength++;
    }
    else if (len <= 10)
    {
        strength +=2;
    }
    else
    {
        strength +=3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    NSAssert(regex, @"Unable to create regular expression");
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    BOOL didValidate = 0;
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    return didValidate;
}


#pragma mark - UITextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.passwordTextField)
    {
//        if ([self checkPasswordStrength:self.passwordTextField.text] == 0)
//        {
//            [UIHelper showMessage:LS(@"Please ensure your password has 1 capital letter, 1 small letter and 1 number atleast and in total should be 7 characters long") withTitle:LS(@"Message")delegate:nil];
//            [self.passwordTextField becomeFirstResponder];
//        }
//        else{
        
            [self.confirmPasswordTextField1 becomeFirstResponder];
//        }
    }
    else if(textField == self.confirmPasswordTextField1)
    {
        if(self.isFromProfileVC)
        {
              [self.confirmPasswordTextField2 becomeFirstResponder];
        }
        else
        {
            [self changePasswordButtonAction:nil];
        }
    }
    else if(textField == self.confirmPasswordTextField2)
    {
        [self changePasswordButtonAction:nil];
    }
    return YES;
}

#pragma mark - Web Service Call -

-(void)sendRequestToChangePassword:(NSString *)password
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Changing Password...", @"Changing Password...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_mobile":flStrForObj(self.phoneNumber),
                                 @"ent_pass":password,
                                 @"ent_user_type":@"2"
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToChangePassword:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
        
    }
    
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:nil];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    //    NSInteger errNum = [response[@"errNum"] integerValue];
    
    if(errFlag == 1)
    {
        if (requestType == RequestTypeChangePassword){
            
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:nil];
            
        }
        
    }
    else  if (errFlag == 0 && requestType == RequestTypeChangePassword)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:LS(@"Message") message:response[@"errMsg"] delegate:nil cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        
//        for (UIViewController* viewController in self.navigationController.viewControllers)
//        {
//            if ([viewController isKindOfClass:[SignInViewController class]])
//            {
//                [self.navigationController popToViewController:viewController animated:YES];
//                return;
//            }
//        }
        [self navigationLeftButtonAction:nil];

    }
    
}


@end
