//
//  InvoicePopUpView.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoicePopUpView : UIView

//TopView
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *bookingIdLabel;
@property (weak, nonatomic) IBOutlet UIView *lastBookingView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewBottomConstraint;


//AddressDetail View
@property (weak, nonatomic) IBOutlet UIView *addressDetailView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpButton;


//Provider Detail View
@property (weak, nonatomic) IBOutlet UIView *providerDetailView;
@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *providerTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCostLabel;
@property (weak, nonatomic) IBOutlet UIButton *recieptButton;


//Cancellation Reason View
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cancelReasonLabel;
@property (weak, nonatomic) IBOutlet UIView *cancelReasonBackgroundView;


//Rate and Comment View
@property (weak, nonatomic) IBOutlet UIView *rateAndCommentView;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *rateYourServiceLabel;
@property (weak, nonatomic) IBOutlet UIButton *leaveACommentButton;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;


//Need Help View
@property (weak, nonatomic) IBOutlet UIView *needHelpBackGroundView;
@property (weak, nonatomic) IBOutlet UILabel *needHelpTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *needHelpSubmitButton;
@property (weak, nonatomic) IBOutlet UITextView *needHelpTextView;


//Comment View
@property (weak, nonatomic) IBOutlet UIView *commentBackGroundView;
@property (weak, nonatomic) IBOutlet UILabel *commentTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentDoneButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;



@property  NSInteger bookingId;
@property (strong, nonatomic) NSDictionary *invoiceBookingDetails;
@property BOOL isFromPastOrder;
@property NSInteger bookingStatus;



+ (id)sharedInstance;
- (void)showBookingDetails:(NSDictionary *)bookingDetails;
- (void)sendRequestTogetBookingDetails;
- (void)removeRecieptPopUp;
- (void)setTopViewFrame;
- (void)setFromPastOrderProperties:(NSDictionary *)bookingDetails;

- (IBAction)needHelpButtonAction:(id)sender;
- (IBAction)recieptButtonAction:(id)sender;
- (IBAction)submitButtonAction:(id)sender;
- (IBAction)needHelpBackButtonAction:(id)sender;
- (IBAction)needHelpSubmitButtonAction:(id)sender;
- (IBAction)leaveACommentButtonAction:(id)sender;
- (IBAction)commentBackButtonAction:(id)sender;
- (IBAction)commentDoneButtonAction:(id)sender;

@end
