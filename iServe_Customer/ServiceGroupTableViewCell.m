//
//  ServiceGroupTableViewCell.m
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ServiceGroupTableViewCell.h"

@implementation ServiceGroupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)showEachRowDetails:(NSMutableArray *)selectedServiceDetails groupDetails:(NSArray *)serviceGroup indexValue:(NSIndexPath *)indexPath
{
    
    self.changeButton.tag = indexPath.row;
    [self.changeButton.layer setBorderColor:[APP_COLOR CGColor]];
    
    if([serviceGroup[indexPath.row][@"cmand"]integerValue] == 1)
    {
        NSString *title = [NSString stringWithFormat:@"%@ *",serviceGroup[indexPath.row][@"group_name"]];
        
        NSMutableAttributedString *attributedTitle = [[NSMutableAttributedString alloc]initWithString:title];
        
        [attributedTitle addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange([serviceGroup[indexPath.row][@"group_name"]length],2)];
        
        
        [self.serviceGroupNameLabel setAttributedText:attributedTitle];
        [self.serviceGroupNameWhenServiceAdded setAttributedText:attributedTitle];
        
    }
    else
    {
        self.serviceGroupNameLabel.text = self.serviceGroupNameWhenServiceAdded.text = [NSString stringWithFormat:@"%@",serviceGroup[indexPath.row][@"group_name"]];
        
    }
    
    
    
    if([selectedServiceDetails[indexPath.row]count] > 0)
    {
        self.serviceNotAddedView.hidden = YES;
        self.serviceAddedView.hidden = NO;
        
        NSString *services= @"";
        for(NSDictionary *dict in selectedServiceDetails[indexPath.row])
        {
            services = [services stringByAppendingString:dict[@"sname"]];
            services = [services stringByAppendingString:@","];
        }
        
        if ([services hasSuffix:@","]) {
            services = [services substringToIndex:[services length]-1];
        }
        
        self.serviceLabel.text = services;
    }
    else
    {
        self.serviceNotAddedView.hidden = NO;
        self.serviceAddedView.hidden = YES;
    }


}

@end
