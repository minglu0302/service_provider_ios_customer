//
//  providerDetailsTableViewCell.m
//  iServe_AutoLayout
//
//  Created by Imma Web Pvt Ltd on 03/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderDetailsTableViewCell.h"

@implementation ProviderDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)showProviderDetails:(NSDictionary *)providerDetails and:(NSDictionary *)providerTypeDetails
{
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    self.ratingView.value = [providerDetails[@"rat"]floatValue];
    
    self.providerId = [providerDetails[@"pid"]integerValue];
    self.providerEmailId = providerDetails[@"e"];
    
    if([providerDetails[@"n"] isEqual:[NSNull null]])
    {
        self.providerNameLabel.text = @"";
    }
    else
    {
        self.providerNameLabel.text = providerDetails[@"n"];
    }
    
    
    float distance = [providerDetails[@"d"] floatValue]/iServeDistanceMetric;
    
    self.milesAwayLabel.text = [NSString stringWithFormat:@"%.2f Kms away",distance];
    
    [self.activityIndicator startAnimating];
    
    
    
    NSString *providerImageUrl = providerDetails[@"i"];
    if([providerImageUrl isEqual:[NSNull null]])
    {
        providerImageUrl = @"";
    }
    
    
    [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:providerImageUrl]
                              placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         [self.activityIndicator stopAnimating];
                                         
                                     }];
    
    
    self.feeTypesArray = @[@"Fixed",@"Hourly",@"Mileage"];
    NSString *currncySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];
    
    if(providerDetails[@"amt"])
    {
        self.isPriceSetByProvider = true;
        self.providerAmountValue = [providerDetails[@"amt"] floatValue];
    }
    else
    {
        self.isPriceSetByProvider = false;
    }
    
    
    NSInteger amountValue;
    switch ([self.feeTypesArray indexOfObject:providerTypeDetails[@"ftype"]])
    {
            //Feixed Based
        case feeTypeFixed:
        {
            
            self.perHourLabel.text = LS(@"Fixed");
            
            if(self.isPriceSetByProvider)
            {
                amountValue = self.providerAmountValue;
            }
            else
            {
                amountValue = [providerTypeDetails[@"fixed_price"]floatValue];
            }
            
            self.amountPerHour.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
            
        }
            break;
            
            //Hourly Based And Milleage Based
        case feeTypeHourly:
        {
            self.perHourLabel.text = LS(@"Per Hour");
            
            
            if(self.isPriceSetByProvider)
            {
                amountValue = self.providerAmountValue;
            }
            else
            {
                amountValue = [providerTypeDetails[@"price_min"]floatValue];
            }
            
            
            
            self.amountPerHour.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
        }
            
            break;
            
        default:
        {
            self.perHourLabel.text = LS(@"KMPH");
            
            if(self.isPriceSetByProvider)
            {
                amountValue = [UIHelper convertMilesToKilometer:self.providerAmountValue];
            }
            else
            {
                amountValue = [UIHelper convertMilesToKilometer:[providerTypeDetails[@"price_mile"]floatValue]];
            }
            
            
            self.amountPerHour.text = [NSString stringWithFormat:@"%@ %td",currncySymbol,amountValue];
        }
            break;
            
    }
    
}

@end
