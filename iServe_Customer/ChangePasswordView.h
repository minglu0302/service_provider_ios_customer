//
//  ChangePasswordView.h
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordView : UIView

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *numbersTestFieldsBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *passwordTextFieldsBackgroundView;
@property (weak, nonatomic) IBOutlet UIButton *resendOTPButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;

@property(weak, nonatomic) IBOutlet UITextField *firstNumber;
@property(weak, nonatomic) IBOutlet UITextField *secondNumber;
@property(weak, nonatomic) IBOutlet UITextField *thirdNumber;
@property(weak, nonatomic) IBOutlet UITextField *fourthNumber;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;




+ (id)sharedInstance;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)resendOTPButtonAction:(id)sender;
- (IBAction)changePasswordButtonAction:(id)sender;


@end
