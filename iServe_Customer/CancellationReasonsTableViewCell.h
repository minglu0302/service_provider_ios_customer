//
//  CancellationReasonsTableViewCell.h
//  iServe_Customer
//
//  Created by Apple on 24/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancellationReasonsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *reasonButton;
@property (weak, nonatomic) IBOutlet UIView *divider;
@property (weak, nonatomic) IBOutlet UILabel *reasonTitleLabel;

@end
