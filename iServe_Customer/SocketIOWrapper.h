//
//  SocketIOWrapper.h
//  Snapchat
//
//  Created by -Tony Lu on 11/09/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIOClient.h"
#import "SIOConfiguration.h"

@protocol SocketWrapperDelegate <NSObject>

-(void)responseFromChannels:(NSDictionary *)responseDictionary;
-(void)didConnect;
-(void)didDisconnect;
-(void)messageSentSuccessfullyForMessageID:(NSMutableDictionary *)messageID;

@required

-(void)receievedMessageOnChannel:(NSString*)channelName withMessage:(NSDictionary*)message;

@end

typedef NS_ENUM(NSUInteger, SocketMessageType) {
    SocketMessageTypeText,
    SocketMessageTypePhoto,
    SocketMessageTypeVideo,
};


@interface SocketIOWrapper : NSObject

@property (nonatomic, weak) id<SocketWrapperDelegate> socketDelegate;
@property (nonatomic, strong) NSMutableArray *channelsName;


+(instancetype) sharedInstance;
-(void)connectSocket;
-(void)publishMessageToChannel:(NSString*)channel withMessage:(NSDictionary*)messageDict;
-(void)sendHeartBeatForUser:(NSString *)email withStatus:(NSString *)status;
-(void)subscribeToProvider:(NSString *)providerChannelName;


@end
