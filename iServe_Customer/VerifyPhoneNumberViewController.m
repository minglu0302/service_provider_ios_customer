//
//  VerifyPhoneNumberViewController.m
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "VerifyPhoneNumberViewController.h"
#import "ChangePasswordViewController.h"

@interface VerifyPhoneNumberViewController ()<WebServiceHandlerDelegate>
{
    
}
@end

@implementation VerifyPhoneNumberViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.firstNumber becomeFirstResponder];
}

#pragma mark - UIButton Actions -

- (IBAction)resendOTPButtonAction:(id)sender
{
    [self sendRequestToResendOTP];
}

- (IBAction)navigationLeftButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)VerifyOTP {
    
    if (_firstNumber.text.length != 0 && _secondNumber.text.length != 0 && _thirdNumber.text.length != 0 && _fourthNumber.text.length != 0 ) {
        
        NSString *code = [NSString stringWithFormat:@"%@%@%@%@",_firstNumber.text,_secondNumber.text,_thirdNumber.text,_fourthNumber.text];
        
//        if ([code isEqualToString:self.verificationCode]) {
        
//            [self performSegueWithIdentifier:@"toChangePasswordVC" sender:self];
            [self verifyOTP:code];
//        }
//        else {
//            
//            _firstNumber.text = _secondNumber.text = _thirdNumber.text = _fourthNumber.text =  @"";
//            [_firstNumber becomeFirstResponder];
//            [UIHelper showMessage:LS(@"Seems like you have entered wrong verification code") withTitle:LS(@"Message")delegate:self];
//        }
        
    }
    else {
        
        [UIHelper showMessage:LS(@"Please enter the code sent to your registered mobile number") withTitle:LS(@"Message")delegate:self];
    }
    
}


#pragma mark - UITextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self findNextResponder:textField];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL shouldProcess = NO; //default to reject
    BOOL shouldMoveToNextField = NO; //default to remaining on the current field
    
    NSUInteger insertStringLength = [string length];
    if(insertStringLength == 0){ //backspace
        shouldProcess = YES; //Process if the backspace character was pressed
    }
    else {
        if([[textField text] length] == 0) {
            shouldProcess = YES; //Process if there is only 1 character right now
        }
    }
    
    //here we deal with the UITextField on our own
    if(shouldProcess){
        //grab a mutable copy of what's currently in the UITextField
        NSMutableString* mstring = [[textField text] mutableCopy];
        if([mstring length] == 0){
            //nothing in the field yet so append the replacement string
            [mstring appendString:string];
            
            shouldMoveToNextField = YES;
        }
        else{
            //adding a char or deleting?
            if(insertStringLength > 0){
                [mstring insertString:string atIndex:range.location];
            }
            else {
                //delete case - the length of replacement string is zero for a delete
                [mstring deleteCharactersInRange:range];
            }
        }
        
        //set the text now
        [textField setText:mstring];
        
        
        if (shouldMoveToNextField) {
            //MOVE TO NEXT INPUT FIELD HERE
            
            [self findNextResponder:textField];
        }
    }
    
    //always return no since we are manually changing the text field
    return NO;
    
}

-(void)findNextResponder:(UITextField *)textField {
    
    switch (textField.tag) {
            
        case 0:
            if(textField == self.firstNumber)
            {
                [self.secondNumber becomeFirstResponder];
            }
            
            break;
        case 1:
            if(textField == self.secondNumber)
            {
                [self.thirdNumber becomeFirstResponder];
            }
            
            break;
        case 2:
            if (textField == self.thirdNumber)
            {
                [self.fourthNumber becomeFirstResponder];
            }
            
            break;
        case 3:
            if (textField == self.fourthNumber)
            {
                [textField resignFirstResponder];
                [self VerifyOTP];
                
            }
            break;
            
        default:
            [textField resignFirstResponder];
            break;
    }
    
    
}


#pragma mark - Web Service Call -

-(void)sendRequestToResendOTP
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Sending OTP...", @"Sending OTP...")];

    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_mobile":flStrForObj(self.phoneNumber),
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_user_type":@"2"
                                };

         [[WebServiceHandler sharedInstance] sendOTPToVerifyPhoneNumber:params andDelegate:self];

    }
    else{

        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];

    }
    
}

-(void)verifyOTP:(NSString *)otpNumber
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Verifying OTP...", @"Verifying OTP...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTMobile:flStrForObj(self.phoneNumber),
                                 @"ent_code":otpNumber,
                                 @"ent_user_type":@"2",
                                 @"ent_service_type":@"2"
                                };
        [[WebServiceHandler sharedInstance] sendRequestToVerifyOTP:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}



#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
//    NSInteger errNum = [response[@"errNum"] integerValue];
    
    if(errFlag == 1)
    {
        if (requestType == RequestTypeForgotPasswordVerifyPhoneNumber){
        
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
        }
        else
        {
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
        }
        
    }
    else  if (errFlag == 0 && requestType == RequestTypeForgotPasswordVerifyPhoneNumber)
    {
        self.verificationCode = [NSString stringWithFormat:@"%@",response[@"code"]];
        [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
    }
    else  if (errFlag == 0 && requestType == RequestTypeVerifyOTP)
    {
         [self performSegueWithIdentifier:@"toChangePasswordVC" sender:self];
    }
    
}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toChangePasswordVC"])
    {
        ChangePasswordViewController *changePasswordVC = [segue destinationViewController];
        changePasswordVC.phoneNumber = self.phoneNumber;
    }
}


@end
