//
//  MyAppTimerClass.m
//  UBER
//
//  Created by -Tony Lu on 30/08/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//


#import "MyAppTimerClass.h"

static MyAppTimerClass *myAppTimer =nil;

@implementation MyAppTimerClass

@synthesize pubnubStreamTimer;

+ (id)sharedInstance
{
    
    if (!myAppTimer)
    
    {
        myAppTimer = [[self alloc]init];
       
    }
     return myAppTimer;
}

-(void)startPublishTimer
{
    
    if (self.pubnubStreamTimer == nil)
    {
        pubnubStreamTimer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(publishPubNubStream:) userInfo:nil repeats:YES];

    }
    
}
-(void)stopPublishTimer
{
    
    if ([self.pubnubStreamTimer isValid])
    {
        [self.pubnubStreamTimer invalidate];
    }
    
}
-(void)publishPubNubStream:(NSTimer *)myTimer
{
    HomeViewController *obj =[HomeViewController getSharedInstance];
    if(obj)
    {
        [obj publishToSocket];
    }
}


@end
