//
//  TWTSupportTableCell.h
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportTableCell : UITableViewCell
/**
 *  Cell Label outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
/**
 *  Background Support Cell outlet
 */
@property (weak, nonatomic) IBOutlet UIView *backgroundSupportCellView;

@end
