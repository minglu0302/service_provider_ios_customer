//
//  CountryPickerCell.h
//  Sup
//
//  Created by Rahul Sharma on 3/11/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPCountryPickerCell : UITableViewCell
/**
 *  Flag Image View Outlet
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageFlag;
/**
 *  Country Name Label Outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *labelCountryName;
/**
 *  Country Code Label Outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *labelCountryCode;

@end
