//
//  SlotsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 07/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlotsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *scrollBackgroundView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollContentViewWidthConstraint;

@end
