//
//
//  Created by -Tony Lu on 4/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface SplashViewController : UIViewController <GetCurrentLocationDelegate>


/**
 *  Background Image View Outlet
 */
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

/**
 *  This Function Used For Removing the Splash
 */
-(void)removeSplash;

@end
