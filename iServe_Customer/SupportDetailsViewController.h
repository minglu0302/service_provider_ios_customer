//
//  TWTDetailsViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

/**
 *  Nav title String for storing the title of nav bar
 */
@property (nonatomic,strong) NSString *navTitle;
/**
 *  weburllink string for storing the web url
 */
@property (nonatomic,strong) NSString *webUrlLink;
/**
 *   Details Array for adding the title for cell
 */
@property(strong,nonatomic) NSMutableArray *detailsArray;
/**
 *   Sender string for adding the title in navigation bar
 */
@property (nonatomic,strong) NSString *senderString;
/**
 *  IT store all object that come from support view controller's and add in detail table view cell
 */
@property (nonatomic,strong) NSMutableArray  *detailsItemsofArray;
/**
 *  Details Table View outlet
 */
@property (weak, nonatomic) IBOutlet UITableView *detailTableView;
/**
 *  Navigation back button outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
/**
 *   Back Button action for going back to root view controller
 */
- (IBAction)backButton:(id)sender;
@end
