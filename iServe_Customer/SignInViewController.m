//
//  SignInViewController.m
//  iServe app
//  Developed by Raghavendra
//  Created by -Tony Lu on 06/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.

#import "SignInViewController.h"
#import "SignUpViewController.h"

static SignInViewController *sharedInstance = nil;

@interface SignInViewController ()<UIKeyboardDelegates>
{
    NSMutableArray *arrayContainingCardInfo;
    BOOL isFBLogin;
}

@end

@implementation SignInViewController


#pragma mark - Initial Methods -

/**
 *  This service Used for create the Object of Class
 */
+ (instancetype)getSharedInstance
{
    return sharedInstance;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    sharedInstance = self;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = self;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width,CGRectGetHeight(self.forgotPasswordButton.frame)+CGRectGetMinY(self.forgotPasswordButton.frame)+30);
    
    if(self.isFromChangePassVC)
    {
        self.isFromChangePassVC = NO;
    }
    else
    {
        [self.emailTextField becomeFirstResponder];
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.keyboardDelegate = nil;
}

#pragma mark - ValidationServiceCall -


/**
 This Method used to Validate Email Address Pattern

 @param emailToValidate input Email ID

 @return true Or False
 */
-(BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}|[0-9]+";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}


#pragma mark - UIButton Actions -

/**
 This Method will Invoke When user tap on screen

 @param sender gesture information
 */
- (IBAction)signInTabGesture:(id)sender
{
    [self.view endEditing:YES];
}


/**
 This Method will Invoke When user click on Back Button

 @param sender button Information
 */
- (IBAction)backButtonClick:(id)sender
{
    [self.view endEditing:YES];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];

    [self.navigationController popViewControllerAnimated:NO];
    
}

/**
 This Method will Invoke When user click on Login Button
 
 @param sender button Information
 */

- (IBAction)signInButtonClicked:(id)sender
{
    NSString *email = flStrForStr(self.emailTextField.text);
    NSString *password = flStrForStr(self.passwordTextField.text);
    
    if((unsigned long)email.length == 0)
    {
        [UIHelper showMessage:NSLocalizedString(@"Enter email or phone number", @"Enter email or phone number") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        [self.emailTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        [UIHelper showMessage:NSLocalizedString(@"Invalid email or phone number", @"Invalid email or phone number") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        self.emailTextField.text = @"";
        [self.emailTextField becomeFirstResponder];
        
    }
    else if((unsigned long)password.length == 0)
    {
        [UIHelper showMessage:NSLocalizedString(@"Enter Password", @"Enter Password") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        [self.passwordTextField becomeFirstResponder];
    }
    else
    {
        [self.view endEditing:YES];
        [self sendServiceForLogin];
    }
}


/**
 This Method will Invoke When user click on Forgot Password Button
 
 @param sender button Information
 */

- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    [self.activeTextField resignFirstResponder];
    
    [self performSegueWithIdentifier:@"toForgotPasswordVC" sender:self];
}


/**
 This Method will Invoke When user click on Login with Facebook Button
 
 @param sender button Information
 */

- (IBAction)loginWithFacebookButtonAction:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    FBLoginHandler *handler = [FBLoginHandler sharedInstance];
    handler.isSignInOrSignUp = 1;
    [handler setDelegate:self];
    [handler loginWithFacebook:self];
    
}


#pragma mark - KeyBoardHideAndShow -
/**
 *   This method will called when the keyboard trigger for Hide
 */
-(void)keyboardWillHide:(NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

/**
 *   This method will called when the keyboard trigger for Show
 */
- (void)keyboardWillShown:(NSNotification *)inputViewNotification
{
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}


#pragma mark - TextFieldDelegate -

/**
 *   This method will called when the Textfield begin to editing
 */
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField=textField;
    return YES;
}

/**
 *   This method will called when the user clicked on clear button in textfield
 */

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

/**
 *   This method will called when the user clicked on Next button in Keyboard
 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == self.emailTextField)
    {
        if ([self emailValidationCheck:self.emailTextField.text] == 0)
        {
            [UIHelper showMessage:LS(@"Please enter a valid email or phone number") withTitle:LS(@"Message")delegate:self];
            [self.emailTextField becomeFirstResponder];
        }
        else{
            
            [self.passwordTextField becomeFirstResponder];
        }
        
        
    }
    else
    {
        [self.view endEditing:YES];
        [self signInButtonClicked:nil];
    }
    
    return YES;
}


#pragma mark - Facebook Login Delegates -

/**
 *  Facebook login is success
 *
 *  @param userInfo Userdict
 */
- (void)didFacebookUserLoginWithDetails:(NSDictionary*)userInfo
{
    NSLog(@"FB Data =  %@", userInfo);
    if (userInfo == nil)
    {
        return;
    }
    else
    {
        isFBLogin = YES;
        self.fbData = userInfo;
        self.emailTextField.text = userInfo[@"email"];
        self.passwordTextField.text = userInfo[@"id"];
        [self.view endEditing:YES];
        [self sendServiceForFacebookLogin:userInfo];
    }
}

/**
 *  Login failed with error
 *
 *  @param error error
 */
- (void)didFailWithError:(NSError *)error
{
    [UIHelper showMessage:[error localizedDescription] withTitle:LS(@"Message")delegate:self];
}

/**
 *  User cancelled
 */
- (void)didUserCancelLogin
{
    
}


#pragma mark - WebServiceCall -

/**
 This Method call the web service for User to Login With Facebook

 @param userData dictionary of users login Informations
 */
-(void)sendServiceForFacebookLogin:(NSDictionary *)userData
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Logging in...", @"Logging in...")];
        
        
        NSDictionary *params = @{
                                 kTWTEmail:flStrForStr(userData[@"email"]),
                                 kTWTPassword:flStrForStr(userData[@"id"]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
//                                 kTWTPushToken:flStrForStr([Utilities getPushToken]),
                                 kTWTDeviceType:flStrForStr([Utilities getDeviceType]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                 @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                                 @"ent_dev_model":[UIHelper getModelName],
                                 @"ent_manf":@"APPLE",
                                 kTWTPushToken:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeFCMTokenKey]),
                                 @"ent_signup_type":[NSNumber numberWithInt:2],
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToSignIN:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}


/**
 This Method call the web service for User to Login
 */

-(void)sendServiceForLogin
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Logging in...", @"Logging in...")];
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        
        NSDictionary *params = @{
                                 kTWTEmail:flStrForStr(self.emailTextField.text),
                                 kTWTPassword:flStrForStr(self.passwordTextField.text),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
//                                 kTWTPushToken:flStrForStr([Utilities getPushToken]),
                                 kTWTDeviceType:flStrForStr([Utilities getDeviceType]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                                 @"ent_dev_os":[UIDevice currentDevice].systemVersion,
                                 @"ent_dev_model":[UIHelper getModelName],
                                 @"ent_manf":@"APPLE",
                                 kTWTPushToken:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeFCMTokenKey]),
                                 @"ent_signup_type":[NSNumber numberWithInt:1],
                                };
        
        [[WebServiceHandler sharedInstance] sendRequestToSignIN:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
        
    }
    
}

#pragma mark - WebServiceDelegate -

/**
 This Method will invoke when web service Response are Came
 */

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        //If got any Error
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    if(errFlag == 1)
    {
        if (requestType == RequestTypeSignIn && errNum == 120 ){
            
            if(isFBLogin)
            {
                [self performSegueWithIdentifier:@"fromLoginToSignUp" sender:nil];
            }
            else
            {
                self.emailTextField.text = @"";
                self.passwordTextField.text = @"";
                [self.emailTextField becomeFirstResponder];
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                
            }
            
        }
        
        else if (requestType == RequestTypeSignIn && errNum == 91){
            
            self.passwordTextField.text = @"";
            [self.passwordTextField becomeFirstResponder];
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            
        }
        else if (requestType == RequestTypeSignIn && errNum == 11){
            
            self.emailTextField.text = @"";
            self.passwordTextField.text = @"";
            [self.emailTextField becomeFirstResponder];
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            
        }

    }
    else  if (errFlag == 0 && requestType == RequestTypeSignIn)
    {
        // Save information
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        
        [ud setObject:flStrForStr(response[@"CustId"]) forKey:iServeCustomerID];
        [ud setObject:flStrForStr(response[@"fname"]) forKey:iServeUserFirstName];
        [ud setObject:flStrForStr(response[@"lname"]) forKey:iServeUserLastName];
        [ud setObject:flStrForStr(response[@"token"]) forKey:iServeCheckUserSessionToken];
        [ud setObject:flStrForStr(response[@"apiKey"]) forKey:iServeAPIKey];
        [ud setObject:flStrForStr(response[@"coupon"]) forKey:iServeCouponkey];
        [ud setObject:flStrForStr(response[@"email"])  forKey:iServeUserEmail];
        [ud setObject:self.passwordTextField.text forKey:iServeUserPassword];
        [ud setObject:flStrForStr(response[@"currency"]) forKey:iServeCurrentCountryCurrencySymbol];
        [ud setObject:flStrForStr(response[@"profilePic"]) forKey:iServeUserProfilepic];

        
        if(response[@"stripe_pub_key"])
        {
            [ud setObject:response[@"stripe_pub_key"] forKey:iServeStripeKey];
            [Stripe setDefaultPublishableKey:response[@"stripe_pub_key"]];
        }
        
        
        
        
        arrayContainingCardInfo = [[NSMutableArray alloc ] initWithArray:response[@"cards"]];
        
        if([response[@"Languages"]count] > 0)
        {
            [ud setObject:response[@"Languages"]  forKey:iServeUserLanguages];
            [ud setInteger:0 forKey:iServeUserSelectedLanguageNumber];
        }
        
        [ud synchronize];
        //Save Card Informations
        if (arrayContainingCardInfo || arrayContainingCardInfo.count){
            
            [[CardAddOrDeleteClass sharedInstance]updateCardsInDatabase:arrayContainingCardInfo];
        }
        
        //send Request to get All On Going Bookings
        [[LiveBookingStatusUpdateClass sharedInstance]sendRequestToGetAllonGoingBookings];
        [[AdressWrapperClass sharedInstance] sendRequestToGetAddress];
        
        //Go to Main Menu View Controller
        [self performSegueWithIdentifier:@"fromSignIntoMainVC" sender:nil];
        
    }
}


#pragma mark - Prepare Segue -


/**
 This method Will used to pass data from this controller to destination Controllers

 @param segue  segue Information
 @param sender passed data Information
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"fromLoginToSignUp"])
    {
        SignUpViewController *signUpVC = [segue destinationViewController];
        signUpVC.isFromSignIn = YES;
        signUpVC.fBData = self.fbData;
    }
    else if([segue.identifier isEqualToString:@"toForgotPasswordVC"])
    {
        [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                  subType:kCATransitionFromTop
                                                  forView:self.navigationController.view
                                             timeDuration:0.3];

    }

}

@end
