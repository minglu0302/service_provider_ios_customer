//
//  ProviderReviewsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderReviewsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *reviewerImageView;
@property (weak, nonatomic) IBOutlet UILabel *reviewerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *daysAgoLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *divider;

-(void)showReviewDetails:(NSDictionary *)reviewDetails;

@end
