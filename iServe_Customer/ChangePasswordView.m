//
//  ChangePasswordView.m
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ChangePasswordView.h"

static ChangePasswordView *changePasswordPopUP = nil;

@interface ChangePasswordView ()<UITextFieldDelegate,WebServiceHandlerDelegate>
{
    UIWindow *window;
    NSString *verificationCode;
}

@end

@implementation ChangePasswordView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self =  [[[NSBundle mainBundle] loadNibNamed:@"ChangePasswordView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance
{
    if (!changePasswordPopUP)
    {
        changePasswordPopUP  = [[self alloc] initWithFrame:CGRectZero];
        changePasswordPopUP.frame = [[UIScreen mainScreen]bounds];
        
    }
    
    [changePasswordPopUP showInitialView];
    
    return changePasswordPopUP;
}

-(void)showInitialView
{
    verificationCode = @"1111";
    self.titleLabel.text = @"Verify Phone Number";
    self.passwordTextFieldsBackgroundView.hidden = YES;
    self.changePasswordButton.hidden = YES;
    
    self.numbersTestFieldsBackgroundView.hidden = NO;
    self.resendOTPButton.hidden = NO;
}

-(void)showPasswordTextFieldsView
{
    self.titleLabel.text = @"Change Password";
    self.passwordTextFieldsBackgroundView.hidden = NO;
    self.changePasswordButton.hidden = NO;
    
    self.numbersTestFieldsBackgroundView.hidden = YES;
    self.resendOTPButton.hidden = YES;

}

-(void)closeWithAnimation
{
    changePasswordPopUP.topViewTopConstraint.constant = -200;
    [UIView animateWithDuration:0.5f
                     animations:^{
                         
                          [changePasswordPopUP layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         [changePasswordPopUP removeFromSuperview];
                         changePasswordPopUP = nil;
                     }];
   
    
}

#pragma mark - UIButton Actions -

- (IBAction)cancelButtonAction:(id)sender
{
    [self closeWithAnimation];
}

- (IBAction)resendOTPButtonAction:(id)sender
{
    
}

- (IBAction)changePasswordButtonAction:(id)sender
{
    NSString *password = flStrForStr(self.passwordTextField.text);
    NSString *confirmPassword = flStrForStr(self.confirmPasswordTextField.text);
    
    if((unsigned long)password.length == 0)
    {
        [UIHelper showMessage:NSLocalizedString(@"Enter password", @"Enter password") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
        [self.passwordTextField becomeFirstResponder];
    }
    else if([self checkPasswordStrength:password] == 0)
    {
        [UIHelper showMessage:LS(@"Please ensure your password has atleast a capital letter, a small letter and a number consisting of minimum 7 characters.") withTitle:LS(@"Message")delegate:nil];
        self.passwordTextField.text = @"";
        [self.passwordTextField becomeFirstResponder];
        
    }
    else if(![password isEqualToString:confirmPassword])
    {
        [UIHelper showMessage:NSLocalizedString(@"Password and Confirm Password should be same", @"Password and Confirm Password should be same") withTitle:NSLocalizedString(@"Message", @"Message") delegate:self];
    }
    else
    {
        [changePasswordPopUP endEditing:YES];
        [self sendRequestToUpdatePassword];
    }

}

- (void)VerifyOTP {
    
    if (_firstNumber.text.length != 0 && _secondNumber.text.length != 0 && _thirdNumber.text.length != 0 && _fourthNumber.text.length != 0 ) {
        
        NSString *code = [NSString stringWithFormat:@"%@%@%@%@",_firstNumber.text,_secondNumber.text,_thirdNumber.text,_fourthNumber.text];
        
        if ([code isEqualToString:verificationCode]) {
            
            [self showPasswordTextFieldsView];
        }
        else {
            
            _firstNumber.text = _secondNumber.text = _thirdNumber.text = _fourthNumber.text =  @"";
            [_firstNumber becomeFirstResponder];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                [UIHelper showMessage:LS(@"Seems like you have entered wrong verification code") withTitle:LS(@"Message")delegate:self];
                
            });
        }
        
    }
    else {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

            [UIHelper showMessage:LS(@"Please enter the code sent to your registered mobile number") withTitle:LS(@"Message")delegate:self];
        });
    }
    
}

#pragma mark - Password Validation -

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    int strength = 0;
    
    if (len == 0 || len < 7)
    {
        return 0;
    }
    else if (len >= 7)
    {
        strength++;
    }
    else if (len <= 10)
    {
        strength +=2;
    }
    else
    {
        strength +=3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength)
    {
        return 0;
    }
    return 1;
}

- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    NSAssert(regex, @"Unable to create regular expression");
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    BOOL didValidate = 0;
    if (matchRange.location != NSNotFound)
        didValidate = 1;
    return didValidate;
}



#pragma mark - UITextFieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.passwordTextField] || [textField isEqual:self.confirmPasswordTextField])
    {
        if(textField == self.passwordTextField)
        {
            if ([self checkPasswordStrength:self.passwordTextField.text] == 0)
            {
                [UIHelper showMessage:LS(@"Please ensure your password has atleast a capital letter, a small letter and a number consisting of minimum 7 characters.") withTitle:LS(@"Message")delegate:nil];
                [self.passwordTextField becomeFirstResponder];
            }
            else{
                
                [self.confirmPasswordTextField becomeFirstResponder];
            }
        
        }
        else
        {
            [self changePasswordButtonAction:nil];
        }
    
    }
    else
    {
         [self findNextResponder:textField];
    }
   
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:self.passwordTextField] || [textField isEqual:self.confirmPasswordTextField])
    {
        return YES;
    }
    else
    {
        BOOL shouldProcess = NO; //default to reject
        BOOL shouldMoveToNextField = NO; //default to remaining on the current field
        
        NSUInteger insertStringLength = [string length];
        if(insertStringLength == 0){ //backspace
            shouldProcess = YES; //Process if the backspace character was pressed
        }
        else {
            if([[textField text] length] == 0) {
                shouldProcess = YES; //Process if there is only 1 character right now
            }
        }
        
        //here we deal with the UITextField on our own
        if(shouldProcess){
            //grab a mutable copy of what's currently in the UITextField
            NSMutableString* mstring = [[textField text] mutableCopy];
            if([mstring length] == 0){
                //nothing in the field yet so append the replacement string
                [mstring appendString:string];
                
                shouldMoveToNextField = YES;
            }
            else{
                //adding a char or deleting?
                if(insertStringLength > 0){
                    [mstring insertString:string atIndex:range.location];
                }
                else {
                    //delete case - the length of replacement string is zero for a delete
                    [mstring deleteCharactersInRange:range];
                }
            }
            
            //set the text now
            [textField setText:mstring];
            
            
            if (shouldMoveToNextField) {
                //MOVE TO NEXT INPUT FIELD HERE
                
                [self findNextResponder:textField];
            }
        }
        
        //always return no since we are manually changing the text field
        return NO;
    }
}

-(void)findNextResponder:(UITextField *)textField {
    
    switch (textField.tag) {
            
        case 0:
            if(textField == self.firstNumber)
            {
                [self.secondNumber becomeFirstResponder];
            }
            
            break;
        case 1:
            if(textField == self.secondNumber)
            {
                [self.thirdNumber becomeFirstResponder];
            }
            
            break;
        case 2:
            if (textField == self.thirdNumber)
            {
                [self.fourthNumber becomeFirstResponder];
            }
            
            break;
        case 3:
            if (textField == self.fourthNumber)
            {
                [textField resignFirstResponder];
                [self VerifyOTP];
                
            }
            break;
            
        default:
            [textField resignFirstResponder];
            break;
    }
    
    
}


#pragma mark - Web Service Call -

-(void)sendRequestToUpdatePassword
{
//    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
//    
//    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
//    if([reachability isNetworkAvailable])
//    {
//        NSDictionary *params = @{
//                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
//                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
//                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
//                                 @"ent_date_time":[UIHelper getCurrentDateTime],
//                                 @"ent_inv":[NSString stringWithFormat:@"1"],
//                                 };
//        
//        [[WebServiceHandler sharedInstance] sendRequestToGetParticularApptStatus:params andDelegate:self];
//        
//    }
//    else{
//        
//        [[ProgressIndicator sharedInstance]hideProgressIndicator];
//        //        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:[UIHelper getCurrentController]];
//        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:nil];
//        
//    }
    
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:nil];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance] deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
        
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeUpdateReview)
            {
                //remove Invoice Popup
//                [self closeWithAnimation];
                //                invoicePopUpView = nil;
                
                
            }
            else if(requestType == RequestTypeGetParticularApptDetails)
            {
//                self.invoiceBookingDetails = response;
//                [self showBookingDetails:response];
            }
            else if (requestType == RequestTypeUpdateDispute)
            {
//                [self closeWithAnimation];
            }
            
            
        }
            break;
        default:
            break;
    }
    
    
}


@end
