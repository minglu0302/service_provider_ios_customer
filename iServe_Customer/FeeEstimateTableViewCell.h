//
//  FeeEstimateTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/15/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeeEstimateTableViewCell : UITableViewCell


//Regular 
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

//Hourly Based
@property (weak, nonatomic) IBOutlet UIView *hourlyBasedBackGroundView;
@property (weak, nonatomic) IBOutlet UILabel *hourlyBasedTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *numberOfHoursButton;
@property (weak, nonatomic) IBOutlet UILabel *hrsLabel;
@property (weak, nonatomic) IBOutlet UILabel *hoursAmountLabel;

@property (strong,nonatomic) NSDictionary *providerTypeDetails;
@property (strong,nonatomic) NSArray *arrayOfSelectedServices;
@property NSInteger bookingType;
@property CGFloat slotPrice;

-(void)setFeeEstimates:(NSIndexPath *)indexPath;




@end
