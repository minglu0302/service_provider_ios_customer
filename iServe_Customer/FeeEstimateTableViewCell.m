//
//  FeeEstimateTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/15/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "FeeEstimateTableViewCell.h"

@interface FeeEstimateTableViewCell ()
{
    NSUserDefaults *ud;
}

@end

@implementation FeeEstimateTableViewCell

- (void)awakeFromNib {
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
   
    // Configure the view for the selected state
}


-(void)setFeeEstimates:(NSIndexPath *)indexPath
{
    ud = [NSUserDefaults standardUserDefaults];
    FeeDetailsClass *feeDetails = [FeeDetailsClass sharedInstance];

    switch (feeDetails.feeType.integerValue)
    {
        //Feixed Based
        case feeTypeFixed:
        {
            [self hidesHourlyBasedView];
            
            if(indexPath.row == 0)
            {
                [self setFixedViewDetail:feeDetails];
            }
            else if(indexPath.row == 1)
            {
                if(self.arrayOfSelectedServices.count > 0)//Show Service Details Price
                {
                    [self setSelectedServiceDetails];
                }
                else if (self.bookingType == 3)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if(feeDetails.discountType.length > 0)//Show Discount Details
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else if(indexPath.row == 2)
            {
                if (self.bookingType == 3 && self.arrayOfSelectedServices.count > 0)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if((feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0)
                        || (feeDetails.discountType.length > 0 && self.bookingType == 3))
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else if(indexPath.row == 3)
            {
                if(feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0 && self.bookingType == 3)
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }

            else{
                
                [self setTotalViewDetails:feeDetails];
            }
            
            
        }
            break;
            
            //Hourly Based
        case feeTypeHourly:
        {
            if(indexPath.row == 0){
                
                [self hidesHourlyBasedView];
                [self setMinimumVisitViewForHourlyDetail:feeDetails];
            }
            else if (indexPath.row == 1){
                
                [self hidesTopView];
                [self setHourlyBasedViewDetails:feeDetails.numberOfHourValue and:feeDetails];
                
            }
            else if(indexPath.row == 2){
                
                [self hidesHourlyBasedView];
                
                //If Any Services Are Slected
                if(self.arrayOfSelectedServices.count > 0)//Show Service Details Price
                {
                    [self setSelectedServiceDetails];
                }
                else if (self.bookingType == 3)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if(feeDetails.discountType.length > 0)//Show Discount Details
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else if(indexPath.row == 3)
            {
                [self hidesHourlyBasedView];
                if (self.bookingType == 3 && self.arrayOfSelectedServices.count > 0)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if((feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0)
                         || (feeDetails.discountType.length > 0 && self.bookingType == 3))
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }

            else if(indexPath.row == 4){
                
                [self hidesHourlyBasedView];
                
                //If Any Services Are Slected AND Discount Added
                if(feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0 && self.bookingType == 3)
                    [self setDiscountViewDetails:feeDetails];
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else{
                
                [self hidesHourlyBasedView];
                [self setTotalViewDetails:feeDetails];
            }
            
        }
            break;
        //Milleage Based
        default:
        {
            if(indexPath.row == 0){
                
                [self hidesHourlyBasedView];
                [self setMinimumVisitViewForMileageDetail:feeDetails];
            }
            else if (indexPath.row == 1){
                
                [self hidesTopView];
                [self setMileageBasedViewDetails:feeDetails.numberOfHourValue and:feeDetails];
                
            }
            else if(indexPath.row == 2){
                
                [self hidesHourlyBasedView];
                
                //If Any Services Are Slected
                if(self.arrayOfSelectedServices.count > 0)//Show Service Details Price
                {
                    [self setSelectedServiceDetails];
                }
                else if (self.bookingType == 3)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if(feeDetails.discountType.length > 0)//Show Discount Details
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else if(indexPath.row == 3)
            {
                 [self hidesHourlyBasedView];
                if (self.bookingType == 3 && self.arrayOfSelectedServices.count > 0)//show Slot Price
                {
                    [self setSlotPriceDetails];
                }
                else if((feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0)
                        || (feeDetails.discountType.length > 0 && self.bookingType == 3))
                {
                    [self setDiscountViewDetails:feeDetails];
                }
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            
            else if(indexPath.row == 4){
                
                [self hidesHourlyBasedView];
                
                //If Any Services Are Slected AND Discount Added
                if(feeDetails.discountType.length > 0 && self.arrayOfSelectedServices.count > 0 && self.bookingType == 3)
                    [self setDiscountViewDetails:feeDetails];
                else
                    [self setTotalViewDetails:feeDetails];
                
            }
            else{
                
                [self hidesHourlyBasedView];
                [self setTotalViewDetails:feeDetails];
            }
            
        }
            
        break;
    }
}

//To show Visit Fee Details
-(void)setFixedViewDetail:(FeeDetailsClass *)feeDetails{
    
    [self setTitle:LS(@"Fixed Visit Fee")];
    feeDetails.visitFee = [NSNumber numberWithFloat:[self.providerTypeDetails[@"fixed_price"]floatValue]];
    [self setAmountValue:feeDetails.visitFee];

}

-(void)setMinimumVisitViewForHourlyDetail:(FeeDetailsClass *)feeDetails{

    [self setTitle:LS(@"Minimum Visit Fee")];
    feeDetails.visitFee = [NSNumber numberWithFloat:[self.providerTypeDetails[@"visit_fees"]floatValue]];
    [self setAmountValue:feeDetails.visitFee];
    
}
-(void)setMinimumVisitViewForMileageDetail:(FeeDetailsClass *)feeDetails{
    
    [self setTitle:LS(@"Minimum Visit Fee")];
    feeDetails.visitFee = [NSNumber numberWithFloat:[self.providerTypeDetails[@"min_fees"]floatValue]];
    [self setAmountValue:feeDetails.visitFee];
    
}


//To show Hourly Based fees
-(void)setHourlyBasedViewDetails:(NSNumber *)numberOfHour and:(FeeDetailsClass *)feeDetails{
    
    if(numberOfHour.integerValue > 1)
    {
        self.hrsLabel.text = LS(@"Hrs");
    }
    else
    {
        self.hrsLabel.text = LS(@"Hr");
    }

    self.hourlyBasedTitleLabel.text = [NSString stringWithFormat:LS(@"Hourly @ %@%.2f"),[ud objectForKey:iServeCurrentCountryCurrencySymbol],[self.providerTypeDetails[@"price_min"]floatValue]];
    [ self.numberOfHoursButton setTitle:[numberOfHour stringValue]  forState:UIControlStateNormal];
    
    feeDetails.hourlyFee = [NSNumber numberWithFloat:self.numberOfHoursButton.titleLabel.text.integerValue * [self.providerTypeDetails[@"price_min"]floatValue]];
    
    self.hoursAmountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],feeDetails.hourlyFee.floatValue];
    
}

//To show Mileage Based fees
-(void)setMileageBasedViewDetails:(NSNumber *)numberOfHour and:(FeeDetailsClass *)feeDetails{
    
    if(numberOfHour.integerValue > 1)
    {
        self.hrsLabel.text = LS(@"Kms");
    }
    else
    {
        self.hrsLabel.text = LS(@"Km");
    }
    
    self.hourlyBasedTitleLabel.text = [NSString stringWithFormat:@"%@%.2f Per Kilometer",[ud objectForKey:iServeCurrentCountryCurrencySymbol],[UIHelper convertMilesToKilometer:[self.providerTypeDetails[@"price_mile"]floatValue]]];
    
    [ self.numberOfHoursButton setTitle:[numberOfHour stringValue]  forState:UIControlStateNormal];
    
    feeDetails.hourlyFee = [NSNumber numberWithFloat:self.numberOfHoursButton.titleLabel.text.integerValue * [UIHelper convertMilesToKilometer:[self.providerTypeDetails[@"price_mile"]floatValue]]];
    
    self.hoursAmountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],feeDetails.hourlyFee.floatValue];
    
}

-(void)setSelectedServiceDetails
{
    
    [self setTitle:LS(@"Total Services Fee")];
    
    CGFloat totalAmount = 0.0;
    for(int i=0; i<self.arrayOfSelectedServices.count; i++)
    {
        totalAmount = totalAmount + [self.arrayOfSelectedServices[i][@"fixed_price"]floatValue];
    }
    
    [self setAmountValue:[NSNumber numberWithFloat:totalAmount]];
    
}

-(void)setSlotPriceDetails
{
    [self setTitle:LS(@"Slot Fee")];
    [self setAmountValue:[NSNumber numberWithFloat:self.slotPrice]];

}

-(void)setDiscountViewDetails:(FeeDetailsClass *)feeDetails
{
    
    if([feeDetails.discountType isEqualToString:@"Fixed"])
    {
        [self setTitle:LS(@"Discount")];
        feeDetails.discountFee = feeDetails.discount;
//        [self setAmountValue:feeDetails.discountFee];
        self.amountLabel.text = [NSString stringWithFormat:@"- %@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],feeDetails.discountFee.floatValue];
    }
    else{
        
        feeDetails.discountFee = feeDetails.discount;//[NSNumber numberWithFloat:feeDetails.visitFee.floatValue * feeDetails.discount.integerValue/100];
        
        [self setTitle:[NSString stringWithFormat:@"Discount (%@%%)",feeDetails.discount]];
        
        float discountValue = (feeDetails.visitFee.floatValue + feeDetails.hourlyFee.floatValue + feeDetails.servicesAmount.floatValue + feeDetails.slotAmount.floatValue) * (feeDetails.discount.floatValue/100);
        
        self.amountLabel.text = [NSString stringWithFormat:@"- %@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],discountValue];
    }
    
}
-(void)setTotalViewDetails:(FeeDetailsClass *)feeDetails{
    
    if([feeDetails.discountType isEqualToString:@"Percent"] && feeDetails.discountType.length > 0)
    {
        //For Percent Discount
        float discountValue = (feeDetails.visitFee.floatValue + feeDetails.hourlyFee.floatValue + feeDetails.servicesAmount.floatValue + feeDetails.slotAmount.floatValue) * (feeDetails.discount.floatValue/100);
        
         feeDetails.totalFee = [NSNumber numberWithFloat:(feeDetails.visitFee.floatValue + feeDetails.hourlyFee.floatValue + feeDetails.servicesAmount.floatValue + feeDetails.slotAmount.floatValue) - discountValue];
    }
    else
    {
        //For Fixed Discount
        feeDetails.totalFee = [NSNumber numberWithFloat:feeDetails.visitFee.floatValue + feeDetails.hourlyFee.floatValue + feeDetails.servicesAmount.floatValue + feeDetails.slotAmount.floatValue - feeDetails.discountFee.floatValue];

    }
    
    [self setTitle:LS(@"Total")];
    [self setAmountValue:feeDetails.totalFee];
    
}


-(void)setTitle:(NSString *)title{
    
    self.titleLabel.text = title;
    
}

-(void)setAmountValue:(NSNumber *)amount{
    
    self.amountLabel.text = [NSString stringWithFormat:@"%@ %.2f",[ud objectForKey:iServeCurrentCountryCurrencySymbol],amount.floatValue];
}

-(void)hidesTopView{
    
    self.topView.hidden = YES;
    self.hourlyBasedBackGroundView.hidden = NO;
}

-(void)hidesHourlyBasedView{
    
    self.hourlyBasedBackGroundView.hidden = YES;
    self.topView.hidden = NO;
}


@end
