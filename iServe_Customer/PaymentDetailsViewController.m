//
//  PaymentDetailsViewController.m
//  privMD
//
//  Created by -Tony Lu on 05/03/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import "PaymentDetailsViewController.h"

@interface PaymentDetailsViewController ()<WebServiceHandlerDelegate>

@end

@implementation PaymentDetailsViewController

@synthesize cardNoLabel,expLabel,expTextField,cvvTextField,mainView,deleteButton;

@synthesize containingDetailsOfCard;

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSString *str1 = @"**** **** **** ";
    cardNoLabel.text =  [containingDetailsOfCard objectForKey:@"last4"];
    str1 = [str1 stringByAppendingString:cardNoLabel.text];
    cardNoLabel.text = str1;
    
    NSString *month = [containingDetailsOfCard objectForKey:@"exp_month"];

    month = [month stringByAppendingString:@"/"];
    month = [month stringByAppendingString:[containingDetailsOfCard objectForKey:@"exp_year"]];

    expTextField.text = month;
    
    cvvTextField.hidden = YES;
    self.editButton.hidden = YES;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIBUtton Actions -

- (IBAction)deleteButtonClicked:(id)sender {
    
    [self sendServiceToDeleteCard];
}


- (IBAction)navigationBackButtonAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editButtonAction:(id)sender {
    
    UIButton *mBut = (UIButton *)sender;
    
    if(mBut.isSelected)
    {
        mBut.selected = NO;
        cvvTextField.hidden = YES;
        expTextField.userInteractionEnabled = NO;
        
    }
    else
    {
        
        mBut.selected = YES;
        expTextField.userInteractionEnabled = YES;
        cvvTextField.hidden = NO;
        
    }

}

#pragma mark - WebService call

-(void)sendServiceToDeleteCard
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Deleting Card...", @"Deleting Card...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *param = @{
                               @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken],
                               @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                               @"ent_cc_id":containingDetailsOfCard[@"id"],
                               @"ent_date_time":[UIHelper getCurrentDateTime],
                            };
        
        [[WebServiceHandler sharedInstance] sendRequestToDeleteCard:param andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
        
    }

}

#pragma mark - WebService Response Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
            
        case 0:
        {
            [Database DeleteCard:containingDetailsOfCard[@"id"]];
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
            [self navigationBackButtonAction:nil];
            break;
            
        }
        default:
            
            break;
    }
    
}

#pragma mark - UITextfieldDelegate -

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


@end
