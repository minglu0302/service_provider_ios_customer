//
//  RPOtpViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 22/03/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPViewController : UIViewController<UITextFieldDelegate,WebServiceHandlerDelegate>


@property (nonatomic,assign) BOOL rangeEnable;
/**
 *  This Function is called for signup
 */
@property (weak, nonatomic) IBOutlet UIButton *resendSMSButton;

@property(weak, nonatomic) IBOutlet UITextField *firstNumber;
@property(weak, nonatomic) IBOutlet UITextField *secondNumber;
@property(weak, nonatomic) IBOutlet UITextField *thirdNumber;
@property(weak, nonatomic) IBOutlet UITextField *fourthNumber;
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

/**
 *  Navigation Back Button Outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
/**
 *  UITextfield Variable
 */
@property (nonatomic,strong) UITextField *activeTextfield;
/**
 *  OTP First Digit Text Field Outlet's
 */

/**
 *   Signup Dictionary Varialbe
 */
@property (nonatomic,strong) NSDictionary *signUpDictionary;
/**
 *  Navigation Back Button Action
 *
 *  @param sender
 */
- (IBAction)navigationBackButton:(id)sender;
/**
 *  This function is called getting the otp from server
 *  @param mobileNumber
 */


-(void) getOTP:(NSString *)mobileNumber;

/**
 *  Tab Gesture Hiding For Keyboard
 *
 *  @param sender
 */




- (IBAction)otpTabGesture:(id)sender;
- (IBAction)resendSMSButton:(id)sender;
- (IBAction)registerButtonAction:(id)sender;


@end
