//
//  ItemDetailsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/20/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *itemLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
