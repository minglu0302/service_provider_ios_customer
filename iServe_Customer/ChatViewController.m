;//
//  ViewController.m
//  WhatsappChat
//
//  Created by Apple on 23/12/16.
//  Copyright © 2016 Apple. All rights reserved.
//

#import "ChatViewController.h"
#import "MessageCell.h"
#import "TableArray.h"
#import "MessageGateway.h"
#import "Inputbar.h"
#import "DAKeyboardControl.h"
#import "LocalStorage.h"
#import "SocketIOWrapper.h"

static ChatViewController *chatVC = nil;


@interface ChatViewController ()<InputbarDelegate,MessageGatewayDelegate,
UITableViewDataSource,UITableViewDelegate,SocketWrapperDelegate,WebServiceHandlerDelegate>
{
    SocketIOWrapper *socketWrapper;
    Contact *contact;
}

@property (strong, nonatomic) TableArray *tableArray;
@property (strong, nonatomic) MessageGateway *gateway;

@end

@implementation ChatViewController

#pragma mark - Initial Methods -

+ (instancetype) getSharedInstance
{
    return chatVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    chatVC = self;
    [self setInitialProperties];
//    [self setInputbar];
        [self.navigationItem addLeftSpace];

    
    // Do any additional setup after loading the view, typically from a nib.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
        
        if(!self.inputbar)
        {
            self.inputbar = [[Inputbar alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 64,  self.view.bounds.size.width, 44)];
            [self setInputbar];
            [self.view addSubview:self.inputbar];
        }
    }else{
        if(!self.inputbar)
        {
            self.inputbar = [[Inputbar alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44,  self.view.bounds.size.width, 44)];
            [self setInputbar];
            [self.view addSubview:self.inputbar];
        }
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)setInitialProperties
{
    [self setInitialDetails];
    [self setTableView];
    [self setGateway];
    [self sendRequestToGetChatMessages];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    __weak Inputbar *inputbar = _inputbar;
    __weak UITableView *tableView = self.chattingTableView;
    __weak ChatViewController *controller = self;
    __weak UIView *view = self.view;
    
    self.view.keyboardTriggerOffset = inputbar.frame.size.height;
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        if(opening)
        {
            CGRect toolBarFrame = inputbar.frame;
            toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
            inputbar.frame = toolBarFrame;
            
            
            CGRect inputViewFrameInView = [view convertRect:keyboardFrameInView fromView:nil];
            CGRect intersection = CGRectIntersection(tableView.frame, inputViewFrameInView);
            UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, intersection.size.height, 0.0);
            tableView.scrollIndicatorInsets = ei;
            tableView.contentInset = ei;
            
//            CGRect tableViewFrame = tableView.frame;
//            tableViewFrame.size.height = toolBarFrame.origin.y - 64;
//            tableView.frame = tableViewFrame;
            
//            [view layoutIfNeeded];
            
            [controller tableViewScrollToBottomAnimated:YES];

        }
        else if (closing)
        {
//            CGRect toolBarFrame = inputbar.frame;
//            toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
            inputbar.frame = CGRectMake(0, view.bounds.size.height - 44,  view.bounds.size.width, 44);
            
//            CGRect tableViewFrame = tableView.frame;
//            tableViewFrame.size.height = inputbar.frame.origin.y;
//            tableView.frame = tableViewFrame;
            
            UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
            [UIView animateWithDuration:0.2 animations:^{
                tableView.scrollIndicatorInsets = ei;
                tableView.contentInset = ei;
            }];


            [controller tableViewScrollToBottomAnimated:YES];

        }
        

    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    socketWrapper = [SocketIOWrapper sharedInstance];
    socketWrapper.socketDelegate = self;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    [self.view removeKeyboardControl];
    [self.gateway dismiss];
}
-(void)viewWillDisappear:(BOOL)animated
{
//    self.chat.last_message = [self.tableArray lastObject];
    [[LocalStorage sharedInstance]removeObjectOfClass];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Methods -

-(void)setInputbar
{
    self.inputbar.placeholder = @"Type a message";
    self.inputbar.delegate = self;
    self.inputbar.leftButtonImage = [UIImage imageNamed:@"share"];
    self.inputbar.rightButtonText = @"Send";
    self.inputbar.rightButtonTextColor = [UIColor colorWithRed:0 green:124/255.0 blue:1 alpha:1];
}

-(void)setTableView
{
    self.tableArray = [[TableArray alloc] init];
    
    self.chattingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    
    [self.chattingTableView registerClass:[MessageCell class] forCellReuseIdentifier: @"MessageCell"];
}

-(void)setGateway
{
    self.gateway = [MessageGateway sharedInstance];
    self.gateway.delegate = self;
    self.gateway.chat = self.chat;
    [self.gateway loadOldMessages];
}

-(void)setChat:(Chat *)chat
{
    _chat = chat;
//    self.title = chat.contact.name;
}

-(void)setInitialDetails
{
    contact = [[Contact alloc] init];
//    self.title = self.providerDetails[@"name"];
    contact.name = self.providerName;
    contact.identifier = self.bookingId;//@"12345";//self.providerDetails[@"pid"];
    
    self.chat = [[Chat alloc] init];
    self.chat.contact = contact;
    
    [self.activityIndicator startAnimating];
    
    [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:self.provioderImageURL]
                              placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         [self.activityIndicator stopAnimating];
                                         
                                     }];

    
}


#pragma mark - UIButton Actions -

- (IBAction)userDidTapScreen:(id)sender
{
    [self.inputbar resignFirstResponder];
}

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.inputbar resignFirstResponder];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController popViewControllerAnimated:NO];

}


#pragma mark - UITableView DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MessageCell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.message = [self.tableArray objectAtIndexPath:indexPath];
    return cell;
}

#pragma mark - UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.tableArray objectAtIndexPath:indexPath];
    return message.heigh;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.tableArray titleForSection:section];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:OpenSans_Regular size:15.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:OpenSans_Regular size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    
    return view;
}
- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
        dispatch_async(dispatch_get_main_queue(),^{
            [self.chattingTableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        });
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Inputbar Delegate -

-(void)inputbarDidPressRightButton:(Inputbar *)inputbar
{
    Message *message = [[Message alloc] init];
    message.text = inputbar.text;
    message.date = [NSDate date];
    message.chat_id = _chat.identifier;
    
    //Store Message in memory
    [self.tableArray addObject:message];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    [self.chattingTableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.chattingTableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.chattingTableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.chattingTableView endUpdates];
    

    
//    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    [appD playNotificationSound];
    
    //Send message to server
    [self.gateway sendMessage:message];
    
    self.messageId = self.messageId + 1;
//    ['msgtype', 'from', 'payload', 'to', 'msgid'];

    NSDictionary *params = @{
                             @"msgtype":@"0",
//                             @"from":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID],
//                             @"to":self.providerDetails[@"pid"],
                             @"bid":[NSNumber numberWithInteger:self.bookingId.integerValue],
//                             @"senderName":@"Raghu",
                             @"payload":message.text,//[self encodeStringTo64:message.text],
                             @"msgid":[NSNumber numberWithInteger:self.messageId],
                             @"usertype":[NSNumber numberWithInteger:2],
                             @"currdt":[UIHelper getCurrentGMTDateTime]
                            };

    NSLog(@"Published Message:%@",params);
    [socketWrapper publishMessageToChannel:@"Message" withMessage:params];
    
//    [self.chattingTableView setContentOffset:CGPointMake(0, self.chattingTableView.contentSize.height -self.chattingTableView.bounds.size.height) animated:true];
    
//    [self tableViewScrollToBottomAnimated:YES];
    
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
        //        dispatch_async(dispatch_get_main_queue(),^{
        [self.chattingTableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        //        });
    }

}

- (NSString*)encodeStringTo64:(NSString*)fromString
{
    // Create NSData object
    NSData *nsdata = [fromString
                      dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    // Print the Base64 encoded string
    NSLog(@"Encoded: %@", base64Encoded);
    return base64Encoded;
}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Left Button Pressed"
                                                        message:nil
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    [alertView show];
}
-(void)inputbarDidChangeHeight:(CGFloat)new_height
{
    //Update DAKeyboardControl
    self.view.keyboardTriggerOffset = new_height;
}

#pragma mark - MessageGateway Delegate -

-(void)gatewayDidUpdateStatusForMessage:(Message *)message
{
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    MessageCell *cell = (MessageCell *)[self.chattingTableView cellForRowAtIndexPath:indexPath];
    [cell updateMessageStatus];
}
-(void)gatewayDidReceiveMessages:(NSArray *)array
{
    self.tableArray = [[TableArray alloc] init];
    [self.tableArray addObjectsFromArray:array];
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self.chattingTableView reloadData];
//        [self tableViewScrollToBottomAnimated:NO];
        NSInteger numberOfSections = [self.tableArray numberOfSections];
        NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
        if (numberOfRows)
        {
            //        dispatch_async(dispatch_get_main_queue(),^{
            [self.chattingTableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                          atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            //        });
        }

    });
}

-(void)updateTableWhenRecieveMessageFromProvider:(Message *)message
{
    [self.tableArray addObject:message];
    dispatch_async(dispatch_get_main_queue(),^{
        
        [self.chattingTableView reloadData];
//        [self tableViewScrollToBottomAnimated:NO];
        NSInteger numberOfSections = [self.tableArray numberOfSections];
        NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
        if (numberOfRows)
        {
            //        dispatch_async(dispatch_get_main_queue(),^{
            [self.chattingTableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                                          atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            //        });
        }

    });

}

#pragma mark - Socket Response -

-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message{
    
    NSLog(@"ChatVC message: %@",message);
    
    if([channelName isEqualToString:@"CustomerStatus"]){//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10){//Booking Response
            
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"Message"])
    {
        if(message[@"deliver"])
        {
//            if([message[@"deliver"] integerValue] == 1)
//                [self gotMessagesFromProvider:message];
        }
        else if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
        
    }
}


-(void)gotMessagesFromProvider:(NSDictionary *)messageDict
{
//    NSArray *texts = @[@"Hello Demo!",
//                       @"This project try to implement a chat UI similar to Whatsapp app."];

//    Message *last_message = nil;
//    for (NSString *text in texts)
//    {
//        Message *message = [[Message alloc] init];
//        message.text = text;
//        message.sender = MessageSenderSomeone;
//        message.status = MessageStatusReceived;
//        message.chat_id = self.chat.identifier;
//        message.date = [NSDate date];
//        message.identifier = @"";

        Message *message = [Message messageFromDictionary:messageDict
                                        andIdentifier:self.bookingId];
    
        [[LocalStorage sharedInstance] storeMessage:message];
       
    
//        [self.gateway loadOldMessages];
    [self updateTableWhenRecieveMessageFromProvider:message];

    
   
    
    //Insert Message in UI
//    [self performSelector:@selector(showMessagesFromProvider:)
//               withObject:messageDict
//               afterDelay:1];

//    dispatch_async(dispatch_get_main_queue(),^{
//
//        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
//        [self.chattingTableView beginUpdates];
//        if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
//            [self.chattingTableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
//                              withRowAnimation:UITableViewRowAnimationNone];
//        [self.chattingTableView insertRowsAtIndexPaths:@[indexPath]
//                                  withRowAnimation:UITableViewRowAnimationNone];
//        [self.chattingTableView endUpdates];
//        
//        [self tableViewScrollToBottomAnimated:YES];
//        
//    });
    
}
-(void)showMessagesFromProvider:(NSDictionary *)messageDict
{
    Message *message = [Message messageFromDictionary:messageDict
                                        andIdentifier:self.bookingId];
    
    //Store Message in memory
    [self.tableArray addObject:message];
    [self.gateway sendMessage:message];
    
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:message];
    dispatch_async(dispatch_get_main_queue(),^{
        [self.chattingTableView beginUpdates];
    
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
    {
        [self.chattingTableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                              withRowAnimation:UITableViewRowAnimationNone];
        
    }
    else {
        
        [self.chattingTableView insertRowsAtIndexPaths:@[indexPath]
                                      withRowAnimation:UITableViewRowAnimationBottom];
        NSRange range = NSMakeRange(indexPath.section, 1);
        NSIndexSet *section = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.chattingTableView reloadSections:section withRowAnimation:UITableViewRowAnimationNone];

    }
//        [self tableViewScrollToBottomAnimated:YES];
   
//            [self.chattingTableView endUpdates];
    });
    
    
    
    
    
////    [[LocalStorage sharedInstance] storeMessage:[Message messageFromDictionary:messageDict
//                                                                         andIdentifier:self.bookingId]];
    
    
//    [self.gateway loadOldMessages];


}

#pragma mark - Web Service Call -

-(void)sendRequestToGetChatMessages
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":self.bookingId,
                                 @"ent_user_type":[NSNumber numberWithInteger:2],
                                };
        
        
        [[WebServiceHandler sharedInstance] sendRequestToGetChatMessages:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
        [self.activityIndicator stopAnimating];
    }
    
}

#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    
    if (error)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeGetChatMessages)
            {
                //Maintain array Of Old Chat Messages
//                dispatch_async(dispatch_get_main_queue(),^{
                
                    [self parseOldChatMessages:response[@"messages"]];
        
//                });
                
                
            }
            else {
                
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
            }
            
        }
            break;
        default:
            break;
    }
    
    
}

-(void)parseOldChatMessages:(NSArray *)arrayOfOldMessages
{
    for(NSDictionary *dict in arrayOfOldMessages)
    {
        [[LocalStorage sharedInstance] storeMessage:[Message messageFromDictionary:dict
                                                                     andIdentifier:self.bookingId]];
    }
    
    [self.gateway loadOldMessages];
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    

}


@end
