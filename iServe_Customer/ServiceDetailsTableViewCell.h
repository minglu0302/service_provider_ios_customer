//
//  ServiceDetailsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServiceDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *serviceNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *serviceSelectedButton;

@end
