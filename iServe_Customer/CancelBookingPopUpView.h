//
//  CancelBookingPopUpView.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CancelBookingPopUpView : UIView 

+ (id)sharedInstance;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *gesture;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;


@property (strong , nonatomic) NSString *bookingCancelReason;
@property NSInteger bookingId;
@property NSInteger selectedCancelReasonIndex;
@property (strong , nonatomic) NSArray *arrayOfCancelReasons;

- (void)setTopViewFrame;
- (void)sendRequestToGetCancelReasons;
- (IBAction)submitButtonAction:(id)sender;
- (IBAction)closeButtonAction:(id)sender;

@end
