//
//  SocketIOWrapper.m
//  Snapchat
//
//  Created by -Tony Lu on 11/09/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "SocketIOWrapper.h"
#import "SIOClient.h"
#import "SIOConfiguration.h"

static SocketIOWrapper *shared = nil;
#define timeStamp [[NSDate date] timeIntervalSince1970]*1000

@interface SocketIOWrapper()<SIOClientDelegate>
@property SIOClient *socketIOClient;
@end

@implementation SocketIOWrapper

@synthesize socketDelegate;


+(instancetype)sharedInstance
{
    if (!shared) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    return shared;
}

-(void)connectSocket {
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [shared socketIOSetup];
    }];
}


-(void)socketIOSetup
{
    SIOConfiguration *config = [SIOConfiguration defaultConfiguration];
    self.socketIOClient = [SIOClient sharedInstance];
    [self.socketIOClient setConfiguration:config];
    [self.socketIOClient setDelegate:self];
    [self.socketIOClient connect];
    self.channelsName = [[NSMutableArray alloc] init];
    
}

-(void)sendHeartBeatForUser:(NSString *)cid withStatus:(NSString *)status {
    
    NSDictionary *dict = @{
                           @"status":status,
                           @"cid":[NSNumber numberWithInteger:cid.integerValue],
                         };
    
    NSLog(@"heartbeat: %@", dict);
    
    [self.socketIOClient publishToChannel:@"CustomerStatus" message:dict];
    
}

-(void)publishMessageToChannel:(NSString *)channel withMessage:(NSDictionary *)messageDict {
    
    [self.socketIOClient publishToChannel:channel message:messageDict];
    
    
}
- (void) sioClient:(SIOClient *)client didConnectToHost:(NSString*)host {
    
    NSLog(@"SIO connected to %@", host);
    
    if(![self.channelsName containsObject:@"CustomerStatus"])
    {
        [self.channelsName addObject:@"CustomerStatus"];
        [self.socketIOClient subscribeToChannels:@[@"CustomerStatus"]];
    }
    if(![self.channelsName containsObject:@"LaterBooking"])
    {
        [self.channelsName addObject:@"LaterBooking"];
        [self.socketIOClient subscribeToChannels:@[@"LaterBooking"]];
    }
    if(![self.channelsName containsObject:@"cancelBooking"])
    {
        [self.channelsName addObject:@"cancelBooking"];
        [self.socketIOClient subscribeToChannels:@[@"cancelBooking"]];
    }

    if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(didConnect)]) {
        [self.socketDelegate didConnect];
    }
    if(![self.channelsName containsObject:@"UpdateCustomer"])
    {
        [self.channelsName addObject:@"UpdateCustomer"];
        [self.socketIOClient subscribeToChannels:@[@"UpdateCustomer"]];
    }
    if(![self.channelsName containsObject:@"Message"])
    {
        [self.channelsName addObject:@"Message"];
        [self.socketIOClient subscribeToChannels:@[@"Message"]];
    }
    if(![self.channelsName containsObject:@"makefree"])
    {
        [self.channelsName addObject:@"makefree"];
        [self.socketIOClient subscribeToChannels:@[@"makefree"]];
    }

    
    [[NSNotificationCenter defaultCenter] postNotificationName:iServeSocketDisconnectedAndConnected object:nil userInfo:nil];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID])
    {
         [self sendHeartBeatForUser:[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID] withStatus:@"1"];
    }

}

-(void)subscribeToProvider:(NSString *)providerChannelName
{
    if(![self.channelsName containsObject:providerChannelName])
    {
        [self.channelsName addObject:providerChannelName];
        [self.socketIOClient subscribeToChannels:@[providerChannelName]];
    }

}

- (void) sioClient:(SIOClient *)client didSubscribeToChannel:(NSString*)channel {
    NSLog(@"SIO subscribe to %@", channel);
}


- (void) sioClient:(SIOClient *)client didSendMessageToChannel:(NSString *)channel {
    NSLog(@"SIO message sent to %@", channel);
}


- (void) sioClient:(SIOClient *)client didRecieveMessage:(NSArray*)message onChannel:(NSString *)channel {
    
    NSLog(@"SIO message recieve to %@", channel);
    if (message.count == 0) {
        return;
    }
    
    NSDictionary *dict = message[0];
    
    if (self.socketDelegate && [self.socketDelegate respondsToSelector:@selector(receievedMessageOnChannel:withMessage:)])
    {
        [socketDelegate receievedMessageOnChannel:channel withMessage:dict];
    }
    
}

- (void) sioClient:(SIOClient *)client didDisconnectFromHost:(NSString*)host {
    
    NSLog(@"SIO disconnected from %@", host);
}

- (void) sioClient:(SIOClient *)client gotError:(NSDictionary *)errorInfo {
    
    NSLog(@"SIO Error : %@", errorInfo);
}

@end
