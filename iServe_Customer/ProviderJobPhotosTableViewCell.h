//
//  ProviderJobPhotosTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderJobPhotosTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;


-(void)reloadCollectionView:(NSArray *)arrayOfJobImagesURL and:(NSInteger)pid;

@end
