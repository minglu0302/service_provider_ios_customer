//
//  RecieptDetailsPopUpView.h
//  iServe_AutoLayout
//  Created by -Tony Lu on 7/19/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptDetailsPopUpView : UIView <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *appointmentDateLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewTopConstraint;


@property (strong, nonatomic) NSDictionary *bookingDetails;
@property (strong, nonatomic) NSArray *arrayOfServiceDetails;

@property float subTotalValue;

+ (id)sharedInstance;
- (void)setTopViewFrame;
- (IBAction)closeButtonAction:(id)sender;
- (void)setTableFooter;

@end
