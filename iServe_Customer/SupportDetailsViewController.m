//
//  TWTDetailsViewController.m
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SupportDetailsViewController.h"
#import "SupportDetailsTableCell.h"
#import "SupportWebViewController.h"
@interface SupportDetailsViewController ()

@end

@implementation SupportDetailsViewController

#pragma mark - UILife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.senderString;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.detailsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SupportDetailsTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
    cell.detailBackgroundView.layer.borderWidth = 1.0f;
    cell.detailBackgroundView.layer.borderColor = [UIColor colorWithRed:232/255.0 green:232/255.0 blue:232/255.0 alpha:1].CGColor;
    cell.cellLabel.text = self.detailsArray[indexPath.row][@"tag"];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"supportWebViewVC" sender:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *path = (NSIndexPath *)sender;
    if ([[segue identifier] isEqualToString:@"supportWebViewVC"])
    {
        SupportWebViewController *webView = (SupportWebViewController*)[segue destinationViewController];
        self.webUrlLink = [NSString stringWithFormat:@"%@",self.detailsArray[path.row][@"link"]];
        webView.title = [flStrForStr(self.detailsArray[path.row][@"tag"])capitalizedString];//NSLocalizedString(@"Learn More", @"Learn More");
        webView.weburlString = self.webUrlLink;
    }
}

#pragma mark - IBAction

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
