//
//  JobDetailsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *jobDetailTextView;

@end
