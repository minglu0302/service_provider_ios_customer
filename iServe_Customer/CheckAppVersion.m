//
//  checkAppVersion.m
//  iServe_Customer
//
//  Created by Apple on 17/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "CheckAppVersion.h"

static CheckAppVersion *checkAppVersion = nil;

@implementation CheckAppVersion

+(instancetype)sharedInstance
{
    if (!checkAppVersion) {
        
        checkAppVersion = [[self alloc] init];
    }
    return checkAppVersion;
}


-(void)checkAppHasUpdatedVersion
{
    NSString* appID = [[NSBundle mainBundle] infoDictionary][@"CFBundleIdentifier"];
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@",appID]];
    NSMutableURLRequest *request =  [NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:100];
    
    NSURLSession *delegateFreeSession;
    delegateFreeSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                        delegate:nil
                                                   delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task;
    task = [delegateFreeSession dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      if (!data || !response || error)
                                      {
                                          NSLog(@"Check App Version:%@",[error localizedDescription]);
                                          
                                          [[[UIAlertView alloc]initWithTitle:@"Error!"
                                                                     message:[error localizedDescription]
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil, nil]show];
                                          
                                          return;
                                      }
                                      
                                      NSDictionary *appDetails = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                                      
                                      
                                      if ([appDetails[@"resultCount"] integerValue] == 1)
                                      {
                                          //Got Result From Appstore
                                          
                                          NSString* appStoreVersion = appDetails[@"results"][0][@"version"];
                                          NSString* currentVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
                                          NSLog(@"Appstore Version %@", appStoreVersion);
                                          
                                          currentVersion = [currentVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
                                          appStoreVersion = [appStoreVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
                                          
                                          if (appStoreVersion.integerValue > currentVersion.integerValue)
                                          {
                                              
                                              [[[UIAlertView alloc]initWithTitle:@"New Update Available"
                                                                         message:[NSString stringWithFormat:@"There is a new update available for %@ on the app store ! Please click on the UDPATE button to download this new update !",[UIHelper getProjectName]]
                                                                        delegate:self
                                                               cancelButtonTitle:@"Later"
                                                               otherButtonTitles:@"Update", nil]show];
                                              
                                          }
                                          
                                      }
                                      
                                      
                                  }];
    [task resume];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
    }
    
    [self performSelector:@selector(setLeftMenuSettings) withObject:nil afterDelay:0.6];
    
    
    
}

-(void)setLeftMenuSettings
{
    [[UIHelper sharedInstance]setLeftMenuSettings];
}
-(void)sendRequestToUpdateVersion
{
    //    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    //    if([reachability isNetworkAvailable])
    //    {
    NSDictionary *params = @{
                             @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                             @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                             @"ent_app_version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                             @"ent_user_type":[NSNumber numberWithInteger:2],
                             };
    
    [[WebServiceHandler sharedInstance] sendRequestToUpdateAppVersion:params andDelegate:self];
    
    //    }
    
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error)
    {
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    //    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            
        }
            break;
        case 0:
            
        {
            if(requestType == RequestTypeUpdateAppVersion)
            {
                [[NSUserDefaults standardUserDefaults]setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"appVersion"];
            }
        }
            break;
        default:
            break;
    }
}

-(void)sendHeartBeatToSocket
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID])
    {
        [[SocketIOWrapper sharedInstance] sendHeartBeatForUser:[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID] withStatus:@"1"];
    }
}

@end
