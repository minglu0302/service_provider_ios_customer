//
//  LiveBookingStatusUpdateClass.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/27/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "LiveBookingStatusUpdateClass.h"
#import "ProviderBookingViewController.h"
#import "InvoicePopUpView.h"
#import "ChatViewController.h"

static LiveBookingStatusUpdateClass *liveBookingStatusUpdate = nil;

@interface LiveBookingStatusUpdateClass ()<WebServiceHandlerDelegate,UIAlertViewDelegate>
{
    NSMutableArray *arrayOfOngoingBookings;
    NSMutableArray *arrayOfMessages;
    NSInteger bid;
    NSInteger bstatus;
    UINavigationController *naviVC;
    ShowOnGoingBookingsViewController *showOngoingBookingVC;
    ChatViewController *chatVC;
    UIAlertView *bookingFlowAlertMessage;
    InvoicePopUpView *invoicePopUp;
    UIWindow *window;
    
    //Chat Message
    NSDictionary *chatDetails;
    NSMutableArray *removeMessages;
}


@end

@implementation LiveBookingStatusUpdateClass

+(instancetype)sharedInstance
{
    if (!liveBookingStatusUpdate) {
        
        liveBookingStatusUpdate = [[self alloc] init];
    }
    return liveBookingStatusUpdate;
}

-(void)sendRequestToGetAllonGoingBookings
{
    arrayOfMessages = [[NSMutableArray alloc]init];
    [[NSUserDefaults standardUserDefaults] setObject:arrayOfMessages forKey:iServeAllMessages];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    removeMessages = [[NSMutableArray alloc]init];
    [[NSUserDefaults standardUserDefaults] setObject:removeMessages forKey:iServeAllRemoveMessages];
    [[NSUserDefaults standardUserDefaults]synchronize];


    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_cust_id":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToGetAllOngoingAppointmentDetails:params andDelegate:self];
        
    }
    
}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error)
    {
//        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:[UIHelper getCurrentController]];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
//                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeGetAllOngoingApptDetails)
            {
                arrayOfOngoingBookings = [[NSMutableArray alloc]initWithArray:response[@"appointments"]];
                
                [[NSUserDefaults standardUserDefaults] setObject:arrayOfOngoingBookings forKey:iServeAllOngoingBookings];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                //To Show Rating PopUp On Home VC
                if([response[@"NotReviewedAppts"]count] > 0)
                {
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
                    
                    UINavigationController *navigationVC = menu.currentActiveNVC;
                    
                    if([navigationVC.topViewController isKindOfClass:[HomeViewController class]])
                    {
                        dispatch_async(dispatch_get_main_queue(),^{
                            [[ProgressIndicator sharedInstance]hideProgressIndicator];
                        });
                        HomeViewController *homeVC = [HomeViewController getSharedInstance];
                        homeVC.arrayOfReviewNotDoneBookings = response[@"NotReviewedAppts"];
                        homeVC.reviewCount = 0;
                        [homeVC showInvoiceRatingPopUP];
                    }
                }
                
            }
            
        }
            break;
        default:
            break;
    }
    
}

-(void)profileGotRejected:(NSDictionary *)response
{
    NSLog(@"Seesion Token: %@",[[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken]);
    if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken])
    {
        [[Logout sharedInstance]deleteUserSavedData:response[@"alert"]];
    }
    
}

-(void)removeChatMessage:(NSDictionary *)message
{
    dispatch_async(dispatch_get_main_queue(),^{
        
        NSMutableArray *oldArrayOfMessages = [[[NSUserDefaults standardUserDefaults] objectForKey:iServeAllMessages]mutableCopy];
        
        if(oldArrayOfMessages == nil)
        {
            oldArrayOfMessages = [[NSMutableArray alloc]init];
        }
    
        if(oldArrayOfMessages.count > 0)
        {
            NSPredicate *samplePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"timestamp = %@",message[@"timestamp"]]];
            NSArray *result = [oldArrayOfMessages filteredArrayUsingPredicate:samplePredicate];
            if(result.count > 0)
            {
                [oldArrayOfMessages removeObject:[result objectAtIndex:0]];
                
                [[NSUserDefaults standardUserDefaults] setObject:oldArrayOfMessages forKey:iServeAllMessages];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
            }
        
        }
    });

}


-(void)didRecieveChatMessageFromPushorSocket:(NSDictionary *)response
{
    //{
    //    bid = 449;
    //    dt = "2017-01-31 07:29:36";
    //    err = 2;
    //    msgid = 1;
    //    msgtype = 0;
    //    payload = Hi;
    //    pic = "https://s3.amazonaws.com/iserve/ProfileImages/venga@gmail.com.png";
    //    timestamp = 1485847777579;
    //    usertype = 1;
    //}
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        arrayOfMessages = [[[NSUserDefaults standardUserDefaults] objectForKey:iServeAllMessages]mutableCopy];
        
        if(arrayOfMessages == nil)
        {
            arrayOfMessages = [[NSMutableArray alloc]init];
        }
        

       //Checking If Message Already shown
        NSArray *result;
        
        if(arrayOfMessages.count > 0)
        {
            NSPredicate *samplePredicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"timestamp = %@",response[@"timestamp"]]];
            result = [arrayOfMessages filteredArrayUsingPredicate:samplePredicate];
        }

        if(result.count == 0)//New Message
        {
            [arrayOfMessages addObject:response];
            
            [[NSUserDefaults standardUserDefaults] setObject:arrayOfMessages forKey:iServeAllMessages];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self performSelector:@selector(removeChatMessage:)
                       withObject:response
                       afterDelay:10];
            
            chatDetails = response;
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
            
            naviVC = menu.currentActiveNVC;
        
    
            if(![naviVC.topViewController isKindOfClass:[ChatViewController class]])
            {
                //Not A Chat VC Class
                
                /**
                 *  dismiss Old Booking Alert Message
                 */
                [bookingFlowAlertMessage dismissWithClickedButtonIndex:0 animated: TRUE];
                
                //Show Chat Message Alert View
                [self showChatMessageAlertView:[NSString stringWithFormat:@"You got new message from provider & BID:%td",[response[@"bid"]integerValue]]];
                
                
            }
            else{
                
                //Chat VC Class
                chatVC = [ChatViewController getSharedInstance];
                
                if(chatVC.bookingId.integerValue != [response[@"bid"]integerValue])
                {
                    //Chat VC with Different BID
                    
                    [bookingFlowAlertMessage dismissWithClickedButtonIndex:0 animated: TRUE];
                   
                    [self showChatMessageAlertView:[NSString stringWithFormat:@"You got new message from driver & BID:%td",[response[@"bid"]integerValue]]];
                    
                }
                else{//Chat VC with same BID
                    
                    chatVC.bookingId = [NSString stringWithFormat:@"%td",[response[@"bid"]integerValue]];
                    [chatVC gotMessagesFromProvider:response];
                    
                }
                
            }
       

        }
        
     });
    
}


-(void)didRecieveBookingStatusFromPushOrSocket:(NSDictionary *)response
{
    bstatus = 0;
    bid = [response[@"bid"]integerValue];
    
    arrayOfOngoingBookings = [[[NSUserDefaults standardUserDefaults] objectForKey:iServeAllOngoingBookings]mutableCopy];
    
    if(arrayOfOngoingBookings == nil)
    {
        arrayOfOngoingBookings = [[NSMutableArray alloc]init];
    }
    
    NSInteger i;
    
    //Updating Booking Status
    for(i=0; i<arrayOfOngoingBookings.count; i++)
    {
        if([arrayOfOngoingBookings[i][@"apntid"] integerValue] == bid)
        {
            bstatus = [arrayOfOngoingBookings[i][@"status"]integerValue];
            
            if(bstatus != [response[@"st"]integerValue])//Booking Status Not Same
            {
                [arrayOfOngoingBookings removeObjectAtIndex:i];
                NSDictionary *dict = @{
                                       @"apntid":[NSNumber numberWithInteger:bid],
                                       @"status":response[@"st"],
                                     };
                
                //Booking Status has Changed
                [arrayOfOngoingBookings addObject:dict];
                
                [[NSUserDefaults standardUserDefaults] setObject:arrayOfOngoingBookings forKey:iServeAllOngoingBookings];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            
            break;
        }
    }
    
    if(i == arrayOfOngoingBookings.count)//Adding New Booking Details To List
    {
        NSDictionary *dict = @{
                               @"apntid":[NSNumber numberWithInteger:bid],
                               @"status":response[@"st"],
                             };
        
        [arrayOfOngoingBookings addObject:dict];
        
        [[NSUserDefaults standardUserDefaults] setObject:arrayOfOngoingBookings forKey:iServeAllOngoingBookings];
        [[NSUserDefaults standardUserDefaults]synchronize];

    }
    if(bstatus == [response[@"st"]integerValue] )//If Booking Status Same //&& [response[@"st"]integerValue]!=15)//If Booking Status Same and Not 15
    {
        return;
    }
    else
    {
        NSLog(@"Live Booking Status Updated");
        dispatch_async(dispatch_get_main_queue(),^{
            //Booking Status Not same
            bstatus = [response[@"st"]integerValue];
            
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
            
            naviVC = menu.currentActiveNVC;
            
            switch (bstatus) {
                case 2:
                {
                    //Booking Status Accepted(2)
                    
                    if([naviVC.topViewController isKindOfClass:[ProviderBookingViewController class]])
                    {
                        NSLog(@"Goto Provider Booking Confirmation Controller");
                        [[ProviderBookingViewController getSharedInstance]parseBookingAcceptedResponse];
                    }
                    
                }
                    break;
                case 3:
                {//Booking Rejected By Provider
                    
                }
                    break;
                case 10:
                {//Booking Cancelled By Provider
                    [UIHelper showMessage:response[@"alert"] withTitle:LS(@"Message")delegate:[UIHelper getCurrentController]];
                    if([naviVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
                    {
                        showOngoingBookingVC = [ShowOnGoingBookingsViewController getSharedInstance];
                        
                        if(showOngoingBookingVC.bookingId == bid)//If Showing Same Booking
                            [showOngoingBookingVC navigationBackButtonAction:nil];
                    }
                }
                    break;
                    
                default:
                {   //For Other Status (5,6,15,16,21,22,7)
                    
                    if(![naviVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
                    {
                        //Not A Booking Flow Class
                        
                        /**
                         *  dismiss Old Booking Alert Message
                         */
                        [bookingFlowAlertMessage dismissWithClickedButtonIndex:0 animated: TRUE];
                        if(bstatus == 7)
                        {
                            //Booking Completed
                            [self showBookingCompletedAlertMessage:response[@"alert"]];
                        }
                        else{
                            [self showBookingFlowAlertMessage:response[@"alert"]];
                        }
                        
                    }
                    else{
                        
                        //ShowOnGoingBookingsViewController  Class
                        showOngoingBookingVC = [ShowOnGoingBookingsViewController getSharedInstance];
                        
                        if(showOngoingBookingVC.bookingId!= bid)
                        {
                            //Booking Flow Controller with Different BID
                            
                            [bookingFlowAlertMessage dismissWithClickedButtonIndex:0 animated: TRUE];
                            if(bstatus == 7)
                            {
                                //Booking Completed
                                [self showBookingCompletedAlertMessage:response[@"alert"]];
                            }
                            else{
                                [self showBookingFlowAlertMessage:response[@"alert"]];
                            }
                            
                        }
                        else{//Booking Flow Controller with same BID
                            
                            showOngoingBookingVC.status = bstatus;
                            showOngoingBookingVC.bookingId = bid;
                            [showOngoingBookingVC changeViews];
                            
                            if(bstatus == 7)
                            {
                                [self showInvoicePopUp];
                            }
                        }
                        
                    }
                    
                }
                    break;
            }
            
            
        });
    }
    
}

#pragma mark - UIAlertView Delegate -

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    naviVC = menu.currentActiveNVC;
    
    switch (alertView.tag) {
            
        case 1://Normal Booking Status Alert Message Action
        {
            if(buttonIndex == 1)
            {
                if([naviVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
                {
                    showOngoingBookingVC.status = bstatus;
                    showOngoingBookingVC.bookingId = bid;
                    showOngoingBookingVC.bookingIdLabel.text = [NSString stringWithFormat:LS(@"BOOKING ID: %zd"),bid];
                    [showOngoingBookingVC changeViews];
                    if(bstatus!=6 && bstatus!=15 && bstatus!=16)
                        [showOngoingBookingVC sendRequestToGetBookingDetails:0];
                }
                else
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    ShowOnGoingBookingsViewController *vc = (ShowOnGoingBookingsViewController*)[storyboard instantiateViewControllerWithIdentifier:@"showOnGoingBookingVC"];
                    vc.status = bstatus;
                    vc.bookingId = bid;
                    [naviVC pushViewController:vc animated:YES];
                }
            }
            else
            {
                if([naviVC.topViewController isKindOfClass:[BookingsHistoryViewController class]])
                {
                    [[BookingsHistoryViewController getSharedInstance]sendRequestToGetBookingDetails];
                }
                
            }
            
        }
            break;
        case 2:
        {
            //Booking Completed Alert Message Action
            [self showInvoicePopUp];
            if([naviVC.topViewController isKindOfClass:[BookingsHistoryViewController class]])
            {
                [[BookingsHistoryViewController getSharedInstance]sendRequestToGetBookingDetails];
            }
            
        }
            break;
            
        case 3://Chat Message
        {
            if(buttonIndex == 1)//Clicked on VIEW Buttoon
            {
                if([naviVC.topViewController isKindOfClass:[ChatViewController class]])
                {
                    chatVC = [ChatViewController getSharedInstance];
                    chatVC.bookingId = [NSString stringWithFormat:@"%td",[chatDetails[@"bid"] integerValue]];
                    chatVC.providerName = chatDetails[@"name"];
                    chatVC.provioderImageURL = chatDetails[@"pic"];
                    
                    chatVC.title = [NSString stringWithFormat:LS(@"BOOKING ID: %td"),[chatDetails[@"bid"] integerValue]];
                    
                    [chatVC setInitialProperties];
                }
                else
                {
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    ChatViewController *vc = (ChatViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
                    vc.bookingId = chatDetails[@"bid"];
                    vc.providerName = chatDetails[@"name"];
                    vc.provioderImageURL = chatDetails[@"pic"];
                    
                    vc.title = [NSString stringWithFormat:LS(@"BOOKING ID: %td"),[chatDetails[@"bid"] integerValue]];
                    
                    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                                              subType:kCATransitionFromTop
                                                              forView:naviVC.view
                                                         timeDuration:0.3];
                    
                    [naviVC pushViewController:vc animated:NO];
                }
            }
            else
            {//Clicked on OK Buttoon
                
            }

        }
            break;
        default:
        {
            
        }
            break;
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    naviVC = menu.currentActiveNVC;
    
//    if([naviVC.topViewController isKindOfClass:[BookingsHistoryViewController class]])
//    {
//        [[BookingsHistoryViewController getSharedInstance]sendRequestToGetBookingDetails];
//    }
    
}

-(void)showInvoicePopUp
{
    window = [[[UIApplication sharedApplication]delegate]window];
    
    invoicePopUp = [InvoicePopUpView sharedInstance];
    invoicePopUp.bookingStatus = bstatus;//completed Booking
    [invoicePopUp setTopViewFrame];

    [window addSubview:invoicePopUp];
    
    invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);

                     }
                     completion:^(BOOL finished){
                         invoicePopUp.bookingId = bid;
                         [invoicePopUp sendRequestTogetBookingDetails];
                     }];

    
    
}

-(void)showBookingCompletedAlertMessage:(NSString *)message
{
    bookingFlowAlertMessage = [[UIAlertView alloc]initWithTitle:LS(@"Message")message:message delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil];
    bookingFlowAlertMessage.tag = 2;
    [bookingFlowAlertMessage show];
    
}

-(void)showBookingFlowAlertMessage:(NSString *)message
{
    bookingFlowAlertMessage = [[UIAlertView alloc]initWithTitle:LS(@"Message")message:message delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:LS(@"View"), nil];
    bookingFlowAlertMessage.tag = 1;
    [bookingFlowAlertMessage show];
    
}

-(void)showChatMessageAlertView:(NSString *)message
{
    bookingFlowAlertMessage = [[UIAlertView alloc]initWithTitle:LS(@"Message")message:message delegate:self cancelButtonTitle:LS(@"OK") otherButtonTitles:LS(@"View"), nil];
    bookingFlowAlertMessage.tag = 3;
    [bookingFlowAlertMessage show];

}


@end
