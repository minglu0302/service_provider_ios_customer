//
//  ChangePasswordViewController.h
//  iServe_Customer
//
//  Created by Apple on 02/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmPasswordLabel1;
@property (weak, nonatomic) IBOutlet UILabel *neewPasswordLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmPasswordLabel2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;


@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField1;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField2;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (strong ,nonatomic) NSString *phoneNumber;
@property (strong ,nonatomic) UITextField *activeTextField;
@property BOOL isFromProfileVC;

- (IBAction)changePasswordButtonAction:(id)sender;
- (IBAction)navigationLeftButtonAction:(id)sender;

@end
