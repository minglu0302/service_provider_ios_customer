//
//  ForgotPasswordViewController.h
//  iServe_Customer
//
//  Created by Apple on 17/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIImageView *countryImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryCodeLabel;


- (IBAction)NavigationLeftButtonAction:(id)sender;
- (IBAction)submitButtonAction:(id)sender;
- (IBAction)forgotPasswordTapGestureAction:(id)sender;
- (IBAction)countryCodeButtonAction:(id)sender;

@end
