//
//  ProviderReviewsTableViewCell.m
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderReviewsTableViewCell.h"

@implementation ProviderReviewsTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)showReviewDetails:(NSDictionary *)reviewDetails
{
    self.reviewerNameLabel.text = reviewDetails[@"by"];
    self.reviewLabel.text = reviewDetails[@"review"];
    
   
    //Show No Of Days Ago
    NSString *reviewedDateinString = reviewDetails[@"dt"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *reviewedDate = [dateFormatter dateFromString:reviewedDateinString];
    
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear
                                                        fromDate:reviewedDate
                                                          toDate:[NSDate new]
                                                         options:0];
    NSLog(@"components:%@",components);
    
    if(components.year > 0)
    {
        if(components.year==1)
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"%td year ago"),components.year];
        }
        else
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"%td years ago"),components.year];
        }
    }
    else if (components.month > 0)
    {
        if(components.month == 1)
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"%td month ago"),components.month];
        }
        else
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"%td months ago"),components.month];
        }
    }
    else
    {
        if(components.day == 0)
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"Today")];
        }
        else if(components.day == 1)
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"Yesterday")];
        }
        else
        {
            self.daysAgoLabel.text = [NSString stringWithFormat:LS(@"%td days ago"),components.day];
        }

    }
    
    
    self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
    self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
    self.ratingView.markFont = [UIFont systemFontOfSize:18];
    self.ratingView.value = [reviewDetails[@"rating"]floatValue];

    [self.activityIndicator startAnimating];
    
    [self.reviewerImageView sd_setImageWithURL:[NSURL URLWithString:reviewDetails[@"patPic"]]
                              placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         
                                         [self.activityIndicator stopAnimating];
                                         
                                     }];

}

@end
