//
//  SIOConfiguration.m
//  SIOClient
//
//  Created by Vinay Raja on 14/09/15.
//  Copyright (c) 2015 3Embed. All rights reserved.
//

#import "SIOConfiguration.h"

#define kHostURL        @"http://www.goclean-service.com"
#define kPortNumber     @"9999"

@interface SIOConfiguration ()

@property (nonatomic, strong) NSString *hostURL;
@property (nonatomic, strong) NSString *portNumber;

@end

@implementation SIOConfiguration

+ (instancetype) defaultConfiguration {
    SIOConfiguration *config = [[SIOConfiguration alloc] init];
    config.hostURL = kHostURL;
    config.portNumber = kPortNumber;
    return config;
}

- (instancetype) initWithHostURL:(NSString*)hostURL portNumber:(NSString*)portNumber {
    self = [super init];
    if (self) {
        self.hostURL = hostURL;
        self.portNumber = portNumber;
    }
    
    return self;
}

@end

@implementation SIOConfiguration (AccessHelper)

- (NSString*) getHostString {
    return [NSString stringWithFormat:@"%@:%@",_hostURL,_portNumber];
}

@end
