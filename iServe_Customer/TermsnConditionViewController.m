#import "TermsnConditionViewController.h"
#import "WebViewController.h"
#import "WebViewController.h"

@interface TermsnConditionViewController ()

@end

@implementation TermsnConditionViewController

#pragma mark - UILife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

#pragma mark - IBAction -

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)termsConditionButton:(id)sender
{
     self.titlestring = LS(@"Terms & Conditions");
     [self performSegueWithIdentifier:@"gotoWebView" sender:[NSString stringWithFormat:@"%@termcondition.html",BASE_IP]];
}

- (IBAction)privacyPolicyButton:(id)sender
{
    self.titlestring = LS(@"Privacy Policy");
    [self performSegueWithIdentifier:@"gotoWebView" sender:[NSString stringWithFormat:@"%@PrivacyPolicy.html",BASE_IP]];
}

#pragma mark - prepareForSegue -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        WebViewController *controller = [segue destinationViewController];
        controller.senderString = sender;
        controller.title = self.titlestring;
    }
}

@end
