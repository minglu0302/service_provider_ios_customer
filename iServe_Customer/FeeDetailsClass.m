//
//  FeeDetailsClass.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/18/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "FeeDetailsClass.h"

static FeeDetailsClass *feeDetails;

@implementation FeeDetailsClass



+ (id)sharedInstance {
    if (!feeDetails) {
        feeDetails  = [[self alloc] init];
    }
    
    return feeDetails;
}


@end
