//
//  providerDetailsTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Imma Web Pvt Ltd on 03/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderDetailsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *providerImageView;
@property (weak, nonatomic) IBOutlet UILabel *providerNameLabel;
@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *milesAwayLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountPerHour;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *perHourLabel;

@property (strong, nonatomic) NSString *providerEmailId;
@property NSInteger providerId;
@property NSArray *feeTypesArray;
@property BOOL isPriceSetByProvider;
@property double providerAmountValue;

-(void)showProviderDetails:(NSDictionary *)providerDetails and:(NSDictionary *)providerTypeDetails;

@end
