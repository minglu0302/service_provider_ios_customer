//
//  BookingHistorySectionHeaderCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingHistorySectionHeaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sectionTitle;

@end
