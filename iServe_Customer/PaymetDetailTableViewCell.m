//
//  PaymetDetailTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PaymetDetailTableViewCell.h"

@implementation PaymetDetailTableViewCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPaymentDetails{
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:@"paymentType"] isEqualToString:@"CARD"]){
        
//        self.cardTypeImage.image = [UIImage imageNamed:@"payment_cash_icon"];
        
        
        NSArray *arrDBResult = [Database getParticularCardDetail:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedCardId"]];
        
        if(arrDBResult.count != 0)
        {
            CardDetails *fav = arrDBResult[0];
            self.cardTypeImage.image = [UIHelper requestForCardTypeImageName:fav.cardtype];
            self.cardNumberLabel.text = [NSString stringWithFormat:@"**** **** ****%@",fav.last4];
            
        }
        else{
            
            self.cardTypeImage.image = [UIImage imageNamed:@"placeholder"];
        }
        
        
    }
    else{
     
        [[NSUserDefaults standardUserDefaults] setObject:@"CASH" forKey:@"paymentType"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        self.cardTypeImage.image = [UIImage imageNamed:@"payment_cash_icon"];
        self.cardNumberLabel.text = @"CASH";
    }
    
    [self.changePaymentButton.layer setBorderColor:APP_COLOR.CGColor];//[APP_COLOR CGColor]];
}

@end
