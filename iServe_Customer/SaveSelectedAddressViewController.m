//
//  SaveSelectedAddressViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SaveSelectedAddressViewController.h"
#import "AddressManageViewController.h"

@interface SaveSelectedAddressViewController ()<UITextFieldDelegate,WebServiceHandlerDelegate,UITextViewDelegate>
{
    NSInteger selectedTagButton;
    CGRect screenSize;
}

@end

@implementation SaveSelectedAddressViewController

#pragma mark - Initial Methods -
- (void)viewDidLoad {
    
    [super viewDidLoad];
    selectedTagButton = 3;
    self.otherButton.selected = YES;
    screenSize = [[UIScreen mainScreen]bounds];
    
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[AMSlideMenuMainViewController getInstanceForVC:self]  disableSlidePanGestureForLeftMenu];
    self.selectedAddressTextView.text = self.selectedAddressDetails[@"address"];
    [self setAddressTextFieldFrame];
}

-(void)setAddressTextFieldFrame
{
    self.selectedAddressTextViewHeightConstraint.constant = [self measureHeightTextField:self.selectedAddressTextView]+8;
}

#pragma mark - Custom Methods -

- (CGFloat)measureHeightTextField:(UITextView *)textView
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-20 , 9999);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:textView.font.fontName size:textView.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:textView.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    CGRect newFrame = textView.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}


#pragma mark - UIBUtton Action -

- (IBAction)tagAddressButtonAction:(id)sender
{
    UIButton *tagButton = (UIButton *)sender;
    for (UIView *button in tagButton.superview.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            [(UIButton *)button setSelected:NO];
        }
    }
    tagButton.selected = YES;
    selectedTagButton = tagButton.tag;
    
}

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAddressButtonAction:(id)sender
{
    if(self.selectedAddressTextView.text.length == 0)
    {
        [UIHelper showMessage:@"Address should not be empty" withTitle:@"Message" delegate:self];
    }
    else
    {
        [self.view endEditing:YES];
        [self sendRequestToSaveAddress];
    }
}

#pragma mark - KeyBoard Methods -

- (void)keyboardHiding:(__unused NSNotification *)inputViewNotification
{
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    self.scrollView.contentOffset = CGPointMake(0,0);
    
    [UIView animateWithDuration:0.2 animations:^{
        
        self.scrollView.scrollIndicatorInsets = ei;
        self.scrollView.contentInset = ei;
    }];
}

- (void)keyboardShowing:(__unused NSNotification *)inputViewNotification
{
    
    CGRect inputViewFrame = [[[inputViewNotification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect inputViewFrameInView = [self.view convertRect:inputViewFrame fromView:nil];
    CGRect intersection = CGRectIntersection(self.scrollView.frame, inputViewFrameInView);
    UIEdgeInsets ei = UIEdgeInsetsMake(0.0,0.0, intersection.size.height, 0.0);
    self.scrollView.scrollIndicatorInsets = ei;
    self.scrollView.contentInset = ei;
}

#pragma mark - UITextField Delegate methods -

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - UITextView Delegate -

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}




#pragma mark - Web Service Call -

-(void)sendRequestToSaveAddress
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Saving...", @"Saving...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSString *selectedTagAddress;
        switch (selectedTagButton) {
            case 1:
                selectedTagAddress = @"HOME";
                break;
            case 2:
                selectedTagAddress = @"OFFICE";
                break;
            case 3:
                selectedTagAddress = @"OTHER";
                break;

            default:
                selectedTagAddress = @"OTHER";
                break;
        }
        
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 @"ent_cust_id":[[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID],
                                 @"ent_latitude":self.selectedAddressDetails[@"lat"],
                                 @"ent_longitude":self.selectedAddressDetails[@"log"],
                                 @"ent_address1":flStrForObj(self.selectedAddressTextView.text),
                                 @"ent_suite_num":flStrForObj(self.flatNumberTextField.text),
                                 @"ent_tag_address":selectedTagAddress,
                               };
        
        [[WebServiceHandler sharedInstance]sendRequestToAddAddress:params andDelegate:self];
        
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}

#pragma mark - Web service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
        
        }
            break;
            
        case 0:
        {
            if(requestType == RequestTypeAddAddress)
            {
                NSString *selectedTagAddress;
                switch (selectedTagButton) {
                    case 1:
                        selectedTagAddress = @"HOME";
                        break;
                    case 2:
                        selectedTagAddress = @"OFFICE";
                        break;
                    case 3:
                        selectedTagAddress = @"OTHER";
                        break;
                        
                    default:
                        selectedTagAddress = @"OTHER";
                        break;
                }

                
                NSDictionary *dict = @{
                                       @"aid":response[@"addressid"],
                                       @"address1":self.selectedAddressTextView.text,
                                       @"suite_num":self.flatNumberTextField.text,
                                       @"tag_address":selectedTagAddress,
                                       @"lat":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"lat"]],
                                       @"long":[NSString stringWithFormat:@"%@",self.selectedAddressDetails[@"log"]],
                                     };
                [[Database sharedInstance]makeDataBaseEntryForManageAddress:dict];
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] saveContext];
                
                if(self.isFromProviderBookingVC)
                {
                    for (UIViewController* viewController in self.navigationController.viewControllers) {
                        
                        if ([viewController isKindOfClass:[AddressManageViewController class]] )
                        {
                            [self.navigationController popToViewController:viewController animated:YES];
                            return;
                        }
                    }
                }
                else
                {
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
                
            }
            
        }
            break;
            
        default:
            break;
    }
    
}


@end
