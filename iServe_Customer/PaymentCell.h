//
//  PaymentCell.h
//  privMD
//
//  Created by -Tony Lu on 10/03/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCell : UITableViewCell


@property(strong,nonatomic) IBOutlet UIImageView *cardImage;
@property(strong,nonatomic) IBOutlet UILabel *cardLast4Number;
@property(strong,nonatomic) IBOutlet UIImageView *cellBgImage;
@property (weak, nonatomic) IBOutlet UIView *addCardView;
@property (weak, nonatomic) IBOutlet UIView *cardDetailView;
@property (weak, nonatomic) IBOutlet UILabel *addCardLabelView;
@property (weak, nonatomic) IBOutlet UIImageView *addPaymentImage;


@end
