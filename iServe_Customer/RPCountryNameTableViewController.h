//
//  TableViewController.h
//  Sup
//
//  Created by Rahul Sharma on 3/5/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPCountryNameTableViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate,UISearchDisplayDelegate>


/**
 *  Country Names Array for storing the country name in the array
 */
@property (nonatomic,strong) NSArray *countryNames;
/**
 *  Search Results Array
 */
@property (nonatomic,strong) NSArray *searchResults;
/**
 *  Code String
 */
@property (strong,  nonatomic) NSString *code;
/**
 *   Flage Country Image View
 */
@property (weak, nonatomic) UIImage *flagCountry;
/**
 *  Country Name Array
 */
@property(strong, nonatomic) NSArray *countryName;
/**
 *  Country Code Array
 */
@property (strong, nonatomic) NSArray *countrycode;
/**
 *  Country Name Array
 */
@property (strong, nonatomic) NSMutableArray *countryNameFirstString;
/**
 *  Global Accesable Array
 */
@property (strong, nonatomic) NSMutableArray *globalyAccesableArray;
/**
 *  NSArray Object
 */
@property(strong , nonatomic) NSArray *details;
/**
 *  Callback Method
 */
@property (nonatomic,copy) void (^oncomplete)(NSString * code,UIImage *flagimg,NSString * countryCode);
/**
 *  Country Table View Outlet
 */
@property (strong, nonatomic) IBOutlet UITableView *countrytableView;
/**
 *  Search Controller Outlet
 */
@property (weak, nonatomic) IBOutlet UISearchBar *searchController;
/**
 *  Back Button Action
 *
 *  @param sender
 */
- (IBAction)backButton:(id)sender;

@end
