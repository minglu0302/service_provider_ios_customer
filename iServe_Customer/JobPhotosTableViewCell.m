//
//  JobPhotosTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "JobPhotosTableViewCell.h"
#import "JobPhotosCollectionViewCell.h"
#import "AddNewPhotoButtonCollectionViewCell.h"
#import "ProviderBookingViewController.h"
#import "JobPhotosPopUpView.h"

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

@interface JobPhotosTableViewCell ()
{
    UIWindow *window;
    JobPhotosPopUpView *jobPhotosPopUpView;
}

@end

@implementation JobPhotosTableViewCell

- (void)awakeFromNib
{
     [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)reloadCollectionView:(NSMutableArray *)imagesArray
{
    self.jobImagesArray = imagesArray;
    [self.collectionView reloadData];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(section == 0){
        
        return self.jobImagesArray.count;
    }
    return 1;
    
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier;
    
    if(indexPath.section == 0)
    {
        cellIdentifier = @"imagesCell";
        
        JobPhotosCollectionViewCell *jobPhotosCell = (JobPhotosCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        jobPhotosCell.jobPhotos.image = self.jobImagesArray[indexPath.row];
        jobPhotosCell.deletePhotoButton.tag = indexPath.row;
        
        return jobPhotosCell;
    }
    else{
        
        cellIdentifier = @"addImagesCell";
        
        AddNewPhotoButtonCollectionViewCell *addPhotosButtonCell = (AddNewPhotoButtonCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        
        return addPhotosButtonCell;
        
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        jobPhotosPopUpView = [JobPhotosPopUpView sharedInstance];
        jobPhotosPopUpView.isFromBookingScreen = YES;
        [jobPhotosPopUpView reloadCollectionView:self.jobImagesArray and:indexPath];
        window = [[UIApplication sharedApplication] keyWindow];
        [window addSubview:jobPhotosPopUpView];
        
        jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             
                             jobPhotosPopUpView.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                             
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }

}

@end
