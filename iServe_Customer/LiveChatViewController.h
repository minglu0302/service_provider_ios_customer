//
//  LiveChatViewController.h
//  Tease
//
//  Created by Rahulsharma on 24/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#define LC_URL        "https://secure.livechatinc.com"
#define LC_CHAT_URL   "https://secure.livechatinc.com/licence/9335030/open_chat.cgi?groups=Goclean+Service&webview_widget=1"
#define LC_LICENSE    "9335030"
#define LC_CHAT_GROUP APP_NAME

@interface LiveChatViewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *chatView;
//@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (assign, nonatomic) NSString *stringUrl;
@property (weak, nonatomic) IBOutlet UIButton *navigationLeftButton;

- (IBAction)navigationLeftButtonAction:(id)sender;

@end
