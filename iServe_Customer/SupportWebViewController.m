//
//  TWTSecWebViewController.m
//  iServePassenger
//
//  Created by -Tony Lu on 19/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SupportWebViewController.h"

@interface SupportWebViewController ()
{
    
}
@end

@implementation SupportWebViewController

#pragma mark - UILife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self.webview loadHTMLString:[NSString stringWithFormat:@"<html><body style=\"background-color:white; font-size:12; font-family:OpenSans_Regular; color: #989898\">%@</body></html>",self.weburlString] baseURL: nil];
    NSURL *url = [NSURL URLWithString:self.weburlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:requestObj];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
     [[AMSlideMenuMainViewController getInstanceForVC:self]  disableSlidePanGestureForLeftMenu];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.webview stopLoading];
    self.webview.delegate = nil;
}

#pragma mark - WebViewDelegates

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self performSelector:@selector(hideProgressIndicator)
               withObject:nil
               afterDelay:1];
   
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self performSelector:@selector(hideProgressIndicator)
               withObject:nil
               afterDelay:1];
}

-(void)hideProgressIndicator
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
}

#pragma mark - IBAction

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
