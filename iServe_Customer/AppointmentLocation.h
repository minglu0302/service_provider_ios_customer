//
//  AppointmentLocation.h
//  privMD
//
//  Created by Surender Rathore on 11/04/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppointmentLocation : NSObject

+ (id)sharedInstance;

@property(nonatomic,strong)NSNumber *currentLatitude;
@property(nonatomic,strong)NSNumber *currentLongitude;
@property(nonatomic,strong)NSNumber *pickupLatitude;
@property(nonatomic,strong)NSNumber *pickupLongitude;
@property(nonatomic,strong)NSNumber *dropOffLatitude;
@property(nonatomic,strong)NSNumber *dropOffLongitude;

@property(nonatomic,strong)NSString *pickUpAddress;

@property(nonatomic,assign) NSInteger bookingType;

@end
