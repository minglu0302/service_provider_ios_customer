//
//  TWTAddCardViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 12/04/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCardViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIButton *scanButton;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *navigationBackButton;
@property (weak, nonatomic) IBOutlet UIButton *skipButton;
@property (weak, nonatomic) IBOutlet UIView *paymentBackGroundView;
//@property (weak, nonatomic) IBOutlet UILabel *navigationTitleLabel;

@property (assign,nonatomic) int isComingFromPayment;
@property (assign,nonatomic)  NSMutableArray *arrayContainingCardInfo;


- (IBAction)doneButtonClicked:(id)sender;
- (IBAction)navigationBackButtonAction:(id)sender;
- (IBAction)skipbuttonAction:(id)sender;
- (IBAction)scancardButtonAction:(id)sender;

@end
