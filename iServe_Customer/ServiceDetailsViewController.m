//
//  ServiceDetailsViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ServiceDetailsViewController.h"
#import "ServiceDetailsTableViewCell.h"

@interface ServiceDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    BOOL isServiceSelected;
    NSString *currncySymbol;
    NSInteger selectedRowWhenSingleSelection;
}
@end

@implementation ServiceDetailsViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    currncySymbol = [[NSUserDefaults standardUserDefaults] objectForKey:iServeCurrentCountryCurrencySymbol];

    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    isServiceSelected = NO;
    self.addButton.hidden = YES;
    
    //set For Selecting Multiple Or Single Row
    if(self.isMultipleSelection.integerValue == 1)
    {
        [self.tableView setAllowsMultipleSelection:YES];
    }
    else
    {
        [self.tableView setAllowsSelection:YES];
    }
    selectedRowWhenSingleSelection = -1;
}
-(void)viewDidAppear:(BOOL)animated
{
    for(int i=0; i<self.arrayOfSelectedServices.count; i++)
    {
        //Showing Previously Selected Index Paths
        NSInteger selectedRow = [self.arrayOfSelectedServices[i][@"indexPath"]integerValue];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]
                                    animated:NO
                              scrollPosition:UITableViewScrollPositionNone];
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
    }
}

#pragma mark - UIButton Actions -

- (IBAction)addButtonAction:(id)sender
{
    self.arrayOfSelectedServices = [[NSMutableArray alloc]init];
    NSMutableDictionary *selectedServicebyAddingIndexPath = [[NSMutableDictionary alloc]init];
    NSIndexPath *selectedIndexPath;
    if(self.isMultipleSelection.integerValue == 1)
    {
        //Add Multiple Selected Services
        NSArray *arrayOfSelectedIndexPaths = [[self.tableView indexPathsForSelectedRows]sortedArrayUsingSelector:@selector(compare:)];
        
        for(int i=0; i<arrayOfSelectedIndexPaths.count; i++)
        {
            selectedIndexPath = arrayOfSelectedIndexPaths[i];
            selectedServicebyAddingIndexPath = self.arrayOfServices[selectedIndexPath.row];
            [selectedServicebyAddingIndexPath setObject:[NSNumber numberWithInteger:selectedIndexPath.row] forKey:@"indexPath"];
            [self.arrayOfSelectedServices addObject:selectedServicebyAddingIndexPath];
        }
    }
    else
    {
        //Add Single Selected Service
        selectedIndexPath = [self.tableView indexPathForSelectedRow];
        if(selectedIndexPath)
        {
            selectedServicebyAddingIndexPath = self.arrayOfServices[selectedIndexPath.row];
            [selectedServicebyAddingIndexPath setObject:[NSNumber numberWithInteger:selectedIndexPath.row] forKey:@"indexPath"];
            [self.arrayOfSelectedServices addObject:selectedServicebyAddingIndexPath];
        }
        
    }
    
    [self.arrayOfSelectedServicesForAllGroups removeObjectAtIndex:self.serviceGroupNumber];
    [self.arrayOfSelectedServicesForAllGroups insertObject:self.arrayOfSelectedServices atIndex:self.serviceGroupNumber];
    

    [self.selectedServiceDetailDelegate getSelectedServiceDetailsFromServiceDetailVC:self.arrayOfSelectedServicesForAllGroups];
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)navigationLeftButtonaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfServices.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"serviceDetailCell";
    
    ServiceDetailsTableViewCell *serviceDetailsCell = (ServiceDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (serviceDetailsCell == nil)
    {
        serviceDetailsCell = [[ServiceDetailsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
  
    serviceDetailsCell.serviceNameLabel.text = [NSString stringWithFormat:@"%@ - %@ %@",self.arrayOfServices[indexPath.row][@"sname"],currncySymbol,self.arrayOfServices[indexPath.row][@"fixed_price"]];
    
   
    return serviceDetailsCell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceDetailsTableViewCell *serviceDetailsCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(self.isMultipleSelection.integerValue == 1)
    {
        serviceDetailsCell.accessoryView = [[ UIImageView alloc ]
                                            initWithImage:[UIImage imageNamed:@"cancellation_reason_correct_icon"]];
    }
    else
    {
        serviceDetailsCell.accessoryView = [[ UIImageView alloc ]
                                            initWithImage:[UIImage imageNamed:@"radio_btn_on"]];
    }
    
    serviceDetailsCell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if([self.tableView indexPathForSelectedRow])
    {
        //If Table View is Multiple Selection
        if(self.isMultipleSelection.integerValue == 1)
        {
            [self.addButton setTitle:[NSString stringWithFormat:@"ADD(%td)",[self.tableView indexPathsForSelectedRows].count] forState:UIControlStateNormal];
            self.addButton.hidden = NO;
        }
        else
        {
            self.addButton.hidden = NO;
        }
        
    }
    else
    {
        self.addButton.hidden = YES;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ServiceDetailsTableViewCell *serviceDetailsCell = [tableView cellForRowAtIndexPath:indexPath];
    serviceDetailsCell.accessoryView = nil;
    serviceDetailsCell.accessoryType = UITableViewCellAccessoryNone;
    
    if([self.tableView indexPathForSelectedRow])
    {
        if(self.isMultipleSelection.integerValue == 1)
        {
            [self.addButton setTitle:[NSString stringWithFormat:@"ADD(%td)",[self.tableView indexPathsForSelectedRows].count] forState:UIControlStateNormal];
        }

        self.addButton.hidden = NO;
    }
    else
    {
        self.addButton.hidden = YES;
    }

}

@end
