//
//  BookingsViewController.h
//  UBER
//
//  Created by -Tony Lu on 05/02/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingsHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property BOOL isFromInvoice;
@property (weak, nonatomic) IBOutlet UIView *loadMoreBackGroundView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

+ (instancetype) getSharedInstance;
- (void)sendRequestToGetBookingDetails;
- (IBAction)loadMoreButtonAction:(id)sender;

@end
