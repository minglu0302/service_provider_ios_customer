//
//  ProviderBookingView.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PulsingHaloLayer.h"

@interface ProviderRequestingView : UIView <UIGestureRecognizerDelegate>
{
    NSInteger count;
    PulsingHaloLayer *layer;
}

+ (id)sharedInstance;

@property (weak, nonatomic) IBOutlet UIImageView *iServeLogoImage;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *bookingCancelButton;


- (IBAction)longPressAction:(id)sender;
-(void)setPulsingAnimation;

@end
