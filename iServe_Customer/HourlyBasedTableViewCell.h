//
//  HourlyBasedTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Rahul Sharma on 7/6/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HourlyBasedTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *hourlyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *hrsLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIButton *hoursCountButton;

@end
