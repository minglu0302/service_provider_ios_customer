//
//  PromoCodeTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PromoCodeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *promocodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *applyPromocodeButton;

@end
