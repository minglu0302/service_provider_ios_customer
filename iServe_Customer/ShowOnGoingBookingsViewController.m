//
//  AcceptedViewController.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ShowOnGoingBookingsViewController.h"
#import "MBCircularProgressBarLayer.h"
#import "CancelBookingPopUpView.h"
#import <MessageUI/MessageUI.h>
#import "SIOClient.h"
#import "ProviderDetailsViewController.h"
#import "InvoicePopUpView.h"
#import "AdressWrapperClass.h"
#import "ChatViewController.h"

@interface ShowOnGoingBookingsViewController ()<GMSMapViewDelegate,MFMessageComposeViewControllerDelegate,WebServiceHandlerDelegate,SocketWrapperDelegate,UIGestureRecognizerDelegate>
{
    CGRect screenSize;
    NSInteger providerId;
    double appointmentLat;
    double appointmentLong;
    CancelBookingPopUpView *cancelBookingPopUp;
    
    BOOL isProviderDetailsCurrentlyShowing;
    UIWindow *window;
    SocketIOWrapper *socketWrapper;
    NSTimer *publishToSocketTimer;
    double providerLat;
    double providerLong;
    GMSMarker *providerMarker;
    GMSMarker *appointmentLocationMarker;
    NSString *providerEmail;
    NSTimer *jobStartedTimer;
    
    NSInteger hours,minutes,seconds;
    float milesAway;
    
    UIBackgroundTaskIdentifier bgTask;
    UIApplication *app;
    
    MFMessageComposeViewController *messageVC;
    UILabel *minuteLabelOfProvider;
    NSMutableDictionary *providerTypeDetails;
    
    BOOL isLoading;
    InvoicePopUpView *invoicePopUp;
    
    NSString *reminderId;
    NSString *proImageURL;
}

@end

static ShowOnGoingBookingsViewController *showOnGoingBookingVC = nil;

@implementation ShowOnGoingBookingsViewController

#pragma mark - Initial Methods -

+ (instancetype) getSharedInstance
{
    return showOnGoingBookingVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    providerEmail = @"";
    proImageURL = @"";
    isLoading = NO;
    window = [[UIApplication sharedApplication] keyWindow];
    screenSize = [[UIScreen mainScreen]bounds];
    
    showOnGoingBookingVC = self;
    app = [UIApplication sharedApplication];
    self.viewProfileButton.layer.borderColor = APP_COLOR.CGColor;
    
    appointmentLat = [[[NSUserDefaults standardUserDefaults] objectForKey:iServeUserCurrentLat] doubleValue];
    appointmentLong = [[[NSUserDefaults standardUserDefaults] objectForKey:iServeUserCurrentLong] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appointmentLat longitude:appointmentLong zoom:mapZoomLevel];
    
    self.mapView.camera = camera;
    self.mapView.delegate = self;
    self.mapView.settings.myLocationButton = YES;
    
    isProviderDetailsCurrentlyShowing = YES;
    self.providerDetailsViewBottomConstraint.constant = 0;
    [self.providerDetailView layoutIfNeeded];
    [self.mapView layoutIfNeeded];

    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    socketWrapper = [SocketIOWrapper sharedInstance];
    socketWrapper.socketDelegate = self;
    
    [self changeViews];
    
    self.bookingIdLabel.text = [NSString stringWithFormat:LS(@"BOOKING ID: %zd"),self.bookingId];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if(self.status != 6 && self.status!= 15 &&self.status!= 16)
    {
        [self.activityIndicator startAnimating];
        [self sendRequestToGetBookingDetails:0];
    }

}

-(void)viewWillDisappear:(BOOL)animated
{
    [publishToSocketTimer invalidate];
    publishToSocketTimer = nil;
    
    [jobStartedTimer invalidate];
    jobStartedTimer = nil;
    
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appD setSocketDelegateToAppDelegate];
//    [self stopBackgroundTask];
}

#pragma mark - UIButton Actions -

- (IBAction)viewProfileButtonAction:(id)sender {
    
    ProviderDetailsViewController *proDetailsVC = (ProviderDetailsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"providerDetailsVC"];
    proDetailsVC.isFromBookingFlowScreen = YES;
    proDetailsVC.providerId = providerId;
    
    NSDictionary *proDetails = @{
                                 @"miles":[NSString stringWithFormat:@"%.2f Kms away",milesAway/iServeDistanceMetric],
                                 @"rating":[NSNumber numberWithFloat:self.ratingView.value],
                                 @"image":self.providerImageView.image,
                                };
    
    proDetailsVC.providerDetailsFromPreviousController = proDetails;
    proDetailsVC.providerTypeDetails = providerTypeDetails;
    
    providerMarker.map = self.mapView;
    providerMarker.map = nil;
    providerMarker = nil;
    
    [AnimationsWrapperClass UIViewAnimationAnimationCurve:UIViewAnimationCurveEaseInOut
                                               transition:UIViewAnimationTransitionCurlUp
                                                  forView:self.navigationController.view
                                             timeDuration:0.75];

    [self.navigationController pushViewController:proDetailsVC animated:NO];
    
}

- (IBAction)callButtonAction:(id)sender {
    
    NSString *cleanedString = [[self.providerPhoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:telURL])
    {
        [[UIApplication sharedApplication] openURL:telURL];
    }
    else
    {
        [UIHelper showMessage:LS(@"This function is only available on an iPhone.") withTitle:LS(@"Alert!") delegate:self];
    }
    
}

- (IBAction)messageButtonAction:(id)sender {
    
    /*if([MFMessageComposeViewController canSendText]){
        
        messageVC = [[MFMessageComposeViewController alloc] init];
        messageVC.messageComposeDelegate = self;
        
        NSArray *recipents = @[self.providerPhoneNumber];
        
        [messageVC setRecipients:recipents];
        
        [self presentViewController:messageVC animated:YES completion:NULL];
    }
    else{
        
        [UIHelper showMessage:LS(@"Your device doesn't support SMS!") withTitle:LS(@"Alert!") delegate:self];
    }*/
    
    ChatViewController *chatVC = (ChatViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    
    chatVC.title = [NSString stringWithFormat:LS(@"BOOKING ID: %td"),self.bookingId];
    
//    NSDictionary *proDetails = @{
//                                 @"imageURL":proImageURL,
//                                 @"name":self.providerNameLabel.text,
//                                 @"pid":[NSNumber numberWithInteger:providerId],
//                                 @"bid":[NSNumber numberWithInteger:self.bookingId]
//                                };
    
    chatVC.bookingId = [NSString stringWithFormat:@"%td",self.bookingId];
    chatVC.provioderImageURL = proImageURL;
    chatVC.providerName = self.providerNameLabel.text;

    
    providerMarker.map = self.mapView;
    providerMarker.map = nil;
    providerMarker = nil;
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:chatVC animated:NO];
    

    
}

- (IBAction)navigationBackButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelBookingButtonAction:(id)sender
{
    cancelBookingPopUp = [CancelBookingPopUpView sharedInstance];
    cancelBookingPopUp.bookingId = self.bookingId;
    [cancelBookingPopUp setTopViewFrame];
    
    window = [[[UIApplication sharedApplication]delegate]window];
    
    [window addSubview:cancelBookingPopUp];
    
    cancelBookingPopUp.backgroundView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
    [UIView animateWithDuration:0.5
                          delay:0.2
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         cancelBookingPopUp.backgroundView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         
                     }
                     completion:^(BOOL finished){
                         [cancelBookingPopUp sendRequestToGetCancelReasons];
                     }];

}

/**
 *  Gesture For Hiding Or Showing Provider Details View
 *
 */

- (IBAction)swipeGestureAction:(id)sender
{
    UISwipeGestureRecognizer *swipeGesture = (UISwipeGestureRecognizer *)sender;
    if(self.status == 5)
    {
        if(swipeGesture.direction == UISwipeGestureRecognizerDirectionUp)
        {
            if(isProviderDetailsCurrentlyShowing == NO)
            {
                [self hideOrShowProviderDetailsButtonAction];
            }
        }
        else if (swipeGesture.direction == UISwipeGestureRecognizerDirectionDown)
        {
            if(isProviderDetailsCurrentlyShowing == YES)
            {
                [self hideOrShowProviderDetailsButtonAction];
            }
            
        }
        
    }
    
}



#pragma mark - Message Share Delegate -

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [UIHelper showMessage:LS(@"Failed to send!") withTitle:LS(@"Message")delegate:self];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - Custom Methods -


/**
 *  Publish SocketStream to receiving pro near around Customer current location
 */
-(void)publishToSocketForLiveTrack
{
    NSDictionary *message = @{
                              @"btype":[NSNumber numberWithInt:3],
                              @"email":[[NSUserDefaults standardUserDefaults] objectForKey:iServeUserEmail],
                              @"lat": [NSNumber numberWithDouble:appointmentLat],
                              @"long": [NSNumber numberWithDouble:appointmentLong],
                              @"pid":[NSNumber numberWithInteger:providerId],
                            };
    
    [socketWrapper publishMessageToChannel:@"UpdateCustomer" withMessage:message];
}


/**
 *  Show The Views Depending On Booking Status
 */
-(void)changeViews
{
    [self setStatusViewProperties];
    switch (self.status)
    {
        case 2://Accepted
        case 21://Arrived
        {
            if(self.status == 2)
            {
                self.cancelButton.hidden = NO;
            }
            else
            {
                self.cancelButton.hidden = YES;
            }
            
            [self showBookingStatusViews];
            [jobStartedTimer invalidate];
            jobStartedTimer = nil;
            
        }
            break;
            
            
        case 15://Start Job Timer
        case 16://Pause Job Timer
        case 6://Job Started
        {
            self.cancelButton.hidden = YES;
            [self showBookingStatusViews];
            [self sendRequestToGetBookingDetails:0];
        }
            break;
            
        case 5://On The Way
        {
            //Timer To publish To Socket
            publishToSocketTimer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(publishToSocketForLiveTrack) userInfo:nil repeats:YES];
            
            
            self.cancelButton.hidden = NO;
            self.mapView.hidden = NO;
            self.statusView.hidden = YES;
            self.providerDetailView.backgroundColor = [UIColor clearColor];
            
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appointmentLat longitude:appointmentLong zoom:mapZoomLevel];
            
            self.mapView.camera = camera;
            
            //Setting properties OF Current Location Button
            for (UIView *object in self.mapView.subviews) {
                
                if([[[object class] description] isEqualToString:@"GMSUISettingsPaddingView"])
                {
                    for(UIView *ob in object.subviews) {
                        
                        if([[[ob class] description] isEqualToString:@"GMSUISettingsView"] )
                        {
                            for(UIButton *button in ob.subviews) {
                                
                                if([[[button class] description] isEqualToString:@"GMSx_QTMButton"] )
                                {
                                    
                                    [button addTarget:self action:@selector(markersToFitInMapView) forControlEvents:UIControlEventTouchUpInside];
                                }
                            }
                            
                        }
                    }
                }
            }
            
            [jobStartedTimer invalidate];
            jobStartedTimer = nil;
            
            
        }
            break;
            
            
        case 22://Invoice generated
        {
            self.cancelButton.hidden = YES;
            [self showBookingStatusViews];
            [jobStartedTimer invalidate];
            jobStartedTimer = nil;
            
        }
            break;
            
        default:
            break;
    }
    
}

/**
 *  Update Views Depending On Booking status
 */
-(void)showBookingStatusViews
{
    self.mapView.hidden = YES;
    self.statusView.hidden = NO;
    
    self.providerDetailView.backgroundColor = [UIColor whiteColor];
    [publishToSocketTimer invalidate];
    publishToSocketTimer = nil;
    
}


-(void)setBookingStatusViewsProperties
{
    self.step2Label.layer.borderWidth = 0;
    self.step2Label.backgroundColor = APP_COLOR;
    self.step2Label.textColor = UIColorFromRGB(0xffffff);
    
    self.step3Label.layer.borderWidth = 0;
    self.step3Label.backgroundColor = APP_COLOR;
    self.step3Label.textColor = UIColorFromRGB(0xffffff);
    
    self.step4Label.layer.borderWidth = 0;
    self.step4Label.backgroundColor = APP_COLOR;
    self.step4Label.textColor = UIColorFromRGB(0xffffff);
    
}

/**
 *  set Views Properties
 */
-(void)setStatusViewProperties
{
    switch (self.status)
    {
        case 2://Accepted
        {
            self.timerLabel.text = LS(@"Provider");
            self.statusLabel.text = LS(@"JOB ACCEPTED");
            
            self.circularView.value = 0;
            
            self.step2Label.layer.borderWidth = 2;
            self.step2Label.backgroundColor = UIColorFromRGB(0xffffff);
            self.step2Label.textColor = UIColorFromRGB(0xcccccc);
            self.step2Label.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
            
            self.step3Label.layer.borderWidth = 2;
            self.step3Label.backgroundColor = UIColorFromRGB(0xffffff);
            self.step3Label.textColor = UIColorFromRGB(0xcccccc);
            self.step3Label.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
            
            self.step4Label.layer.borderWidth = 2;
            self.step4Label.backgroundColor = UIColorFromRGB(0xffffff);
            self.step4Label.textColor = UIColorFromRGB(0xcccccc);
            self.step4Label.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
            
        }
            break;
            
        case 21://Arrived
        {
            self.timerLabel.text = LS(@"Provider");
            self.statusLabel.text = LS(@"PROVIDER ARRIVED");
            
            self.circularView.value = 25;
            
            self.step2Label.layer.borderWidth = 0;
            self.step2Label.backgroundColor = APP_COLOR;
            self.step2Label.textColor = UIColorFromRGB(0xffffff);
            
            self.step3Label.layer.borderWidth = 0;
            self.step3Label.backgroundColor = APP_COLOR;
            self.step3Label.textColor = UIColorFromRGB(0xffffff);
            
            self.step4Label.layer.borderWidth = 2;
            self.step4Label.backgroundColor = UIColorFromRGB(0xffffff);
            self.step4Label.textColor = UIColorFromRGB(0xcccccc);
            self.step4Label.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
        }
            break;
            
            
        case 6://Job Started
        case 15://Start or Pause Timer
        {
            self.circularView.value = 37.5;
//            self.timerLabel.text = @"00:00:00";
            self.statusLabel.text = LS(@"JOB STARTED");
            [self setBookingStatusViewsProperties];
        }
            break;
        case 16:
        {
            self.circularView.value = 37.5;
            //            self.timerLabel.text = @"00:00:00";
            self.statusLabel.text = LS(@"JOB PAUSED");
            [self setBookingStatusViewsProperties];

        }
            break;
        case 22://Invoice Generated
        {
            [self setBookingStatusViewsProperties];
            self.circularView.value = 50;
            self.timerLabel.text = LS(@"Provider");
            self.statusLabel.text = LS(@"Appointment completed, invoice being prepared");
        }
            break;
            
        case 7://Booking Completed
        {
            [self setBookingStatusViewsProperties];
            self.circularView.value = 50;
            self.timerLabel.text = LS(@"Provider");
            self.statusLabel.text = LS(@"JOB COMPLETED");
        }
            break;
            
        default:
            break;
    }
    
    
}

/**
 *  Show Job Timer (start Or Pause)
 *
 */
-(void)setUpTimer:(NSDictionary *)response
{
    switch ([response[@"timer_status"]integerValue])
    {
            //1  start  2  pause  3  stop
        case 0:
        case 1:
        {
            //When Provider Job Started And timer Running
            
            NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            NSString *time = response[@"timer_start_time"];
            NSDate *jobStartedTime = [dateFormatter dateFromString:time];
            
            self.statusLabel.text = LS(@"JOB STARTED");
            
            if([response[@"timer"]integerValue] > 0)
            {
                //When Provider Stoping the Job And Started Again
                
                double timeValue = [response[@"timer"]doubleValue];
                
//                time = response[@"timer_start_time"];
                jobStartedTime = [dateFormatter dateFromString:time];
                NSTimeInterval timeDifference = [[NSDate date] timeIntervalSinceDate:jobStartedTime];
                
                timeValue = timeValue +timeDifference;
                
                hours = timeValue/3600;
                minutes = fmod(timeValue, 3600)/60;
                seconds = timeValue - ((hours*3600)+(minutes*60));
                
            }
            else
            {
                //When Provider Job Started From begining
                
                NSTimeInterval timeDifference = [[NSDate date] timeIntervalSinceDate:jobStartedTime];
                hours = timeDifference/3600;
                
                minutes = fmod(timeDifference, 3600)/60;
                seconds = timeDifference - ((hours*3600)+(minutes*60));
                
                NSLog(@"%f",timeDifference);
            }
            
            [self showJobTimerValues];
            
            
//            [self startBackgroundTask];
            jobStartedTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(showJobStartedTimer) userInfo:nil repeats:YES];
            
        }
            break;
        case 2:
        {
            //When Provider Pause the Job
            self.statusLabel.text = LS(@"JOB PAUSED");
            double timeValue = [response[@"timer"]doubleValue];
            
            hours = timeValue/3600;
            minutes = fmod(timeValue, 3600)/60;
            seconds = timeValue - ((hours*3600)+(minutes*60));
            
            [self showJobTimerValues];
            
        }
            break;
        case 3:
        {
            
        }
            break;
        default:
            break;
    }
    
    
}

-(void)showJobStartedTimer
{
    seconds++;
    if(seconds == 60)
    {
        minutes++;
        seconds = 0;
    }
    if(minutes == 60)
    {
        hours++;
        minutes = 0;
    }
    [self showJobTimerValues];
    
}

/**
 *  show Job Timer Values On Screen Every Second
 */
-(void)showJobTimerValues
{
    NSString *hoursValue,*minutesValue,*secondsValue;
    
    if(seconds < 10)
    {
        secondsValue = [NSString stringWithFormat:@"0%zd",seconds];
    }
    else
    {
        secondsValue = [NSString stringWithFormat:@"%zd",seconds];
    }
    
    if(minutes < 10)
    {
        minutesValue = [NSString stringWithFormat:@"0%zd",minutes];
    }
    else
    {
        minutesValue = [NSString stringWithFormat:@"%zd",minutes];
    }
    
    if(hours < 10)
    {
        hoursValue = [NSString stringWithFormat:@"0%zd",hours];
    }
    else
    {
        hoursValue = [NSString stringWithFormat:@"%zd",hours];
    }
    
    self.timerLabel.text = [NSString stringWithFormat:@"%@:%@:%@",hoursValue,minutesValue,secondsValue];
    NSLog(@"Job Timer = %@",self.timerLabel.text);
    
}


/**
 *  Show Appointment Location Marker On Map
 */
-(void)showAppointmentLocationMarker
{
    if(!appointmentLocationMarker)
    {
        appointmentLocationMarker = [[GMSMarker alloc]init];
        appointmentLocationMarker.flat = YES;
        appointmentLocationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        appointmentLocationMarker.icon = [UIImage imageNamed:@"home_mappin"];
        appointmentLocationMarker.map = self.mapView;
    }
    appointmentLocationMarker.position = CLLocationCoordinate2DMake(appointmentLat, appointmentLong);
    
}

/**
 *  Show Provider Marker on Map
 *
 *  @param providerDetails providersLat and Long details
 */
-(void)showProviderLiveTracking:(NSDictionary *)providerDetails
{
    providerLat = [providerDetails[@"lt"] doubleValue];
    providerLong = [providerDetails[@"lg"] doubleValue];
    milesAway = [providerDetails[@"d"]floatValue];
    
    if(!providerMarker)
    {
        providerMarker = [[GMSMarker alloc]init];
        providerMarker.flat = YES;
        providerMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        providerMarker.iconView = [self downloadProviderImage:providerDetails[@"i"]];
        providerMarker.map = self.mapView;
        providerMarker.position = CLLocationCoordinate2DMake(providerLat, providerLong);
        [self markersToFitInMapView];
        [self.mapView setSelectedMarker:providerMarker];
        minuteLabelOfProvider.text = [NSString stringWithFormat:LS(@"1\nMIN")];
        
        if(isLoading)
        {
            isLoading = NO;
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
        }
    }
    else
    {
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        providerMarker.position = CLLocationCoordinate2DMake(providerLat, providerLong);
        [CATransaction commit];
    }
    
    [self performSelector:@selector(showTimeBetweenTwoLocations:)
               withObject:providerDetails
               afterDelay:1];
    
    
}

-(void)showTimeBetweenTwoLocations:(NSDictionary *)dict
{
    [self showTimeBetweenCurrentLocationToProviderLocation:[dict[@"lt"] doubleValue]
                                                       and:[dict[@"lg"] doubleValue]
                                              onCompletion:^(NSArray * results)
     {
         if(results.count>0)
         {
             NSMutableArray *arrLeg=[[results objectAtIndex:0]objectForKey:@"legs"];
             NSMutableDictionary *dictleg=[arrLeg objectAtIndex:0];
             NSInteger timeInSec =  [[[dictleg   objectForKey:@"duration"] objectForKey:@"value"] integerValue];
             
             NSInteger timeInMin = [self convertTimeInMin:timeInSec];
             NSLog(@"Time:- %td",timeInMin);
             
             minuteLabelOfProvider.text = [NSString stringWithFormat:LS(@"%td\nMIN"),timeInMin];
             
         }
         else
         {
             
         }
         
     }];
    
}
-(void)showTimeBetweenCurrentLocationToProviderLocation:(double)lat
                                                    and:(double)log
                                           onCompletion:(void (^)(NSArray *))complete
{
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&key=%@",appointmentLat,appointmentLong,lat,log,iServeGoogleServerMapsAPIKey];
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    
    if(jsonData != nil)
    {
        NSError *error = nil;
        
        id result = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        NSMutableArray *arrDistance=[result objectForKey:@"routes"];
        
        complete(arrDistance);
        
    }
    else
    {
        NSLog(@"N.A.");
    }
}

-(NSInteger)convertTimeInMin:(NSInteger)timeFromServer
{
    NSInteger min;
    if (timeFromServer < 60 && timeFromServer <= 0)
    {
        min = 1;
    }
    else if (timeFromServer > 60)
    {
        min = timeFromServer/60;
        
        NSInteger remainingTime = timeFromServer%60;
        
        if (remainingTime < 60 && remainingTime >= 30)
        {
            min++;
        }
    }
    else
    {
        min = 1;
    }
    return min;
}


/**
 *  Download Provider Image
 */
-(UIView *)downloadProviderImage:(NSString *)providerImageUrl
{
    UIView *markerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 44, 60)];
    markerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_map_pin_profile_icon"]];
    
    
//    UIImageView *markerImageViewInner = [[UIImageView alloc] initWithFrame:CGRectMake(4.2,4,36,36)];
//    markerImageViewInner.layer.cornerRadius = markerImageViewInner.frame.size.height/2;
//    markerImageViewInner.clipsToBounds = YES;
    
    minuteLabelOfProvider = [[UILabel alloc]initWithFrame:CGRectMake(4.2,4,36,36)];
    minuteLabelOfProvider.layer.cornerRadius = minuteLabelOfProvider.frame.size.height/2;
    minuteLabelOfProvider.clipsToBounds = YES;
    minuteLabelOfProvider.backgroundColor = UIColorFromRGB(0xebebeb);

    minuteLabelOfProvider.font = [UIFont fontWithName:OpenSans_Regular size:10];
    minuteLabelOfProvider.textColor = [UIColor blackColor];
    minuteLabelOfProvider.numberOfLines = 0;
    minuteLabelOfProvider.adjustsFontSizeToFitWidth = YES;
    minuteLabelOfProvider.textAlignment = NSTextAlignmentCenter;
    
    [markerView addSubview:minuteLabelOfProvider];
    
    
//    [markerImageViewInner sd_setImageWithURL:[NSURL URLWithString:[UIHelper removeWhiteSpaceFromURL:providerImageUrl]]
//                            placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
//                                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
//                                       
//                                   }];
    
    
    
    return markerView;
    
}

/**
 *  Show Markers to Fit In MapView
 */
-(void)markersToFitInMapView
{
    if(providerMarker)
    {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
        bounds = [bounds includingCoordinate:appointmentLocationMarker.position];
        bounds = [bounds includingCoordinate:providerMarker.position];
    
        [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:55]];
    }
    
}

/**
 *  show Provider Detals Screen On Bottom Of View
 */
-(void)showProviderDetailsView
{
    isProviderDetailsCurrentlyShowing = NO;
    [self hideOrShowProviderDetailsButtonAction];
}

/**
 *  showing Or Hiding Provider Details View Animation
 */
- (void)hideOrShowProviderDetailsButtonAction
{
    if(isProviderDetailsCurrentlyShowing == NO)
    {
        isProviderDetailsCurrentlyShowing = YES;
        self.providerDetailsViewBottomConstraint.constant = 0;
        [UIView animateWithDuration:0.5f animations:^{
            
//            [self.providerDetailView layoutIfNeeded];
//            [self.mapView layoutIfNeeded];
            [self.view layoutIfNeeded];
        }];
        
    }
    else
    {
        isProviderDetailsCurrentlyShowing = NO;
        self.providerDetailsViewBottomConstraint.constant = -70 ;
        [UIView animateWithDuration:0.5f animations:^{
            
//            [self.providerDetailView layoutIfNeeded];
//            [self.mapView layoutIfNeeded];
             [self.view layoutIfNeeded];
            
        }];
        
    }
}

//-(void)startBackgroundTask
//{
//    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
//        
//        [app endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//        
//    }];
//}
//-(void)stopBackgroundTask
//{
//    [app endBackgroundTask:bgTask];
//}

-(void)showBookingCancelAndCompletedStatus
{
    switch (self.status) {
        case 4:
        case 8:
        case 9:
        case 10:
        {
            [self navigationBackButtonAction:nil];
        }
            break;
        case 7:
        {
            window = [[[UIApplication sharedApplication]delegate]window];
            
            invoicePopUp = [InvoicePopUpView sharedInstance];
            invoicePopUp.bookingStatus = self.status;//completed Booking
            [invoicePopUp setTopViewFrame];

            [window addSubview:invoicePopUp];
            
            invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
            [UIView animateWithDuration:0.5
                                  delay:0.2
                                options: UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 
                                 invoicePopUp.topView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                                 
                             }
                             completion:^(BOOL finished){
                                 invoicePopUp.bookingId = self.bookingId;
                                 [invoicePopUp sendRequestTogetBookingDetails];
                             }];

        }
            break;
        default:
             [self changeViews];
            break;
    }
}

- (void)removeReminderFromCalendar
{
    if(reminderId && reminderId.length > 0)
    {
        [[AdressWrapperClass sharedInstance]removeReminder:reminderId];
    }

}

#pragma mark - Socket Response -

-(void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message
{
//    NSLog(@"OnGoingBookingVC message: %@",message);
    
    if([channelName isEqualToString:@"CustomerStatus"])
    {//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22 ||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10){//Booking Response
            
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"UpdateCustomer"])
    {
        if ([[message objectForKey:@"flag"] intValue] == 0) //will get Provider Types From server
        {
            NSLog(@"Provider Details:%@",message[@"msg"][@"track"]);
            
            if([message[@"msg"][@"track"]count] > 0)
            {
                if(providerLat != [message[@"msg"][@"track"][@"lt"] doubleValue])
                {
                    dispatch_async(dispatch_get_main_queue(),^{
                        
                        [self showProviderLiveTracking:message[@"msg"][@"track"]];
                    });
                }
            }
            
        }
        
    }
    else if([channelName isEqualToString:@"Message"])//Chat Message Response
    {
        if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
    }

    
}

#pragma mark - Web Service Call -

-(void)sendRequestToGetBookingDetails:(NSInteger)tag
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_inv":[NSString stringWithFormat:@"%td",tag],
                                };
        
        
        [[WebServiceHandler sharedInstance] sendRequestToGetParticularApptStatus:params andDelegate:self];
       
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
        [self.activityIndicator stopAnimating];
    }
    
}

- (void)sendRequestToCancelBooking:(NSString *)message
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Booking Cancel...", @"Booking Cancel...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_sess_token":flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken]),
                                 @"ent_dev_id":flStrForStr([Utilities getDeviceId]),
                                 
                                 @"ent_bid":[NSNumber numberWithInteger:self.bookingId],
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"canc_mess":flStrForStr(message),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToCancelBooking:params andDelegate:self];
        
    }
    else{
        
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
    
}

#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    if (error)
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag)
    {
        case 1:
        {
            [[ProgressIndicator sharedInstance]hideProgressIndicator];
            
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [[Logout sharedInstance]deleteUserSavedData:response[@"errMsg"]];
            }
            else
            {
                 [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            }
            
        }
            break;
        case 0:
        {
            if(requestType == RequestTypeCancelBooking)
            {
                [[ProgressIndicator sharedInstance]hideProgressIndicator];
                [cancelBookingPopUp removeFromSuperview];
            }
            
            else if(requestType == RequestTypeGetParticularApptDetails){
                
//                BOOL statusChanged = NO;
                if(self.isNetworkComeBackRefresh)//Refresh Booking Screen When Network come Back
                {
                    self.isNetworkComeBackRefresh = NO;
                    if(self.status != [response[@"status"] integerValue])
                    {
                        self.status = [response[@"status"] integerValue];
//                        statusChanged = YES;
                        [self showBookingCancelAndCompletedStatus];
                        
                    }
                    
                }
                
                providerTypeDetails = [response[@"cat_data"] mutableCopy];
                self.providerNameLabel.text = [NSString stringWithFormat:@"%@ %@",response[@"fName"],response[@"lName"]];
                self.providerPhoneNumber = response[@"mobile"];
                providerEmail = response[@"email"];
                
                self.ratingView.highlightColor = UIColorFromRGB(0xffd200);
                self.ratingView.baseColor = UIColorFromRGB(0xcccccc);
                self.ratingView.value = [response[@"rating"]floatValue];
                
                providerId = [response[@"pid"]integerValue];
                
                if(self.status == 5)
                {
                    if(!providerMarker)
                    {
                        providerMarker.map = self.mapView;
                        providerMarker.map = nil;
                        providerMarker = nil;
                        providerLat = providerLong = 0;
                        isLoading = YES;
                    }
                }
                else
                {
                    [[ProgressIndicator sharedInstance]hideProgressIndicator];
                }
                if(self.status == 6 || self.status == 15 || self.status == 16)
                {
                    [jobStartedTimer invalidate];
                    jobStartedTimer = nil;
                    
                    [self setUpTimer:response];
                }
                
                appointmentLat = [response[@"apptLat"] doubleValue];
                appointmentLong = [response[@"apptLong"] doubleValue];
                
                reminderId = response[@"cust_reminder"];
                
                [self showAppointmentLocationMarker];
                
                
                if(response[@"pPic"])
                {
                    proImageURL = response[@"pPic"];
                }
                else
                {
                    proImageURL = @"";
                }
                
                [self.providerImageView sd_setImageWithURL:[NSURL URLWithString:proImageURL]
                                          placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                     
                                                     [self.activityIndicator stopAnimating];
                                                     
                                                 }];
                
            }
            
            
        }
            break;
        default:
            break;
    }
    
    
}

@end
