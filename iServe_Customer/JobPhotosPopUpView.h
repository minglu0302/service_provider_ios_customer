//
//  JobPhotosPopUpView.h
//  OnTheWay_Customer
//
//  Created by Apple on 05/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPhotosPopUpView : UIView

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
//@property (weak, nonatomic) IBOutlet UIImageView *jobPhoto;
@property (weak, nonatomic) IBOutlet UIButton *jobPhoto;

@property (weak, nonatomic) IBOutlet UILabel *jobPhotoNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *jobPhotosLabel;

@property (strong, nonatomic) NSArray *arrayOfJobPhotos;
@property NSIndexPath *selectedIndexPath;
@property NSInteger providerId;
@property BOOL isFromBookingScreen;

+ (id)sharedInstance;
-(void)reloadCollectionView:(NSMutableArray *)jobPhotos and:(NSIndexPath *)selectedJobPhotoIndexPath;
- (IBAction)closeButtonAction:(id)sender;

@end
