//
//  JobPhotosTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPhotosTableViewCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong,nonatomic)NSMutableArray *jobImagesArray;

-(void)reloadCollectionView:(NSMutableArray *)imagesArray;

@end
