//
//  ProviderBookingView.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/6/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ProviderRequestingView.h"
#import "ProviderBookingViewController.h"


#define kMaxRadius 200
#define kMaxDuration 10

static ProviderRequestingView *requestingView = nil;


@implementation ProviderRequestingView


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self =  [[[NSBundle mainBundle] loadNibNamed:@"ProviderRequestingView" owner:self options:nil]objectAtIndex:0];
        
    }
    return self;
}


+ (id)sharedInstance
{
    if (!requestingView) {
        
        requestingView  = [[self alloc] initWithFrame:CGRectZero];
        requestingView.frame = [[UIScreen mainScreen]bounds];
    }
    
    [requestingView setPulsingAnimation];
    
    return requestingView;
}

-(void)setPulsingAnimation{
    
    layer = [PulsingHaloLayer layer];
    layer.position = CGPointMake(self.center.x, self.center.y-25);
    
    [self.topView.layer insertSublayer:layer below:self.iServeLogoImage.layer];
    
    layer.haloLayerNumber = 5;
    layer.radius = 0.7 * kMaxRadius;
    layer.animationDuration = 0.5 * kMaxDuration;
       
    [layer setBackgroundColor:UIColorFromRGB(0x2598ed).CGColor];

     
    [layer start];

}

- (IBAction)longPressAction:(id)sender
{
    
    UILongPressGestureRecognizer *gesture = (UILongPressGestureRecognizer *)sender;
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        NSLog(@"Long Pressing End");
        [layer setBackgroundColor:UIColorFromRGB(0xFF0000).CGColor];
        requestingView = nil;
        [[ProviderBookingViewController getSharedInstance] cancelBookingButtonAction:nil];
        
    }
    
}
@end
