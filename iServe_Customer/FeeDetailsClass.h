//
//  FeeDetailsClass.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/18/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeeDetailsClass : NSObject

+ (id)sharedInstance;

@property(nonatomic,strong)NSNumber *feeType;
@property(nonatomic,strong)NSString *discountType;
@property(nonatomic,strong)NSNumber *discount;
@property(nonatomic,strong)NSNumber *visitFee;
@property(nonatomic,strong)NSNumber *hourlyFee;
@property(nonatomic,strong)NSNumber *discountFee;
@property(nonatomic,strong)NSNumber *numberOfHourValue;
@property(nonatomic,strong)NSNumber *totalFee;
@property(nonatomic,strong)NSNumber *servicesAmount;
@property(nonatomic,strong)NSNumber *slotAmount;

@end
