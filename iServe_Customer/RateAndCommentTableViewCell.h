//
//  RateAndCommentTableViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 23/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RateAndCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AXRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *commentbackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *commentlabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBackgroundViewHeightConstraint;

@end
