//
//  AppDelegate.m
//  Twotray
//
//  Created by -Tony Lu on 12/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AppDelegate.h"
#import "NetworkStatusShowingView.h"
#import <GoogleMaps/GoogleMaps.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AMSlideMenuLeftTableViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <UserNotifications/UserNotifications.h>
#import "CheckAppVersion.h"
#import <Firebase/Firebase.h>
#import <GooglePlaces/GooglePlaces.h>
#import "CheckPushEnabledOrNot.h"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@interface AppDelegate ()<UNUserNotificationCenterDelegate,FIRMessagingDelegate>
{
    UIBackgroundTaskIdentifier bgTask;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    NSLog(@"currentDeviceId: %@", currentDeviceId);
    
    // Override point for customization after application launch.
    
    //Check App Updated In App Srore
    [[CheckAppVersion sharedInstance] checkAppHasUpdatedVersion];

//    [[CheckPushEnabledOrNot sharedInstance] checkNotificationEnabledOrNot];
   
    
    bgTask = 0;
    
    //Connecting Socket
    [[SocketIOWrapper sharedInstance] connectSocket];
    [self setSocketDelegateToAppDelegate];
    
    [AmazonTransfer setConfigurationWithRegion:AWSRegionUSWest1
                                     accessKey:AmazonAccessKey secretKey:AmazonSecretKey];
    
//    STPPaymentCardTextField *paymentTextField = [[STPPaymentCardTextField alloc]init];
//    NSLog(@"%@",paymentTextField);
    
    [GMSServices provideAPIKey:iServeGoogleMapKey];
    [GMSPlacesClient provideAPIKey:iServeGoogleMapKey];
    
    _networkStatus = AFNetworkReachabilityStatusUnknown;
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 _networkStatus = status;
                 [[PMDReachabilityWrapper sharedInstance]setNetworkStatus:status];
                 [NetworkStatusShowingView removeViewShowingNetworkStatus];
                 
             }
                 break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
             {
                 _networkStatus = status;
                 [NetworkStatusShowingView sharedInstance];
             }
                 break;
         }
         
     }];
    
    [self setupAppearance];
    [self handlePushNotificationWithLaunchOption:launchOptions];
    
    
    NSString *uuidSting =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:uuidSting forKey:iServeDeviceId];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [Fabric with:@[[Crashlytics class]]];
    
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (userInfo)
    {
        [self performSelector:@selector(showPushNotification:)
                   withObject:userInfo
                   afterDelay:5];
    }
    
    [self registerPushNotification];
    
    
    
    [FIRMessaging messaging].remoteMessageDelegate = self;
      
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    

    [FIRApp configure];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    [self shouldRecieveNotifications:[[NSUserDefaults standardUserDefaults] objectForKey:@"MyApp_Notifications"]];
    
    return YES;
    
}

#pragma mark - Push Notification management

- (void) shouldRecieveNotifications:(NSString*) hasRegistered
{
    if(hasRegistered)
    {
        if ([hasRegistered isEqualToString:@"SEND"])
            [self registerPushNotification];
    }else{
        [self registerPushNotification];
    }
    
}

-(void)registerAppForNotifications{
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    //-- Set Notification
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
#else   // iOS < 8 Notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
#endif
    
    [[NSUserDefaults standardUserDefaults] setValue:@"SEND" forKey:@"MyApp_Notifications"];
}

-(void)unregisterAppForNotifications{
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"DONT SEND" forKey:@"MyApp_Notifications"];
}

-(void)showPushNotification:(NSDictionary *)userInfo
{
    NSLog(@"Push Notification:- %@",userInfo);
    UIApplication *application = [UIApplication sharedApplication];
    [self application:application didReceiveRemoteNotification:userInfo];
    
}


-(void)checkNotificationEnabledOrNot
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){ // Check it's iOS 8 and above
        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (grantedSettings.types == UIUserNotificationTypeNone) {
            NSLog(@"No permiossion granted");
        }
        else if (grantedSettings.types & UIUserNotificationTypeSound & UIUserNotificationTypeAlert ){
            NSLog(@"Sound and alert permissions ");
        }
        else if (grantedSettings.types  & UIUserNotificationTypeAlert){
            NSLog(@"Alert Permission Granted");
        }
    }
    
    
}

-(void)registerPushNotification
{
    if( SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0") )
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    // do something time consuming here, like network request or the like
                    // when done, update the UI on the main queue:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // update the UI here
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                });
            }
        }];
    }
    else
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:@"SEND" forKey:@"MyApp_Notifications"];
    
}


#pragma mark - Remote Notifications Delegate -

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"%@",userInfo[@"aps"][@"badge"]);
    if (userInfo[@"aps"][@"badge"])
    {
        application.applicationIconBadgeNumber = [userInfo[@"aps"][@"badge"] integerValue];
    }
    else
    {
        application.applicationIconBadgeNumber = 0;
    }
    
    
    NSLog(@"Push:%@",userInfo);
    
//    if(!userInfo[@"aps"][@"bid"])
//    {
//        return;
//    }
    [self playNotificationSound];
    [self handleNotificationForUserInfo:userInfo];
    
}

//FireBase Notification
-(void)applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"remoteMessage :%@", remoteMessage);
    [self playNotificationSound];
    [self handleNotificationForUserInfo:remoteMessage.appData];
}



- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    UIApplication *application = [UIApplication sharedApplication];
    
    NSLog( @"Handle push from background or closed" );
    NSLog(@"%@",userInfo[@"aps"][@"badge"]);
    
    if (userInfo[@"aps"][@"badge"])
    {
        application.applicationIconBadgeNumber = [userInfo[@"aps"][@"badge"] integerValue];
    }
    else
    {
        application.applicationIconBadgeNumber = 0;
    }
    
    NSLog(@"Push:%@",userInfo);
    
//    if(!userInfo[@"aps"][@"bid"])
//    {
//        return;
//    }
    [self playNotificationSound];
    [self handleNotificationForUserInfo:userInfo];

    // custom code to handle push while app is in the foreground
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    UIApplication *application = [UIApplication sharedApplication];
    
    NSLog( @"Handle push from background or closed" );
    NSLog(@"%@",userInfo[@"aps"][@"badge"]);
    
    if (userInfo[@"aps"][@"badge"])
    {
        application.applicationIconBadgeNumber = [userInfo[@"aps"][@"badge"] integerValue];
    }
    else
    {
        application.applicationIconBadgeNumber = 0;
    }
    
    NSLog(@"Push:%@",userInfo);
    
    [self playNotificationSound];
    [self handleNotificationForUserInfo:userInfo];
    // if you set a member variable in didReceiveRemoteNotification, you will know if this is from closed or background
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    
//    NSString *model = [[UIDevice currentDevice] model];
    if (TARGET_IPHONE_SIMULATOR)
    {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"garbagevalue" forKey:iServePushTokenKey];
    }
    else {
        
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:iServePushTokenKey];
        //for testing
        NSLog(@"Device Push Token:%@",dt);
        
    }
    NSLog(@"Push Token:%@",dt);
    
    [[FIRInstanceID instanceID]setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
    [[FIRInstanceID instanceID]setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];

    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    
    [[NSUserDefaults standardUserDefaults]setObject:@"123" forKey:iServePushTokenKey];
    NSLog(@"Failed To Register Push Notification");
}


#pragma mark - FireBase Methods -

// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification
{
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"FCM token: %@", refreshedToken);
//    [FCMPlugin.fcmPlugin notifyOfTokenRefresh:refreshedToken];
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm
{
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:[[FIRInstanceID instanceID] token] forKey:iServeFCMTokenKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/ios"];
            [[FIRMessaging messaging] subscribeToTopic:@"/topics/all"];
        }
    }];
}


#pragma mark - FBLogin Methods -

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
//        if ([[url scheme]isEqualToString:FACEBOOK_SCHEME])
//        {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
    
    //    }
    return NO;
}

#pragma mark - Application Methods -

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:self.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    
    //If Showing ProviderBooking VC
    if([navigationVC.topViewController isKindOfClass:[ProviderBookingViewController class]])
    {
        //check Requesting Screen is Showing
        ProviderBookingViewController *providerBookingVC = [ProviderBookingViewController getSharedInstance];
        if(providerBookingVC.requestingView)
        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:iServeSocketDisconnectedAndConnected object:nil userInfo:nil];
            
            if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCustomerID])
            {
                //Update Heartbeat
                [[CheckAppVersion sharedInstance] sendHeartBeatToSocket];
            }

            
            [application beginBackgroundTaskWithExpirationHandler:^{
                
                [application endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
                        
            }];

        }
        
    }
    else if([navigationVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
    {
        //check OnGoing Booking Screen is Showing
        ShowOnGoingBookingsViewController *onGoingVC = [ShowOnGoingBookingsViewController getSharedInstance];
        if(onGoingVC.status == 6 || onGoingVC.status == 15 || onGoingVC.status == 16)
        {
            [application beginBackgroundTaskWithExpirationHandler:^{
                
                [application endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
                
            }];
            
        }

    }
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:self.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    
    //end Background Task
    [application endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
    
    //If Showing ProviderBooking VC
    if([navigationVC.topViewController isKindOfClass:[ProviderBookingViewController class]])
    {
        //check Requesting Screen is Showing
        ProviderBookingViewController *providerBookingVC = [ProviderBookingViewController getSharedInstance];
        if(providerBookingVC.requestingView)
        {
            [providerBookingVC.requestingView setPulsingAnimation];
        }
        
    }

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //    [FBSDKAppEvents activateApp];
    // Use Reachability to monitor connectivity
    
    application.applicationIconBadgeNumber = 1;
    application.applicationIconBadgeNumber = 0;
    
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    
    //Current App Version
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSLog(@"Version:%@",currentVersion);
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"appVersion"] && [[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken])
    {
        //Previous App Version
        NSString *previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"appVersion"];
        
        currentVersion = [currentVersion stringByReplacingOccurrencesOfString:@"." withString:@""];
        previousVersion = [previousVersion stringByReplacingOccurrencesOfString:@"." withString:@""];

        if(![previousVersion isEqualToString:currentVersion])
        {
            //Version Changed
            //Do Service Call
            [[CheckAppVersion sharedInstance]sendRequestToUpdateVersion];
            
        }
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"appVersion"];
    }

  
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [FBSDKAppEvents activateApp];
    [self saveContext];
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
  completionHandler:(void (^)())completionHandler
{
    /* Store the completion handler.*/
    [AWSS3TransferUtility interceptApplication:application handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}

#pragma mark - Set Properties Of Navigatin Bar -
- (void)setupAppearance
{
    [[UINavigationBar appearance] setBackgroundColor:UIColorFromRGB(0xffffff)];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:OpenSans_Regular size:15], NSFontAttributeName,APP_COLOR, NSForegroundColorAttributeName, nil];
    
   
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    
}

#pragma mark - Core Data stack -

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
    // The directory the application uses to store the Core Data store file. This code uses a directory named "-Embed.Twotray" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"iServe_DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"iServe_DataModel.sqlite"];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error])
    {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext
{
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator)
    {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support -

- (void)saveContext
{
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Handle Push Notifications -
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions
{
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload)
    {
        //check if user is logged in
        if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken])
        {
            [self handleNotificationForUserInfo:remoteNotificationPayload];
        }
    }
}


-(void)playNotificationSound
{
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms-received" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}

- (void)handleNotificationForUserInfo:(NSDictionary*)userInfo
{
//    if(userInfo[@"payload"])
//    {
//        [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:userInfo];
//    }
//    else if([userInfo[@"aps"][@"action"]integerValue] == 23)
//    {
//        [[LiveBookingStatusUpdateClass sharedInstance]profileGotRejected:userInfo[@"aps"]];
//    }
//    else if([userInfo[@"aps"][@"st"]integerValue] != 3)
//    {
//        [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:userInfo[@"aps"]];
//    }
//    else
//    {
//        
//    }
    if(userInfo[@"payload"])
    {
        [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:userInfo];
    }
    else if([userInfo[@"action"]integerValue] == 23)
    {
        [[LiveBookingStatusUpdateClass sharedInstance]profileGotRejected:userInfo];
    }
    else if(userInfo[@"st"])
    {
        if([userInfo[@"st"]integerValue] != 3)
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:userInfo];
        }
        
    }
    else
    {
        
    }

}


#pragma mark - Socket Response -

- (void)setSocketDelegateToAppDelegate
{
    [SocketIOWrapper sharedInstance].socketDelegate = self;
}

- (void)receievedMessageOnChannel:(NSString *)channelName withMessage:(NSDictionary *)message
{
    NSLog(@"AppDelegate message: %@",message);
    
    if([channelName isEqualToString:@"CustomerStatus"]){//Booking Status Response
        
        NSInteger bstatus = [message[@"st"]integerValue];
        
        if(bstatus == 2 || bstatus == 5 || bstatus == 21 || bstatus == 6 ||bstatus == 22||bstatus == 7 ||bstatus == 15 || bstatus == 16 || bstatus == 10){//Booking Response
            
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveBookingStatusFromPushOrSocket:message];
        }
        
    }
    else if([channelName isEqualToString:@"Message"])//Chat Message Response
    {
        if(message[@"payload"])
        {
            [[LiveBookingStatusUpdateClass sharedInstance]didRecieveChatMessageFromPushorSocket:message];
        }
    }

    
}

#pragma mark - UIKeyboard Methods -

/**
 *   This method will called when the user wants the keyboard's
 */
- (void)keyboardWillBeHidden:(__unused NSNotification *)inputViewNotification {
    
    if (self.keyboardDelegate && [self.keyboardDelegate respondsToSelector:@selector(keyboardWillHide:)]) {
        [self.keyboardDelegate keyboardWillHide:inputViewNotification];
    }
    
}

- (void)keyboardWillBeShown:(__unused NSNotification *)inputViewNotification {
    
    if (self.keyboardDelegate && [self.keyboardDelegate respondsToSelector:@selector(keyboardWillShown:)]) {
        [self.keyboardDelegate keyboardWillShown:inputViewNotification];
    }

}

@end
