//
//  checkAppVersion.h
//  iServe_Customer
//
//  Created by Apple on 17/11/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckAppVersion : NSObject<WebServiceHandlerDelegate,UIAlertViewDelegate>

+(instancetype) sharedInstance;
-(void)sendRequestToUpdateVersion;
-(void)checkAppHasUpdatedVersion;
-(void)sendHeartBeatToSocket;

@end
