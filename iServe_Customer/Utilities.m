//
//  Utilities.m
//  Kidzo
//
//  Created by Vinay Raja on 28/12/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "Utilities.h"
//#import "WebServiceConstants.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@implementation Utilities

+ (NSString*) currentDateTime:(NSString*)format {
    if (!format) {
        format = @"yyyy-MM-dd HH:mm:ss";
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    return [dateFormatter stringFromDate:[NSDate date]];
}


+ (NSString*) dateStringFor:(NSDate*)date format:(NSString*)format {
    if (!format) {
        format = @"yyyy-MM-dd HH:mm:ss";
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    return [dateFormatter stringFromDate:date];
}

+ (NSString*) changeDateFormat:(NSString*)inFormat outFormat:(NSString*)outFormat date:(NSString*)dateString {
    if (!inFormat || !outFormat) {
        return nil;
    }
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = inFormat;

    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return [self dateStringFor:date format:outFormat];
    
}

+ (NSDate*) dateFromString:(NSString*)dateString format:(NSString*)format {
    if (!format)
        return [NSDate date];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    return date;
    
}



+ (BOOL) isValidEmail:(NSString*)emailId
{
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailId];
}

+ (NSString*)getDeviceType
{
    NSString *token = @"1";
    return token;
}

+ (NSString*)getDeviceId
{
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kTWTDeviceIdKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kTWTDeviceIdKey];
    }
    return deviceId;
}

+ (NSString*)getDeviceOS
{
    NSString *token = @"iOS";
    return token;
}

+ (NSString*)getProfileURL {
    
    NSString *token = @"iOS";
    
    return token;
    
}

+ (NSString*) getDeviceToken {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *dt = [ud objectForKey:@"deviceid"];
    
    if (!dt.length) {
        dt = @"1234";
    }
    return [self stringForString:dt];
}

+ (NSString*)getSessionToken {
    
    NSString *token = @"";
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    token = [ud objectForKey:@"deviceid"];
    
    return token;
    
}

+ (UIColor*)colorWithHex:(NSInteger)hex {
    return [self colorWithHex:hex alpha:1.0];
}

+ (UIColor*)colorWithHex:(NSInteger)hex alpha:(float)alpha {
    return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0
                           green:((float)((hex & 0x00FF00) >>  8))/255.0
                            blue:((float)((hex & 0x0000FF) >>  0))/255.0
                           alpha:alpha];
}

+ (NSString*) stringForString:(NSString*)string {
    NSString *newstring = @"";
    if ([string isKindOfClass:[NSNull class]]) {
        return newstring;
    }
    if (string && string.length) {
        newstring = string;
    }
    
    return newstring;
}

+ (NSString*) getAmazonURLForFile:(NSString*)fileName {
    return [@"https://s3.amazonaws.com/mgkhapp/" stringByAppendingString:[self stringForString:fileName]];
}

+ (NSString*) getDocumentsDirPathForFile:(NSString*)fileName {
    NSString* docDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *file = [docDirectory stringByAppendingPathComponent:fileName];
    
    return file;

}

+ (void) playSystemSound {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory: AVAudioSessionCategoryPlayback  error:&err];
    //AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    AudioServicesPlaySystemSound (1007);

}
+(NSString*)getPushToken
{
    NSString *pToken = @"garbagevalue";
    if([[NSUserDefaults standardUserDefaults]objectForKey:kTWTPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:kTWTPushToken];
    }
    return pToken;
}
+ (NSString*)getUserType
{
    NSString *userType=@"2";
    return userType;
}
+(NSString*)getZipCode
{
    NSString *getZipCode=@"560024";
    return getZipCode;
}




@end
