//
//  ProviderJobPhotosCollectionViewCell.h
//  iServe_AutoLayout
//
//  Created by Apple on 12/08/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProviderJobPhotosCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *jobPhoto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
