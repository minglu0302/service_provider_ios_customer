//
//  TWTDetailsViewCell.m
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "SupportDetailsTableCell.h"

@implementation SupportDetailsTableCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
    {
        self.detailBackgroundView.backgroundColor=[UIColor colorWithRed:0.6667 green:0.6667 blue:0.6667 alpha:0.5];
    }
    else
    {
        self.detailBackgroundView.backgroundColor=[UIColor whiteColor];
    }
}

@end
