//
//  ServiceGroupViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 16/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ServiceGroupViewController.h"
#import "ServiceGroupTableViewCell.h"
#import "ServiceDetailsViewController.h"

@interface ServiceGroupViewController ()<UITableViewDataSource,UITableViewDelegate,SelectedServiceDetailsDelegate>
{
    BOOL isMandatoryServiceSelected;
    BOOL isClearButtonClicked;
    
    BOOL isServiceSelected;
    NSInteger selectedRow;
    
    NSMutableArray *tempArrayOfSelectedServices;
}

@end

@implementation ServiceGroupViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tempArrayOfSelectedServices = self.arrayOfSelectedServices.mutableCopy;
    [self getSelectedServiceDetailsFromServiceDetailVC:tempArrayOfSelectedServices];
    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
}

#pragma mark - UIButton Actions -

- (IBAction)navigationLeftButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButtonAction:(id)sender
{
    
    self.arrayOfSelectedServices = tempArrayOfSelectedServices.mutableCopy;
    [self.serviceDetailDelegate getSelectedServiceDetails:self.arrayOfSelectedServices];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)changeServicesButtonAction:(id)sender
{
    UIButton *changeButton = (UIButton *)sender;
    
    NSArray *serviceArray = [NSArray new];
    
    [tempArrayOfSelectedServices removeObjectAtIndex:changeButton.tag];
    [tempArrayOfSelectedServices insertObject:serviceArray atIndex:changeButton.tag];
    
    [self.tableView reloadData];
    
}


#pragma mark - UITableView Methods -

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayOfServiceGroups.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"serviceGroupCell";
    
    ServiceGroupTableViewCell *serviceGroupCell = (ServiceGroupTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (serviceGroupCell == nil)
    {
        serviceGroupCell = [[ServiceGroupTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [serviceGroupCell showEachRowDetails:tempArrayOfSelectedServices groupDetails:self.arrayOfServiceGroups indexValue:indexPath];
    
    
    return serviceGroupCell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"toListOfServicesVC" sender:self];
}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toListOfServicesVC"])
    {
        ServiceDetailsViewController *serviceDetailsVC = [segue destinationViewController];
        serviceDetailsVC.arrayOfServices = self.arrayOfServiceGroups[selectedRow][@"services"];
        serviceDetailsVC.arrayOfSelectedServicesForAllGroups = [[NSMutableArray alloc]initWithArray:tempArrayOfSelectedServices copyItems:YES];
        serviceDetailsVC.arrayOfSelectedServices = [[NSMutableArray alloc]initWithArray:tempArrayOfSelectedServices[selectedRow] copyItems:YES];
        serviceDetailsVC.isMultipleSelection = self.arrayOfServiceGroups[selectedRow][@"cmult"];
        serviceDetailsVC.serviceGroupNumber = selectedRow;
        serviceDetailsVC.selectedServiceDetailDelegate = self;
    }
}

#pragma mark - Delegate From Service VC -

-(void)getSelectedServiceDetailsFromServiceDetailVC:(NSMutableArray *)selectedServices
{
    tempArrayOfSelectedServices = [[NSMutableArray alloc]initWithArray:selectedServices copyItems:YES];
    [self.tableView reloadData];
    
    //    NSInteger mandatoryGroupCount = 0;
    //    NSInteger normalServiceSelectedCount = 0;
    //    for(int i=0; i<self.arrayOfServiceGroups.count; i++)
    //    {
    //        //If Mandatory Group Available
    //        if([self.arrayOfServiceGroups[i][@"cmand"]integerValue] == 1)
    //        {
    //            mandatoryGroupCount = mandatoryGroupCount + 1;
    //            if([self.arrayOfSelectedServices[i] count] > 0)
    //            {
    //                isMandatoryServiceSelected = YES;
    //            }
    //            else
    //            {
    //                isMandatoryServiceSelected = NO;
    //                break;
    //            }
    //        }
    //        else
    //        {
    //            //Not a Mandatory Group Services Selected or Not
    //            if([self.arrayOfSelectedServices[i] count] > 0)
    //            {
    //                normalServiceSelectedCount = normalServiceSelectedCount + 1;
    //            }
    //
    //        }
    //
    //    }
    
    //Show Or Hide Done Button When Mandatory Group Available
    //    if(mandatoryGroupCount > 0)
    //    {
    //        if(isMandatoryServiceSelected)
    //        {
    //            self.doneButton.hidden = NO;
    //        }
    //        else
    //        {
    //            self.doneButton.hidden = YES;
    //        }
    //    }
    //    else
    //    {
    //        //Show Or Hide Done Button When Mandatory Group Not Available
    //        if(normalServiceSelectedCount > 0)
    //        {
    //            self.doneButton.hidden = NO;
    //        }
    //        else
    //        {
    //            self.doneButton.hidden = YES;
    //        }
    //
    //    }
    
    //    isServiceSelected = NO;
    
    //For Any Service has Selected Check
    //    for(int i=0; i<self.arrayOfServiceGroups.count; i++)
    //    {
    //        //For all service details
    //        if([selectedServices[i] count] > 0)
    //        {
    //            isServiceSelected = YES;
    //            break;
    //        }
    //    }
    
}

-(void)checkMandatoryServiceSelected:(NSMutableArray *)selectedServices
{
    //NSInteger mandatoryGroupCount = 0;
    //NSInteger normalServiceSelectedCount = 0;
    // isMandatoryServiceSelected = NO;
    
    //For Mandatory Services Check
    /* for(int i=0; i<self.arrayOfServiceGroups.count; i++)
     {
     //If Mandatory Group Available
     if([self.arrayOfServiceGroups[i][@"cmand"]integerValue] == 1)
     {
     mandatoryGroupCount = mandatoryGroupCount + 1;
     if([selectedServices[i] count] > 0)
     {
     isMandatoryServiceSelected = YES;
     }
     else
     {
     isMandatoryServiceSelected = NO;
     break;
     }
     }
     else
     {
     //Not a Mandatory Group Services Selected or Not
     if([selectedServices[i] count] > 0)
     {
     normalServiceSelectedCount = normalServiceSelectedCount + 1;
     }
     
     }
     
     }
     
     //Show Or Hide Done Button When Mandatory Group Available
     if(mandatoryGroupCount > 0)
     {
     if(!isMandatoryServiceSelected)
     {
     [UIHelper showMessage:@"Please select mandatory service" withTitle:@"Message" delegate:nil];
     }
     else
     {
     [self.navigationController popViewControllerAnimated:YES];
     }
     }
     else
     {
     //Show Or Hide Done Button When Mandatory Group Not Available
     if(normalServiceSelectedCount > 0)
     {
     [self.navigationController popViewControllerAnimated:YES];
     //            self.doneButton.hidden = NO;
     }
     else
     {
     [UIHelper showMessage:@"Please select any service" withTitle:@"Message" delegate:nil];
     //            self.doneButton.hidden = YES;
     }
     
     }*/
    
}

@end
