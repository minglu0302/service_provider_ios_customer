//
//  NotesFromProviderTableViewCell.h
//  iServe_Customer
//
//  Created by Apple on 24/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotesFromProviderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *notesFromProviderBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *noteFromProviderLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notesFromProviderBackgroundViewHeightConstraint;

@end
