//
//  TWTAboutThirdWebViewController.h
//  iServePassenger
//
//  Created by -Tony Lu on 19/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutWebViewController : UIViewController<UIWebViewDelegate>

/**
 *  About web View Outlet
 */
@property (weak, nonatomic) IBOutlet UIWebView *aboutWebView;
/**
 *  Back Button Outlet
 */
@property (weak, nonatomic) IBOutlet UIButton *backButton;
/**
 *  Recv Array Variable
 */
@property (nonatomic,strong) NSArray *recvArray;
/**
 *  Back Button Action
 *
 *  @param sender
 */
- (IBAction)backButton:(id)sender;

@end
