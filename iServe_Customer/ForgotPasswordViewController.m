//
//  ForgotPasswordViewController.m
//  iServe_Customer
//
//  Created by Apple on 17/10/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "CountryNameTableViewController.h"
#import "ChangePasswordView.h"
#import "VerifyPhoneNumberViewController.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate,WebServiceHandlerDelegate>
{
    UIWindow *window;
    NSString *verificationCode;
    ChangePasswordView *changePasswordPopUP;
}
@end

@implementation ForgotPasswordViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    self.countryCodeLabel.text = [NSString stringWithFormat:@"+%@",stringCountryCode];
    self.countryImageView.image = [UIImage imageNamed:imagePath];

    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [self.emailTextField becomeFirstResponder];
}

#pragma mark - UIButton Actions -

- (IBAction)NavigationLeftButtonAction:(id)sender
{
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionReveal
                                              subType:kCATransitionFromBottom
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController popViewControllerAnimated:NO];

}

- (IBAction)submitButtonAction:(id)sender
{
    if([self emailValidationCheck:self.emailTextField.text] == 0)
    {
        [UIHelper showMessage:LS(@"Please enter a valid email or phone number") withTitle:LS(@"Message")delegate:self];
    }
    else
    {
        [self.view endEditing:YES];
        if([self checkStringIsNumbers:self.emailTextField.text])
        {
            //Mobile Number
//            [self performSegueWithIdentifier:@"toVerifyPhoneNumberVC" sender:self];
            [self sendOTP];
            
//            window = [[[UIApplication sharedApplication]delegate]window];
//            
//            changePasswordPopUP = [ChangePasswordView sharedInstance];
//            [window addSubview:changePasswordPopUP];
            
            
            
//            [UIView animateWithDuration:0.5f
//                             animations:^{
            
//                                 [changePasswordPopUP layoutIfNeeded];
//                             }
//                             completion:^(BOOL finished) {
            
//                                 [changePasswordPopUP.firstNumber becomeFirstResponder];
//                                 
//                                 [UIView animateWithDuration:1.0
//                                                       delay:0.0
//                                      usingSpringWithDamping:0.4
//                                       initialSpringVelocity:0
//                                                     options:UIViewAnimationOptionLayoutSubviews
//                                                  animations:^{
//                                                         //Animations
//                                                      changePasswordPopUP.topViewTopConstraint.constant = 20;
//                                                         [changePasswordPopUP.topView layoutIfNeeded];
//                                                     }
//                                                  completion:^(BOOL finished) {
//                                                      //Completion Block
//                                                      
//                                                  }];

//                             }];

            
        }
        else
        {
            //Email Id
            [self retrievePassword];
        }
    }
}

- (IBAction)forgotPasswordTapGestureAction:(id)sender
{
     [self.view endEditing:YES];
}

- (IBAction)countryCodeButtonAction:(id)sender
{
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
    {
        self.countryImageView.image=flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        self.countryCodeLabel.text = countryCode;
    };
    [self presentViewController:navBar animated:YES completion:nil];

}

-(BOOL)emailValidationCheck: (NSString *)emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}|[0-9]+";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

-(BOOL)checkStringIsNumbers:(NSString *)string
{
    NSString *regexForEmailAddress = @"[0-9]+";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:string];
}

#pragma mark - UITextField Delegates -

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self submitButtonAction:nil];
    return YES;
}

#pragma mark - Web Service Call -

- (void)retrievePassword
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 kTWTEmail:self.emailTextField.text,
                                 kTWTEnterUserTypekey:flStrForStr([Utilities getUserType]),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToForgetPassword:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}
- (void)sendOTP
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ([reachability isNetworkAvailable])
    {
        NSDictionary *params = @{
                                 @"ent_mobile":self.emailTextField.text,
                                 @"ent_date_time":[UIHelper getCurrentDateTime],
                                 @"ent_user_type":@"2"
                                };
        
        [[WebServiceHandler sharedInstance] sendOTPToVerifyPhoneNumber:params andDelegate:self];
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:self];
    }
}


#pragma mark - Web Service Delegate -

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:self];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    if(errFlag == 1)
    {
        if (requestType == RequestTypeForgotPasswordVerifyPhoneNumber){
            
            self.emailTextField.text = @"";
            [self.emailTextField becomeFirstResponder];
            [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:self];
            
        }
        else if (requestType == RequestTypeForgetPassword && errNum == 66){
            
            self.emailTextField.text = @"";
            [self.emailTextField becomeFirstResponder];
            [UIHelper showMessage:response[@"errMsg"] withTitle:@"Message" delegate:self];
            
        }
    }
    else  if (errFlag == 0 && requestType == RequestTypeForgotPasswordVerifyPhoneNumber)
    {
        verificationCode = [NSString stringWithFormat:@"%@",response[@"code"]];
        NSLog(@"response %@",verificationCode);

        [self performSegueWithIdentifier:@"toVerifyPhoneNumberVC" sender:self];
    }
    else  if (errFlag == 0 && requestType == RequestTypeForgetPassword)
    {
        [self NavigationLeftButtonAction:nil];
        [UIHelper showMessage:response[@"errMsg"] withTitle:@"Message" delegate:self];
    }

}

#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toVerifyPhoneNumberVC"])
    {
        VerifyPhoneNumberViewController *verifyPhoneNumberVC = [segue destinationViewController];
        verifyPhoneNumberVC.phoneNumber = self.emailTextField.text;
        verifyPhoneNumberVC.verificationCode = verificationCode;
    }
}

@end
