//
//  ItemDetailsTableViewCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/20/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "ItemDetailsTableViewCell.h"

@implementation ItemDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
