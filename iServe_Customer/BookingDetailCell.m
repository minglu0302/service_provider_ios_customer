//
//  BookingDetailCell.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "BookingDetailCell.h"

@implementation BookingDetailCell

- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)showEachBookingRow:(NSDictionary *)bookingDetail
{
    NSString *myString = bookingDetail[@"apntDate"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *yourDate = [dateFormatter dateFromString:myString];
    dateFormatter.dateFormat = @"dd MMM yyyy";
    
    self.bookingDateLabel.text = [dateFormatter stringFromDate:yourDate];
    
    self.bookingTimeLabel.text = bookingDetail[@"apntTime"];
    
    self.bookingIdLabel.text = [NSString stringWithFormat:@"BOOKING ID:%@",bookingDetail[@"bid"]];
    
    self.bookingStatusLabel.text = bookingDetail[@"status"];//[UIHelper getBookingStatusString:[bookingDetail[@"statCode"]integerValue]];
    
    
    
    if([bookingDetail[@"statCode"]integerValue] == 3 ||[bookingDetail[@"statCode"]integerValue] == 4||[bookingDetail[@"statCode"]integerValue] == 8 ||[bookingDetail[@"statCode"]integerValue] == 9 ||[bookingDetail[@"statCode"]integerValue] == 10)
    {
        self.bookingStatusLabel.textColor = [UIColor redColor];
    }
    else
    {
        if([bookingDetail[@"statCode"]integerValue] == 7 && [bookingDetail[@"disputed"]integerValue] == 1)
        {
            self.bookingStatusLabel.textColor = [UIColor redColor];
        }
        else
        {
            self.bookingStatusLabel.textColor = UIColorFromRGB(0x3FC380);
        }
    }
    
    self.providerNameLabel.text = bookingDetail[@"fname"];
    self.bookingAddressLabel.text = bookingDetail[@"addrLine1"];
    self.providerTypeLabel.text = bookingDetail[@"cat_name"];
    
    [self.activityIndicator startAnimating];
    
    NSLog(@"local image url: %@", bookingDetail[@"pPic"]);
    
    [self.providerImage sd_setImageWithURL:[NSURL URLWithString:bookingDetail[@"pPic"]]
                             placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                        
                                        [self.activityIndicator stopAnimating];
                                        
                                    }];
    
}

@end
