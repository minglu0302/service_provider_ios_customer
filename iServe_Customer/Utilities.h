//
//  Utilities.h
//  Kidzo
//
//  Created by Vinay Raja on 28/12/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utilities : NSObject

+ (NSString*) currentDateTime:(NSString*)format;
+ (NSString*) dateStringFor:(NSDate*)date format:(NSString*)format;
+ (NSString*) changeDateFormat:(NSString*)inFormat outFormat:(NSString*)outFormat date:(NSString*)date;
+ (NSDate*) dateFromString:(NSString*)date format:(NSString*)format;

+ (BOOL) isValidEmail:(NSString*)emailId;

+ (NSString*)getDeviceType;
+ (NSString*)getDeviceToken;
+ (NSString*)getDeviceId;
+ (NSString*)getDeviceOS;
+ (NSString*)getSessionToken;
+ (NSString*)getProfileURL;
+ (NSString*)getPushToken;



+ (UIColor*)colorWithHex:(NSInteger)hex;
+ (UIColor*)colorWithHex:(NSInteger)hex alpha:(float)alpha;


+ (NSString*) stringForString:(NSString*)string;

+ (NSString*) getAmazonURLForFile:(NSString*)fileName;
+ (NSString*) getDocumentsDirPathForFile:(NSString*)fileName;

+ (void) playSystemSound;

+ (NSString*)getUserType;

+(NSString*)getZipCode;
@end
