//
//  TableViewController.h
//  Sup
//
//  Created by -Tony Lu on 3/5/15.
//  Copyright (c) 2015 3embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryNameTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate,UISearchControllerDelegate>

//@property (weak, nonatomic) IBOutlet UILabel *navigationTitle;
@property(strong , nonatomic) NSArray *details;
@property (nonatomic,copy) void (^oncomplete)(NSString * code,UIImage *flagimg,NSString * countryCode);

- (IBAction)backButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *countrytableView;



@end
