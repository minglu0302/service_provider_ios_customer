
//
//  HelpViewController.m
//  iServe app
//
//  Created by -Tony Lu on 06/02/16.
//   Developed by Raghavendra
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "LocationServiceViewController.h"

@interface HelpViewController ()<GetCurrentLocationDelegate>
{
    AVPlayerLayer *avPlayerLayer;
    UIStoryboard *mainstoryboard;
    SignInViewController *signInVC;
    SignUpViewController *signUpVC;
    GetCurrentLocation *getLocation;
}

@end

@implementation HelpViewController

#pragma mark - UILife Cycle -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //    [self createMoviePlayer];
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    
    if (screenSize.height < 568)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone4_Screen"]];
    }
    else if(screenSize.height < 667)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone5_Screen"]];
    }
    else if(screenSize.height < 736)
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone6_Screen"]];
    }
    else
    {
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhone6Plus_Screen"]];
    }
    
    if((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        NSLog(@"iphone x image");
        [self.backgroundImageView setImage:[UIImage imageNamed:@"iPhoneX_Screen"]];
    }
    
    [self.view bringSubviewToFront:self.bottomView];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
        [self.signinButton addTarget:self action:@selector(signHighlightButtonBorder:) forControlEvents:UIControlEventTouchDown];
        [self.registerButton addTarget:self action:@selector(regHighlightButtonBorder:) forControlEvents:UIControlEventTouchDown];
        
        [self.signinButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
    }
 
}

- (void)signHighlightButtonBorder:(id)sender
{
    NSLog(@"sign in event call");
    self.signinButton.tintColor = [UIColor whiteColor];
}

- (void)regHighlightButtonBorder:(id)sender
{
    NSLog(@"register in event call");
    self.registerButton.tintColor = [UIColor whiteColor];
}


- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self animateIn];
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.registerButton.selected == NO){
        
        [self.signinButton setSelected:YES];
    }
    else
        [self.signinButton setSelected:NO];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    //  [self.avplayer pause];
}

- (void)animateIn
{
    self.bottomViewConstraint.constant = 0;
    [UIView animateWithDuration:0.75 animations:^{
//        [self.view layoutIfNeeded];
    }];
}

#pragma mark - IBAction

- (IBAction)signinButton:(id)sender
{
    [self changeButtonState:sender];
    
    signInVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"signInVC"];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];

    
    [self.navigationController pushViewController:signInVC animated:NO];
    
}

- (IBAction)registerButton:(id)sender
{
    [self changeButtonState:sender];
    
    signUpVC = [mainstoryboard instantiateViewControllerWithIdentifier:@"signUpVC"];
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:signUpVC animated:NO];
    
}

-(void)changeButtonState:(id)sender{
    
    UIButton *mBtn = (UIButton *)sender;
    for (UIView *button in mBtn.superview.subviews) {
        if ([button isKindOfClass:[UIButton class]]) {
            [(UIButton *)button setSelected:NO];
        }
    }
    mBtn.selected = YES;
    
}
#pragma mark - StatusBarHidden

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
