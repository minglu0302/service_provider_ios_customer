//
//  TWTDetailsViewCell.h
//  iServePassenger
//
//  Created by -Tony Lu on 18/02/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportDetailsTableCell : UITableViewCell
/**
 *  Cell label outlet
 */
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
/**
 *  Detail Background View outlet for adding the background colour when the cell Selected 
 */
@property (weak, nonatomic) IBOutlet UIView *detailBackgroundView;

@end
