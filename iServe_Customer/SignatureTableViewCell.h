//
//  SignatureTableViewCell.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/20/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignatureTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
