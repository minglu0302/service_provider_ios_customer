//
//  PickAddressFromMapViewController.m
//  iServe_AutoLayout
//
//  Created by Apple on 14/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "PickAddressFromMapViewController.h"
#import "SaveSelectedAddressViewController.h"
#import "GetCurrentLocation.h"
#import "LocationServiceViewController.h"
#import "PickUpViewController.h"

@interface PickAddressFromMapViewController ()<GMSMapViewDelegate,GetCurrentLocationDelegate,PickUpAddressDelegate>
{
    double currentLatitude;
    double currentLongitude;
    GetCurrentLocation *getCurrentLocation;
    BOOL isAddressManuallyPicked;
}

@end

@implementation PickAddressFromMapViewController

#pragma mark - Initial Methods -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //Add Attributed String To Message Label
    NSString *str = LS(@"Place the pin on exact location\nor Allow location services");
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString:str];

    [attributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:OpenSans_Regular size:11] range:NSMakeRange(35,23)];

    [attributedString addAttribute:NSForegroundColorAttributeName value:APP_COLOR range:NSMakeRange(35,23)];

    [self.messageLabel setAttributedText:attributedString];

   
    
    //Set Properties Of MapView
    currentLatitude = [[[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLat]floatValue];
    currentLongitude = [[[NSUserDefaults standardUserDefaults]objectForKey:iServeUserCurrentLong]floatValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLatitude longitude:currentLongitude zoom:mapZoomLevel];
    
    self.selectedAddressLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:iServeUserCurrentAddress];
    
    self.mapView.camera = camera;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    self.mapView.myLocationEnabled = YES;
    
//    getCurrentLocation = [GetCurrentLocation sharedInstance];
//    getCurrentLocation.delegate = self;
//    [getCurrentLocation getLocation];


    // Do any additional setup after loading the view.
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"11.0") && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        // Remove navigation bar bottom shadow line in iOS 11
        //        [self.navigationController :[self generateSinglePixelImageWithColor:[UIColor clearColor]]];
        [self.navigationController.navigationBar setBackgroundImage:[self generateSinglePixelImageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
        self.navigationController.navigationBar.shadowImage = [self generateSinglePixelImageWithColor:[UIColor clearColor]];
    }
}

- (UIImage *)generateSinglePixelImageWithColor:(UIColor *)color {
    CGSize imageSize = CGSizeMake(1.0f, 1.0f);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0.0f);
    
    CGContextRef theContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(theContext, color.CGColor);
    CGContextFillRect(theContext, CGRectMake(0.0f, 0.0f, imageSize.width, imageSize.height));
    
    CGImageRef theCGImage = CGBitmapContextCreateImage(theContext);
    UIImage *theImage;
    if ([[UIImage class] respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        theImage = [UIImage imageWithCGImage:theCGImage scale:[UIScreen mainScreen].scale orientation:UIImageOrientationUp];
    } else {
        theImage = [UIImage imageWithCGImage:theCGImage];
    }
    CGImageRelease(theCGImage);
    
    return theImage;
}

-(void)viewWillAppear:(BOOL)animated
{
   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIButton Actions -

- (IBAction)confirmLocationButtonAction:(id)sender
{
    [self performSegueWithIdentifier:@"toSaveAddressVC" sender:sender];
}

- (IBAction)navigationBackButtonAction:(id)sender
{
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)searchAddressButtonAction:(id)sender
{
    PickUpViewController *pickController = (PickUpViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"pickUpLocationVC"];
    
    pickController.latitude =  [NSString stringWithFormat:@"%f",currentLatitude];
    pickController.longitude = [NSString stringWithFormat:@"%f",currentLongitude];
    pickController.pickUpAddressDelegate = self;
    
    
    [AnimationsWrapperClass CATransitionAnimationType:kCATransitionMoveIn
                                              subType:kCATransitionFromTop
                                              forView:self.navigationController.view
                                         timeDuration:0.3];
    
    [self.navigationController pushViewController:pickController animated:NO];
}

- (IBAction)currentLocationButtonAction:(id)sender
{
    CLLocation *location = self.mapView.myLocation;
    currentLatitude = location.coordinate.latitude;
    currentLongitude = location.coordinate.longitude;
    
    if (location) {
        
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(currentLatitude, currentLongitude) zoom:mapZoomLevel];
        [self.mapView animateWithCameraUpdate:zoomCamera];
    }

}

#pragma mark - GMSMapview Delegate -

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    if (isAddressManuallyPicked) {
        //Checking Address Picked Manually (Get Address From Search Address Controller)
        
        isAddressManuallyPicked = NO;
        return;
    }

    else
    {
    
        CGPoint point1 = self.mapView.center;
        CLLocationCoordinate2D coor = [self.mapView.projection coordinateForPoint:point1];
        currentLatitude = coor.latitude;
        currentLongitude = coor.longitude;
        
        CLLocation *location = [[CLLocation alloc]initWithLatitude:currentLatitude longitude:currentLongitude];
        
        [self getAddress:location];
    }
    
}

#pragma mark - Get Current Location Delegates -

- (void)updatedLocation:(double)latitude and:(double)longitude
{
    //change map camera postion to current location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:mapZoomLevel];
    [self.mapView setCamera:camera];
    
    //save current location to plot direciton on map
    currentLatitude = latitude;
    currentLongitude =  longitude;
    
}

-(void)updatedAddress:(NSString *)currentAddress
{
    self.selectedAddressLabel.text = currentAddress;
    NSLog(@"Current Address In PickUp Address Class:%@",self.selectedAddressLabel.text);
}

//-(void)didFailedLocationUpdate
//{
//    [self gotoLocationServicesMessageViewController];
//}

#pragma mark - Location Services Methods -

///**
// *  Notification method when Location service Changed
// *
// *  @param notification
// */
//-(void)locationServicesChanged:(NSNotification*)notification
//{
//    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
//    {
//        [self gotoLocationServicesMessageViewController];
//    }
//    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
//    {
//        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
//    }
//}
//
///**
// *  Showing Location Service Message Controller
// */
//-(void)gotoLocationServicesMessageViewController
//{
//    LocationServiceViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
//    
//    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
//    
//    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
//}


#pragma mark - Get Current Location Methods -

- (void)getAddress:(CLLocation *)coordinate
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:coordinate
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!error)
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             
             self.selectedAddressLabel.text = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSLog(@"Selected Address in PickUp Address Class:%@",self.selectedAddressLabel.text);
             
         }
         else
         {
             NSLog(@"Failed to update location : %@",error);
         }
     }];
    
}

#pragma mark - GetAddress From Pick UP VC Delegate Method -

-(void)getAddressFromPickUpAddressVC:(NSDictionary *)addressDetails
{
    isAddressManuallyPicked = YES;
    
    NSString *addressText = flStrForStr(addressDetails[@"address"]);
//    if ([addressDetails[@"address2"] rangeOfString:addressText].location == NSNotFound) {
//        
//        addressText = [addressText stringByAppendingString:@","];
//        addressText = [addressText stringByAppendingString:flStrForStr(addressDetails[@"address2"])];
//        
//    } else {
//        
//        if([addressDetails[@"address2"] length])
//        {
//            addressText = flStrForStr(addressDetails[@"address2"]);
//        }
//        
//    }
    
    currentLatitude = [addressDetails[@"lat"] doubleValue];
    currentLongitude = [addressDetails[@"long"] doubleValue];
    self.selectedAddressLabel.text = addressText;
    
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(currentLatitude, currentLongitude) zoom:mapZoomLevel];
    [self.mapView animateWithCameraUpdate:zoomCamera];
    
}


#pragma mark - Prepare Segue -

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toSaveAddressVC"])
    {
        SaveSelectedAddressViewController *saveAddressVC = [segue destinationViewController];
        saveAddressVC.isFromProviderBookingVC = self.isFromProviderBookingVC;
        saveAddressVC.selectedAddressDetails = @{
                                                @"address":flStrForStr(self.selectedAddressLabel.text),
                                                @"lat":[NSNumber numberWithDouble:currentLatitude],
                                                @"log":[NSNumber numberWithDouble:currentLongitude],
                                               };

    }
}

@end
