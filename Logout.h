//
//  Logout.h
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/18/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logout : NSObject <WebServiceHandlerDelegate>
{
    NSUserDefaults *ud;
}

+(instancetype)sharedInstance;

-(void)sendRequestToLogOut;
-(void)deleteUserSavedData:(NSString *)message;

@end
