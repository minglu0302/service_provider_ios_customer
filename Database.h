//
//  Database.h
//  Towtray
//
//  Created by -Tony Lu on 17/06/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Database : NSObject

+(instancetype) sharedInstance;

//Card Methods
+(NSArray *)getParticularCardDetail:(NSString *)cardId;
+ (NSArray *)getCardDetails;
- (void)makeDataBaseEntry:(NSDictionary *)dictionary;
+ (BOOL)DeleteCard:(NSString*)Campaign_id;
+ (void)DeleteAllCard;


//For Previously Selected Address
+ (NSArray *)getAddressDetails;
- (void)makeDataBaseEntryForAddress:(NSDictionary *)dictionary;
+ (void)DeleteAllAddress;

//For Manage Addresses
+ (NSArray *)getManageAddressDetails;
+ (NSDictionary *)getParticularManageAddressDetail:(NSString *)addressId;
- (void)makeDataBaseEntryForManageAddress:(NSDictionary *)dictionary;
+ (void)DeleteAllManageAddress;
+ (BOOL)DeleteParticularManageAddress:(NSString *)addressId;

@end
