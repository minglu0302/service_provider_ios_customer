//
//  TWTShareVC.h
//  Towtray
//
//  Created by -Tony Lu on 20/05/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *couponCodeLabel;

- (IBAction)facebookButtonAction:(id)sender;
- (IBAction)twitterButtoAction:(id)sender;
- (IBAction)messageButtonAction:(id)sender;
- (IBAction)emailButtonAction:(id)sender;

@end
