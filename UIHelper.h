//
//  UIHelper.h
//  Kidzo
//
//  Created by Vinay Raja on 08/12/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIHelper : NSObject<UIAlertViewDelegate>

+ (instancetype) sharedInstance;
+ (void) showMessage:(NSString*)message withTitle:(NSString*)title delegate:(id) delegate;
+ (CGFloat)measureHeightLabel: (UILabel *)label;
+ (void) addStatusBarBackgroundColor:(UIColor*)color forNavigationController:(UINavigationController*)navC;
+ (NSString *)getCurrentDateTime;
+ (NSString *)getCurrentGMTDateTime;
+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url;
+ (NSString *)getBookingStatusString:(NSInteger)status;
+ (NSString *)getBookingCancelReason:(NSInteger)tag;
+ (UIImage *)requestForCardTypeImageName:(NSString *)cardType;
+ (double)convertMilesToKilometer:(double)miles;
+ (double)convertKilometerToMiles:(double)kiloMeter;
+ (UIViewController *)getCurrentController;
+ (NSString *)getModelName;
- (void)setLeftMenuSettings;
- (void)refreshTheCurrentController;
+ (BOOL)showLeftMenuOrNot;
+ (NSString*)getProjectName;
+ (NSString*)getProjectBundleId;
- (void)updateLeftMenuSettings;

@end
