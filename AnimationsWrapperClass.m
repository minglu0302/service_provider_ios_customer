//
//  AnimationsWrapperClass.m
//  iServe_AutoLayout
//
//  Created by Apple on 15/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "AnimationsWrapperClass.h"

@implementation AnimationsWrapperClass

+(void)UIViewAnimationAnimationCurve:(UIViewAnimationCurve)animationCurve
                          transition:(UIViewAnimationTransition)animationTransition
                             forView:(UIView *)view
                        timeDuration:(NSTimeInterval)duration
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:animationCurve];
    
    [UIView setAnimationTransition:animationTransition
                           forView:view cache:NO];
    
    [UIView commitAnimations];

}

+(void)CATransitionAnimationType:(NSString *)type
                         subType:(NSString *)subType
                         forView:(UIView *)view
                    timeDuration:(NSTimeInterval)duration
{
    CATransition* transition = [CATransition animation];
    transition.duration = duration;
    transition.type = type;//For the destination VC
    transition.subtype = subType;
    
    [view.layer addAnimation:transition forKey:kCATransition];
}

+(void)CATransitionForViewsinSameVCAnimationType:(NSString *)type
                                         subType:(NSString *)subType
                                timeFunctionType:(NSString *)timeFunctionType
                                         forView:(UIView *)view
                                    timeDuration:(NSTimeInterval)duration
{
    CATransition *animation = [CATransition animation];
//    [animation setDelegate:self]; // set your delegate her to know when transition ended
    
    [animation setType:type];
    [animation setSubtype:subType];
    [animation setDuration:duration];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:timeFunctionType]];
    [[view layer] addAnimation:animation forKey:@"viewPush"];

}




//CATransition *animation = [CATransition animation];
//[animation setDelegate:self]; // set your delegate her to know when transition ended
//
//[animation setType:kCATransitionPush];
//[animation setSubtype:kCATransitionFromRight]; // set direction as you need
//
//[animation setDuration:0.5];
//[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//
//[[self.needHelpBackGroundView layer] addAnimation:animation forKey:@"viewPush"];



//CATransition* transition = [CATransition animation];
//transition.duration = 0.3;
//transition.type = kCATransitionReveal;//For the destination VC
//transition.subtype = kCATransitionFromBottom;
//
//[navigationVC.view.layer addAnimation:transition forKey:kCATransition];



//   [UIView beginAnimations:@"View Flip" context:nil];
//   [UIView setAnimationDuration:0.5];
//   [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//
//   [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
//                          forView:self.navigationController.view cache:NO];
//   [UIView commitAnimations];

@end
