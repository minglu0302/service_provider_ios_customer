//
//  CheckPushEnabledOrNot.m
//  iServe
//
//  Created by Apple on 14/06/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import "CheckPushEnabledOrNot.h"
#import <UserNotifications/UserNotifications.h>

@interface CheckPushEnabledOrNot ()
{
    UIAlertController *alertController;
}

@end


@implementation CheckPushEnabledOrNot


static CheckPushEnabledOrNot *share;

/*---------------------------------------*/
#pragma mark - Initial Method
/*---------------------------------------*/


+ (id)sharedInstance
{
    if (!share)
    {
        share  = [[self alloc] init];
    }
    return share;
}

-(void)showPushEnableAlertMessage
{
    
}

-(void)checkNotificationEnabledOrNot
{
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification * _Nonnull note) {
                                                      
                                                      
                          [[UNUserNotificationCenter currentNotificationCenter] getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings)
                           {
                               
                               if(settings.authorizationStatus == UNAuthorizationStatusNotDetermined)
                               {
                                   NSLog(@"Push Status: NotDetermined");
                               }
                               else if(settings.authorizationStatus == UNAuthorizationStatusDenied)
                               {
                                   NSLog(@"Push Status: Denied");
//                                   [self performSelector:@selector(showPushEnableAlertMessage) withObject:nil afterDelay:0.8];
                                   alertController = [UIAlertController  alertControllerWithTitle:LS(@"Notification not enabled !")
                                                                                                             message:LS(@"Please enable notifications to get booking staus updately.\nTo enable push notification go to\nSettings -> Goclean Service")
                                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                   
                                   [alertController addAction:[UIAlertAction actionWithTitle:@"Enable Push Notification"
                                                                                       style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction *action) {
                                                                                         
                                                                                         if([[NSUserDefaults standardUserDefaults] objectForKey:iServeCheckUserSessionToken])
                                                                                         {
                                                                                             [[UIHelper sharedInstance]updateLeftMenuSettings];
                                                                                         }
                                                                                         
                                                                                         
                                                                                         NSURL *appUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                                                         //                                                                             [[UIApplication sharedApplication] openURL:appUrl];
                                                                                         
                                                                                         
                                                                                         if([[UIApplication sharedApplication] canOpenURL:appUrl])
                                                                                         {
                                                                                             [[UIApplication sharedApplication] openURL:appUrl];
                                                                                         }
                                                                                         
                                                                                         
                                                                                         
                                                                                     }]];
                                   
                                   
                                   
                                   [[[[UIApplication sharedApplication]delegate]window].rootViewController presentViewController:alertController animated:YES completion:nil];

                               }
                               else if(settings.authorizationStatus == UNAuthorizationStatusAuthorized)
                               {
                                   NSLog(@"Push Status: Authorized");
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }
                               
                           }];
                                                      
                                                      
                                                      
                }];

}


@end
