//
//  ReminderWrapperClass.h
//  iServe_Customer
//
//  Created by Apple on 05/12/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>
@import EventKit;

@interface ReminderWrapperClass : NSObject<WebServiceHandlerDelegate>

+(instancetype) sharedInstance;
-(void)addReminder:(NSDate *)startDate andEndDate:(NSDate *)endDate Title:(NSString *)title Message:(NSString *)message BookingId:(NSString *)bid;
-(void)removeReminder:(NSString *)eventId;

@end
