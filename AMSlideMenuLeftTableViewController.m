//
//  AMSlideMenuLeftTableViewController.m
//  AMSlideMenu
//
// The MIT License (MIT)
//
// Created by : arturdev
// Copyright (c) 2014 SocialObjects Software. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

#import "AMSlideMenuLeftTableViewController.h"

#import "AMSlideMenuMainViewController.h"

#import "AMSlideMenuContentSegue.h"

#import "AmSlideTableViewCell.h"



@interface AMSlideMenuLeftTableViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSUserDefaults *ud;
}

@end

@implementation AMSlideMenuLeftTableViewController

/*----------------------------------------------------*/
#pragma mark - Lifecycle
/*----------------------------------------------------*/


static AMSlideMenuLeftTableViewController *controller = nil;


+ (id)sharedMenu
{
    return controller;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    controller = self;
    ud = [NSUserDefaults standardUserDefaults];
    self.dataArray=[[NSArray alloc]initWithObjects:
                    LS(@"Book A Service"),
                    LS(@"Booking History"),
                    LS(@"Payments"),
                    LS(@"Manage Addresses"),
//                    LS(@"Support"),
//                    LS(@"Share"),
                    LS(@"About"),
                    LS(@"LiveChat"),
                    nil];
    
    self.normalImageArray = [[NSArray alloc]initWithObjects:
                             @"menu_book_iserve_btn_unselector",
                             @"menu_booking_btn_unselector",
                             @"menu_payment_btn_unselector",
                             @"menu_address_btn_unselector",
//                             @"menu_support_btn_unselector",
//                             @"menu_share_btn_unselector",
                             @"menu_about_btn_unselector",
                             @"menu_livechat",
                             nil];
    
    self.highlightedImageArray = [[NSArray alloc]initWithObjects:
                                  @"menu_book_iserve_btn_selector",
                                  @"menu_booking_btn_selector",
                                  @"menu_payment_btn_selector",
                                  @"menu_address_btn_selector",
//                                  @"menu_support_btn_selector",
//                                  @"menu_share_btn_selector",
                                  @"menu_about_btn_selector",
                                  @"menu_livechat",
                                  nil];

  
    self.userNameLabel.adjustsFontSizeToFitWidth = YES;
    [self setUserNameInLeftMenu];
    [self setProfileDetialsInLeftMenu];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setProfileDetialsInLeftMenu) name:iServeProfileImageChanged object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUserNameInLeftMenu) name:iServeUserNameChanged object:nil];
}

- (void)openContentNavigationController:(UINavigationController *)nvc
{
#ifdef AMSlideMenuWithoutStoryboards
    AMSlideMenuContentSegue *contentSegue = [[AMSlideMenuContentSegue alloc] initWithIdentifier:@"contentSegue" source:self destination:nvc];
    [contentSegue perform];
#else
    NSLog(@"This methos is only for NON storyboard use! You must define AMSlideMenuWithoutStoryboards \n (e.g. #define AMSlideMenuWithoutStoryboards)");
#endif
}

 //Set User Name in Left Menu
-(void)setUserNameInLeftMenu
{
    NSString *profileFirstName = flStrForStr([ud objectForKey:iServeUserFirstName]);
    NSString *profileLastName = flStrForStr([ud objectForKey:iServeUserLastName]);
    NSString *combinedProfileName = [profileFirstName stringByAppendingString:@" "];
    NSString *finalName=[combinedProfileName stringByAppendingString:profileLastName];
    
    [finalName stringByReplacingOccurrencesOfString:@"  " withString:@" "];
    
    if ([finalName hasPrefix:@" "]) {
        finalName = [finalName substringFromIndex:1];
    }
    
    if ([finalName hasSuffix:@" "]) {
        finalName = [finalName substringToIndex:[finalName length]-1];
    }
    
    self.userNameLabel.text = finalName;

}

//Set Profile Image In Left Menu
-(void)setProfileDetialsInLeftMenu{
    
    NSString *strImageUrl = [NSString stringWithFormat:@"%@",[ud objectForKey:iServeUserProfilepic]];
    
    if([[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:strImageUrl])
    {
        [[SDImageCache sharedImageCache] removeImageForKey:strImageUrl withCompletion:nil];
    }

    
//   dispatch_async(dispatch_get_main_queue(), ^{
    [self.activityIndicator startAnimating];
//   });
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                      placeholderImage:[UIImage imageNamed:@"register_profile_default_image"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                 
                                 [self.activityIndicator stopAnimating];
                                
                                 if(error)
                                 {
                                     NSLog(@"Got Error Downloading Again");
                                 }
                                 else
                                 {
                                    NSLog(@"Profile Photo Added in LeftMenu");
                                 }
                                 
                             }];
   

}

/*----------------------------------------------------*/
#pragma mark - TableView DataSource -
/*----------------------------------------------------*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AmSlideTableViewCell *tableViewCell;
    if (!tableViewCell)
    {
        tableViewCell = [tableView dequeueReusableCellWithIdentifier:@"AmslidemenuTableViewcell"];
    }
    
    
    tableViewCell.eachRowTitleLabel.text = self.dataArray[indexPath.row];
    
//    [tableViewCell.eachRowTitleButton setBackgroundImage:[UIImage imageNamed:self.normalImageArray[indexPath.row]] forState:UIControlStateNormal];
//    [tableViewCell.eachRowTitleButton setBackgroundImage:[UIImage imageNamed:self.highlightedImageArray[indexPath.row]]forState:UIControlStateHighlighted];
//    [tableViewCell.leftImageView setImage:[UIImage imageNamed:self.normalImageArray[indexPath.row]]];
    
    if(indexPath.row == self.dataArray.count-1){
        
        tableViewCell.divider.hidden = YES;
    }
    
    return tableViewCell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


/*----------------------------------------------------*/
#pragma mark - TableView Delegate -
/*----------------------------------------------------*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.mainVC respondsToSelector:@selector(navigationControllerForIndexPathInLeftMenu:)])
    {
        UINavigationController *navController = [self.mainVC navigationControllerForIndexPathInLeftMenu:indexPath];
        AMSlideMenuContentSegue *segue = [[AMSlideMenuContentSegue alloc] initWithIdentifier:@"ContentSugue" source:self destination:navController];
        [segue perform];
    }
    else
    {
        NSString *segueIdentifier = [self.mainVC segueIdentifierForIndexPathInLeftMenu:indexPath];
        if (segueIdentifier && segueIdentifier.length > 0)
        {
            [self performSegueWithIdentifier:segueIdentifier sender:nil];
        }
    }
}

#pragma mark - UIButton Actions -

- (IBAction)profileButtonAction:(id)sender {
    
    [self performSegueWithIdentifier:@"Profile" sender:nil];
    
}

- (IBAction)eachRowAction:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }

}

@end
