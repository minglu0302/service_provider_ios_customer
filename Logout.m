//
//  Logout.m
//  iServe_AutoLayout
//
//  Created by -Tony Lu on 7/18/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import "Logout.h"

static Logout *logout = nil;

@implementation Logout

+(instancetype)sharedInstance
{
    if (!logout) {
        
        logout = [[self alloc] init];
    }
    return logout;
}


-(void)sendRequestToLogOut
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if([reachability isNetworkAvailable])
    {
        
        NSDictionary *params = @{
                                 kTWTSessionTokenkey:flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:iServeCheckUserSessionToken]),
                                 kTWTDeviceId:flStrForStr([Utilities getDeviceId]),
                                 kTWTEnterUserTypekey:flStrForStr([Utilities getUserType]),
                                 kTWTDateTime:flStrForStr([UIHelper getCurrentDateTime]),
                                 };
        
        [[WebServiceHandler sharedInstance] sendRequestToSignOut:params andDelegate:self];
        
    }
    else
    {
        [[ProgressIndicator sharedInstance]hideProgressIndicator];
        [UIHelper showMessage:iServeNetworkErrormessage withTitle:LS(@"oops!")delegate:[UIHelper getCurrentController]];
    }

}

-(void)didFinishLoadingRequest:(RequestType)requestType withResponse:(id)response error:(NSError *)error{
    
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    
    if (error)
    {
        [UIHelper showMessage:error.localizedDescription withTitle:LS(@"Message")delegate:[UIHelper getCurrentController]];
        return;
    }
    
    NSInteger errFlag = [response[@"errFlag"] integerValue];
    NSInteger errNum = [response[@"errNum"] integerValue];
    
    switch (errFlag) {
        case 1:
        {
        
            if (errNum == 7 || errNum == 6 || errNum == 78 || errNum == 83)//Session Expired
            {
                [self deleteUserSavedData:response[@"errMsg"]];
                
            }
            else
            {
                [UIHelper showMessage:response[@"errMsg"] withTitle:LS(@"Message")delegate:[UIHelper getCurrentController]];
            }

        }
            break;
            
        case 0://SuccessFully LogOut
        {
            [self deleteUserSavedData:nil];
            
        }
            break;
            
        default:
            break;
    }

}

//Delete All Users Data
-(void)deleteUserSavedData:(NSString *)message
{
    if(message)
    {
        [[[UIAlertView alloc]initWithTitle:LS(@"Message")message:message delegate:nil cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil]show];
    }
    
    ud = [NSUserDefaults standardUserDefaults];
    
    NSString *deviceId = [ud objectForKey:iServeDeviceId];
    NSString *pushToken = [ud objectForKey:iServePushTokenKey];
    NSString *fcmPushToken = [ud objectForKey:iServeFCMTokenKey];
    
    [Database DeleteAllCard];
    [Database DeleteAllAddress];
    [Database DeleteAllManageAddress];
    
    [ud removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [ud setObject:deviceId forKey:iServeDeviceId];
    [ud setObject:pushToken forKey:iServePushTokenKey];
    [ud setObject:fcmPushToken forKey:iServeFCMTokenKey];
    
    [self goToSplashVC];
}

//Goto Splash VC
-(void)goToSplashVC
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UINavigationController *naviVC = (UINavigationController*) appDelegate.window.rootViewController;
    
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    naviVC = menu.navigationController;

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    SplashViewController *splashVC = [storyboard instantiateViewControllerWithIdentifier:@"splashVC"];
    naviVC.viewControllers = [NSArray arrayWithObjects:splashVC, nil];

}

@end
