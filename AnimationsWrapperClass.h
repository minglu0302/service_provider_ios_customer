//
//  AnimationsWrapperClass.h
//  iServe_AutoLayout
//
//  Created by Apple on 15/09/16.
//  Copyright © 2016 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnimationsWrapperClass : NSObject

//UIView Animations
+(void)UIViewAnimationAnimationCurve:(UIViewAnimationCurve)animationCurve
                          transition:(UIViewAnimationTransition)animationTransition
                             forView:(UIView *)view
                        timeDuration:(NSTimeInterval)duration;


//CATransitionAnimation
+(void)CATransitionAnimationType:(NSString *)type
                         subType:(NSString *)subType
                         forView:(UIView *)view
                    timeDuration:(NSTimeInterval)duration;


//CATransitionForViewsinSameVC
+(void)CATransitionForViewsinSameVCAnimationType:(NSString *)type
                                         subType:(NSString *)subType
                                timeFunctionType:(NSString *)timeFunctionType
                                         forView:(UIView *)view
                                    timeDuration:(NSTimeInterval)duration;
@end
