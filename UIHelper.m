//
//  UIHelper.m
//  Kidzo
//
//  Created by Vinay Raja on 08/12/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "UIHelper.h"
#import <AVFoundation/AVFoundation.h>
#import "BookingsHistoryViewController.h"
#import "PaymentViewController.h"
#import "AddressManageViewController.h"
#import "SupportViewController.h"
#import "SupportWebViewController.h"
#import "ShareViewController.h"
#import "AboutViewController.h"
#import "AboutWebViewController.h"
#import "ProvidersListViewController.h"
#import "ProviderDetailsViewController.h"
#import "ProfileViewController.h"
#import "UIViewController+AMSlideMenu.h"
#import <sys/utsname.h>
#import "ChatViewController.h"

static UIHelper *helper = nil;

@implementation UIHelper

static NSDictionary* deviceNamesByCode = nil;

+(instancetype)sharedInstance
{
    if (!helper) {
        
        helper = [[self alloc] init];
    }
    return helper;
}

+ (void) showMessage:(NSString*)message withTitle:(NSString*)title delegate:(id)delegate
{
    //    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:title
    //                                                                              message:message
    //                                                                       preferredStyle:UIAlertControllerStyleAlert];
    //    [alertController addAction:[UIAlertAction actionWithTitle:@"OK"
    //                                                        style:UIAlertActionStyleDefault
    //                                                      handler:^(UIAlertAction *action) {
    //
    //          AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //          AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    //
    //          UINavigationController *navigationVC = menu.currentActiveNVC;
    //
    //          if(![navigationVC.topViewController isKindOfClass:[ProvidersListViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[BookingsHistoryViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[AddressManageViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[SupportViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[ShareViewController class]]
    //             &&![navigationVC.topViewController isKindOfClass:[AboutViewController class]])
    //          {
    //              [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
    //          }
    //          else if([navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
    //                  ||[navigationVC.topViewController isKindOfClass:[AddressManageViewController class]])
    //          {
    //               if([appDelegate.window.rootViewController isKindOfClass:[ProviderBookingViewController class]])
    //               {
    //                   [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
    //               }
    //          }
    //
    //
    //    }]];
    //
    //    [[[[UIApplication sharedApplication]delegate]window].rootViewController presentViewController:alertController animated:YES completion:nil];
    
    dispatch_async(dispatch_get_main_queue(),^{
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:[UIHelper sharedInstance] cancelButtonTitle:LS(@"OK") otherButtonTitles:nil, nil];
        alertView.tag = 2;
        [alertView show];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == [alertView cancelButtonIndex])
    {
        [self performSelector:@selector(setLeftMenuSettings) withObject:nil afterDelay:0.6];
    }
    
}

-(void)updateLeftMenuSettings
{
    [self performSelector:@selector(setLeftMenuSettings) withObject:nil afterDelay:0.6];
}

-(void)setLeftMenuSettings
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    
    if(![navigationVC.topViewController isKindOfClass:[HomeViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[ProvidersListViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[BookingsHistoryViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[AddressManageViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[SupportViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[ShareViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[AboutViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[ProfileViewController class]])
    {
        [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
    }
    else if([navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
            ||[navigationVC.topViewController isKindOfClass:[AddressManageViewController class]])
    {
        if([appDelegate.window.rootViewController isKindOfClass:[ProviderBookingViewController class]])
        {
            [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
        }
    }
    else
    {
        [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] enableSlidePanGestureForLeftMenu];
    }
    
}


+(UIViewController *)getCurrentController
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    return navigationVC.topViewController;
}


+ (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    if (requiredHeight.size.width > label.frame.size.width) {
        requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
    }
    CGRect newFrame = label.frame;
    newFrame.size.height = requiredHeight.size.height;
    return  newFrame.size.height;
}

+ (void) addStatusBarBackgroundColor:(UIColor*)color forNavigationController:(UINavigationController*)navC {
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, -20, CGRectGetWidth([ UIScreen mainScreen].bounds), 20)];
    statusBarView.backgroundColor = color;
    [navC.navigationBar addSubview:statusBarView];
    
}

+(NSString *)getCurrentDateTime
{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSLocale *englishLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:englishLocale];
    
    NSString *dateInStringFormated = [dateFormatter stringFromDate:now];
    
    return dateInStringFormated;
    
}

+(NSString *)getCurrentGMTDateTime
{
    NSDate *now = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:now];
    return dateTimeInIsoFormatForZuluTimeZone;
}


+ (NSString *)removeWhiteSpaceFromURL:(NSString *)url
{
    NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
    [string replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    return string;
}


+(NSString *)getBookingStatusString:(NSInteger)status
{
    switch (status) {
        case 1:
        {
            return LS(@"NEW BOOKING REQUEST");
        }
            break;
        case 2:
        {
            return LS(@"PROVIDER ACCEPTED");
        }
            break;
        case 3:
        {
            return LS(@"PROVIDER REJECTED");
        }
            break;
        case 4:
        {
            return LS(@"CUSTOMER CANCELLED WITHOUT FEES");
        }
            break;
        case 5:
        {
            return LS(@"PROVIDER ON THE WAY");
        }
            break;
        case 6:
        {
            return LS(@"JOB STARTED");
        }
            break;
        case 7:
        {
            return LS(@"JOB COMPLETED");
        }
            break;
        case 8:
        {
            return LS(@"JOB REQUEST TIMEOUT OR PROVIDER DID NOT RESPOND");
        }
            break;
        case 9:
        {
            return LS(@"CUSTOMER CANCELLED AND FEE CHARGED");
        }
            break;
        case 21:
        {
            return LS(@"ARRIVED AT JOB LOCATION");
        }
            break;
        case 22:
        {
            return LS(@"PROVIDER HAS RAISED INVOICE");
        }
            break;
            
        case 10:
        {
            return LS(@"PROVIDER CANCELLED");
        }
            break;
            
        default:
            return @"";
            break;
    }
    
}

+ (UIImage *)requestForCardTypeImageName:(NSString *)cardTypeName{
    
    NSArray *cardTypeNameArray = @[@"American Express",@"Diners Club",@"Discover",@"JCB",@"MasterCard",@"Visa"];
    
    UIImage *image = [[UIImage alloc]init];
    switch ([cardTypeNameArray indexOfObject:cardTypeName]) {
            
        case 0:
        {
            image = [STPImageLibrary amexCardImage];//[UIImage imageNamed:@"stp_card_amex"];
            return image;
        }
            break;
        case 1:
        {
            image = [STPImageLibrary dinersClubCardImage];//[UIImage imageNamed:@"stp_card_diners"];
            return image;
        }
            break;
        case 2:
        {
            image = [STPImageLibrary discoverCardImage];//[UIImage imageNamed:@"stp_card_discover"];
            return image;
        }
            break;
        case 3:
        {
            image = [STPImageLibrary jcbCardImage];//[UIImage imageNamed:@"stp_card_jcb"];
            return image;
        }
            break;
        case 4:
        {
            image = [STPImageLibrary masterCardCardImage];//[UIImage imageNamed:@"stp_card_mastercard"];
            return image;
        }
            break;
        case 5:
        {
            image = [STPImageLibrary visaCardImage];//[UIImage imageNamed:@"stp_card_visa"];
            return image;
        }
            break;
            
        default:
        {
            image = [STPImageLibrary unknownCardCardImage];//[UIImage imageNamed:@"placeholder"];
            return image;
        }
            break;
    }
    
}

+ (NSString *)getBookingCancelReason:(NSInteger)tag
{
    switch (tag) {
            
        case 1:
            return @"Booked by mistake";
            break;
        case 2:
            return @"Changed my mind";
            break;
        case 3:
            return @"Provider did not show up";
            break;
        case 4:
            return @"My reason is not listed";
            break;
            
        default:
            return @"";
            break;
    }
}

+ (double)convertMilesToKilometer:(double)miles
{
    return miles*1.6;
}
+ (double)convertKilometerToMiles:(double)kiloMeter
{
    return kiloMeter/1.6;
}

+ (NSString *)getModelName
{
    // Do any additional setup after loading the view from its nib.
    
    
    
    NSString *platform;
    struct utsname systemInfo;
    uname(&systemInfo);
    platform = [NSString stringWithCString:systemInfo.machine
                                    encoding:NSUTF8StringEncoding];

    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{
                              @"i386"      :@"iPhone Simulator",
                              @"x86_64"    :@"iPhone Simulator",
                              @"iPod1,1"   :@"iPod Touch",        // (Original)
                              @"iPod2,1"   :@"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   :@"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   :@"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   :@"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" :@"iPhone",            // (Original)
                              @"iPhone1,2" :@"iPhone",            // (3G)
                              @"iPhone2,1" :@"iPhone",            // (3GS)
                              @"iPad1,1"   :@"iPad",              // (Original)
                              @"iPad2,1"   :@"iPad 2",            //
                              @"iPad3,1"   :@"iPad",              // (3rd Generation)
                              @"iPhone3,1" :@"iPhone 4",          // (GSM)
                              @"iPhone3,3" :@"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" :@"iPhone 4S",         //
                              @"iPhone5,1" :@"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" :@"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   :@"iPad",              // (4th Generation)
                              @"iPad2,5"   :@"iPad Mini",         // (Original)
                              @"iPhone5,3" :@"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" :@"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" :@"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" :@"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" :@"iPhone 6 Plus",     //
                              @"iPhone7,2" :@"iPhone 6",          //
                              @"iPhone8,1" :@"iPhone 6S",         //
                              @"iPhone8,2" :@"iPhone 6S Plus",    //
                              @"iPhone8,4" :@"iPhone SE",         //
                              @"iPhone9,1" :@"iPhone 7",          //
                              @"iPhone9,3" :@"iPhone 7",          //
                              @"iPhone9,2" :@"iPhone 7 Plus",     //
                              @"iPhone9,4" :@"iPhone 7 Plus",     //
                              
                              @"iPad4,1"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   :@"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   :@"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   :@"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   :@"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   :@"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   :@"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   :@"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                            };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:platform];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([platform rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([platform rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([platform rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
    
    
//    NSLog(@"iPhone Device%@",platform);
//    
//    return platform;
}

-(void)methodToRefreshController
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    
    if([navigationVC.topViewController isKindOfClass:[BookingsHistoryViewController class]])
    {
        [[BookingsHistoryViewController getSharedInstance]sendRequestToGetBookingDetails];
    }
    else if([navigationVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]])
    {
        ShowOnGoingBookingsViewController *bookingFlowVC = [ShowOnGoingBookingsViewController getSharedInstance];
        bookingFlowVC.isNetworkComeBackRefresh = YES;
        [bookingFlowVC sendRequestToGetBookingDetails:1];
    }
    else if([navigationVC.topViewController isKindOfClass:[ChatViewController class]])
    {
        ChatViewController *chatVC = [ChatViewController getSharedInstance];
        [chatVC sendRequestToGetChatMessages];
    }
    
    //    if(![navigationVC.topViewController isKindOfClass:[BookingsHistoryViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[ShowOnGoingBookingsViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[ProviderBookingViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[ProviderD                  etailsViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[SupportWebViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[AboutWebViewController class]]
    //       &&![navigationVC.topViewController isKindOfClass:[ProfileViewController class]])
    //    {
    //        [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
    //    }

}

-(void)refreshTheCurrentController
{
    [self performSelector:@selector(methodToRefreshController) withObject:nil afterDelay:1];
    
    
}
+ (BOOL)showLeftMenuOrNot
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AMSlideMenuMainViewController *menu = [AMSlideMenuMainViewController getInstanceForVC:appDelegate.window.rootViewController];
    
    UINavigationController *navigationVC = menu.currentActiveNVC;
    
    if(![navigationVC.topViewController isKindOfClass:[HomeViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[BookingsHistoryViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[AddressManageViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[SupportViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[ShareViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[AboutViewController class]]
       &&![navigationVC.topViewController isKindOfClass:[ProfileViewController class]])
    {
        return NO;
    }
    else if([navigationVC.topViewController isKindOfClass:[PaymentViewController class]]
            ||[navigationVC.topViewController isKindOfClass:[AddressManageViewController class]])
    {
        if([appDelegate.window.rootViewController isKindOfClass:[ProviderBookingViewController class]])
        {
            [[AMSlideMenuMainViewController getInstanceForVC:navigationVC.topViewController] disableSlidePanGestureForLeftMenu];
            return NO;
        }

    }
    
    return YES;
}

+ (NSString*)getProjectName
{
    return NSBundle.mainBundle.infoDictionary[@"CFBundleDisplayName"];
}

+ (NSString*)getProjectBundleId
{
    return NSBundle.mainBundle.infoDictionary[@"CFBundleIdentifier"];
}


@end
