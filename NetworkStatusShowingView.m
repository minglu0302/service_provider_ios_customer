//
//  NetworkStatusShowingView.m
//  UBER
//
//  Created by -Tony Lu on 03/02/15.
//  Copyright (c) 2015 -Tony Lu. All rights reserved.
//

#import "NetworkStatusShowingView.h"

@implementation NetworkStatusShowingView

static NetworkStatusShowingView *showNetworkStatusView = nil;

+ (id)sharedInstance {
    
    if (!showNetworkStatusView) {
        
        showNetworkStatusView  = [[self alloc] init];
    }
    
    return showNetworkStatusView;
}

-(id)init
{
    self = [super init];
    if (self) {
        // Initialization code

        [self addViewToShowNetworkStatus];
    }
    return self;
}


-(void)addViewToShowNetworkStatus {
    
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIView *showStatus = [[UIView alloc]initWithFrame:CGRectMake(0, 64, [[UIScreen mainScreen]bounds].size.width, 44)];
    showStatus.tag = 1000;
    showStatus.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.7];
    UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, showStatus.frame.size.width, 44)];
    msg.backgroundColor = [UIColor clearColor];
    msg.textAlignment = NSTextAlignmentCenter;
    msg.text = LS(@"Could not connect to the server");
    msg.textColor = [UIColor whiteColor];
    [showStatus addSubview:msg];
    [frontWindow addSubview:showStatus];
}

-(void)hide
{
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        
                        
                     }
                     completion:^(BOOL finished){
                         
                         UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
                         UIView *showStatus = [frontWindow viewWithTag:1000];
                         [showStatus removeFromSuperview];
                         showNetworkStatusView = nil;
                     }];
    
}


+(void)removeViewShowingNetworkStatus
{
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *showStatus = [frontWindow viewWithTag:1000];
    if(showStatus)
    {
        //Do refresh to the current controller
        [[UIHelper sharedInstance]refreshTheCurrentController];
    }
    [showStatus removeFromSuperview];
    showNetworkStatusView = nil;
    
}


@end
