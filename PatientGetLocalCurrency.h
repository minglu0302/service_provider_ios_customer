//
//  PatientGetLocalCurrency.h
//  UBER
//
//  Created by -Tony Lu on 14/08/14.
//  Copyright (c) 2014 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PatientGetLocalCurrency : NSObject
+(NSString *)getCurrencyLocal :(float)amount;

@end
