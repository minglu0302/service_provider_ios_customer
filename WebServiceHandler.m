//
//  WebServiceHandler.m
//  MODA
//
//  Created by -Tony Lu on 2/19/16.
//  Copyright © 2016 Abhishek. All rights reserved.
//

#import "WebServiceHandler.h"
#import <AFNetworking/AFHTTPSessionManager.h>

NSString * const MODARequesttypeJSON = @"application/json";
NSString * const MODARequestTypeFormURLEncoded = @"application/x-www-form-urlencoded";
NSString * const MODARequestTypeXML = @"application/xml";
NSString * const MODARequestTypeTextXML = @"text/xml";
NSString * const MODARequestTypeTextHTML = @"text/html";

static WebServiceHandler *webservice = nil;

@interface WebServiceHandler ()
{
    BOOL forLiveBooking;
}

@end

@implementation WebServiceHandler


#pragma mark - Private Methods

+(id)sharedInstance{
    
    if (!webservice) {
        
        webservice = [[self alloc] init];
    }
    return webservice;

    
}
- (void) makePostRequest:(RequestType)requestType
                    path:(NSString*)path
                  params:(NSDictionary*)params
                delegate:(id<WebServiceHandlerDelegate>)delegate
{
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appD.networkStatus == AFNetworkReachabilityStatusNotReachable) {
        if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)])
        {
            NSError *error = [NSError errorWithDomain:LS(@"Network") code:404 userInfo:@{NSLocalizedDescriptionKey: LS(@"Network unreachable")}];
            
            [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
            return;
        }
    }
    
    AFHTTPSessionManager *manager;

    manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves];
    
    
    [manager POST:path
       parameters:params
         progress:nil
          success:^(NSURLSessionTask *task, id responseObject) {
              
              NSLog(@"JSON: %@", responseObject);
              //send success response with data
              if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                  [delegate didFinishLoadingRequest:requestType withResponse:responseObject error:nil];
              }
              
          } failure:^(NSURLSessionTask *operation, NSError *error) {
              
              NSLog(@"JSON: %@", error);
              //here is place for code executed in success case
              if (delegate && [delegate respondsToSelector:@selector(didFinishLoadingRequest:withResponse:error:)]) {
                  [delegate didFinishLoadingRequest:requestType withResponse:nil error:error];
              }
              
    }];
}
#pragma mark -
#pragma mark Other Methods -

- (void)placeWebserviceRequestWithString:(NSMutableURLRequest *)string Target:(id)_target Selector:(SEL)_selector
{

    urlConnection = [[NSURLConnection alloc] initWithRequest:string delegate:self];
    timeOut = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(cancelDownload) userInfo:nil repeats:NO];
    responseData = [[NSMutableData alloc] init];
    target = _target;
    selector = _selector;
}

- (void) cancelDownload
{
    if (timeOut == nil)
    {
        return;
    }
    else
    {
        [urlConnection cancel];
        [target performSelectorOnMainThread:selector withObject:[NSDictionary dictionaryWithObject:@"Connection Timed-out" forKey:@"Error"] waitUntilDone:NO];
        timeOut = nil;
    }
    
}


#pragma mark - Public Methods -

//App Version
- (void)sendRequestToUpdateAppVersion:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeUpdateAppVersion path:kRequestTypeUpdateAppVersion params:params delegate:delegate];
}


//SignUp
- (void)sendRequestToverifyEmail:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeVerifyEmail path:kRequestTypeVerifyEmail params:params delegate:delegate];
}

- (void)sendRequestToverifyMobileNumber:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeVerifyMobileNumber path:kRequestTypeCheckMobile params:params delegate:delegate];
}
- (void)sendRequestToverifyReferralCode:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeVerifyReferralCode path:kRequestTypeVerifyReferralCode params:params delegate:delegate];
}

//OTP
- (void)sendRequestToGetOTP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetOTP path:kRequestTypeGetOTP params:params delegate:delegate];
}

- (void)sendRequestToVerifyOTP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeVerifyOTP path:kRequestTypeVerifyOTP params:params delegate:delegate];
}

- (void)sendRequestToSignUP:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeSignUp path:kRequestTypeSignUp params:params delegate:delegate];
}


//SignIn
- (void)sendRequestToSignIN:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeSignIn path:kRequestTypeSignIn params:params delegate:delegate];
}

- (void)sendRequestToForgetPassword:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeForgetPassword path:kRequestTypeForgetPassword params:params delegate:delegate];
}

- (void)sendOTPToVerifyPhoneNumber:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeForgotPasswordVerifyPhoneNumber path:kRequestTypeForgotPasswordVerifyPhoneNumber params:params delegate:delegate];
}

- (void)sendRequestToChangePassword:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
     [self makePostRequest:RequestTypeChangePassword path:kRequestTypeChangePassword params:params delegate:delegate];
}


//Profile
- (void)sendRequestToGetProfileData:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetProfileData path:kRequestTypeGetProfileData params:params delegate:delegate];
}

- (void)sendRequestToUpdateProfileData:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeUpdateProfileData path:kRequestTypeUpdateProfileData params:params delegate:delegate];
}


//signOut
- (void)sendRequestToSignOut:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeSignOut path:kRequestTypeSignOut params:params delegate:delegate];
}


//Check Promocode
- (void)sendRequestToCheckCoupon:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeCheckCoupon path:kRequestTypeCheckCoupon params:params delegate:delegate];

}


//Live Booking
- (void)sendRequestToLiveBooking:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
//    forLiveBooking = YES;
    [self makePostRequest:RequestTypeLiveBooking path:kRequestTypeLiveBooking params:params delegate:delegate];
}

- (void)sendRequestToCancelBooking:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeCancelBooking path:kRequestTypeCancelBooking params:params delegate:delegate];
}

- (void)sendRequestToGetServiceGroupsDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetServiceGroupDetails path:kRequestTypeGetServiceGroupDetails params:params delegate:delegate];
}

- (void)sendRequestToGetCancelReasons:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetCancelReasons path:kRequestTypeGetCancelReasons params:params delegate:delegate];
}


//Booking History
- (void)sendRequestToGetAllOngoingAppointmentDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetAllOngoingApptDetails path:kRequestTypeGetAllOngoingApptStatus params:params delegate:delegate];

}

- (void)sendRequestToGetAllAppointments:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetAllApptDetails path:kRequestTypeGetAllApptDetails params:params delegate:delegate];
    
}

- (void)sendRequestToGetParticularApptStatus:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetParticularApptDetails path:kRequestTypeGetParticularApptDetails params:params delegate:delegate];
    
}

- (void)sendRequestToUpdateReview:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeUpdateReview path:kRequestTypeUpdateReview params:params delegate:delegate];
}

- (void)sendRequestToGetProviderDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetProviderDetails path:kRequestTypeGetProviderDetails params:params delegate:delegate];
    
}

- (void)sendRequestToUpdateDispute:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeUpdateDispute path:kRequestTypeUpdateDispute params:params delegate:delegate];
}


//ChatVC
- (void)sendRequestToGetChatMessages:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetChatMessages path:kRequestTypeGetChatMessages params:params delegate:delegate];
    
}


//Payment (card)
- (void)sendRequestToAddCard:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{

    [self makePostRequest:RequestTypeAddCard path:kRequestTypeAddCard params:params delegate:delegate];
}

- (void)sendRequestToGetCardDetails:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{

    [self makePostRequest:RequestTypeGetCardDetails path:kRequestTypeGetCardDetails params:params delegate:delegate];
}

- (void)sendRequestToDeleteCard:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{

    [self makePostRequest:RequestTypeDeleteCard path:kRequestTypeDeleteCard params:params delegate:delegate];
}


//My Address
- (void)sendRequestToAddAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{

    [self makePostRequest:RequestTypeAddAddress path:kRequestTypeAddAddress params:params delegate:delegate];
}

- (void)sendRequestToDeleteAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{

    [self makePostRequest:RequestTypeDeleteAddress path:kRequestTypeDeleteAddress params:params delegate:delegate];
}

- (void)sendRequestToGetAddress:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{

    [self makePostRequest:RequestTypeGetAddress path:kRequestTypeGetAddress params:params delegate:delegate];
}


//Support
- (void)sendRequestToGetSupportDetails:(NSDictionary*)params andDelegate:(id<WebServiceHandlerDelegate>)delegate
{
    [self makePostRequest:RequestTypeGetSupportDetails path:kRequestTypeGetSupportDetails params:params delegate:delegate];
}


//Store Event
- (void)sendEventToServer:(NSDictionary *)params andDelegate:(id<WebServiceHandlerDelegate>)delegate{
    
    [self makePostRequest:RequestTypeSendEventId path:kRequestTypeSendEventId params:params delegate:delegate];
}

@end
