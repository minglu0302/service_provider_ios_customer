//
//  AmazonTransfer.m
//
//
//  Created by -Tony Lu on 04/09/14.
//  Copyright (c) 2014 3Embed. All rights reserved.
//

#import "AmazonTransfer.h"

static AmazonTransfer *shared = nil;

@implementation AmazonTransfer

+(instancetype)sharedInstance
{
    if (!shared) {
        
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            shared = [[self alloc] init];
        });
    }
    
    return shared;
}


/*
 List of AmazonRegion
 
 AWSRegionUnknown,
 AWSRegionUSEast1,
 AWSRegionUSWest1,
 AWSRegionUSWest2,
 AWSRegionEUWest1,
 AWSRegionEUCentral1,
 AWSRegionAPSoutheast1,
 AWSRegionAPNortheast1,
 AWSRegionAPSoutheast2,
 AWSRegionSAEast1,
 AWSRegionCNNorth1,
 
 */

+ (void) setConfigurationWithRegion:(AWSRegionType)regionType
                          accessKey:(NSString*)accessKey
                          secretKey:(NSString*)secretKey
{
    
    AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:accessKey
                                                                                                     secretKey:secretKey];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:regionType
                                                                         credentialsProvider:credentialsProvider];
    
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
}


+ (void) upload:(NSString *) localFilePath
        fileKey:(NSString *) fileKey
       toBucket:(NSString *) bucket
       mimeType:(NSString *) mimeType
completionBlock:(AWSS3TransferUtilityUploadCompletionHandlerBlock) completionBlock
{
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    NSData *fileData = [NSData dataWithContentsOfFile:[NSURL fileURLWithPath:localFilePath].path];
    
    AWSS3TransferUtilityUploadExpression *expression = [AWSS3TransferUtilityUploadExpression new];
    
    [expression setValue:@"public-read" forRequestHeader:@"x-amz-acl"];
    
    expression.progressBlock = ^(AWSS3TransferUtilityTask *task, NSProgress *progress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // Do something e.g. Update a progress bar.
            NSLog(@"upload progress: %@", progress);
        });
    };

    
    [transferUtility uploadData:fileData
                         bucket:bucket
                            key:fileKey
                    contentType:mimeType
                     expression:expression
              completionHandler:^(AWSS3TransferUtilityUploadTask * _Nonnull task, NSError * _Nullable error) {
                  
                  completionBlock(task,error);
                  
              }];
    
}

+ (void) download:(NSString *) fileKey
       fromBucket:(NSString *) bucket
  completionBlock:(AWSS3TransferUtilityDownloadCompletionHandlerBlock) completionBlock {
    
    //    AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
    //    downloadRequest.bucket = bucket;
    //    downloadRequest.key = fileKey;
    //    downloadRequest.downloadingFileURL = [NSURL fileURLWithPath:localFilePath];
    //
    //    if (progressBlock) {
    //        downloadRequest.downloadProgress = (^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                progressBlock (totalBytesWritten, totalBytesExpectedToWrite);
    //            });
    //
    //        });
    //    }
    //
    //    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    //
    //    [[transferManager download:downloadRequest] continueWithBlock:^id(AWSTask *task) {
    //
    //        if (task.error) {
    //            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
    //                switch (task.error.code) {
    //                    case AWSS3TransferManagerErrorCancelled:
    //                    case AWSS3TransferManagerErrorPaused:
    //                    {
    //                    }
    //                        break;
    //                    default:
    //                        break;
    //                }
    //            }
    //
    //            if (completionBlock) {
    //                dispatch_async(dispatch_get_main_queue(), ^{
    //                    completionBlock (NO, task.result, task.error);
    //                });
    //
    //            }
    //
    //        }
    //
    //        if (task.result) {
    //            dispatch_async(dispatch_get_main_queue(), ^{
    //                completionBlock (YES, task.result, task.error);
    //            });
    //        }
    //
    //        return nil;
    //
    //
    //    }];
    
    AWSS3TransferUtility *transferUtility = [AWSS3TransferUtility defaultS3TransferUtility];
    
    [transferUtility downloadDataFromBucket:bucket
                                        key:fileKey
                                 expression:nil
                          completionHandler:^(AWSS3TransferUtilityDownloadTask * _Nonnull task, NSURL * _Nullable location, NSData * _Nullable data, NSError * _Nullable error) {
                              
                              completionBlock(task,location,data,error);
                              
                          }];
    
}

+(void)deleteObjectFromAWS:(NSString *)imageKey
{
    AWSS3 *s3 = [AWSS3 defaultS3];
    AWSS3DeleteObjectRequest *deleteRequest = [AWSS3DeleteObjectRequest new];
    deleteRequest.bucket = Bucket;
    deleteRequest.key = imageKey;
    
    [[s3 deleteObject:deleteRequest]
     continueWithBlock:^id(AWSTask *task) {
         if(task.error != nil){
             if(task.error.code != AWSS3TransferManagerErrorCancelled && task.error.code != AWSS3TransferManagerErrorPaused){
                 NSLog(@"%s Error: [%@]",__PRETTY_FUNCTION__, task.error);
             }
         }else{
             // Completed logic here
         }
         return nil;
     }];
}


@end
