//
//  CheckPushEnabledOrNot.h
//  iServe
//
//  Created by Apple on 14/06/17.
//  Copyright © 2017 -Tony Lu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CheckPushEnabledOrNot : NSObject

+ (id)sharedInstance;
- (void)checkNotificationEnabledOrNot;

@end
